#!/usr/bin/env python2.7

from astropy import units as u
from astropy.coordinates import SkyCoord
from astropy.coordinates import EarthLocation
from astropy.time import Time
from astropy.coordinates import AltAz

import numpy as np
import sys

##########################################################################
##########################################################################
########################################### Edit this to change the source
source = SkyCoord("6h33m54.153s", "17d46m12.91s", frame='icrs') ## Geminga

##########################################################################
##########################################################################
########################################### Edit this to change the island
magic   = EarthLocation(lat='28d45m43s', lon='-17d53m24s', height=2200*u.m)

#### Dummy date values, the source does always the same path on the sky.
obstime = Time('2018-01-01 0:00')
obstime = obstime+np.arange(0, 86400)*u.second

altaz = AltAz(location=magic, obstime=obstime)
source_altaz = source.transform_to(altaz)

#for i in range(86400):
#  print source_altaz.az.deg[i],"  ",source_altaz.alt.deg[i]

azimut =  (np.array([source_altaz.az.deg]).T).astype(np.float128)
altitude = (np.array([source_altaz.alt.deg]).T).astype(np.float128)

a = np.concatenate((azimut,altitude),axis=1)

a[:,1] = 90-a[:,1]
a[:,0] = (180-a[:,0]) + ((180-a[:,0])<0)*360

#print a
#print a.shape

np.savetxt("CORZD.txt",a)
