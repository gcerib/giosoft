#!/bin/bash

NBINS=$(wc -l BINS.txt | awk '{print($1)}')
echo $NBINS

mkdir -p input_cards

for (( i=1; i<=${NBINS}; i++ )); do
  RIGA=$(head -n $i BINS.txt | tail -n 1)
  X1=$(echo $RIGA | awk '{print($5)}');
  X2=$(echo $RIGA | awk '{print($6)}');
  Y1=$(echo $RIGA | awk '{print($7)}');
  Y2=$(echo $RIGA | awk '{print($8)}');
  NE=$(echo $RIGA | awk '{print($9)}');
  echo "RUNNR    $i                                                          "      >> input_cards/input_card_$i
  echo "NSHOW    $NE                         number of showers to generate  "       >> input_cards/input_card_$i
  echo "THETAP   $Y1  $Y2                  range of zenith angle (degree) "         >> input_cards/input_card_$i
  echo "PHIP     $X1  $X2                   range of azimuth angle (degree)"        >> input_cards/input_card_$i
  sleep .0$RANDOM                                                                
  echo "SEED     $RANDOM         0         0      seed values managed by daemon  "  >> input_cards/input_card_$i
  sleep .0$RANDOM                                                                
  echo "SEED     $RANDOM         0         0      seed values managed by daemon  "  >> input_cards/input_card_$i
  sleep .0$RANDOM                                                                  
  echo "SEED     $RANDOM         0         0      seed values managed by daemon  "  >> input_cards/input_card_$i
  cat input_card_fixed >> input_cards/input_card_$i
done
