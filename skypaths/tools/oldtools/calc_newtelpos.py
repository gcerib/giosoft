#!/usr/bin/env python2.7

import numpy as np

##### CONVEZIONE DEI SEGNI DELLA DECLINAZIONE MAGNETICA:
   ## da NORD in senso ORARIO       (verso EST)  =  +   (POSITIVO)
   ## da NORD is senso ANTIORARIO (VERSO OVEST)  =  -   (NEGATIVO)

DELTA_VECCHIA = np.loadtxt("DECLINAZIONE_MAGNETICA_VECCHIA.txt",dtype=np.float128)
DELTA_NUOVA   = np.loadtxt("DECLINAZIONE_MAGNETICA_NUOVA.txt",dtype=np.float128)

deltadelta = DELTA_NUOVA-DELTA_VECCHIA

##### PASSAGGIO ALLA CONVENZIONE STANDARD DEI SEGNI:
   ## antioratio =  +
   ## oratio     =  -

deltadelta = -deltadelta

##### CALCOLO DELLE POSIZIONI DEI TELESCOPI:
   ## Se il sistema ruota di X in senso antiorario,
   ## le coordinate devono  ruotare di -X in senso
   ## orario, per manterere le posizioni fisiche
   ## dove sono.

angolo_coordinate = -deltadelta

   ## (quindi e' uguale al deltadelta di prima).
   ## Lo converto in radianti.

angrad = angolo_coordinate/180.0*np.pi

##### LE COORDINATE ORIGINARIE

COORD1 = np.asarray((3180.0,-2810.0),dtype=np.float128)
COORD2 = np.asarray((-3180.0,2810.0),dtype=np.float128)

print COORD1
print COORD2

##### APPLICO LA ROTAZIONE ANTIORARIA DI "angolo_coordinate"

print "Ruoto il sistema    di ",deltadelta," in senso antiorario."
print "ruoto le coordinate di ",angolo_coordinate," in senso antiorario."

c1x,c1y = COORD1[0],COORD1[1]
c2x,c2y = COORD2[0],COORD2[1]

COORD1[0] = np.cos(angrad) * c1x    -    np.sin(angrad) * c1y
COORD1[1] = np.sin(angrad) * c1x    +    np.cos(angrad) * c1y

COORD2[0] = np.cos(angrad) * c2x    -    np.sin(angrad) * c2y
COORD2[1] = np.sin(angrad) * c2x    +    np.cos(angrad) * c2y

##### STAMPO LE NUOVE COORDINATE

print COORD1
print COORD2
