#!/usr/bin/env python2.7

import numpy as np

## 180-$4+((180-$4)<0)*360):(90-$3)

a = np.loadtxt("AZALT.txt",dtype=np.float128)
a[:,1] = 90-a[:,1]
a[:,0] = (180-a[:,0]) + ((180-a[:,0])<0)*360

np.savetxt("CORZD.txt",a)
