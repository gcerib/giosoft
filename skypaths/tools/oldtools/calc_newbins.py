#!/usr/bin/env python2.7

import numpy as np

##### CONVEZIONE DEI SEGNI DELLA DECLINAZIONE MAGNETICA:
   ##  - da NORD in senso ORARIO       (verso EST)  =  +   (POSITIVO)
   ##  - da NORD is senso ANTIORARIO (VERSO OVEST)  =  -   (NEGATIVO)
   ##  - da -180 a 180.

DELTA  =  np.loadtxt("DECLINAZIONE_MAGNETICA_NUOVA.txt",dtype=np.float128)     ## NEGATIVO (ANTIORARIO)


##### CONVENZIONE DEI SEGNI DI CORSIKA:
##  - vettori che puntano da SUD verso NORD            =  0
##  - vettori che puntano nel semicerchio occidentale  =  +
##  - da 0 a 360.
## E' ANTIORARIA!! CON L'INTERVALLO DEFINITO SU (0,360). Giro il delta
## nel sistema antiorario.

ZETA = -DELTA                                                                  ## POSITIVO (ANTIORARIO)

## I bin sono  stati originariamente  generati nel  sistema geografico
## che ha l'asse  zero coincidente col nord  geografico.  Se ora ruoto
## il sistema di ZETA in senso ANTIORARIO per mantenere le  coordinate
## devo ruotare tutto di -ZETA  in  senso ANTIORARIO, ossia di ZETA in
## senso ORARIO. Siccome l'azimuth dei bin cresce in senso antiorario,
## significa che devo aggiungere all'azimuth il valore -ZETA = DELTA.

BINS = np.loadtxt("BINS.txt",dtype=np.float128)
BINS[:,4] = (np.float128(BINS[:,4]) + DELTA)  +  ((np.float128(BINS[:,4]) + DELTA)<0)*360
BINS[:,5] = (np.float128(BINS[:,5]) + DELTA)  +  ((np.float128(BINS[:,5]) + DELTA)<0)*360

print BINS
np.savetxt("BINSRUOT.txt",BINS,fmt="%0.3f")
