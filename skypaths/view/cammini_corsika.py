#!/usr/bin/env python2.7
# encoding=utf8

import sys
import numpy as np
import matplotlib.pyplot as plt

from scipy.optimize import fmin

reload(sys)
sys.setdefaultencoding('utf8')

LAMBDA  =  float(sys.argv[1])  ##LATITUDE [deg]
DEC     =  float(sys.argv[2])  ##DECLINATION [deg]

LAMBDA=LAMBDA/180.0*np.pi
DEC=DEC/180.0*np.pi

Sin    = np.sin
Cos    = np.cos
Sqrt   = np.sqrt
Arccos = np.arccos
Arctan = np.arctan2

def theta(alfa):
    return Arccos( Cos(LAMBDA)*Cos(DEC)*Cos(alfa) + Sin(LAMBDA)*Sin(DEC)  )

def phi(alfa):
    return Arctan( Cos(DEC)*Sin(alfa) , Sin(LAMBDA)*Cos(DEC)*Cos(alfa) - Cos(LAMBDA)*Sin(DEC)  )

def Dtheta(alfa):
    return ( Cos(LAMBDA)*Cos(DEC)*Sin(alfa) ) / ( Sqrt( 1 - ( Cos(LAMBDA)*Cos(DEC)*Cos(alfa) + Sin(LAMBDA)*Sin(DEC)  )**2  )  )

def Dphi(alfa):
    return ( Cos(DEC)*Cos(DEC)*Sin(LAMBDA) - Cos(DEC)*Sin(DEC)*Cos(LAMBDA)*Cos(alfa)  )  /  ( (Sin(LAMBDA)*Cos(DEC)*Cos(alfa)  - Cos(LAMBDA)*Sin(DEC))**2  +  ( Cos(DEC)*Sin(alfa)  )**2 )

def Clip(x):
    return np.log10(np.clip(x,0,1e5))

alfa = np.linspace(0,2*np.pi,86400)

merix = np.asarray((180,180))
meriy = np.asarray((0,360))

plt.figure()
PHI=phi(alfa+np.pi)/np.pi*180
plt.plot(alfa/np.pi*180,(PHI<0)*(360+PHI)+(PHI>0)*PHI)
plt.plot(alfa/np.pi*180,theta(-(alfa+np.pi))/np.pi*180)
plt.fill_between(alfa/np.pi*180,90,360,alpha=0.5)
plt.plot(merix,meriy)
plt.xlabel("Hour angle α [deg]")
plt.ylabel("Zd, Az(CORSIKA) [deg]")
plt.title("Curve affini")
axes = plt.gca()
axes.set_xlim([0,360])
axes.set_ylim([0,360])


plt.figure()
plt.plot(alfa/np.pi*180,Dphi(alfa+np.pi))
plt.plot(alfa/np.pi*180,-Dtheta(alfa+np.pi))
plt.plot(alfa/np.pi*180,np.zeros(len(alfa)))
plt.xlabel("Hour angle α [deg]")
plt.ylabel("(d/dα Zd), (d/dα Az(CORSIKA))  [deg/deg]")
plt.title("Derivatives")
axes = plt.gca()
axes.set_xlim([0,360])

#corzd=np.loadtxt("CORZD.txt")

plt.figure()
PHI=np.roll(phi(alfa+np.pi)/np.pi*180,len(PHI)/2)
THETA=np.roll((theta(-(alfa+np.pi))/np.pi*180),len(PHI)/2)
plt.plot((PHI<0)*(360+PHI)+(PHI>0)*PHI,THETA)
plt.fill_between((PHI<0)*(360+PHI)+(PHI>0)*PHI,90,180,alpha=0.5)
#plt.plot(corzd[:,0],corzd[:,1])
plt.xlabel("Az(CORSIKA) [deg]")
plt.ylabel("Zd [deg]")
plt.title("Coordinate space")
axes = plt.gca()
axes.set_xlim([0,360])
axes.set_ylim([0,180])

plt.show()

#max_x = fmin(lambda x: theta(x)/np.pi*180, 0)
#print max_x
