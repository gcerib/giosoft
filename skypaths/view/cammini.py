#!/usr/bin/env python2.7
# encoding=utf8

import sys
import numpy as np
import matplotlib.pyplot as plt

reload(sys)
sys.setdefaultencoding('utf8')

LAMBDA  =  float(sys.argv[1])  ##LATITUDINE [gradi]
DEC     =  float(sys.argv[2])  ##DECLINAZIONE [gradi]

LAMBDA=LAMBDA/180.0*np.pi
DEC=DEC/180.0*np.pi

Sin    = np.sin
Cos    = np.cos
Sqrt   = np.sqrt
Arccos = np.arccos
Arctan = np.arctan2

def theta(alfa):
    return Arccos( Cos(LAMBDA)*Cos(DEC)*Cos(alfa) + Sin(LAMBDA)*Sin(DEC)  )

def phi(alfa):
    return Arctan( Cos(DEC)*Sin(alfa) , Sin(LAMBDA)*Cos(DEC)*Cos(alfa) - Cos(LAMBDA)*Sin(DEC)  )

def Dtheta(alfa):
    return ( Cos(LAMBDA)*Cos(DEC)*Sin(alfa) ) / ( Sqrt( 1 - ( Cos(LAMBDA)*Cos(DEC)*Cos(alfa) + Sin(LAMBDA)*Sin(DEC)  )**2  )  )

def Dphi(alfa):
    return ( Cos(DEC)*Cos(DEC)*Sin(LAMBDA) - Cos(DEC)*Sin(DEC)*Cos(LAMBDA)*Cos(alfa)  )  /  ( (Sin(LAMBDA)*Cos(DEC)*Cos(alfa)  - Cos(LAMBDA)*Sin(DEC))**2  +  ( Cos(DEC)*Sin(alfa)  )**2 )

def Clip(x):
    return np.log10(np.clip(x,0,1e5))

alfa = np.arange(0.00001,2*np.pi-0.00001,0.0001)

merix = np.asarray((180,180))
meriy = np.asarray((-180,180))

plt.figure()
plt.plot(alfa/np.pi*180,phi(alfa+np.pi)/np.pi*180)
plt.plot(alfa/np.pi*180,90-theta(alfa+np.pi)/np.pi*180)
plt.fill_between(alfa/np.pi*180,0,-180,alpha=0.5)
plt.plot(merix,meriy)
plt.xlabel("Angolo orario α [gradi]")
plt.ylabel("Alt, Az [gradi]")
plt.title("Curve affini")
axes = plt.gca()
axes.set_xlim([0,360])
axes.set_ylim([-180,180])
#plt.plot(alfa,Dphi(alfa+np.pi)/np.pi*180)
#plt.plot(alfa,Dtheta(alfa+np.pi)/np.pi*180)
plt.figure()
plt.plot(alfa/np.pi*180,Dphi(alfa+np.pi))
plt.plot(alfa/np.pi*180,-Dtheta(alfa+np.pi))
plt.xlabel("Angolo orario α [deg]")
plt.ylabel("(d/dα Alt), (d/dα Az)  [gradi/gradi]")
plt.title("Derivate")
plt.figure()
plt.plot(phi(alfa+np.pi)/np.pi*180,90-theta(alfa+np.pi)/np.pi*180)
plt.fill_between(alfa/np.pi/2*360-180,0,-90,alpha=0.5)
plt.xlabel("Az [gradi]")
plt.ylabel("Alt [gradi]")
plt.title("Spazio delle coordinate")
axes = plt.gca()
axes.set_xlim([-180,180])
axes.set_ylim([-90,90])
plt.show()
