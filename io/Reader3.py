"""  
  READER.py, a module to read MAGIC and Fermi files

          __...--~~~~~-._   _.-~~~~~--...__
        //               `V'               \\ 
       //                 |                 \\ 
      //__...--~~~~~~-._  |  _.-~~~~~~--...__\\ 
     //__.....----~~~~._\ | /_.~~~~----.....__\\
    ====================\\|//====================
                        `---`
    Stat rosa pristina nomine, nomina nuda tenemus.
  
"""

import numpy as np
import warnings
import textwrap
try:
  import uproot3 as uproot
except ImportError:
  import uproot
  if int(uproot.__version__.split('.')[0])>3:
    raise ImportError('Reader does not work with uproot4. Install uproot3.')
import copy
import glob
import sys

from astropy.table import Table,vstack

####################################################################################################

def wol(message, category, filename, lineno, file=None, line=None):
    sys.stdout.flush()
    sys.stderr.flush()
    wstr =  '------------------------------------------------------------\n'
    wstr += 'WARNING:\n'
    wstr += '\n'.join(textwrap.wrap('%s:%s' % (filename, lineno),60))+'\n'
    wstr += "\n".join(textwrap.wrap(str('%s:  %s' % (category.__name__, message)),60))+'\n'
    wstr += '------------------------------------------------------------\n'
    return wstr

def raisewarn(message):
    old = warnings.formatwarning
    warnings.formatwarning = wol
    warnings.warn(message)
    warnings.formatwarning = old

class Reader:
  """Helper class to read MAGIC root files. It can
     read also fits files (via astropy tables) and
     txt files produced by tempo2. Essentially a
     wrapper around an astropy Table object.

     Members:
       
      Input:    the Input parameter used in the
                constructor
      Format:   the Format parameter used in the
                constructor
      Contents: the Contents parameter used in the
                constructor why I am writing this
                three times.

      Filenames: a list with expanded filenames
                 from the Input parameter. The
                 class issues a Warning if an input
                 element does not expand, but
                 proceeds unless no file matching
                 the Input criteria is found.

      Data: the most important thing! The read data
            as an astropy Table. The table can be
            called directly from the reader as:
               r[...] = r.Data[...]
            Warning: if you need to manipulate the
            data and keep the original you make a
            copy or use the copyData() function.

      Tree: the tree where to look (defaults to
            'Events').

     Futhermore, you can add two Reader objects and
     obtain the stacked Data table:
        r1 = Reader(...)
        r2 = Reader(...)
        data_stacked = r1 + r2

     Scroll down for the description of functions.

  """

  def __init__(self,
               Input      = ('',)  ,
               Contents   =  '*'   ,
               Format     =  ''    ,
               ConvertMjd = True   ,
               Tree       = 'Events' ):
    """Input: can be a string with an input file, a
       string with wildcards, or a tuple of strings
       for different files/folders. In any case, it
       is converted internally to a tuple of str.


       Format: 'root', 'fits', 'tempotxt' or ''.
       The default is '', which tries and guesses
       the format from the first file extension.
       If Format is 'tempotxt', Contents has no
       effect.


       Contents: this specifies what to read from
       the file and how to name the table columns.
       It can be one of these:
         (1) a str or a tuple of str:
              (str, str, ...)
         (2) a tuple of 2-tuples of strings:
              ((str,str), (str,str), ...)
         (3) a dictionary with strings:
              {key:value, key:value, ...}
         (4) the special wildcard '*'

       If (1), the strings represent the branches
       or columns to read from the root or fits
       file. The column names are mainteined in the
       resulting astropy table.

       If (2), elements are pairs of the kind
       (key,branch_name). This is the typical
       situation for root files, where having a
       column named 'MPointingPos_1.fDirectionZd'
       may not be appealing. Branches are read from
       branch_name and saved as new_name.

       If (3), the dictionary is internally converted
       to a tuple like the one in (2) and then goes
       the same way.

       Mixing (1) and (2) is not possible. All
       elements should be str or tuple(str,str).
       A special behavior happens in root files if
       a branch identifies an MTime object. In such
       a case, the MTime object is interpreted and
       the MJD times as np.float128 are returned.
       To enable this, provide "MTime." (with the
       dot!) as a branch name. If you provide any
       name of a composite branch like "MHillas_1.",
       all the subbranches are read in with their
       name appended as column names. So, if you
       say Content=(('HILLAS','MHillas_1.'),) you
       will get HILLAS.fSize, HILLAS.fLength,...

       In (4), the special wildcard '*' is used to
       read everything. Root files are walked thru
       recursively, saving elemental values. This
       means that an MParContainer object will be
       unbundled in its fBits and fUniqueID members
       and the "MParContainer." branch will be
       removed from the file. In this mode the
       special behavior for the MTime. containers
       does not work.

       For fits files, if a column is found with
       the name 'BARY_TIME' it is converted from
       MET to MJD.

       Tree: the tree where to look. Defaults to
       'Events'.
    """
    


    self.Input      = Input
    self.Format     = Format.lower()
    self.Contents   = Contents

    self.Filenames  = []
    self.Data       = None
    self.Tree       = Tree


    ##If Input and Contents are simple strings, tuplify them. 
    if type(self.Input) == str:
      self.Input = (self.Input,)

    if type(self.Contents) == str:
      self.Contents = (self.Contents,)

    if type(self.Contents) == dict:
      self.Contents = tuple(self.Contents.items())

    ##Next comes the checks on the input and the requested content.
    print(self.Input)
    IsEmpty = not any(bool(f) for f in self.Input)
    if IsEmpty:
      raise IOError('Input is undefined.')

    ##Determines the type of behavior. 
    HasStrContents   = all(type(c) is str   for c in self.Contents)
    HasTupleContents = all(type(c) is tuple for c in self.Contents)

    if (not HasStrContents) and (not HasTupleContents):
      errstr = 'Contents: '+repr(self.Contents)+'\nThe mapping must be a list of (str, str) tuples or a list of str.'
      raise IOError(errstr)

    ##If in renaming mode, Input must be ((str,str), (str,str),... )
    if HasTupleContents:
       AreTuplesTwo   = all(len(c) == 2 for c in self.Contents)
       InceptionStr   = all(all(type(s) is str for s in c) for c in self.Contents)
       if (not AreTuplesTwo) or (not InceptionStr):
         errstr = 'Contents: '+repr(self.Contents)+'\nTuple mappings must be (str, str) tuples.'
         raise IOError(errstr)

    ##Expanding the paths
    for path in self.Input:
      glpath = glob.glob(path)
      if not glpath:
        raisewarn('Path "'+path+'" did not expand to anything\n')
        continue
      self.Filenames += glob.glob(path)

    if not self.Filenames:
      raise IOError('None of the inputs exist! :-(')

    ##The format guessing code
    if self.Format == '':
      if '.root' in self.Filenames[0]:
        self.Format = 'root'
      elif '.fits' in self.Filenames[0]:
        self.Format = 'fits'
      elif '.txt' in self.Filenames[0]:
        self.Format = 'tempotxt'
      else:
        errstr = 'No format given and could not guess the format from extension.'
        raise IOError(errstr)
 
    ##If in tempotxt mode, the behavior is fixed.
    ##We set in any case Contents to what we actually
    ##do, to let the user inspect .Contents.
    if self.Format == 'tempotxt':
      HasTupleContents = True
      HasStrContents   = False
      self.Contents    = (('col3','BARY'),('col4','PHASE'))

    #Header to inform the user of what happens.
    print("Reading files from: ", end=' ')
    if len(self.Filenames) == 1:
      print(self.Filenames[0])
    elif len(self.Filenames) <= 10:
      print(self.Filenames)
    else:
      print('(', end=' ')
      for path in self.Input:
        glpath = glob.glob(path)
        if not glpath:
          continue
        print(path,', ', end=' ')
      print(')')

    #If self.Contents is '*' the special behavior
    #triggers.
    ReadAll = any(c == '*' for c in self.Contents)
    if not ReadAll:
      print("Reading these elements: ",self.Contents)


    ##Code to read a MAGIC rootfile.
    if   (self.Format == 'root'):
      if HasTupleContents:
        branches = [] 
        branames = []
        for cont in self.Contents:
           branches.append(bytes(cont[1],'ascii'))
           branames.append(cont[0])
      if HasStrContents:
        branches = [ bytes(cont,'ascii') for cont in self.Contents ]
        branames = self.Contents
 
      ##Data is initially treated as a dictionary.
      ##It is later converted to a table
      self.Data = {}

      ##Cycling on filenames 
      for b,name in enumerate(self.Filenames):
        print("Reading ",name,"...  [%d/%d]" %(b+1,len(self.Filenames)))
        ##Opening the tree
        ievents = uproot.open(name)[self.Tree]
        dropbranches = []
        if ReadAll:
          ##Special behavior. We iterate recursively on all
          ##branches of the file. This array contains both
          ##good branches such as MHillas_1.fSize and bad
          ##ones like MHillas_1. (which can not be cast to
          ##a numpy type). The bad branches have subbranches
          ##which can be seen with the keys() method. We
          ##keep only branches for which the keys() method
          ##returns [], i.e. the good ones.
          branches = list(ievents.iterkeys(recursive=True))
          branches = [ br for br in branches if (list(ievents[br].keys()) == []) ]
          branames = [ bytebranch.decode() for bytebranch in branches ]
        try:
          ##This one should be faster, if applicable. It is
          ##executed if no composite branch is in the file.
          idata   = ievents.arrays(branches)
        except (ValueError, NameError):
          ##This is executed if we have a composite branch in
          ##our branches (but not if '*' was used). We add
          ##branches which don't end with the dot '.'. Among
          ##the others, we check for the existence of a
          ##subbranch fTime.fMilliSec. If so, it means that
          ##this is an "MTime." object and if the relative
          ##option was specified we convert the weird time
          ##representation of MARS into an np.float128 object.
          ##The same applies if there exists a sub-branch
          ##MTime.fTime.fMilliSec, in which case we are
          ##dealing with an MBary object time.
          ##In any other case, we walk the tree of subbranches
          ##and add them manually to the dictionary if they
          ##are not composite too.

          if (b == 0):
            newbranches = []
            newbranames = []
            for iname,ibranch in zip(branames,branches):
              if ibranch.decode()[-1]!='.':
                newbranches.append(ibranch)
                newbranames.append(iname)
              elif ConvertMjd and (ibranch+b'fTime.fMilliSec' in ievents[ibranch].allkeys()):
                newbranches.append(ibranch)
                newbranches.append(ibranch+b'fMjd')
                newbranches.append(ibranch+b'fTime.fMilliSec')
                newbranches.append(ibranch+b'fNanoSec')

                newbranames.append(iname)
                newbranames.append(iname+'@MJD')
                newbranames.append(iname+'@MILLI')
                newbranames.append(iname+'@NANO')
              elif ConvertMjd and ibranch+b'MTime.fTime.fMilliSec' in ievents[ibranch].allkeys():
                newbranches.append(ibranch)
                newbranches.append(ibranch+b'MTime.fMjd')
                newbranches.append(ibranch+b'MTime.fTime.fMilliSec')
                newbranches.append(ibranch+b'MTime.fNanoSec')

                newbranames.append(iname)
                newbranames.append(iname+'@MJD')
                newbranames.append(iname+'@MILLI')
                newbranames.append(iname+'@NANO')
              else:
                allbranches = list(ievents[ibranch].iterkeys(recursive=True))
                subbranches = [ br for br in allbranches if (br in list(ievents[ibranch].allkeys()) and list(ievents[br].keys()) == []) ]
                if iname==ibranch.decode():
                  subbranames = [ (br.decode()).replace(ibranch.decode(),iname) for br in subbranches ]
                else:
                  subbranames = [ (br.decode()).replace(ibranch.decode(),(iname+'.')) for br in subbranches ]

                newbranches = newbranches + subbranches
                newbranames = newbranames + subbranames
                
                
            branches = newbranches
            branames = newbranames



          idata = {}
          for ibranch in branches:
            if ConvertMjd and (ibranch+b'fTime.fMilliSec' in list(ievents[ibranch].allkeys())):
              fMjd      = ievents[ibranch+b'fMjd'].array()
              fMilliSec = ievents[ibranch+b'fTime.fMilliSec'].array()
              fNanoSec  = ievents[ibranch+b'fNanoSec'].array()

              fMjd_ld      = fMjd.astype(np.float128)
              fMilliSec_ld = fMilliSec.astype(np.float128)
              fNanoSec_ld  = fNanoSec.astype(np.float128)

              ##np.float128(24) != np.float128('24') ROUNDING!!
              oneday = np.float128('24')*np.float128('3600')*np.float128('1000')

              fMjd_ld[fMilliSec_ld<0] = fMjd_ld[fMilliSec_ld<0]-np.float128('1')

              fMilliSec_ld = fMilliSec_ld/oneday
              fMilliSec_ld[fMilliSec_ld<0] = fMilliSec_ld[fMilliSec_ld<0]+np.float128('1')

              fNanoSec_ld = fNanoSec_ld/oneday/np.float128('1e6')


              time = fMjd_ld + fMilliSec_ld + fNanoSec_ld

              idata.update({ibranch: time})
              continue
            elif ConvertMjd and (ibranch+b'MTime.fTime.fMilliSec' in list(ievents[ibranch].allkeys())):
              fMjd      = ievents[ibranch+b'MTime.fMjd'].array()
              fMilliSec = ievents[ibranch+b'MTime.fTime.fMilliSec'].array()
              fNanoSec  = ievents[ibranch+b'MTime.fNanoSec'].array()

              fMjd_ld      = fMjd.astype(np.float128)
              fMilliSec_ld = fMilliSec.astype(np.float128)
              fNanoSec_ld  = fNanoSec.astype(np.float128)

              ##np.float128(24) != np.float128('24') ROUNDING!!
              oneday = np.float128('24')*np.float128('3600')*np.float128('1000')

              fMjd_ld[fMilliSec_ld<0] = fMjd_ld[fMilliSec_ld<0]-np.float128('1')

              fMilliSec_ld = fMilliSec_ld/oneday
              fMilliSec_ld[fMilliSec_ld<0] = fMilliSec_ld[fMilliSec_ld<0]+np.float128('1')

              fNanoSec_ld = fNanoSec_ld/oneday/np.float128('1e6')


              time = fMjd_ld + fMilliSec_ld + fNanoSec_ld

              idata.update({ibranch: time})
              continue
            ###
            try:
              idata.update({ibranch: ievents[ibranch].array()})
            except (ValueError, NameError):
              warnstr="Unable to join "+ibranch.decode()+" . Skipping."
              dropbranches.append(ibranch)
              raisewarn(warnstr)

        for ibranch in dropbranches:
            idx = branches.index(ibranch)
            branches.remove(ibranch)
            branames.pop(idx)

        ##Joining of multiple files
        if b == 0:
          self.Data = idata
          continue
        for branch in branches:
          self.Data[branch] = np.concatenate((self.Data[branch],idata[branch]))

      ##Final conversion to a table and renaming
      self.Data = Table([self.Data[address] for address in branches], names=branames)


    ##The code for fits files. These could be
    ##Fermi files or MAGIC files converted to
    ##fits.
    elif ((self.Format == 'fits')):

      columns = [] 
      colnames = []
      
      if HasTupleContents:
        for cont in self.Contents:
           columns.append(cont[1])
           colnames.append(cont[0])
      if HasStrContents:
        columns  = self.Contents
        colnames = self.Contents

      ##Cycle on files
      self.Data = Table()
      for b,filename in enumerate(self.Filenames):
        #Unfortunately the stupid astropy developers
        #stopped porting back updates for the python2
        #version. Shame on them. memmap is very useful
        #but works only with astropy > 3.
        try:
          itable = Table.read(filename,hdu=1,memmap=True)
        except:
          itable = Table.read(filename,hdu=1)

        ##Slice if needed, updates columns otherwise
        if not ReadAll:        
          itable = itable[columns]
        else:
          columns = itable.columns

        tcols = list('@MJD' in col for col in itable.columns)
        tcols = np.array([col for col in itable.columns],dtype=str)[tcols]
        for i,tcol in enumerate(tcols):
          tcols[i] = tcol[:-4]
          

        ##Cycle on columns
        for i,c in enumerate(columns):
          ##Converts MET to MJD
          if ConvertMjd:
            if c == 'BARY_TIME' :
              itable['BARY_TIME'] = itable['BARY_TIME'].astype(np.float128)/np.float128('86400.') \
                                    + np.float128('51910.')                                       \
                                    + np.float128('7.428703703703703e-04')
              raisewarn('Converting '+c+' column with high precision MJD time values')
            if c in tcols:
              Mjd      = itable[c+'@MJD']  .astype(np.float128)
              MilliSec = itable[c+'@MILLI'].astype(np.float128)
              NanoSec  = itable[c+'@NANO'] .astype(np.float128)

              oneday = np.float128('24')*np.float128('3600')*np.float128('1000')

              Mjd[MilliSec<0] = Mjd[MilliSec<0]-np.float128('1')

              MilliSec = MilliSec/oneday
              MilliSec[MilliSec<0] = MilliSec[MilliSec<0]+np.float128('1')

              NanoSec = NanoSec/oneday/np.float128('1e6')

              itable[c] = np.zeros_like(itable[c]).astype(np.float128)
              itable[c] = Mjd + MilliSec + NanoSec
              raisewarn('Replacing '+c+' column with high precision time values')

          ##Renames, if needed. If no new names were given.
          ##this has no effect.
          if not ReadAll:
            itable[c].name = colnames[i]

       ##Appening data
        if b == 0:
          self.Data = itable
        else:
          self.Data = vstack([self.Data,itable])  

  
    ##Code for the tempo2 output format. This
    ##is really small and can be done manually.
    elif (self.Format == 'tempotxt'):
      self.Data = Table()
      for b,filename in enumerate(self.Filenames):
        txtdata = Table.read(filename,format='ascii',delimiter='\s')['col3','col4']
        txtdata['col3'].name = 'BARY'
        txtdata['col4'].name = 'PHASE'
 
        if b == 0:
          self.Data = txtdata
        else:
          self.Data = vstack([self.Data,txtdata]) 
      
    else:
      raise IOError('Format "'+self.Format+'" not recognized.')

  def saveFits(self,TableName):
    """ Saves data as a Fits Table
    """
    print("Saving fits table to: %s" %(TableName))
    savecols = {}
    for col in self.Data.columns:
      if self.Data[col].dtype == 'float128':
        sys.stdout.flush()
        warnstr = 'Astropy/FITS can not handle Long Double (aka np.float128): '
        if col+'@MJD' in self.Data.columns:
          warnstr += 'Saving original data as integer columns'
          savecols[col] = copy.deepcopy(self.Data[col]) #The original float128 column
          ##We cast the column to float64.
          self.Data[col] = self.Data[col].astype(np.float64)
        else:
          warnstr += 'Precision will be lost'
        raisewarn(warnstr)
          
    self.Data.write(TableName,format='fits',overwrite=True)
    ##We restore the original column
    for saved in savecols.keys():
      self.Data[saved] = savecols[saved]

  def saveAscii(self,TxtName):
    """ Saves data as an Ascii textfile, using the
        ecvs format.
    """
    print("Saving text file to: %s" %(TxtName))
    self.Data.write(TxtName,format='ascii.ecsv',fast_writer=True,overwrite=True)

  def copyData(self):
    """ Returns a deepcopy of the Data member
    """
    return copy.deepcopy(self.Data)

  def __add__(self,other):
    """ Enables stacking the Data tables with the
        + operator
    """
    if isinstance(other,Reader):
      return vstack([self.Data,other.Data])
    raise TypeError('Adding '+str(type(other))+' does not work.')

  def __getitem__(self,key):
    """ Enables access to the Data table with the
        [] operator
    """
    return self.Data[key]

  def __repr__(self):
    """ If printed, the reader prints its content.
    """
    return repr(self.Data)

def ReadFermiData(Input):
  r = Reader(Input,(('BARY_TIME','BARY'),('PULSE_PHASE','PHASE'),('ENERGY','ENERGY')))
  return r.Data
