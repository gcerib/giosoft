#!/usr/bin/python3

import numpy as np
import sys

class GCuts:
  """Giovanni's class for dealing with energy-dependent hadronness and theta2
     cuts.
  """
  _low_ebins = np.array([])
  _up_ebins  = np.array([])
  _had_cuts  = np.array([])
  _th2_cuts  = np.array([])

  _cutsfilename = ''
  
  _nbins     = 0
  _init      = False
  def __init__(self,cutsname=''):
     """Initializer with a cuts file"""
     if(cutsname!=''):
        self.SetCuts(cutsname)

  def SetCuts(self,cutsname):
     """Set the cuts file"""
     if(self._init):
        self.ClearCuts()

     try:
        a = np.loadtxt(cutsname)
     except:
        sys.stderr.write("Error reading the file: %s\n" %cutsname)
        sys.stderr.flush()
        return

     #Any extra stuff will be ignored
     a = a[:,0:4]

     self._nbins = len(a)
     self._low_ebins = a[:,0]
     self._up_ebins  = a[:,1]
     self._th2_cuts  = a[:,2]
     self._had_cuts  = a[:,3]

     self._ebins = self._low_ebins.tolist()+[self._up_ebins[-1]]
     self._hc = [-1]+self._had_cuts.tolist()+[-1]
     self._tc = [-1]+self._th2_cuts.tolist()+[-1]
     

     self._cutsfilename = cutsname
     self._init = True
     #self.PrintCuts()

  def ClearCuts(self):
     """Clear the cuts in memory"""
     if(self._init):
        self._low_ebins = np.array([])
        self._up_ebins  = np.array([])
        self._had_cuts  = np.array([])
        self._th2_cuts  = np.array([])

        self._cutsfilename = ''
        self._nbins     = 0
        self._init      = False

  def PrintCuts(self):
     """Print the cuts in memory"""
     if(not (self._init)):
        sys.stderr.write("Error: cuts not initialized.\n")
        sys.stderr.flush()
        return
     print("Reading cuts from: %s" %self._cutsfilename)
     print("        Energy bin        Theta2     Hadronness")
     for i in range(self._nbins):
        print(" %8.2f   %8.2f   %8.3f   %8.3f" %(self._low_ebins[i],self._up_ebins[i],self._th2_cuts[i],self._had_cuts[i]))
     print()

  def GetCutsFileName(self):
     """Returns the name of the file the cuts were read from"""
     return self._cutsfilename

  def Hadronness(self,energy):
     """Returns Hadronness cut for a given energy"""
     if(not (self._init)):
        sys.stderr.write("Error: cuts not initialized.\n")
        sys.stderr.flush()
        return 100
     try:
        return self._had_cuts[(self._low_ebins <= energy)*(self._up_ebins > energy)][0]
     except IndexError:
        return -100

  def Theta2(self,energy):
     """Returns Theta2 cut for a given energy"""
     if(not (self._init)):
        sys.stderr.write("Error: cuts not initialized.\n")
        sys.stderr.flush()
        return 100
     try:
        return self._th2_cuts[(self._low_ebins <= energy)*(self._up_ebins > energy)][0]
     except IndexError:
        return -100

  def MultiCuts(self,energies):
     """Computes cuts in both hadronness and theta2 on an array of energies,
        returning two arrays with the theta2 and hadroness cuts for each.
     """
     if(not (self._init)):
        sys.stderr.write("Error: cuts not initialized.\n")
        sys.stderr.flush()
        return 100
     mask = np.zeros((len(energies),2))
     for j in range(self._nbins):
        emask = ((energies >= self._low_ebins[j]) & (energies < self._up_ebins[j]))
        mask += np.array([emask*self.GetBinTheta2(j),emask*self.GetBinHadronness(j)]).T
     return mask[:,0],mask[:,1]

  def HadronnessNew(self,energy):
     """Returns Hadronness cut for a given energy. New method."""
     if(not (self._init)):
        sys.stderr.write("Error: cuts not initialized.\n")
        sys.stderr.flush()
        return 100
     if hasattr(energy,'__iter__'):
       return np.array([ self._hc[j] for j in np.digitize(energy,self._ebins) ])
     else:
       return self._hc[np.digitize(energy,self._ebins)]
     
  def Theta2New(self,energy):
     """Returns Theta2 cut for a given energy. New method."""
     if(not (self._init)):
        sys.stderr.write("Error: cuts not initialized.\n")
        sys.stderr.flush()
        return 100
     if hasattr(energy,'__iter__'):
       return np.array([ self._tc[j] for j in np.digitize(energy,self._ebins) ])
     else:
       return self._tc[np.digitize(energy,self._ebins)]
     
  def MultiCutsNew(self,energies):
     """Computes cuts in both hadronness and theta2 on an array of energies,
        returning two arrays with the theta2 and hadroness cuts for each.
     """
     if(not (self._init)):
        sys.stderr.write("Error: cuts not initialized.\n")
        sys.stderr.flush()
        return 100
     evbins = np.digitize(energies,self._ebins)
     return [ self._tc[j] for j in evbins ], [ self._hc[j] for j in evbins ]

  def GetBinHadronness(self,i):
     """Returns the Hadroness cut for the i-th bin"""
     if(not (self._init)):
        sys.stderr.write("Error: cuts not initialized.\n")
        sys.stderr.flush()
        return 100
     try:
        return self._had_cuts[i]
     except IndexError:
        return -100

  def GetBinTheta2(self,i):
     """Returns the Theta2 cut for the i-th bin"""
     if(not (self._init)):
        sys.stderr.write("Error: cuts not initialized.\n")
        sys.stderr.flush()
        return 100
     try:
        return self._th2_cuts[i]
     except IndexError:
        return -100

  def GetMinEnergy(self):
     """Returns the minimum energy in the cuts"""
     if(not (self._init)):
        sys.stderr.write("Error: cuts not initialized.\n")
        sys.stderr.flush()
        return -1
     return self._low_ebins[0]

  def GetMaxEnergy(self):
     """Returns the maximum energy in the cuts"""
     if(not (self._init)):
        sys.stderr.write("Error: cuts not initialized.\n")
        sys.stderr.flush()
        return -1
     return self._up_ebins[-1]

  def GetLowBinEdge(self,i):
     """Returns the lower energy edge of the i-th bin"""
     if(not (self._init)):
        sys.stderr.write("Error: cuts not initialized.\n")
        sys.stderr.flush()
        return -1
     try:
        return self._low_ebins[i]
     except IndexError:
        return -1

  def GetUpBinEdge(self,i):
     """Returns the upper energy edge of the i-th bin"""
     if(not (self._init)):
        sys.stderr.write("Error: cuts not initialized.\n")
        sys.stderr.flush()
        return -1
     try:
        return self._up_ebins[i]
     except IndexError:
        return -1

  def GetNBins(self):
     """Returns the number of bins"""
     if(not (self._init)):
        sys.stderr.write("Error: cuts not initialized.\n")
        sys.stderr.flush()
     return self._nbins

  def GetInitState(self):
     """Returns the initialization state"""
     return self._init

  def WhichBin(self,energy):
     """Returns the bin number given the energy"""
     if(not (self._init)):
        sys.stderr.write("Error: cuts not initialized.\n")
        sys.stderr.flush()
        return 0.5
     try:
        #return np.flatnonzero((self._low_ebins <= energy)*(self._up_ebins > energy))[0]
        np.digitize(energy,self._ebins)-1
     except IndexError:
        return 0.5












if __name__ == '__main__':
  help(GCuts)
  print("Hey, run me inside some macro.")
