# Phaseogram

Phaseogram building class that can be used to produce and plot various phaseograms, and accomplish related tasks. Depends on Reader.py and GCuts.py in "io". Provided in python2 and python3 versions.
