#!/usr/bin/python3
"""
        ______ _                                                    
        | ___ \ |                                                   
        | |_/ / |__   __ _ ___  ___  ___   __ _ _ __ __ _ _ __ ___  
        |  __/| '_ \ / _` / __|/ _ \/ _ \ / _` | '__/ _` | '_ ` _ \ 
        | |   | | | | (_| \__ \  __/ (_) | (_| | | | (_| | | | | | |
        \_|   |_| |_|\__,_|___/\___|\___/ \__, |_|  \__,_|_| |_| |_|
                                           __/ |                    
                                          |___/                     



   
   This is the Phaseogram.py script and module. If you whish to use the script
   directly, just edit the configuration section below. Otherwise, play with it
   by importing it as a module in your program.

   If you are reading this in the documentation of the module but just want to
   make a quick phaseogram, open the file Phaseogram.py and edit it there!
"""

################################################################################
###  CONFIGURATION CONFIGURATION CONFIGURATION CONFIGURATION CONFIGURATION   ###
################################################################################
Input      = ("/remote/sumtdata/Userspace/giovanni/10_GEMINGA/upper/melibea20180813/tempo2/20*.root",)
TableName  = 'Geminga_MAGIC.fits'
SaveTable  = False


PhaseStr   = "MPhase2_1.fPhase"
StereoCon  = "MStereoParDisp"
EnergyCon  = "MEnergyEst"
HadronCon  = "MHadronness"
TrgPatCon  = "MTriggerPattern_2"
ExtrasCon  = None
#ExtrasCon  = (('BARY','MBary_1.'),)

NBins      = 54
LowEn      = 25
HighEn     = 100
NumLogBins = 1

CutsFile   = "t75h90fine.cuts"

OnRegions  = { 
               'P1' :  ( ( 0.056 , 0.161 ) , ),
               'P2' :  ( ( 0.550 , 0.642 ) , )
             }

OffRegion  = ( (0.700, 0.950),) 

PrintStats = True
PlotErrors = False
SwitchHori = False
PlotLabels = True

LogOnFile  = True

################################################################################
### END END END END END END END END END END END END END END END END END END  ###
################################################################################ 

import sys
import glob

import numpy as np

import colorsys

import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.patheffects as PathEffects

from copy import deepcopy
from scipy.stats import chi2
from scipy.special import erfinv
from matplotlib.patches import Rectangle

from astropy.table import Table

import pickle as pickle

import GCuts3 as GCuts
import Reader3 as Reader

from numpy.fft import fft, ifft

################################################################################

def CalcLima17(Non,Noff,alfa):
  """
     Calculated LiMa17. If the excess is lower than zero,
     returns a negative significance.
  """
  significance = np.sqrt( 2*Non* np.log( (1+alfa)/alfa * ( Non / (Non+Noff) ) ) + \
                          2*Noff*np.log( (1+alfa)      * ( Noff/ (Non+Noff) ) )   )
  if(Non>alfa*Noff):
    return significance
  else:
    return -significance

def CalcOnOffRatio(on,off):
  """
     Both "on" and "off" should be tuples of tuples,
     like ((0,0.02),(0.98,1)).
  """
  won = 0
  woff = 0
  for r in on:
    won += r[1]-r[0]
  for r in off:
    woff += r[1]-r[0]
  return won/woff

def CalcHarmonicSum(phases,harmonic):
  """
     Phases must be a numpy array, harmonic an integer.
  """
  a = np.sum(np.cos(phases*harmonic*2*np.pi))**2
  b = np.sum(np.sin(phases*harmonic*2*np.pi))**2
  z = 2*(a+b)/len(phases)
  return z

def FourierKDE(x,coeffAB):
  """
     Value of the Fourier density estimator at phase x
     given the coefficients coeffAB, which is a 2d array
     with the order of the coefficient as rows and
     the cosine and sine coefficients on columns: (N,2).
     See de Jager et al, "A powerful test for weak
     periodic signals with unknown light curve shape
     in sparse data.", A&A August 1989, equations
     3,5,6,7
  """
  N = coeffAB.shape[0]
  if hasattr(x,'__iter__'):
    iphi = np.zeros((N,2,len(x)))
  else:
    iphi = np.zeros((N,2))
  for i in range(N):
      iphi[i,0] = np.cos(x*2*np.pi*(i+1))
      iphi[i,1] = np.sin(x*2*np.pi*(i+1))
  if hasattr(x,'__iter__'):
    return 1 + 2*(iphi*coeffAB[...,None]).sum(axis=(0,1))
  else:
    return 1 + 2*(iphi*coeffAB).sum(axis=(0,1))

def BuildFourierKDE(phases,N):
  """
     Build the Fourier density estimator coefficients
     coeffAB from a phase series and with maximum harmonic
     N, and returns a specialized version of FourierKDE()
     to use with such coefficients.
  """
  coeffAB = np.zeros((N,2))
  for i in range(N):
      coeffAB[i,0] = np.average(np.cos(phases*2*np.pi*(i+1)))
      coeffAB[i,1] = np.average(np.sin(phases*2*np.pi*(i+1)))
  myKDE = lambda x: FourierKDE(x,coeffAB)
  return myKDE

def genTableaux(n):
  """ Genera i tableaux di Young per un intero n. Preso da
      http://jeromekelleher.net/category/combinatorics.html """
  a = [0 for i in range(n + 1)]
  k = 1
  y = n - 1
  while k != 0:
      x = a[k - 1] + 1
      k -= 1
      while 2*x <= y:
          a[k] = x
          y -= x
          k += 1
      l = k + 1
      while x <= y:
          a[k] = x
          a[l] = y
          yield a[:k + 2]
          x += 1
          y -= 1
      a[k] = x + y
      y = x + y - 1
      yield a[:k + 1]

################################################################################

class Phaseogram:
  """A small class to keep phaseogram informations.
     Attributes:

      Foundamental attributes:
        nbins                Can be used to store len(x).
        multi                How many times to repeat the phase axis.
        x                    Mid-bin position in phase   (x-axis)
        y                    Number of events in the bin (y-axis)
        nevents              Number of events (total)

      Statistics attributes (computed if "regions" is provided)
        non                  On events  (total)
        noff                 Off events (total)
        alfa                 alpha: ratio of the ON to the OFF region
        exc                  Excess events
        sig                  LiMa17 Significance
        regstats             Dictionary with the names of the regions as keys and the
                             non,alfa,exc,sig values for the subregion (i.e., P1,P2).
                             The latter are stored as sub-dictionaries with keys 'non',
                             'alfa','exc','sig'.
        Z                    array, if you initialize with the constructor it will store
                             the Z_n^2 values as Z[n]. Z[0]=0
        H                    H value
        opth                 Optimum harmonics from the H test

        reg                  Copy of the regions object.

      Extra attributes (to be set manually):    
        name                 A name for the phaseogram
        title                A title to be shown above the plot
        color                A base color in hex format: "#ffffff"
        energy_range         An energy range for the phaseogram
        nevents_before       Number of events before the cuts

  """

  def __init__(self,data=None,nbins=None,regions=None,multi=None):
      self.nbins = 100
      self.multi = 1

      self.x = np.array([])   
      self.y = np.array([])   
      self.nevents = 0        

      self.non  = 0           
      self.noff = 0           
      self.alfa = 0           
      self.exc  = 0           
      self.sig  = 0           
      self.regstats = {}      

      self.Z   = np.zeros(21)
      self.H   = 0         
      self.opth = 0           

      self.name = ''
      self.title = ''
      self.color = None
      self.energy_range = (0,0)
      self.nevents_before = 0 

      self.reg = Config()

      ##This is the non-default constructor
      if nbins is not None:
        if nbins == 0:
          errstr = 'Can\'t make a phaseogram with zero bins.'
          raise ValueError(errstr)
        self.nbins     = nbins
        #else the default, which is 100.
      if multi:
        if not int(multi)==multi:
          errstr = 'Invalid multiplicity valie: %f' %multi
          raise ValueError(errstr)
        if multi <= 0:
          errstr = 'Invalid multiplicity value: %d' %multi
          raise ValueError(errstr)
        self.multi     = int(multi)
        #else the default, which is 1.
      if data is not None:
        #Computing the phaseogram
        if hasattr(data,'keys'):
          if 'PHASE' in data.keys():
            dataph = data['PHASE']
          else:
            errstr = 'Data should be a list of phases, or a dict-like with a \'PHASE\' attribute.'
            raise ValueError(errstr)
        else:
          dataph = data
            
        if self.multi == 1:
          self.y, self.x = np.histogram(dataph,bins=   self.nbins,range=(0,1),density=False)
        else:
          m = self.multi
          self.y, self.x = np.histogram(dataph,bins= m*self.nbins,range=(0,m),density=False)
          for j in np.arange(self.nbins,m*self.nbins):
            self.y[j]=self.y[j-self.nbins]
        self.x         = (self.x[:-1]+self.x[1:])/2
        self.nevents   = len(data)

        #Computing Z and H
        for h in np.arange(1,21):
          self.Z[h] = CalcHarmonicSum(dataph,h)
          self.Z[h] += self.Z[h-1]
        for h in np.arange(1,21):
          hi = self.Z[h] - 4*h + 4
          if(hi>self.H):
            self.H    = hi
            self.opth = h

        #Statistics, if regions were provided
        if regions:
          #First we calculate the off counts...
          self.noff = 0.
          for r in regions.OffRegion:
            self.noff += np.sum((dataph >= r[0]) & (dataph < r[1]))
         
          #...then the on ones
          self.non  = 0.
          self.alfa = 0.
          self.regstats = {}
          #For each of the subregions...
          for k,v in regions.OnRegions.items():
            valfa = CalcOnOffRatio(v,regions.OffRegion)
            vnon = 0.
            for r in v:
              vnon += np.sum((dataph >= r[0]) & (dataph < r[1]))
            vexc = vnon - valfa*self.noff
            vsig = CalcLima17(vnon,self.noff,valfa)
         
            self.non  += vnon
            self.alfa += valfa
         
            #Fill the dictionary
            self.regstats[k] = {'non':vnon,'alfa':valfa,'exc':vexc,'sig':vsig}
         
          #...and the total
          self.exc  = self.non - self.alfa*self.noff
          self.sig  = CalcLima17(self.non,self.noff,self.alfa)

          #Finally we save the information of the regions
          self.reg = regions

        #Having no name is sad...
        self.name = 'Phaseogram'
        
  def __repr__(self):
      """ Defaults to GetPrintString()
      """
      return self.GetPrintString()

  def GetStatString(self):
      """ Returns a string with the statistics in LaTeX
          format to be put in the infobox
      """
      StatString = r"${\bf Statistics:}$"+'\n'
      #This line of spaces is fundamental to get boxes of the same shape.
      StatString += "                                \n"
      StatString += "$N_{on}$ = %d\n" %self.non
      StatString += "$N_{off}$ = %d\n" %self.noff
      StatString += "${\\alpha\\cdot}N_{off}$ = %0.1f\n" %(self.noff*self.alfa)
      StatString += "$\\alpha$ = %0.4f\n" % self.alfa
      StatString += "${N_{exc} = %0.1f}$\n" %self.exc
      if(self.sig<5):
        StatString += "${\\sigma  = %0.2f}$\n" %self.sig
      else:
        StatString += "${\\bf\\sigma  = %0.2f}$\n" %self.sig
      StatString += '\n'
      if(len(self.reg.OnRegions)>1):
        for k,v in self.regstats.items():
           StatString += '${\\bf %8s}$:\n' %k
           StatString += "  $N_{on}$ = %d\n" %v['non']
           StatString += "  ${\\alpha\\cdot}N_{off}$ = %0.1f\n" %(self.noff*v['alfa'])
           StatString += "  $\\alpha$ = %0.4f\n" % v['alfa']
           StatString += "  ${ N_{exc} = %0.1f}$\n" %v['exc']
           if(v['sig']<5):
             StatString += "  ${ \\sigma  = %0.2f}$\n" %v['sig'] 
           else:
             StatString += "  ${\\bf \\sigma  = %0.2f}$\n" %v['sig'] 
           StatString += '\n'
      sigma_z10 = np.sqrt(2)*erfinv((chi2.cdf(self.Z[10], 20)))
      sigma_hvl = np.sqrt(2)*erfinv(1-np.exp(-0.4*self.H))
      if(sigma_z10<5):
        StatString += '${Z^2_{10}}$ = %0.1f, %0.1f$\\sigma$\n' %(self.Z[10],sigma_z10)
      else:
        if(sigma_z10 != float('inf')):
          StatString += '${\\bf{Z^2_{10}} = %0.1f, %0.1f\\sigma}$\n' %(self.Z[10],sigma_z10)
        else:
          StatString += '${\\bf{Z^2_{10}} = %0.1f, \\infty\\sigma}$\n' %(self.Z[10])
      if(sigma_hvl<5):
          StatString += '${H}$ = %0.1f, %0.1f$\\sigma$\n' %(self.H,sigma_hvl)
      else:
        if(sigma_hvl != float('inf')):
          StatString += '${\\bf{H} = %0.1f, %0.1f\\sigma}$\n' %(self.H,sigma_hvl)
        else:
          StatString += '${\\bf{H} = %0.1f, \\infty\\sigma}$\n' %(self.H)
      StatString += '${n_{opt}}$ = %d' %self.opth
      #These two aren't very useful in the phaseogram.
      #StatString += "$N_{tot}$ =  %d\n" %self.nevents
      #StatString += '${R_{\\%%}}$ = %0.2f%%' %(float(100*self.nevents)/self.nevents_before)
      return StatString

  def GetPrintString(self):
      """ Returns an ASCII string to be printed in the terminal.
      """
      PrintString =  '########################################\n'
      PrintString +=  self.name+' {\n'
      PrintString += " ERANGE\t=  (%0.1f,%0.1f)\n" %self.energy_range
      PrintString += " NON\t=  %d\n" %self.non
      PrintString += " NOFF\t=  %d\n" %self.noff
      PrintString += " a*NOFF\t=  %0.1f\n" %(self.noff*self.alfa)
      PrintString += " alpha\t=  %0.4f\n" % self.alfa
      PrintString += " NEXC\t=  %0.1f\n" %self.exc
      PrintString += " sigma\t=  %0.2f\n" %self.sig
      PrintString += "\n"
      if(len(OnRegions)>1):
        for k,v in self.regstats.items():
           PrintString += ' %-5s: {\n' %k
           PrintString += "    NON \t=  %d\n" %v['non']
           PrintString += "    a*NOFF\t=  %0.1f\n" %(self.noff*v['alfa'])
           PrintString += "    alpha\t=  %0.4f\n" % v['alfa']
           PrintString += "    NEXC\t=  %0.1f\n" %v['exc']
           PrintString += "    sigma\t=  %0.2f\n" %v['sig'] 
           PrintString += ' }\n\n'
      sigma_z10 = np.sqrt(2)*erfinv((chi2.cdf(self.Z[10], 20)))
      sigma_hvl = np.sqrt(2)*erfinv(1-np.exp(-0.4*self.H))
      PrintString += " Z^2_10\t=  %0.1f\n" %self.Z[10]
      PrintString += " sigmaZ\t=  %0.2f\n" %sigma_z10
      PrintString += " H\t=  %0.1f\n" %self.H
      PrintString += " sigmaH\t=  %0.2f\n" %sigma_hvl
      PrintString += " nopt\t=  %d\n\n" %self.opth
      PrintString += " NTOT\t=  %d\n" %self.nevents
      if self.nevents_before != 0:
        PrintString += " R%%\t=  %0.2f%%\n" %(float(100*self.nevents)/self.nevents_before)
      PrintString += "}\n"
      PrintString += '########################################\n\n'
      return PrintString

  def DrawPhaseogram(self,ax=None,PlotErrors=False,PlotLabels=True,PlotTitle=True, BaseLevel=0, **kwargs):
      """This function draws a single phaseogram on the given axis.
      """
 
      ShowAtTheEnd = False
      if ax is None:
        fig,ax = plt.subplots()
        ShowAtTheEnd = True
 
      #We read the base-color from the phaseogram attribute
      #and generate the darker color for the phaseogram line.
      if(self.color == None):
        self.color = '#0044cc'
      r = int(self.color[1:3],16)/255.
      g = int(self.color[3:5],16)/255.
      b = int(self.color[5:7],16)/255.
      h,s,v = colorsys.rgb_to_hsv(r,g,b)
      v *= 0.75
      r,g,b = colorsys.hsv_to_rgb(h,s,v)
  
      r = (hex(int(255*r))[2:]).zfill(2)
      g = (hex(int(255*g))[2:]).zfill(2)
      b = (hex(int(255*b))[2:]).zfill(2)
  
      darkcolor = '#'+r+g+b
  
      #First we make the solid line of the histogram.
      ax.plot(self.x,self.y-BaseLevel,drawstyle='steps-mid',color=darkcolor, **kwargs)
  
      #Setting a bit larger range, but avoiding it to become
      #negative.
      ylim    = ax.get_ylim()
      ywide   = ylim[1]-ylim[0]
      newylim = (ylim[0]-ywide*0.05,ylim[1]+ywide*0.05)
      newywide= ywide*1.1
      fontcol = 'white'
      fontbol = 400
      if(newylim[0] < 0):
        newylim = (0,ylim[1]+ywide*0.05)
        newywide=ylim[1]+ywide*0.05
  
  
      #Now we plot the filled bars. The bar width is a nihil
      #larger than the histogram bin to avoid rendering problems
      if(PlotErrors):
        ax.bar(self.x,self.y-BaseLevel,yerr=np.sqrt(self.y),width=1.02/(self.nbins),color=self.color,ecolor=darkcolor, **kwargs)
      else:
        ax.bar(self.x,self.y-BaseLevel,width=1.02/(self.nbins),color=self.color,ecolor=darkcolor, **kwargs)
      ax.set_ylim(newylim)
      ax.set_xlim((0,self.multi))
   
      #If we have the regions and the statistics this will exist. 
      if(self.regstats):
        #On and Off regions are plotted twice each, so that the part
        #above the phaseogram is lighter than the one outside it.
        for k,v in self.reg.OnRegions.items():
          for sr in v:
            irect  = Rectangle((sr[0]  ,newylim[0]),sr[1]-sr[0],newywide,color="#00ee77",alpha=0.25)
            irect2 = Rectangle((sr[0]+1,newylim[0]),sr[1]-sr[0],newywide,color="#00ee77",alpha=0.25)
            ax.add_patch(irect)
            ax.add_patch(irect2)
            irect  = Rectangle((sr[0]  ,newylim[0]),sr[1]-sr[0],newywide,color="#00ee77",alpha=0.25,zorder=-1)
            irect2 = Rectangle((sr[0]+1,newylim[0]),sr[1]-sr[0],newywide,color="#00ee77",alpha=0.25,zorder=-1)
            ax.add_patch(irect)
            ax.add_patch(irect2)
          if(PlotLabels):
            #We add now the regions' labels. To simplify things,
            #the label is put at the middle of the various subregions.
            vmid = np.array([sr[1]-sr[0] for sr in v])
            vmid = (vmid.sum()/len(vmid))/2+sr[0]
            if(newylim[0] != 0):
              ax.text(vmid,ylim[0],k,color='white',weight='400',horizontalalignment='center',verticalalignment='center')
            else:
              #This code is executed if the risk of getting the label
              #over the white zone or on the phaseogram itself is high.
              #It detects the two cased and turnes the label black if it
              #stands above the white part of the plot, or adds a border
              #to it if it is on the phaseogram's line itself.
              yonlab = self.y[(self.x>v[0][0])*(self.x<v[0][1])]
              frover = float(np.sum(yonlab>0.05*newywide))/len(yonlab)
              if(frover < 0.5):
                fontcol = '#121212'
              txt = ax.text(vmid,0.05*newywide,k,color=fontcol,weight=400,horizontalalignment='center',verticalalignment='center')
              if((0.125 <= frover) and (frover < 0.5  )):
                txt.set_path_effects([PathEffects.withStroke(linewidth=1.7, foreground='white')])
              if((0.5   <= frover) and (frover < 0.875)):
                txt.set_path_effects([PathEffects.withStroke(linewidth=1.7, foreground='black')])
        
        #See comments from the previous section. Here we don't have
        #different Off regions, so "v" is replaced with OffRegion.
        for sr in self.reg.OffRegion:
          irect  = Rectangle((sr[0]  ,newylim[0]),sr[1]-sr[0],newywide,color="#aaaaaa",alpha=0.25)
          irect2 = Rectangle((sr[0]+1,newylim[0]),sr[1]-sr[0],newywide,color="#aaaaaa",alpha=0.25)
          ax.add_patch(irect)
          ax.add_patch(irect2)
          irect  = Rectangle((sr[0]  ,newylim[0]),sr[1]-sr[0],newywide,color="#aaaaaa",alpha=0.25,zorder=-1)
          irect2 = Rectangle((sr[0]+1,newylim[0]),sr[1]-sr[0],newywide,color="#aaaaaa",alpha=0.25,zorder=-1)
          ax.add_patch(irect)
          ax.add_patch(irect2)
        if(PlotLabels):
          vmid = np.array([sr[1]-sr[0] for sr in self.reg.OffRegion])
          vmid = (vmid.sum()/len(vmid))/2+sr[0]
          if(newylim[0] != 0):
            ax.text(vmid,ylim[0],'OFF',color='white',weight=400,horizontalalignment='center',verticalalignment='center')
          else: 
            yonlab = self.y[(self.x>sr[0]) * (self.x<sr[1])]
            frover = float(np.sum(yonlab>0.05*newywide))/len(yonlab)
            if(frover < 0.5):
              fontcol = '#121212'
            txt = ax.text(vmid,0.05*newywide,'OFF',color=fontcol,weight=400,horizontalalignment='center',verticalalignment='center')
            if((0.125 <= frover) and (frover < 0.5  )):
              txt.set_path_effects([PathEffects.withStroke(linewidth=1.7, foreground='white')])
            if((0.5   <= frover) and (frover < 0.875)):
              txt.set_path_effects([PathEffects.withStroke(linewidth=1.7, foreground='black')])
   
      ax.set_xlabel("Phase")
      ax.set_ylabel("Counts")

      if BaseLevel != 0:
        if BaseLevel > 0:
          ax.text(-0.01,1.01,'+%g' %BaseLevel,transform=ax.transAxes,clip_on=False)
        else:
          ax.text(-0.01,1.01,'-%g' %(-BaseLevel),transform=ax.transAxes,clip_on=False)

      #In this order:
      # (1) If the option PlotTitle is true (default)
      #    (2) If the energy range is not empty
      #       (3) If the range is finite
      #           ... uses title + range
      #       (3) Else
      #           ... uses title + unlimited range
      #    (2) Else If the title exists
      #        ... uses the title alone
      #     Otherwise don't print anything
      if PlotTitle:
        if any(self.energy_range):
          if not np.isinf(self.energy_range[1]):
            ax.set_title("%10s: %0.3g GeV - %0.3g GeV" % ((self.title,)+(self.energy_range)))
          else:
            ax.set_title("%10s > %0.3g GeV" % (self.title,self.energy_range[0]))
        elif self.title:
          ax.set_title("%10s" % (self.title))

      if ShowAtTheEnd:
        plt.show()
  
  
  def DrawStatBox(self,fig,ax): 
      """Function to draw the Stat box. It needs the figure as input
         as well because we do some guessing of the size to rescale
         the font and the line spacing accordingly."""
  
      #We read the base-color from the phaseogram attribute
      #and generate the grayer color for the box background.
      r = int(self.color[1:3],16)/255.
      g = int(self.color[3:5],16)/255.
      b = int(self.color[5:7],16)/255.
      h,s,v = colorsys.rgb_to_hsv(r,g,b)
      s *= 0.75
      r,g,b = colorsys.hsv_to_rgb(h,s,v)
      r = (hex(int(255*r))[2:]).zfill(2)
      g = (hex(int(255*g))[2:]).zfill(2)
      b = (hex(int(255*b))[2:]).zfill(2)
  
      greycolor = '#'+r+g+b
  
      StatString = self.GetStatString()
  
      #If you are reading this, it means that something went wrong
      #with the scaling of the stat boxes. I am sorry, but I really
      #don't think that it is my fault. It's the developers of
      #matplotlib who are dei colossali cagoni.
      #Text boxes in matplotlib can't have a fixed size. You cannot
      #say them: go from (0,0 to 1,1) and arrange the text inside it.
      #It's not possible. There are hacks to do something similar, but
      #not really the same.
      #Because of this, we are forced to try and rescale THE TEXT to
      #get the box to have more or less the same size...
  
      #We get width and height in pixel.
      bbox = ax.get_window_extent().transformed(fig.dpi_scale_trans.inverted())
      aw,ah = bbox.width,bbox.height
  
  
      #Empiric limits on the font points. 72 is the conversion
      #between points and inches.
      limfontw = aw/30.*72*2.634
      limfonth = ah/30.*72*0.800
  
      #... part of the trick is that the StatString contains a line with
      #32 whitespaces, which are normally longer than any other line in
      #it. Therefore, the box width is being set by the common whitespace
      #line, rather than by the varying content.
  
      #We set the limit to the minimum among the one on width
      #and height of the region.
      limfont = (limfontw<limfonth) and limfontw or limfonth
  
      #Even more empiric scaling of the interline.
      linespacing = np.tanh(limfont*2.71828/8)**3
  
      #We plot it with a slight offset to the right.
      tbox = ax.text(0.07071,0.5,StatString,
                           verticalalignment='center',horizontalalignment='left',
                           bbox=dict(boxstyle='round', facecolor=greycolor, alpha=0.4,),
                           fontsize=limfont,linespacing=linespacing)

  def save(self,filename):
      with open(filename,'wb') as ofile:
        pickle.dump(self, ofile)

  
  @classmethod
  def read(cls,filename):
      with open(filename,'r') as ifile:
        try:
          return pickle.load(ifile)
        except:
          raise

#END of the Phaseogram Class




class Config():
  """Configuration class to store the settings to be passed to
     the various functions. Default setting (which won't produce
     any phaseogram) are:

      Input parameters:

         Input      = ('',)
         TableName  = ''        
         SaveTable  = False

         PhaseStr   = ''     
         StereoCon  = ''
         EnergyCon  = ''
         HadronCon  = ''
         TgrPatCon  = ''
         ExtrasCon  = (('',''),...)
         
      Here, 'Input' is a tuple of strings which get expanded to
      match files. For instance, it may be ('20170*.root', '201
      81*.root','2019*.root'). ExtrasCon contains tuples of
      the kind ('name','container'), so something like:
         ( ('TELESCOPE_ZD','MPointingPos_1.fZd'),
           ('MILLI1','MTime_1.fMilli'),
           ('MJD','MTime_1.fMjd')
         )
      that will be added to the read containers.
      
         

      Analysis Settings:

         NBins      = 0
         LowEn      = 0
         HighEn     = 0
         NumLogBins = 0
         
         CutsFile   = ''

         OnRegions  = { '' : ( ( 0, 0) , ), }
         OffRegion  = ( (0, 0),) 

      OffRegion is a tuple of tuples each containing the edges
      of the subregions in phase that build up the off region.
      OnRegions is a dictionary having a string as a key (the
      name of the particular on region, i.e. 'P1') and a tuple
      of tuples similar to the one of OffRegion as element.



      Graphics Settings:

         PrintStats = True
         PlotErrors = False
         SwitchHori = False
         PlotLabels = True

      These settings control the graphic display if using the
      PlotPhaseograms() function. SwitchHori changes the
      arrangement of the phaseograms in an horizontal mode.
      PlotLabels controls whether labels for the regions are
      drawn or not. Similarly PlotErrors can be used to
      enable erros on the phaseogram bins. PrintStats enables
      one extra matplotlib "axis" where the statbox is drawn.
  """

  def __init__(self                                    ,
               Input      = ('',)                      ,
               TableName  = ''                         ,
               SaveTable  = False                      ,
               PhaseStr   = ''                         ,
               StereoCon  = ''                         ,
               EnergyCon  = ''                         ,
               HadronCon  = ''                         ,
               TrgPatCon  = ''                         ,
               ExtrasCon  = None                       ,
               NBins      = 0                          ,
               LowEn      = 0                          ,
               HighEn     = 0                          ,
               NumLogBins = 0                          ,
               CutsFile   = ''                         ,
               OnRegions  = { '' : ( ( 0, 0) , ), }    ,
               OffRegion  = ( (0, 0),)                 ,
               PrintStats = True                       ,
               PlotErrors = False                      ,
               SwitchHori = False                      ,
               PlotLabels = True                 ,
               copy       = None
              ):
    """Constructor: takes separare inputs, or another
       Config object in 'copy' as copy constructor.
    """
    self.Input      = Input     
    self.TableName  = TableName 
    self.SaveTable  = SaveTable 
    self.PhaseStr   = PhaseStr  
    self.StereoCon  = StereoCon 
    self.EnergyCon  = EnergyCon
    self.HadronCon  = HadronCon
    self.TrgPatCon  = TrgPatCon
    self.ExtrasCon  = ExtrasCon
    self.NBins      = NBins     
    self.LowEn      = LowEn     
    self.HighEn     = HighEn    
    self.NumLogBins = NumLogBins
    self.CutsFile   = CutsFile  
    self.OnRegions  = OnRegions 
    self.OffRegion  = OffRegion 
    self.PrintStats = PrintStats
    self.PlotErrors = PlotErrors
    self.SwitchHori = SwitchHori
    self.PlotLabels = PlotLabels
    if(copy):
      self.Input      = deepcopy(copy.Input     )
      self.TableName  = deepcopy(copy.TableName )
      self.SaveTable  = deepcopy(copy.SaveTable )
      self.PhaseStr   = deepcopy(copy.PhaseStr  )
      self.StereoCon  = deepcopy(copy.StereoCon )
      self.EnergyCon  = deepcopy(copy.EnergyCon )
      self.HadronCon  = deepcopy(copy.HadronCon )
      self.TrgPatCon  = deepcopy(copy.TrgPatCon )
      self.ExtrasCon  = deepcopy(copy.ExtrasCon )
      self.NBins      = deepcopy(copy.NBins     )
      self.LowEn      = deepcopy(copy.LowEn     )
      self.HighEn     = deepcopy(copy.HighEn    )
      self.NumLogBins = deepcopy(copy.NumLogBins)
      self.CutsFile   = deepcopy(copy.CutsFile  )
      self.OnRegions  = deepcopy(copy.OnRegions )
      self.OffRegion  = deepcopy(copy.OffRegion )
      self.PrintStats = deepcopy(copy.PrintStats)
      self.PlotErrors = deepcopy(copy.PlotErrors)
      self.SwitchHori = deepcopy(copy.SwitchHori)
      self.PlotLabels = deepcopy(copy.PlotLabels)
      

  def __repr__(self):
    """Prints on screen the configuration."""
    rstr  =  ''
    rstr +=  'Input      = ' + repr(self.Input     ) + '\n'
    rstr +=  'TableName  = ' + repr(self.TableName ) + '\n'
    rstr +=  'SaveTable  = ' + repr(self.SaveTable ) + '\n'
    rstr +=  'PhaseStr   = ' + repr(self.PhaseStr  ) + '\n'
    rstr +=  'StereoCon  = ' + repr(self.StereoCon ) + '\n'
    rstr +=  'EnergyCon  = ' + repr(self.EnergyCon ) + '\n'
    rstr +=  'HadronCon  = ' + repr(self.HadronCon)  + '\n'
    rstr +=  'TrgPatCon  = ' + repr(self.TrgPatCon)  + '\n'
    rstr +=  'ExtrasCon  = ' + repr(self.ExtrasCon ) + '\n'
    rstr +=  'NBins      = ' + repr(self.NBins     ) + '\n'
    rstr +=  'LowEn      = ' + repr(self.LowEn     ) + '\n'
    rstr +=  'HighEn     = ' + repr(self.HighEn    ) + '\n'
    rstr +=  'NumLogBins = ' + repr(self.NumLogBins) + '\n'
    rstr +=  'CutsFile   = ' + repr(self.CutsFile  ) + '\n'
    rstr +=  'OnRegions  = ' + repr(self.OnRegions ) + '\n'
    rstr +=  'OffRegion  = ' + repr(self.OffRegion ) + '\n'
    rstr +=  'PrintStats = ' + repr(self.PrintStats) + '\n'
    rstr +=  'PlotErrors = ' + repr(self.PlotErrors) + '\n'
    rstr +=  'SwitchHori = ' + repr(self.SwitchHori) + '\n'
    rstr +=  'PlotLabels = ' + repr(self.PlotLabels) + '\n'
    return rstr

  def save(self,filename):
    """Saves the configuration on a file, using __repr__"""
    with open(filename,"w") as ofile:
      ofile.write(repr(self))

  @classmethod
  def read(cls,filename):
    """Read back a configuration file"""
    try:
      cdict = {}
      with open(filename, "r") as fh:
        exec(fh.read()+"\n", cdict)
      return Config(cdict['Input']     , cdict['TableName'] , cdict['SaveTable'] ,
                    cdict['PhaseStr']  , cdict['StereoCon'] , cdict['EnergyCon'] , 
                    cdict['HadronCon'] , cdict['TrgPatCon'] , cdict['ExtrasCon'] ,

                    cdict['NBins']     ,
                    cdict['LowEn']     , cdict['HighEn']    , cdict['NumLogBins'],
                    cdict['CutsFile']  , cdict['OnRegions'] , cdict['OffRegion'] ,

                    cdict['PrintStats'], cdict['PlotErrors'], cdict['SwitchHori'],
                    cdict['PlotLabels']                                          )
    except:
      raise


#END of the Config class




class Logger(object):
  def __init__(self):
    self.terminal = sys.stdout
    self.log = open("logfile.log", "a")

  def write(self, message):
    self.terminal.write(message)
    self.terminal.flush()
    self.log.write(message)
    self.log.flush()

  def flush(self):
    #this flush method is needed for python 3 compatibility.
    #this handles the flush command by doing nothing.
    #you might want to specify some extra behavior here.
    pass


################################################################################

def GetCrossCorrelation(ph1,ph2):
    """Compute the cross correlation among two Phaseogram objects.
       The phaseogram must have exactly the same shape. Returns a
       numpy ndarray with the x-axis and y-axis of the histogram
       storing the cyclic discrete cross-correlation.
    """
    if not ( (isinstance(ph1,Phaseogram)) or (isinstance(ph2,Phaseogram)) ):
      errstr = 'Inputs must be Phaseogram objects!'
      raise IOError(errstr)
    
    if len(ph1.x) != len(ph2.x):
      errstr = 'Phaseograms must have the same number of bins to cross correlate'
      raise IOError(errstr)
    
    else:
      cross_y = ifft(fft(ph1.y) * fft(ph2.y).conj() ).real
      #cross_x = abs(ph1.x -1 - .5/len(ph1.x))
      cross_x = ph1.x-(ph1.x[1]-ph1.x[0])/2
      return np.array([cross_x,cross_y])


################################################################################


###############################
## Reading the data          ##
###############################

def ReadMAGICData(Input     = None,
                  PhaseStr  = 'MPhase2_1.fPhase',
                  StereoCon = 'MStereoParDisp',
                  EnergyCon = 'MEnergyEst',
                  HadronCon = 'MHadronness',
                  TrgPatCon = 'MTriggerPattern_2',
                  ExtrasCon = None,
                  TableName = None,
                  SaveTable = False,
                  config    = None ):
  """Reading MAGIC data following the config object. Gives as output
     an astropy Table with the data corresponding to energy, phase,
     hadronness, theta2 and trigger pattern. Add more if needed.
  """


  if config:
    Input     = config.Input    
    PhaseStr  = config.PhaseStr 
    StereoCon = config.StereoCon
    EnergyCon = config.EnergyCon
    HadronCon = config.HadronCon
    TrgPatCon = config.TrgPatCon
    ExtrasCon = config.ExtrasCon
    TableName = config.TableName
    SaveTable = config.SaveTable

  if not Input and not TableName:
    errstr = 'Calling without Input or TableName'
    raise IOError(errstr)

  if TableName and not SaveTable:
    if ExtrasCon is None:
      r = Reader.Reader(TableName,Format='fits')
    else:
      r = Reader.Reader(TableName,Format='fits',Contents=ExtrasCon)
      
    return r.Data

  containers = []

  if EnergyCon: 
    containers.append( ('ENERGY'    ,  EnergyCon+'.fEnergy') )
  if PhaseStr:
    containers.append( ('PHASE'     ,  PhaseStr            ) )
  if StereoCon:
    containers.append( ('THETA2'    ,  StereoCon+'.fTheta2') )
  if HadronCon:
    containers.append( ('HADRONNESS',  HadronCon+'.fHadronness') )
  if TrgPatCon:
    containers.append( ('TRIGPAT'   ,  TrgPatCon+'.fPrescaled') )

  if ExtrasCon:
    for extra in ExtrasCon:
       containers.append(extra)

  if containers == []:
    errstr = 'Not reading a table and no containers given.'
    raise IOError(errstr)

  containers = tuple(containers)
  
  r = Reader.Reader(Input,Contents=containers,Format='root')
  if SaveTable:
    r.saveFits(TableName)

  return r.Data




###############################
## Cuts and Analysis         ##
###############################

def CutMAGICDataSimple(data,config,name='MAGIC'):
  """Process MAGIC data loaded in the 'data' astropy table and using
     the settings defined in the 'config' object. Gives 'name' with
     incremental numbers as name to the phaseograms.
     Returns an array of phaseogram objects, one per logarithmic
     energy bin (not the same binning as the one of the cuts).
  """

  Cuts = GCuts.GCuts(config.CutsFile)
  if not Cuts.GetInitState():
     sys.stderr.write('WARNING: Cuts are undefined! No cut will be applied! \n')
     return False

  if 'ENERGY' not in data.columns:
     sys.stderr.write('WARNING: Data has no energy! No cut will be applied! \n')
     return False

  Cuts.PrintCuts()
  ##### Removing events outside the cuts energies:
  idata = data[(data['ENERGY'] >= Cuts.GetMinEnergy()) & (data['ENERGY'] < Cuts.GetMaxEnergy())]
  ##### Removing events with the wrong trigger pattern:
  if 'TRIGPAT' in idata.columns:
    idata = idata[idata['TRIGPAT'] == 128]
  ##### Removing events with undefined theta2 (IMPORTANT!):
  idata = idata[idata['THETA2'] >= 0 ] 
 
  if Cuts.GetInitState():
     thecuts,hadcuts = Cuts.MultiCutsNew(idata['ENERGY'])
     idata = idata[(idata['THETA2'] < thecuts) & (idata['HADRONNESS'] < hadcuts)]
  return idata


def ProcessMAGICData(data,config,name='MAGIC',multi=2,keepdata=False):
  """Process MAGIC data loaded in the 'data' astropy table and using
     the settings defined in the 'config' object. Gives 'name' with
     incremental numbers as name to the phaseograms.
     Returns an array of phaseogram objects, one per logarithmic
     energy bin (not the same binning as the one of the cuts). If
     the argument 'keepdata' is set to True it returns also a list
     of the data that passed the cuts.
  """

  Cuts = GCuts.GCuts(config.CutsFile)
  if not Cuts.GetInitState():
     sys.stderr.write('WARNING: Cuts are undefined! No cut will be applied! \n')

  HasEnergy = True
  if 'ENERGY' not in data.columns:
     sys.stderr.write('WARNING: Data has no energy! No cut will be applied! \n')
     HasEnergy = False

  if Cuts.GetInitState() and HasEnergy:
     Cuts.PrintCuts()
     ##### Removing events outside the cuts energies:
     data = data[(data['ENERGY'] >= Cuts.GetMinEnergy()) & (data['ENERGY'] < Cuts.GetMaxEnergy())]
     ##### Removing events with the wrong trigger pattern:
     if 'TRIGPAT' in data.columns:
       data = data[data['TRIGPAT'] == 128]
     ##### Removing events with undefined theta2 (IMPORTANT!):
     data = data[data['THETA2'] >= 0 ] 

  if not np.isinf(np.float(config.HighEn)): 
    enbins  = np.logspace(np.log10(config.LowEn),np.log10(config.HighEn),config.NumLogBins+1)
  elif config.LowEn>=0:
    enbins  = np.array((config.LowEn,np.inf))
    config.NumLogBins = 1
  else:
    enbins  = np.array((-2,np.inf))
    config.NumLogBins = 1

  if not HasEnergy:
    enbins  = np.array((-2,np.inf))
    config.NumLogBins = 1

  results = []
  oridata = []

  print("ON regions: ")
  for k,v in config.OnRegions.items():
    print("%8s:" %k, end=' ')
    for r in v:
      print("   ",r, end=' ')
    print()
  print("OFF region: ")
  print("%8s:" %'OFF', end=' ')
  for r in config.OffRegion:
    print("   ",r)
  print()
  
  for i in range(config.NumLogBins):
    #For each display energy bin, we slice the data with the bin limits
    if HasEnergy:
      idata = data[(data['ENERGY'] >= enbins[i]) & (data['ENERGY'] < enbins[i+1])]
    else:
      idata = data
    nevents_before = len(idata)
    #Computing and applying theta2 and hadronness cuts
    HasTh2Had = ('THETA2' in idata.columns) and ('HADRONNESS' in idata.columns)
    if HasEnergy:
      if not HasTh2Had:
        sys.stderr.write('WARNING: Data has no Theta2 or Hadronness! No cut will be applied! \n')
      if Cuts.GetInitState() and HasTh2Had:
        thecuts,hadcuts = Cuts.MultiCutsNew(idata['ENERGY'])
        idata = idata[(idata['THETA2'] < thecuts) & (idata['HADRONNESS'] < hadcuts)]
    nevents = len(idata)
  
    phaseogram = Phaseogram(idata,config.NBins,regions=config,multi=multi)

    #We register these information in the phaseogram
    phaseogram.nevents_before = nevents_before
    phaseogram.energy_range = (enbins[i],enbins[i+1])
    phaseogram.name = '%s-%02d' %(name,i)
    phaseogram.title = name
    phaseogram.color = '#0044cc'
 
    results += [phaseogram]
    print(phaseogram.GetPrintString())
    if keepdata:
      oridata += [idata]
  
  if len(results)==1:
    results = results[0]
    if(keepdata):
      oridata = oridata[0]
  
  if keepdata:
    return results, oridata
  else:
    return results





###############################
## Display and Graphics      ##
###############################


def PlotPhaseograms(results,outputfile='',PrintStats = True, PlotErrors = False, SwitchHori = False, PlotLabels=True,config=None):
  """Display the phaseograms on the screen with Matplotlib and saves
     a pdf file with the results. Takes as input an array of
     phaseogram objects and some options.
  """

  if not hasattr(results,'__getitem__'):
    results = [results]

  NumPlotBins = len(results)

  if config is not None:
    PrintStats = config.PrintStats
    PlotErrors = config.PlotErrors
    SwitchHori = config.SwitchHori
    PlotLabels = config.PlotLabels

  #Searching the best layout with Young's tableaux
  rows,columns = (100,100)
  for tableau in genTableaux(NumPlotBins):
    x1 = len(tableau)
    y1 = np.max(tableau)
    area = x1*y1
    semiperi = x1+y1
    oldarea = rows*columns
    oldsemiperi = rows+columns
    if(semiperi < oldsemiperi):
      rows=x1
      columns=y1
    elif(semiperi == oldsemiperi):
      if(area < oldarea):
        rows=x1
        columns=y1

  if SwitchHori:
    (rows,columns) = (columns,rows)

  if(PrintStats):
    #9/6 is the aspect ratio taking into account extra space
    aspectratio=float(columns)/rows*9.0/5.0
  else:
    #7/5 is the aspect ratio of a single phaseogram
    aspectratio=float(columns)/rows*7.0/5.0

  basewidth = 9
  if(NumPlotBins > 2):
    basewidth = 12
  if(SwitchHori):
    basewidth = 15

  if(aspectratio>=1):
    xwidth  = basewidth
    yheight = xwidth/aspectratio
  else:
    yheight = 12
    xwidth = yheight*aspectratio

  #Generating the parameters for the grid. This needs some explanation.
  #Arranging plots in matplotlib is incredibly difficult. It looks that
  #the developers think that for no reason an user should be willing to
  #modify what they provided. For instance, it's impossible to create a
  #fixed size text box and let the text in it be rescaled automatically
  #with the dimension of the box. They provided plt.tight_layout() and
  #think that it is OK with everything, but in this case it fails
  #miserably.
  #Here we cope with this thing. If we want to plot stat boxes, we'll
  #create a grid with three columns per phaseogram:
  #  ___________________
  # |           |   |   |
  # |     1     | 2 | 3 | 
  # |___________|___|___|
  #
  # The first axis 1 contains the phaseogram. The second axis the stat
  # box, whereas the third is just a faxe axis which is there just to
  # add an extra margin on the right side. Unfortunately, it almost
  # impossible to modify the margins of the individual subplots, so
  # this was the only feasible solution. The last column of histograms
  # doesn't need the extra pad, so after generating the widths for the
  # grid we remove the last column.
  #
  # Moreover, since the text of the titles and labels generally don't
  # scale with the dimensions, the pad among the subplots gets tighter
  # and tighter as the number of plots increase. Because of this, we
  # manually rescale the pad among subplots with the number of columns
  # and rows. The base pad has been chosen by trial and error...

  xwidths=[]
  if(PrintStats):
    xwidths += ([7.071,1.4142,1.4142])*columns
    xwidths = xwidths[:-1]
    wspace = 0.05*columns
    hspace = 0.08*rows
    nmulti = 3
  else:
    xwidths += ([1])*columns
    wspace = 0.12*columns
    hspace = 0.08*rows
    nmulti = 1
  nmulticol = (nmulti*columns)-(nmulti>1)

  #... and here comes part two. If the phaseogram has a really large
  #number of counts on the y axis, its labels (which don't scale
  #because Matplotlib developers are truly 'oi d'un can) will
  #eventually get so large compared to the plot that they will overrun
  #the space of the third column and get either on top of the stat box
  #or on top of the plot itself. Enlarging the buffer column is not a
  #good option, because  it shrinks further the space for the plots.
  #Therefore, we do two things: we "read" how many decimal positions
  #the label needs and compare them with the number of columns. If a
  #certain condition happens, we set the labels to be smaller. If it
  #is even worse, we set them to be even smaller and rotate them so
  #that they take less space...

  yaxmax = 0.001
  for iph in results:
    yaxmax = (iph.y.max() > yaxmax) and iph.y.max() or yaxmax
  yaxmax = np.ceil(np.log10(yaxmax))

  rotation = 0
  ylabtoobig = False
  if(yaxmax*(columns+1+int(not(PrintStats))) > 15):
    ylabtoobig = {'fontsize': 'x-small', 'horizontalalignment': 'right'}
  if(yaxmax*(columns+1+int(not(PrintStats))) > 23):
    ylabtoobig = {'fontsize': 'xx-small', 'horizontalalignment': 'right'}
    rotation = PrintStats and -60 or -45

  #print yaxmax,columns+1+int(not(PrintStats))

  yaxmax += 4

  #... finally. The ylabel "Counts" also doesn't scale with the size.
  #So we are forced to determine somehow a padding to the left to
  #avoid that it gets outside of the plot. The four more counts in
  #yaxmax are to account for the " space Counts" label of the y-axis.

  if(PrintStats):
    yaxmax = yaxmax*(4./3.)/100.
  else:
    yaxmax = yaxmax*(5./4.)/100.

  gs_kw = dict(width_ratios=xwidths,wspace=wspace,hspace=hspace,left=yaxmax,right=0.95,top=0.95,bottom=0.09)

  #Generating figures
  fig, axs = plt.subplots(ncols=nmulticol, nrows=rows, figsize=(xwidth,yheight), constrained_layout=False,gridspec_kw=gs_kw,squeeze=False,sharex=True)
 
  for i,ph in enumerate(results):
    
    #By default it orders phaseograms horizontally first
    #and then vertically. Uncomment to switch behaviour.
    r = i//columns  #r = i%rows
    c = i%columns  #c = i/rows

    if(PrintStats):
      ph.DrawPhaseogram(axs[r,3*c],PlotErrors=PlotErrors,PlotLabels=PlotLabels)
      if(ylabtoobig):
        labels = ((axs[r,3*c]).get_yticks()).astype(int).tolist()
        #rotation_mode ensured the label sticks to the tick and doesn't rotate away from it
        (axs[r,3*c]).set_yticklabels(labels, fontdict=ylabtoobig, rotation=rotation, rotation_mode='anchor')
      #This is the axis on which we'll paint the statistics box.
      axs[r,3*c+1].set_axis_off()
      ph.DrawStatBox(fig,axs[r,3*c+1])
      #Check that we are not in the last column. If not, clear the buffer column from the axes.
      if(3*c+2<nmulticol):
       axs[r,3*c+2].set_axis_off()
    else:
      ph.DrawPhaseogram(axs[r,c],PlotErrors=PlotErrors,PlotLabels=PlotLabels)
      if(ylabtoobig):
        labels = ((axs[r,c]).get_yticks()).astype(int).tolist()
        (axs[r,c]).set_yticklabels(labels, fontdict=ylabtoobig, rotation=rotation, rotation_mode='anchor')

  #This sets unused axes to be blank.
  for i in np.arange(len(results),columns*rows):
    r = i//columns  #r = i%rows
    c = i%columns  #c = i/rows
    if(PrintStats):
      axs[r,3*c].set_axis_off()
      axs[r,3*c+1].set_axis_off()
      if(3*c+2<nmulticol):
        axs[r,3*c+2].set_axis_off()
     
    else:
      axs[r,c].set_axis_off()

  plt.show()
  if(outputfile!=''):
    print("Saving %s..." %(outputfile))
    fig.savefig(outputfile)
  else:
    if(NumPlotBins>2):
      print("Saving %s-multi.pdf..." %(results[0].name))
      fig.savefig("%s-multi.pdf" %(results[0].name))
    else:
      print("Saving %s.pdf..." %(results[0].name))
      fig.savefig("%s.pdf" %(results[0].name))


def Crab():

   print('''
                             #          .    .                                  
                                                                           .    
                      %    *        .   .                                       
                                  ..   ....          .,.. ...  .                
                                . .,*... ,......,.,...,,*,....,,,... .          
                              ,. .  / ,*,**./*,**/*/.,(*,.,**(*/*,,* * *        
                              .*  ...,//*, ,/**/**&/*##/*//(%(%(//(//.          
                            ,*,/*,*/**/*#,,//*#%(#(%/(#*#&****,//%*/*,*(..      
                       . ,,.,,/**(*//#%(##&###%(/##**//#*/#//*,*#%((,(/,*,.     
                   .,.,,,,.(/#*##(/*//(#&&##**(/(#///(////*//*(*//((/,*,**..    
                  .**,(./*/,((%%&&%%#(@#*((((//((/(##/#(/((/(%@(/##(,,,*,,...   
               .  ...(%*///(*%%&%&%(*//&//(((%(((/////(//(#/&#***%((/*,,,,*.    
                .,..,#%#*(&((/(%/#(*(%(##/((((((#((/////(**/#((/%*%#(**(*,..    
               ,,////(/***/(%*(#*///(/(#(/((//(((((((///****/@(#/*/&**...,      
       *...*/*,,/*/(##(//((#%#/*////(#(/((/(//(#((((////**/***(#/(&*/,,*,,.     
       . ,/,.,*//(((##/#(((%%&@@//////#(//((((((((%(////*(/&&%(*/(///*,..  .    
       .,,/**(.*///(.  *##&&&&%%%&@%&/###%((((((#(#%%%&&&&%%/(&&#((**./....     
       *,.,**/#(#... ,**. ,/#(/(#%%#/(#((//(#((%%@@&@&@&(//%/%(##(//**..        
     ..//*(//%,..,,,(,*.,//(/,,*##&((((((#(((((#@@%@&%%(&%#/((/(,*.,*,.....     
    .,#,*.*(%#**,/&/*,,*,/,,**,**&%/((#/((((((((/((/*(/,/%&#%.,,,,**, . ..      
     *//,(*,(#/((*,//*(**,***(**/(#///((((((#(((((/(/#/***&&(,%*/ . .           
     ,*&#((%%(%***###/#***#&/**/(((@((//////*(@%%/#(/,,/*.*#//**.,,,...    .    
   . .,,/(//(/##((*/#(/#,*/,*(*,*/%#(***////&(&(/#&%#,*/%*////*,*,....          
  ,.  *..*,*(,/%%#@,,(#%/(//#(/*,(##&#(/%#%%*#(&(/#%#%##//**,/*,,**..           
  , ,, .  .,,**.(.*%#,(%%&(**,#.*.*(%/####/(&#,/(*,/***/,,.**,.,... .*          
  *...../   .,./,*/#%#%%#**#***/(/#(,*//##(&***//,,/,,,*,,../..,,.  .           
   .       .   ,,(#(#(/..* ,,(//(./,,.*./**(/,,,.,,..     .   .                 
         ,#  .. ,*,*,,/.../*/(*/*/%#(*,/,.*/**/,**. .                           
                   , ...,**((,//,/(**  ..,,   ,        #                        
                      /...,  ..    ... ,.                                       
                                                                                
                                                                                
''')

################################################################################

###############################
## Standalone mode           ##
###############################

#This is executed if the program is called standalone.
if __name__ == '__main__':
  if(LogOnFile):
    sys.stdout = Logger()

  if len(sys.argv)>1:
    config = Config.read(sys.argv[1])
    print('Reading configuration from:',sys.argv[1])
  else:
    config = Config(Input     , TableName , SaveTable ,
                    PhaseStr  , StereoCon , EnergyCon , 
                    HadronCon , TrgPatCon , ExtrasCon ,

                    NBins     ,
                    LowEn     , HighEn    , NumLogBins,
                    CutsFile  , OnRegions , OffRegion ,

                    PrintStats, PlotErrors, SwitchHori,
                    PlotLabels                        )
  print(config)

  data = ReadMAGICData(config=config)
  phs  = ProcessMAGICData(data,config)
  PlotPhaseograms(phs,config=config)
