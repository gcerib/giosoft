#!/bin/bash

test x$1 = x$'\x00' && shift || { set -o pipefail ; ( exec 2>&1 ; $0 $'\x00' "$@" ) | tee tempo2_install.log ; exit $? ; }

unset TEMPO2
echo -e '\e[44m  (1)  DOWNLOAD TEMPO2...  \e[49m'
git clone https://bitbucket.org/psrsoft/tempo2.git
cd tempo2
sleep 3
echo -e '\e[44m  (2)  PATCHING GTEST...   \e[49m'
cd tests/gtest-1.7.0/include/gtest/internal
wget -nc https://www.mpp.mpg.de/~ceribell/gtest-port_disable.patch
patch -R < gtest-port_disable.patch
cd ../../../../../
sleep 3
echo -e '\e[44m  (3)  PATCHING INITIALISE.C...   \e[49m'
wget -nc https://www.mpp.mpg.de/~ceribell/initialize.C_400.patch
patch < initialize.C_400.patch
sleep 3
echo -e '\e[44m  (4)  CHECKING GCC VERSION...   \e[49m'
which g++-5;
if [ "$?" -eq "0" ]
 then
  export CC=gcc-5
  export CXX=g++-5
 else
  echo -e '... \e[o43mWARNING: G++-5 NOT FOUND, COMPILATION MIGHT FAIL...\e[49m'
fi
sleep 3
echo -e '\e[44m  (5)  CONFIGURE...  \e[49m'
cd T2runtime/
export TEMPO2=$(pwd)
cd ..
./bootstrap
sleep 3 
./configure
sleep 3
echo -e '\e[44m  (6)  BUILDING...  \e[49m'
make -j8 && make install && make -j8 plugins && make plugins-install
sleep 10
cd plugin/
echo -e '\e[44m  (7)  BUILDING MAGIC PLUGIN...  \e[49m'
wget --user MAGIC --ask-password http://wiki.magic.pic.es/images/b/be/Magic_plug.C
wget --user MAGIC --ask-password http://wiki.magic.pic.es/images/7/74/Build_magic_plug.sh -O build_magic_plug.sh
if [ "$CXX" == "g++-5" ]
 then
  awk '{gsub("g\\+\\+","g\\+\\+-5");print}' build_magic_plug.sh > tmp; mv tmp build_magic_plug.sh;
  echo -e '... \e[42mINFO: USING G++-5.\e[49m'
fi
bash build_magic_plug.sh
sleep 5
echo -e '\e[44m  (8)  CHECK MAGIC PLUGIN...  \e[49m'
$TEMPO2/bin/tempo2 -H
cd ..
cd T2runtime/
echo -e '\e[44m  (9)  ADDING MAGIC TELESCOPES...  \e[49m'
cat <<- EOF | tee -a observatory/observatories.dat
	# MAGIC-1 (by Gianluca Giavitto)
	5326878.7967 -1719509.5201  3051884.5175 MG1 mg1 UTC(GPS)  
	# MAGIC-2 (by Gianluca Giavitto)
	5326878.7967 -1719509.5201  3051884.5175 MG2 mg2 UTC(GPS)
	EOF
cd earth/
echo -e '\e[44m  (10)  UPDATING EARTH POSITION...  \e[49m'
#./update_eop.sh
echo "before:"
tail -n 3 eopc04_IAU2000.62-now
wget -N https://hpiers.obspm.fr/iers/eop/eopc04/eopc04_IAU2000.62-now
echo ""
echo "after:"
tail -n 3 eopc04_IAU2000.62-now
echo ""
sleep 5
cd ..
cd clock/
echo -e '\e[44m  (11)  DOWNLOADING CLOCK CORRECTION UPDATER...  \e[49m'
wget https://www.mpp.mpg.de/~ceribell/tempo2_auxiliary.tar.gz
tar xzf tempo2_auxiliary.tar.gz 
cd tempo2_auxiliary/
echo -e '\e[44m  (12)  PATCHING CLOCK CORRECTION UPDATER...  \e[49m'
sed -i '1i#include <cstring>' ClockConversion.C
sed -i '1i#include <cstring>' update_clkcorr.C 
echo -e '\e[44m  (13) BUILDING CLOCK CORRECTION UPDATER...  \e[49m'
make
sleep 10
mkdir circularT
cd circularT/
echo -e '\e[44m  (14) DOWNLOADING CLOCK CORRECTIONS...  \e[49m'
wget -N ftp://ftp2.bipm.org/pub/tai/publication/cirt/*
cd ..
echo -e '\e[44m  (15) UPDATING CLOCK CORRECTIONS...  \e[49m'
./update_clkcorr -t gpsutc circularT/cirt.*
cd ..
cp gps2utc.clk gps2utc.clk.old
mv gps2utc.clk.new gps2utc.clk
echo -e '\e[44m  (16) SCRIPT TERMINATED!  \e[49m'
