#!/bin/bash
#PLUGIN_SRC=magic_plug2.C                         
PLUGIN_SRC=Magic_plug2.C
ROOTINC=`root-config --incdir`
ROOTLIB=`root-config --libdir`
#CXXFLAGS="-g -O -I${ROOTINC} -I${MARSSYS}"
# MLM: I added I${TEMPO2}/include/ to look for TKlongdouble.h search by tempo2.h
CXXFLAGS="-g -O -I${ROOTINC} -I${MARSSYS} -I${TEMPO2}/include/"
LDFLAGS="-g -L${ROOTLIB} -L${MARSSYS} -L${MARSSYS}/lib -lmars -lCore -lCint -lHist -lGraf -lGraf3d -lGpad -lTree -lRint -lPostscript -lMatrix -lPhysics -pthread -lm -ldl -rdynamic"

MARSSUBDIR_PATHS="${MARSSYS}/manalysis  ${MARSSYS}/mastro  ${MARSSYS}/mbadpixels  ${MARSSYS}/mbase  ${MARSSYS}/mcalib  ${MARSSYS}/mcamera  ${MARSSYS}/mcta  ${MARSSYS}/mdata  ${MARSSYS}/mdisp  ${MARSSYS}/mfbase  ${MARSSYS}/mfileio  ${MARSSYS}/mfilter  ${MARSSYS}/mflux  ${MARSSYS}/mgeom  ${MARSSYS}/mgui  ${MARSSYS}/mhbase  ${MARSSYS}/mhcalib  ${MARSSYS}/mhflux  ${MARSSYS}/mhft  ${MARSSYS}/mhist  ${MARSSYS}/mhistmc  ${MARSSYS}/mhvstime  ${MARSSYS}/mimage  ${MARSSYS}/mjobs  ${MARSSYS}/mmain  ${MARSSYS}/mmc  ${MARSSYS}/mmccamera  ${MARSSYS}/mmontecarlo  ${MARSSYS}/mmuon  ${MARSSYS}/mpedestal  ${MARSSYS}/mpointing  ${MARSSYS}/mranforest  ${MARSSYS}/mraw  ${MARSSYS}/mreflector  ${MARSSYS}/mreport  ${MARSSYS}/msignal  ${MARSSYS}/mskyplot  ${MARSSYS}/msql  ${MARSSYS}/mstarcam  ${MARSSYS}/msupercuts  ${MARSSYS}/mtools  ${MARSSYS}/mtrigger  ${MARSSYS}/mpulsar"

for d in ${MARSSUBDIR_PATHS}
do
CXXFLAGS="${CXXFLAGS} -I${d}"
done
if [ -x $TEMPO2/plugins/magic2_${OSTYPE}_plug.t2 ]
then
rm $TEMPO2/plugins/magic2_${OSTYPE}_plug.t2
fi
g++ $CXXFLAGS -fPIC -shared -o $TEMPO2/plugins/magic2_${OSTYPE}_plug.t2 $PLUGIN_SRC $LDFLAGS
