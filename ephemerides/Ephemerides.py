#!/usr/bin/python2.7

import sys
import re
import numpy as np

import warnings

from copy import deepcopy


class Ephemeris:
  """A class for reading and manipulating .par files of TEMPO2. Supported operations
     include:

        - Moving the PEPOCH of an ephemeris keeping it the same: .transfer()
        - Moving the TZRMJD keeping it the same:                 .tzrshift()
        - Shifting all resulting phases by a constant value:     .shift()
        - Getting the phase and the frequencies at some time:    .__call__()
        - Reading and saving .par files.
        - Dealing with the FITWAVES sinusoids.
        - Segmenting an ephemeris, converting it into a cubic spline.

     If the TZRSITE parameter of the ephemeris is different from '@' or 'BAT',
     operations that include a change in frequency or in phase (with the exeption of
     the .transfer() method) will also result in a constant shift of the ephemeris.
     You can change TZRSITE to '@' without losing the accuracy of the ephemeris, but
     if you do so a global phase shift will result. So in most situations if you
     have to update an ephemeris it is better to set its TZRSITE to '@', then do
     your job on it and realign it at the end. If not, each time interval that you
     process will have a different PHI0 and you'll have to realign each of them
     singularly at the end.

     Methods with a capitalized name return a new ephemeris, the ones with small
     caps act on the object itself instead.
  """

  def __init__(self,filename=None):
    """Ephemeris(self,filename=None), takes a filename of
       a .par file to read or creates an empty object.
    """

    #Array containing the keys, preserving
    #the order of the original file. A comment
    #is indicated by a '#N' key with N the line
    #number the comment is at. Removing a key
    #element will prevent it to be saved to a
    #file or printed, even if the corresponding
    #content is preserved.
    self.keys    = []

    #Dictionary with the elements of 'keys' as
    #keys (really?!) and a list with the tokens
    #on the line corresponding to the key as
    #value. So eph.content['F0'][0] is the
    #string with the frequency and
    #eph.content['F0'][2] is the string with the
    #frequency error (if any).
    self.content = {}

    #Comments are saved with a single string in a
    #dictionary with ckeys ("#N") as keys.
    self.comment = {}

    #The Epoch of the ephemeris.
    self.PEPOCH  = np.float128(0)

    #Frequencies
    self.F       = []

    #TZR stuff
    self.TZRSITE = ''
    self.TZRMJD  = np.float128(0)
    self.TZRFRQ  = np.float128(0)

    #START and FINISH
    self.START  = -1
    self.FINISH = -1

    #WAVES, if any. WAVE[0] is the foundamental
    #frequency.
    self.WAVE    = []

    #GLEP_*,GLPH_*,GLF0_*,GLF1_*,GLF2_*
    self.GLEP  = []
    self.GLPH  = []
    self.GLF0  = []
    self.GLF1  = []
    self.GLF2  = []
    self.GLTD  = []
    self.GLF0D = []

    #A name
    self.name    = ''

    #Whether the ephemeris is segmented or not
    self.segmented = False

    #Initializing constructor
    if filename:
      #Read the file into keys, content and comment
      with open(filename,'r') as input_file:
        self.name = filename
        nline = 0
        for line in input_file:
          #The first line is 1, we put it here
          #istead of the end of the cycle.
          nline += 1
          line = line.strip('\n')
          elem = line.split() #Tokens
          if len(elem)==0:
            continue
          if elem[0][0] == '#':
            ckey = "#%d" % nline
            self.keys.append(ckey)
            self.comment[ckey] = line
            continue
          self.keys.append(elem[0])
          self.content[elem[0]] = elem[1:]

      #Fill the frequency array and other variable objects.
      #For the frequencies we first put them as (N,val_F0)
      #to reorder them later and convert to a simple list.
      for k,v in self.content.items():
        #I hope nobody ever needs F100
        if   re.match("F\d+",k)   :
          self.F.append((np.uint8(k[1:]),np.float128(v[0])))
        elif re.match("PEPOCH",k) :
          self.PEPOCH = np.float128(v[0])
        elif re.match("TZRMJD",k) :
          self.TZRMJD = np.float128(v[0])
        elif re.match("START",k) :
            self.START = np.float128(v[0])
        elif re.match("FINISH",k) :
            self.FINISH = np.float128(v[0])
        elif re.match("TZRFRQ",k) :
          self.TZRFRQ = np.float128(v[0])
        elif re.match("TZRSITE",k) :
          self.TZRSITE = v[0]
        #Waves have two amplitudes, so the syntax is even worse.
        elif re.match("WAVE_FREQ",k) or re.match("WAVE_OM",k)  :
          self.WAVE.append((np.uint8(0),(np.float128(v[0]),np.float128(v[1]))))
        elif re.match("WAVE\d+",k)   :
          self.WAVE.append(  (np.uint8(k[4:]) , ( np.float128(v[0]) , np.float128(v[1]) ) ) )
        elif re.match("GLEP_\d+",k)   :
          self.GLEP.append((np.uint8(k[5:]),np.float128(v[0])))
        elif re.match("GLPH_\d+",k)   :
          self.GLPH.append((np.uint8(k[5:]),np.float128(v[0])))
        elif re.match("GLF0_\d+",k)   :
          self.GLF0.append((np.uint8(k[5:]),np.float128(v[0])))
        elif re.match("GLF1_\d+",k)   :
          self.GLF1.append((np.uint8(k[5:]),np.float128(v[0])))
        elif re.match("GLF2_\d+",k)   :
          self.GLF2.append((np.uint8(k[5:]),np.float128(v[0])))
        elif re.match("GLTD_\d+",k)   :
          self.GLTD.append((np.uint8(k[5:]),np.float128(v[0])))
        elif re.match("GLF0D_\d+",k)   :
          self.GLF0D.append((np.uint8(k[6:]),np.float128(v[0])))


      #Crazy array manipulation to make an ordered F[0] = val_0, F[1]= val_1,...
      self.F = np.sort(np.asarray(self.F,dtype=[('order',np.float128),('value',np.float128)]),order='order')['value']
      self.F = np.array(self.F)

      #Even crazier array manipulation to to the same with waves.
      self.WAVE = np.sort(    np.asarray(   self.WAVE,dtype=[('order',np.float128),('value',(np.float128, (2,)))]   ),order='order'    )['value']

      #The same for glitches
      self.GLEP  = np.sort(np.asarray(self.GLEP,dtype=[('order',np.float128),('value',np.float128)]),order='order')['value'].tolist()
      self.GLPH  = np.sort(np.asarray(self.GLPH,dtype=[('order',np.float128),('value',np.float128)]),order='order')['value'].tolist()
      self.GLF0  = np.sort(np.asarray(self.GLF0,dtype=[('order',np.float128),('value',np.float128)]),order='order')['value'].tolist()
      self.GLF1  = np.sort(np.asarray(self.GLF1,dtype=[('order',np.float128),('value',np.float128)]),order='order')['value'].tolist()
      self.GLF2  = np.sort(np.asarray(self.GLF2,dtype=[('order',np.float128),('value',np.float128)]),order='order')['value'].tolist()
      self.GLTD  = np.sort(np.asarray(self.GLTD,dtype=[('order',np.float128),('value',np.float128)]),order='order')['value'].tolist()
      self.GLF0D = np.sort(np.asarray(self.GLF0D,dtype=[('order',np.float128),('value',np.float128)]),order='order')['value'].tolist()

      if len(self.GLPH) < len(self.GLEP):
        self.GLPH = np.concatenate([self.GLPH,np.zeros(len(self.GLEP)-len(self.GLPH))])
      if len(self.GLF0) < len(self.GLEP):
        self.GLF0 = np.concatenate([self.GLF0,np.zeros(len(self.GLEP)-len(self.GLF0))])
      if len(self.GLF1) < len(self.GLEP):
        self.GLF1 = np.concatenate([self.GLF1,np.zeros(len(self.GLEP)-len(self.GLF1))])
      if len(self.GLF2) < len(self.GLEP):
        self.GLF2 = np.concatenate([self.GLF2,np.zeros(len(self.GLEP)-len(self.GLF2))])
      if len(self.GLTD) < len(self.GLEP):
        self.GLTD = np.concatenate([self.GLTD,np.zeros(len(self.GLEP)-len(self.GLTD))])
      if len(self.GLF0D) < len(self.GLEP):
        self.GLF0D = np.concatenate([self.GLF0D,np.zeros(len(self.GLEP)-len(self.GLF0D))])

    #Ensures that waves are not changed by translating the ephemeris
    #by adding WAVEEPOCH if it was not specified.
    if ( ("WAVE_FREQ" in self.keys) or ("WAVE_OM" in self.keys) ) and ("WAVEEPOCH" not in self.keys):
      self.keys.append('WAVEEPOCH')
      self.content['WAVEEPOCH'] = ["%0.21g" % self.PEPOCH]


  def __getitem__(self, key):
    """If you call eph['key'] you get eph.content['key']
    """
    return self.content[key]


  def __call__(self,time,burn=False,glitch=False):
    """The ephemeris object is callable like a function! It
       returns a tuple with its fractional phase and the
       frequencies at the time. If the ephemeris has waves,
       this is NOT the same as transfer, because the waves
       are burnt into the ephemeris at the specified time.
       See burnWaves() if you don't know what it means.
    """
    time = np.float128(time)
    delta = (time - self.PEPOCH)*np.float128('86400.')
    newf = np.zeros_like(self.F,dtype=np.float128)
    for i,newfn in enumerate(newf):
      for j,oldfn in enumerate(self.F[i:]):
       newfn = newfn + oldfn*delta**(j)/np.math.factorial(j)
      newf[i]=newfn

    #If they exist, burn the waves into the ephemeris
    #at that time.
    if burn and self.WAVE.tolist() != []:
      wffund = self.WAVE[0][0]/np.float128('365.25')*2*np.pi
      wepoch = np.float128(self['WAVEEPOCH'][0])
      wdelta = np.float128(time) - wepoch
      terms = np.zeros_like(newf)
      for k,wave in enumerate(self.WAVE[1:],1):
        alpha = k*wffund*wdelta
        c1    = (wave[1]-(1j)*wave[0])/2
        c2    = (wave[1]+(1j)*wave[0])/2
        for n,freq in enumerate(newf,1):
          term  = (1j)**n * c1 * np.exp(1j*alpha) + (-1j)**n * c2 * np.exp(-1j*alpha)
          term  = np.real(term)
          term *= (wffund/np.float128('86400'))**n * k**n
          term *= self.F[0]
          terms[n-1] += np.float128(term)
      newf += terms 

    phifrac = np.float128(0)
    if (self.TZRSITE != '@') and (self.TZRSITE != 'BAT'):
      warnstr = 'TZRSITE in not \'@\' or \'BAT\', cannot calculate absolute phases.'
      warnings.warn(warnstr)
    else:
      tmp = self.copy()
      tmp.transfer(self.TZRMJD)
      TZRF = tmp.F

      #The problems come from F[0]*delta, which is, obviously,
      #an enormous number. However:
      #     f0 = floor(f0)+mod(f0)
      #  delta = time - epoch
      #   time = floor(time)  + mod(time)
      #  epoch = floor(epoch) + mod(epoch)
      #  phi   = mod(f0*delta*86400)
      #and we know that floor(..)*floor(..) is integer too,
      #so we can skip it. Moreover, as long as we deal with
      #the frequency only, everything is linear in time, so
      #we avoid multiplying by 86400 and do it at the end.
      #In principle the modulo operator is distributive but
      #it's not linear, but since 86400 is also integer in
      #this case it fortunately works.

      #After the multiplications we take just the fractional
      #parts, so that we keep the integer ones (which are not
      #interesting) away from the precious mantissae. For the
      #frequencies there is no problem, as there the delta is
      #aways multiplied by small numbers that operate on the
      #exponent, and do not fill up the mantissa.

      frac_t = np.mod(time,1)
      intg_t = np.floor(time)
      frac_e = np.mod(self.TZRMJD,1)
      intg_e = np.floor(self.TZRMJD)

      frac_f = np.mod(TZRF[0],1)
      intg_f = np.floor(TZRF[0])

      phifrac = np.mod(np.mod(intg_f*(frac_t-frac_e)+frac_f*(intg_t-intg_e)+frac_f*(frac_t-frac_e),1)*np.float128('86400'),1)

      #Now, since the other terms are non linear, we have to
      #add the second/day conversion, but these elements do
      #not scale linearly and they behave like perturbations.
      delta  = time-self.TZRMJD
      delta *= np.float128('86400')
      arg = delta*delta
      for j,fn in enumerate(TZRF[1:],1):
        phifrac += (fn*arg/np.float128(np.math.factorial(j+1)))
        arg *= delta

      #Since we distributed the modulo, the result is not
      #ensured to be fractional. We do it here.
      phifrac = np.mod(phifrac,1)
    
    if glitch:
      gldd  = np.array([(time - igl) for igl in self.GLEP],dtype=np.float128)
      gldt  = gldd*np.float128('86400.')
      for glddi,gldti,glph,glf0,glf1,glf2,gltd,glf0d \
                    in zip(gldd,gldt,self.GLPH,self.GLF0,self.GLF1,self.GLF2,self.GLTD,self.GLF0D):
           if gldti >= 0:
             phifrac   +=           glph
             phifrac   +=           glf0*gldti
             phifrac   +=    0.5  * glf1*gldti*gldti
             phifrac   +=    1./6 * glf2*gldti*gldti*gldti
             if gltd != 0:
               phifrac +=   glf0d * gltd * np.float128('86400.0') * ( 1.0 - np.exp(-glddi/gltd) )

    phifrac = np.mod(phifrac,1)
      
    return phifrac,newf
      


  def __repr__(self):
    """The representation of the ephemeris. We re-build the text
       file as it was, but replacing the values which we may have
       changed. Notice the loop on self.keys: if you delete 'F0'
       from there your ephemeris will not have F0 anymore even if
       eph.F[0] still exists and is alive!! This allows you to
       turn off or add features just by appending keys to self.keys
       and content=[token1,token2] to self.content.
    """
    reprstr = ''
    for i,k in enumerate(self.keys):
      if k[0] == '#':
        #We reinsert the comment exactly where it was.
        reprstr += self.comment[k]
      else:
        #We insert the key.
        reprstr += k
        #Now we process variable content. The index
        #j stores the position of the added token.
        #If the variable token is unique, we interrupt
        #the cycle and continue on the next key, other-
        #wise at the end we add the remaining tokens.
        j = 0
        if k == 'PEPOCH':
          reprstr += "        %0.21g\n"       %self.PEPOCH
          continue
        if k == 'TZRMJD':
          reprstr += "        %0.21g\n"       %self.TZRMJD
          continue
        if k == 'TZRFRQ':
          reprstr += "        %0.21g\n"       %self.TZRFRQ
          continue
        if k == 'TZRSITE':
          reprstr += "        %s\n"           %self.TZRSITE
          continue
        if k == 'START':
          reprstr += "        %0.21g\n"       %self.START
          continue
        if k == 'FINISH':
          reprstr += "        %0.21g\n"       %self.FINISH
          continue
        if k == 'WAVE_FREQ' or k == 'WAVE_OM':
          reprstr += "        %.12e        %.12e\n"    %tuple(self.WAVE[0])
          continue
        if re.match("WAVE\d+",k):
          nwave = int(k[4:])
          reprstr += "        %.12e        %.12e\n"    %tuple(self.WAVE[nwave])
          continue

        #Frequencies come with the error and the fit flag.
        #For now we keep the latter two as they were originally
        if re.match("F\d+",k):
          nfreq = int(k[1:])
          reprstr += "        %.21e" %self.F[nfreq]
          #We already added the frequency value on the line
          #and should not add it again, so j=1. In this
          #way the final cycle on tokens adds back the fit
          #flag and the error
          j = 1

        #Glitch parameters
        if re.match("GLEP_\d+",k):
          nfreq = int(k[5:])
          reprstr += "        %0.12g\n" %self.GLEP[nfreq-1]
          continue
        if re.match("GLPH_\d+",k):
          nfreq = int(k[5:])
          reprstr += "        %.12e\n" %self.GLPH[nfreq-1]
          continue
        if re.match("GLF0_\d+",k):
          nfreq = int(k[5:])
          reprstr += "        %.12e\n" %self.GLF0[nfreq-1]
          continue
        if re.match("GLF1_\d+",k):
          nfreq = int(k[5:])
          reprstr += "        %.12e\n" %self.GLF1[nfreq-1]
          continue
        if re.match("GLF2_\d+",k):
          nfreq = int(k[5:])
          reprstr += "        %.12e\n" %self.GLF2[nfreq-1]
          continue
        if re.match("GLTD_\d+",k):
          nfreq = int(k[5:])
          reprstr += "        %.12e\n" %self.GLTD[nfreq-1]
          continue
        if re.match("GLF0D_\d+",k):
          nfreq = int(k[6:])
          reprstr += "        %.12e\n" %self.GLF0D[nfreq-1]
          continue

        #Now if none of the above matched, the line is immutable
        #or it has some more tokens. We add them from the raw
        #content.
        for token in self.content[k][j:]:
          reprstr = reprstr + '        ' + token
      reprstr = reprstr + '\n'
    return reprstr

  def __sub__(self,other):
    """Subtract two ephs. This is intended as a
       diagnostic tool. To do this, they are first
       reduced to the same pepoch, the one of the
       first ephem. In this case we do not want to
       use .setfreq(), as that would try to account
       for the shift, but this is meaningless when
       you subtract the ephemerides.
    """

    eph0 = self.copy()
    eph1 = other.copy()
    
    eph0.tzrshift(eph0.PEPOCH)

    eph1.transfer(eph0.PEPOCH)
    eph1.tzrshift(eph0.PEPOCH)

    minphi, minf = eph0(eph0.PEPOCH)
    subphi, subf = eph1(eph0.PEPOCH)

    df   = (minf-subf)
  
    eph0.shift(-subphi)
    eph0.F = df

    return eph0

  def savepar(self,filename):
    """Saves a par file using the __repr__ defined above.
    """
    with open(filename,'w') as ofile:
      ofile.write(repr(self))


  def copy(self):
    """Return a deepcopy of itself.
    """
    return deepcopy(self)


  def transfer(self,newpepoch):
    """This transfers the ephemeris to a new epoch by 
       adjusting the frequencies automagically.
       It operates on self, but it is reversible.
       Transferring the ephemeris does not change its
       phi0, that is determined by the TZR parameters.
       IT DOES NOT ACCOUNT FOR GLITCHES.
    """
    delta = (np.float128(newpepoch) - self.PEPOCH)*np.float128('86400.')

    newf = np.zeros_like(self.F,dtype=np.float128)
    for i,newfn in enumerate(newf):
      for j,oldfn in enumerate(self.F[i:]):
       newfn = newfn + oldfn*delta**(j)/np.math.factorial(j)
      newf[i]=newfn
    self.F = newf
    self.PEPOCH = np.float128(newpepoch)


  ##### From here on operators may use the __call__ method
  ##### themselves, so if you see something like self(...)
  ##### we are using the __call__ method defined above.



  def shift(self,dphi):
    """This is used to shift all phases by a constant,
       which, for TEMPO2, means changing the TZRMJD
       value. However TEMPO2 treats this value just as
       another TOA and applies ALL TIME CORRECTIONS to
       it as well (including the noise waves!).
       Because of this, using this function only makes
       sense if TZRSITE is "@" or "BAT" (they are
       synonims), which in TEMPO2 means: TZRMJD is
       already barycentered, don't touch it more.
    """
    dphi = np.mod(np.float128(dphi),1)
    if (self.TZRSITE != '@') and (self.TZRSITE != 'BAT'):
      errstr = 'TZRSITE is not \'@\' or \'BAT\', cannot shift the phase.'
      raise TypeError(errstr)
    #Here we do not want to account for waves, as
    #we are not burning them in. burn is false by
    #default but like this it's clearer.
    _,TZRF = self(np.float128(self.TZRMJD),burn=False)
    dt=dphi/TZRF[0]/np.float128('86400')
    #dt is tiny, so there is no need to account
    #for the change of the noise waves between the
    #old and the new TZRMJD here. Such accounting
    #is needed for larger values because of the
    #weird way tempo2 calculates phases.
    self.TZRMJD = np.float128(self.TZRMJD)-dt


  def tzrshift(self,time):
    """Translate the TZR time to 'time', adjusting it
       to have the same phi0 (so that the ephemeris
       does not get shifted in phase too). Accounting
       for the wave noise seems wrong at first, but
       it depends on the way that TEMPO2 calculates
       the phases and the reference phase. See the end
       note. It should be reversible.
    """
    time = np.float128(time)
    phi0,_ = self(time)
    if self.WAVE.tolist() != []:
      phi0  += self.noise(time)
      phi0  -= self.noise(self.TZRMJD)
    self.TZRMJD = time
    #phi1,_ = self(time) #Zero by def
    self.shift(phi0)

  
  def clip(self,n):
    """Clip the number of derivatives to F(n), so
       the resulting order of the ephemeris is n+1.
       If possible, we redefine TZRMJD close to the
       PEPOCH, so that no phase shift happens because
       of the clipping. Otherwise the phase at TZRMJD 
       may be very different from what it was before
       the clipping and this would add a constant and
       unwanted phase shift.
       This operation is DESTRUCTIVE.
    """
    phi0i,_ = self(self.PEPOCH)
    n = int(n)
    if (self.TZRSITE == '@') or (self.TZRSITE == 'BAT'):
      self.tzrshift(self.PEPOCH)
    else:
      warnstr='TZRSITE is not \'@\' or \'BAT\', a phase shift will appear.'
      warnings.warn(warnstr)
    self.F = self.F[:n+1]
    phi0f,_ = self(self.PEPOCH)
    self.shift(phi0i-phi0f)
    #Removing keys with RE hieroglyphs.
    if(n<9):
      r = re.compile("^((?!F(?:["+str(n+1)+"-9]|\d\d\d*)).)*$")
    else:
      De = int((n+1)/10)
      u  = (n+1)%10
      r = re.compile("^((?!F(["+str(De)+"-9]["+str(u)+"-9]|\d{3,})).)*$")
    self.keys = filter(r.match,self.keys)


  def setfreq(self,newF):
    """Changes the frequency and its derivatives at the
       PEPOCH in a safe way, possibly accounting for
       the phase shifts that would develop. This is the
       preferred method to set new frequencies, as
       doing self.F = newF would amost surely generate
       a shift in phase. In order for it to work, the
       ephemeris must have "@" or "BAT" as TZRSITE.
       This operation is destructive.
    """
    phi0,_ = self(self.PEPOCH)
    self.tzrshift(self.PEPOCH)
    if len(newF) > len(self.F):
      self.F = newF
    else:
      self.F[:len(newF)]=newF
    phi1,_ = self(self.PEPOCH)
    self.shift(-phi1+phi0)



  ##### From here on: functions related to FITWAVES
  ##### and the noise model only.



  def noise(self,time):
    """The noise model based on the FITWAVES parameters
       WAVE_FREQ, WAVE1 WAVE2, etc. The function returns
       the dPHI value that TEMPO2 adds at a specific time.
       WAVE[n][0] is the coefficient of sin(n*f0*t) and
       WAVE[n][1] is the one of cos(n*f0*t). WAVE_FREQ is
       in 1/year. Have a look at the note at the bottom
       of the script if you are confused.
    """
    if self.WAVE.tolist() == []:
      return 0

    wffund = self.WAVE[0][0]/np.float128('365.25')*2*np.pi
    wepoch = np.float128(self['WAVEEPOCH'][0])
    wdelta = np.float128(time) - wepoch
    dphi = 0
    for k,wave in enumerate(self.WAVE[1:],1):
      alpha = k*wffund*wdelta
      term = wave[0]*np.sin(alpha)+wave[1]*np.cos(alpha)
      #TEMPO2 multiplies this further by F0, so that
      #the amplitues of the waves are in fact seconds!
      dphi += term*self.F[0]

    return np.mod(dphi,1)

    
  def removewaves(self):
    """This disables the waves by removing them from the
       keys list and deleting the array that stores them.
       As such, this is a DESTRUCTIVE operation.
    """
    r = re.compile("^((?!WAVE).)*$")
    self.keys = filter(r.match,self.keys)
    self.WAVES = []


  def burnwaves(self):
    """This burns in the waves at the epoch and it is
       A DESTRUCTIVE OPERATION.
       Burning the waves means getting the dF0,dF1,dF2,...
       given by the waves at a specific time and adding
       them to F0,F1,F2, then removing the waves. See
       the end note on burning waves.
    """
    #The new frequencies at PEPOCH
    _,newF = self(self.PEPOCH,burn=True)
    if (self.TZRSITE == '@') or (self.TZRSITE == 'BAT'):
      #It looks that we are accounting for the noise
      #twice, first inside tzrshift and later with
      #dphiwave, but it is because of the way TEMPO2
      #calculates phases.
      self.tzrshift(self.PEPOCH)
      dphiwave=self.noise(self.PEPOCH)
      phi0,_ = self(self.PEPOCH)
      tzrnoise=self.noise(self.TZRMJD)
      #We now change the frequencies and re-evaluate
      #the phase at PEPOCH.
      self.F = newF
      self.removewaves()
      phi1,_ = self(self.PEPOCH)
      #Remove the current shift   (-phi1) = 0
      #Add the previous shift     (+phi0) = phi0
      #Add the noise shift    (+dphiwave) = phi0+dphiwave
      #Remove prev. baseline  (-tzrnoise) = phi0+dphiwave-tzrnoise
      self.shift(phi0+dphiwave-tzrnoise-phi1)
    else:
      warnstr='TZRSITE is not \'@\' or \'BAT\', a phase shift will appear.'
      warnings.warn(warnstr)
      self.F = newF
      self.removewaves()



  ##### From here on: functions related to segmented
  ##### ephemerides and their creation.



  def Segment(self,N_SEG):
    """Create a segmented ephemeris, which
       in practise is a cubic spline (with
       a C0 continuity condition) based on
       the current one. It returns the
       joint cubic spline ephemeris (the
       chimera), the segment extremes, the
       central epochs of each small segment
       and the micro-ephemerides for each
       segment.
    """
  
    DELTA = self.FINISH-self.START
    T_SEG = np.float128(DELTA)/np.float128(str(N_SEG))
    #Generating segments and central epochs
    segments = np.array([ self.START+T_SEG*np.float128(i) for i in range(N_SEG+1) ])
    segepoch = (segments[1:]+segments[:-1])/np.float128('2')
  
    #Cubic segments.
    ephs = []
    for i,epo in enumerate(segepoch):
      ephi = self.copy()
      ephi.transfer(epo)
      ephi.burnwaves()
      ephi.removewaves()
      ephi.clip(2)
      ephi.START=segments[i]
      ephi.FINISH=segments[i+1]
      posi = np.floor(np.log10(N_SEG)+1.000001)
      ephi.name = (('seg_%0'+str(posi)+'d') % i)+'_'+str(ephi.START)+'_'+str(ephi.FINISH)+'.par'
      ephs.append(ephi)
 
    #Joining them 
    chimera = ephs[0].copy()
    for i,ephi in enumerate(ephs[1:],1):
      phi_two,f_two = ephi(segments[i]) 
      phi_one,f_one = ephs[i-1](segments[i])
  
      delta_phi = phi_two-phi_one
      delta_fs  = np.array(f_two)-np.array(f_one)
  
      chimera.GLEP.append(segments[i])
      chimera.GLPH.append(delta_phi)
      chimera.GLF0.append(delta_fs[0])
      chimera.GLF1.append(delta_fs[1])
      chimera.GLF2.append(delta_fs[2])
  
      chimera.keys.append("GLEP_%d" % i)
      chimera.keys.append("GLPH_%d" % i)
      chimera.keys.append("GLF0_%d" % i)
      chimera.keys.append("GLF1_%d" % i)
      chimera.keys.append("GLF2_%d" % i)
  
      chimera.FINISH=segments[-1]
  
    chimera.name = self.name+'_segmented'
    chimera.segmented = True
    return chimera,segments,segepoch,ephs


  def Unsegment(self):
    """Recalculates single segments from a segmented
       ephemeris. The recovery in frequencies is
       perfect, but there are some 0.1% variations
       in the phase zero. I still did not understand
       why, but they are not directly related to
       the values of the GLPH parameters.

       eph0 = Ephemeris('test.par')
       chimera,segments,segepoch,ephs = eph0.Segment()
       segments2,segepoch2,ephs2 = chimera.Unsegment()

       Now ephs and ephs2 should be equivalent.

       Returns: segments,segmentepochs,ephs
    """

    if not self.segmented:
      errstr = 'Can not unsegment: ephemeris %s is not segmented.' % self.name
      raise TypeError(errstr)

    segments = [self.START]+self.GLEP+[self.FINISH]
    segepoch = ((np.array(segments[:-1])+np.array(segments[1:]))/2).tolist()

    #The first ephem is the same as self,
    #without the GL parameters.
    eph0 = self.copy()
    r = re.compile("^((?!GLEP_).)*$")
    eph0.keys = filter(r.match,eph0.keys)
    eph0.GLEP = []
    r = re.compile("^((?!GLPH_).)*$")
    eph0.keys = filter(r.match,eph0.keys)
    eph0.GLPH = []
    r = re.compile("^((?!GLF0_).)*$")
    eph0.keys = filter(r.match,eph0.keys)
    eph0.GLF0 = []
    r = re.compile("^((?!GLF1_).)*$")
    eph0.keys = filter(r.match,eph0.keys)
    eph0.GLF1 = []
    r = re.compile("^((?!GLF2_).)*$")
    eph0.keys = filter(r.match,eph0.keys)
    eph0.GLF2 = []

    ephs = [eph0]*(len(segments)-1)

    for i in range(1,len(segments)-1):
      glepi = segments[i]
      ephep = segepoch[i]

      ephs[i] = ephs[i-1].copy()

      ephs[i].transfer(glepi)
      ephs[i].tzrshift(glepi)

      oldPHI,oldF = ephs[i](glepi)
      newF = oldF + np.array([self.GLF0[i-1],self.GLF1[i-1],self.GLF2[i-1]])
      ephs[i].setfreq(newF)
      ephs[i].shift(self.GLPH[i-1])

      ephs[i].transfer(ephep)
      ephs[i].tzrshift(ephep)

      ephs[i].START = glepi
      ephs[i].FINISH = segments[i+1]


    return segments,segepoch,ephs


##################################################
######      END OF THE EPHEMERIS CLASS      ######
##################################################




if __name__ == '__main__':
  
  eph0    = Ephemeris("J0633+1746_54683_58514_I10A@.par")
  eph_trans = eph0.copy()
  eph_trans.transfer(58000)
  eph_trans.tzrshift(eph0.PEPOCH)

  eph_remov = eph_trans.copy()
  eph_remov.removewaves()

  eph_burnt = eph_trans.copy()
  eph_burnt.burnwaves()

  eph_froze = eph_burnt.copy()
  eph_froze.clip(2)

  eph_chimr,_,_,_ = eph0.Segment(128)

  eph_trans.savepar('step_ephems/J0633+1746_trans.par')
  eph_remov.savepar('step_ephems/J0633+1746_remov.par')
  eph_burnt.savepar('step_ephems/J0633+1746_burnt.par')
  eph_froze.savepar('step_ephems/J0633+1746_froze.par')
  eph_chimr.savepar("step_ephems/J0633+1746_chimera.par")


##################################################
######                 NOTES                ######
##################################################

'''
ABOUT THE TEMPO2 PHASE CALCULATION:

  TEMPO2 uses the Fn parameters to model the Taylor
  polynomial rotational expansion at PEPOCH, but it
  does  not define the  PHI0 at PEPOCH. Instead, it
  uses the TZRSITE,TZRFREQ and TZRMJD parameters to
  compute it. The parameters read like this:

      "If an event was  observed at TZRMJD
       by a  telescope which is at TZRSITE
       on the TZRFREQ frequency band, such
       an event, ONCE PROCESSED AS ALL THE
       OTHER EVENTS, would have phase = 0".

  In  practise, TEMPO2 adds an  extra event (in its
  jargon,  an  "observation") at the  end  of  each
  array that it processes, with the TZR parameters.
  If one does specify TZRSITE=COE (Center Of Earth)
  or anything else, TEMPO2 will actually barycenter
  that time BEFORE assuming it to be phase 0!
  Because of this, it is possible to shift the PHI0
  of the  ephemeris up and down only if  TZRSITE is
  set to  "@" or "BAT" (meaning the  Barycenter  of
  the Solar System), because the frequencies in the
  reference  frame of "COE" or similar ones are not
  the  ones given by the ephemeris, which are valid
  after all  corrections have been applied (in case
  of a double pulsar they  would be barycentered in
  the barycenter of the pulsar system!).

  Once the  PHASE for the TZR  event has been fully
  computed, including all  possible corrections, it
  gets subtracted from all of the other phases.
  This means that if the ephemeris has the FITWAVES
  parameters, the delta PHI  value corresponding to
  TZRMJD will be subtracted from ALL PHASES, like a
  constant phase shift. Because  of this, if one is
  willing to shift the  phase of the ephemeris, the
  extra  component coming  from the noise expansion
  at TZRMJD  needs to be accounted, even when waves
  are not being "burnt" into the ephemeris!
'''

'''
ABOUT BURNING THE WAVES:
   
  Burning  waves in allows the  accuracy of a waved
  ephemeris to  be kept locally even if you want to
  remove the waves.
  Compare the  results on the sample Geminga "I10A"
  ephemeris when doing these things:

    eph0 = Ephemeris("...I10A.par")
    eph_trans = eph0.copy()
    eph_trans.transfer(58000)
    eph_remov = eph_trans.copy()
    eph_remov.removeWaves()
    eph_burnt = eph_trans.copy()
    eph_burnt.burnWaves()
  
  The ephemeris "eph_trans" behaves exactly as eph0
  did, the waves are still there even if  the epoch
  of the polynomial expansion has been changed.
  The ephemeris "eph_remov" doesn't  have the waves
  any more, so it  produces wipples all  around and
  neither close to its epoch it is precise enough.
  The ephemeris "eph_burnt" accounts  for the waves
  in the proximity of the epoch,  and there it will
  be sufficiently precise. Getting away from there,
  however, it becomes less and less precise.
  
  Burning the waves may be a good way of converting
  an ephemeris  with waves  to a segmented one, but
  one needs then to clip them accordingly.  This is
  done automatically in the .segment() method.
'''
