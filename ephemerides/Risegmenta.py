#!/usr/bin/python2.7

import numpy
import matplotlib.pyplot as plt
from Ephemerides import *

from scipy import interpolate

###############################################
# This script takes an ephemeris and the phase
# drift file produced with it and updates its
# segments using a cubic spline.
# In order for it to work, the drift must be
# produced with the segmented version of the
# ephemeris (ephtot). See the end notes.


# Number of cubic segments
N = 128
# Width of the spline arc in segments.
M = 4


# Here we regenerate the segmented ephemeris that
# was used to produce the drift.
eph0 = Ephemeris("J0633+1746_54683_58514_I10A@.par")
ephtot,segments,pepochs,ephs = eph0.Segment(N)

# Interpolation needs float64.
segments = np.float64(segments)
pepochs  = np.float64(pepochs)

# Loading the drift
DRIFT = np.loadtxt("DRIFT_CHIMERA.txt",dtype=np.float64)
DRIFT = DRIFT[DRIFT[:,0].argsort()]

# We use here values from the hyperbolic fit
mjd = DRIFT[:,0]
psi = DRIFT[:,3]
err = DRIFT[:,4]

# The knots MUST be within the data. We make
# two different arrays: "joints" that keeps
# the extremes of the original segments and
# the new ones that we are making to cover
# the new part, and "knots" that are going
# to be used by the interpolation. Don't mix
# them up.
seglen   = segments[1]-segments[0]
nextra   = int(np.floor((mjd[-1] - segments[-1])/seglen))


# We add segments of the same size till
# all data is covered. By construction
# there is always some data right of
# the last joint (which is required).
joints  = segments.tolist()
joints += [segments[-1]+i*seglen for i in np.arange(1,nextra+1)]
joints  = np.array(joints)

# Similarly, we copy over the last ephemeris
# onto the new segments (it's the one that
# has been used on them!) and transfer it to
# the pepochs at the center of the segments.
newpepochs  = [segments[-1]+(i+0.5)*seglen for i in range(nextra)]
newephs  = [ephs[-1].copy() for i in range(nextra)]
# Cycling on newephs would not work, careful.
for i in range(len(newephs)):
  newephs[i].transfer(newpepochs[i])


# We use only some of the segment joints
# as knots for the spline, as using all
# of them produces very oscillating
# results. We clip every M joint from
# the end to the beginning (so that we
# don't lose knots where they are more
# needed.
knots = joints[joints>mjd[0]].astype(np.float64)
knots = np.flip(knots)
knots = knots[::M]
knots = np.flip(knots)

# The interpolation iself
tck,fp,ier,msg = interpolate.splrep(mjd,psi,k=3,t=knots,full_output=1)

# dPHI,dFN of all the segments. We cat
# the old ones and the new ones
pepochs = np.concatenate([pepochs,newpepochs])
ephs = np.concatenate([ephs,newephs])

# Miracle!
dPHI = interpolate.splev(pepochs,tck,der=0)
dF0  = interpolate.splev(pepochs,tck,der=1)/86400.
dF1  = interpolate.splev(pepochs,tck,der=2)/86400./86400.
dF2  = interpolate.splev(pepochs,tck,der=3)/86400./86400./86400.

# Updating the segments. As the drifts
# were determined by the x-correlation,
# they need to be ADDED to the previous
# ephemeris.
for i,eph in enumerate(ephs):
  # Adding the dPHI.
  eph.shift(dPHI[i])
  # This is for changing the frequency
  # in a safe way. It should be made a
  # function of the Ephemeris class.
  phi0i,_ = eph(eph.PEPOCH)
  eph.tzrshift(eph.PEPOCH)
  eph.F[0]  += dF0[i]
  eph.F[1]  += dF1[i]
  eph.F[2]  += dF2[i]
  phi0f,_ = eph(eph.PEPOCH)
  eph.shift(phi0i-phi0f)
  ephs[i] = eph #Otherwise it's lost


# Joining them. Now we use the joints
# array and not the knots one, otherwise
# we would mess things up! 
chimera = ephs[0].copy()
for i,ephi in enumerate(ephs[1:],1):
  phi_two,f_two = ephi(joints[i]) 
  phi_one,f_one = ephs[i-1](joints[i])

  delta_phi = phi_two-phi_one
  delta_fs  = np.array(f_two)-np.array(f_one)

  chimera.GLEP.append(joints[i])
  chimera.GLPH.append(delta_phi)
  chimera.GLF0.append(delta_fs[0])
  chimera.GLF1.append(delta_fs[1])
  chimera.GLF2.append(delta_fs[2])

  chimera.keys.append("GLEP_%d" % i)
  chimera.keys.append("GLPH_%d" % i)
  chimera.keys.append("GLF0_%d" % i)
  chimera.keys.append("GLF1_%d" % i)
  chimera.keys.append("GLF2_%d" % i)

# We update FINISH and save the results
chimera.FINISH=mjd[-1]
chimera.savepar('ChimeraCubic.par')

# Next a bit of plottery.
fig = plt.figure(figsize=(6,6))
ax1 = fig.add_axes((0.12,0.325,0.83,0.625))
ax2 = fig.add_axes((0.12,0.05 ,0.83,0.225))

# Evaluation of the drift model with xs,ys
xs = np.linspace(mjd[0],mjd[-1],100000)
ys = interpolate.splev(xs,tck,der=0)
# The model on the drift points themselves...
psi_spline = interpolate.splev(mjd,tck,der=0)
# ...and on the knots.
psi_knots  = interpolate.splev(knots,tck,der=0)

ax1.errorbar(mjd,psi,err,fmt='bo')
ax1.plot(xs,ys,'r-',zorder=5)
ax1.plot(knots,psi_knots,'D',color='darkred',zorder=5)

# Residuals
ax2.errorbar(mjd,psi-psi_spline,err,fmt='ko')
avgpsi = np.average(psi-psi_spline,weights=1./err/err)
ax2.plot(ax2.get_xlim(),[avgpsi]*2,'--',color='lightgray')

# A new figure with the histogram.
fig, ax = plt.subplots()
H = ax.hist(psi-psi_spline,64)

# Bye.
plt.show()






#################
##    NOTES    ##
#################


## Number of cubic segments
#N = 128
## Width of the spline arc in segments.
#M = 4
#
## Acts like a smoothing parameter. Since in
## one segment of the ephemeris there are few
## data points (five or six) we use a larger
## interval for the definition of the cubic
## arcs to be fitted by the spline. I call
## these arcs "supersegments", and M segments
## are one supersegment. Each ephemeris in the
## same supersegment will get the same cubic
## arc correction, but each segment will get
## a different correction depending on its
## PEPOCH.
#
##
##
##   |----|----|----|----|----|----|
##
## Each dash is a cubic segment of the ephemeris.
## so with N=128, one month. Every four months
## define a joint of the cubic spline (bars). In
## between there are multiple ephemeris segments
## but just one cubic polynomial to model the
## ephemeris.

