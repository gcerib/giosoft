# Ephemerides

Python class to manipulate tempo2 .par files. An example script based on it, that reads a previous segmented ephemeris and updates it, is provided.
