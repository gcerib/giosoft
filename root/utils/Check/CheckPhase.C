#include <TString.h>
#include <TChain.h>
#include <TBranch.h>
#include <TCanvas.h>
#include <TH1.h>
#include <TH1D.h>

#include <iostream>

using namespace std;


void CheckPhase2(TString Input, TString Input2){
     TString phaseStr("MPhase2_1.fPhase");
     TString phaseStr2("MPhase2_1.fPhase");

     TChain * events = new TChain("Events");
     events->Add(Input.Data());

     TChain * events2 = new TChain("Events");
     events2->Add(Input2.Data());

     Double_t phase;
     TBranch  *b_phase;

     Double_t phase2;
     TBranch  *b_phase2;


//MRawEvtHeader_2.fStereoEvtNumber : UInt_t
     UInt_t stereo;
     TBranch *b_stereo;

     UInt_t stereo2;
     TBranch *b_stereo2;

     events->SetMakeClass(1);
     events->SetBranchAddress(phaseStr.Data(), &phase, &b_phase);
     events->SetBranchAddress("MRawEvtHeader_2.fStereoEvtNumber", &stereo, &b_stereo);

     events2->SetMakeClass(1);
     events2->SetBranchAddress(phaseStr2.Data(), &phase2, &b_phase2);
     events2->SetBranchAddress("MRawEvtHeader_2.fStereoEvtNumber", &stereo2, &b_stereo2);

     const Int_t numEntries = events->GetEntries();
     const Int_t numEntries2 = events2->GetEntries();
     cout << "## NumEntries: " << numEntries << "   " << numEntries2 << endl;

     //TCanvas * c1 = new TCanvas("c1","c1");
     //TH1D * isto = new TH1D("isto","isto",200,-1e-4,1e-4);

     ofstream file1, file2;
     file1.open("phases1.txt");
     file1.open("phases2.txt");

     for(Int_t i=0; i<10; i++)
     {
         Long64_t localEntry = events->LoadTree(i);
         Long64_t localEntry2 = events2->LoadTree(i);


         b_phase->GetEntry(localEntry);
         b_phase2->GetEntry(localEntry2);

         b_stereo->GetEntry(localEntry);
         b_stereo2->GetEntry(localEntry2);

         cerr.precision(15);
         file1 << stereo << "  " << phase << endl;
         file2 << stereo2 << "  " << phase2 << endl;
         //isto->Fill(phase-phase2);
     }
     //isto->Draw();
     //c1->Update();

     return;
}
