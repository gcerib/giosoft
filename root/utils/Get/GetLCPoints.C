void GetLCPoints(){
  TFile * _file0 = new TFile("Output_flute.root");

  TGraphErrors * lc = (TGraphErrors*)_file0->Get("LightCurve");

  ofstream ofile;
  ofile.open("lightcurve.txt");

  for(Int_t j=0; j<LightCurve->GetN(); ++j){
    Double_t x,y;
    LightCurve->GetPoint(j,x,y);
    Double_t exlow = LightCurve->GetErrorXlow(j);
    Double_t exhigh = LightCurve->GetErrorXhigh(j);
    Double_t eylow = LightCurve->GetErrorYlow(j);
    Double_t eyhigh = LightCurve->GetErrorYhigh(j);

    ofile << j << "\t"
          << Form("%5.6f",x) << "\t"
          << y << "\t"
          << exlow << "\t"
          << exhigh << "\t"
          << eylow << "\t"
          << eyhigh << endl;
     
  }

  ofile.close();

  return;
}
