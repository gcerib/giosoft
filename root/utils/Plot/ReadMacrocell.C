//          __
//       __/  \__
//    __/  \__/  \__
//   /  \__/  \__/  \         ____                 ____  ___                                ____
//   \__/  \__/  \__/        / __ \___  ____ _____/ /  |/  /___ _______________  ________  / / /
//   /  \__/  \__/  \       / /_/ / _ \/ __ `/ __  / /|_/ / __ `/ ___/ ___/ __ \/ ___/ _ \/ / / 
//   \__/  \__/  \__/      / _, _/  __/ /_/ / /_/ / /  / / /_/ / /__/ /  / /_/ / /__/  __/ / /  
//   /  \__/  \__/  \     /_/ |_|\___/\__,_/\__,_/_/  /_/\__,_/\___/_/   \____/\___/\___/_/_/ 
//   \__/  \__/  \__/
//      \__/  \__/                               A little great macro for macrocells!
//         \__/

#include <iostream>
#include <iomanip>
#include <string>

#include <TChain.h>
#include <TBranch.h>
#include <TArrayF.h>
#include <TFile.h>
#include <TGraph.h>
#include <TAxis.h>

#include <MReportSumTrigger.h>
#include <MTime.h>

using namespace std;

void ReadMacrocell(string instring="20*_Y_*.root"){

  // Works on mono files, like _Y_ from sorcerer.
  // To change this, add "_1" or "_2" to the con-
  // tainer names.
  TChain * t_SumTrigger = new TChain("SumTrigger");
  t_SumTrigger->Add(instring.c_str());
 
  t_SumTrigger->SetBranchStatus("*",1);
 
  unsigned nentries = t_SumTrigger->GetEntries();
  unsigned nmacrocell = 0; 

  MReportSumTrigger * report = new MReportSumTrigger();
  MTime * time = new MTime();
 
  TBranch * b_report = 0;
  TBranch * b_time = 0;
 
  t_SumTrigger->SetBranchAddress("MReportSumTrigger.",&report,&b_report);
  t_SumTrigger->SetBranchAddress("MTimeSumTrigger.",&time,&b_time);
 
  Double_t * macroTime = new  Double_t[nentries];
  Double_t ** macroRate = new Double_t*[nentries];
  Double_t ** macroThre = new Double_t*[nentries];

  //unsigned nread = 0;

  // Reading 
  for(unsigned i=0;i<nentries;i++){
    Long64_t localEntry = t_SumTrigger->LoadTree(i);
    b_report->GetEntry(localEntry);
    b_time->GetEntry(localEntry);
 
    macroTime[i] = ((time->GetMjd())-(time->GetDay()))*24.0;
    nmacrocell = (report->GetSPR()).GetSize();

    //Double_t thr, threrr;
    //report->GetMedianDTW(thr, threrr);

    //cerr << setprecision(15) << macroTime[i] << "   " << thr << "  " << threrr << endl;
 
    macroRate[i] = new Double_t[nmacrocell];
    macroThre[i] = new Double_t[nmacrocell];
 
    for(unsigned j=0;j<nmacrocell;j++){
      //Double_t temp = (report->GetSPR())[j];
      //memcpy(*(macroRate+i)+j,&temp,sizeof(Double_t));
      macroRate[i][j] = (Double_t)((report->GetSPR())[j]);
      //macroRate[i][j] = 0;
      macroThre[i][j] = (Double_t)((report->GetDTW())[j]);
      //cerr << i << "  " << j << "  " << macroRate[i][j] << endl;
      //nread = nread+1;
    }
  }
  //cerr << nread << endl;

  Double_t ** transRate = new Double_t*[nmacrocell];
  Double_t ** transThre = new Double_t*[nmacrocell];

  for(unsigned j=0;j<nmacrocell;j++){
     transRate[j] = new Double_t[nentries];
     transThre[j] = new Double_t[nentries];
     for(unsigned i=0;i<nentries;i++){
      //cerr << j << "  " << i << " " << macroRate[i][j] << endl;
      //Double_t temp = macroRate[i][j];
      //cerr << "WATCH ME! " << temp << endl;
      transRate[j][i] = macroRate[i][j];
      transThre[j][i] = macroThre[i][j];
     }
  }

  //cerr << endl << endl;

  //for(unsigned k=0;k<nmacrocell;k++){
  //   for(unsigned l=0;l<nentries;l++){
  //     cerr << k << "  " << l << " " << setprecision(12) << macroTime[l]<< "    " << transRate[k][l] << endl;
  //   }
  //}

  TFile * file = new TFile("Macrocells.root","RECREATE");
  TGraph ** rates = new TGraph*[nmacrocell];
  TGraph ** thres = new TGraph*[nmacrocell];
  for(unsigned j=0;j<nmacrocell;j++){
    rates[j] = new TGraph(nentries,macroTime,transRate[j]);
    rates[j]->SetMarkerColor(2);
    rates[j]->SetMarkerStyle(2);
    rates[j]->SetLineColor(kBlack);
    rates[j]->SetName(Form("rates%02d",j));
    rates[j]->SetTitle(Form("Macrocell N.%d",j));
    rates[j]->GetYaxis()->SetTitle("Rate [Hz]");
    rates[j]->GetXaxis()->SetTitle(Form("Time + MJD%05d [h]",time->GetDay()));
    //rates[j]->Draw("APL");
    rates[j]->Write();

    thres[j] = new TGraph(nentries,macroTime,transThre[j]);
    thres[j]->SetMarkerColor(kBlue);
    thres[j]->SetMarkerStyle(2);
    thres[j]->SetLineColor(kBlack);
    thres[j]->SetName(Form("thres%02d",j));
    thres[j]->SetTitle(Form("Macrocell N.%d",j));
    thres[j]->GetYaxis()->SetTitle("Threshold [mV]");
    thres[j]->GetXaxis()->SetTitle(Form("Time + MJD%05d [h]",time->GetDay()));
    //thres[j]->Draw("APL");
    thres[j]->Write();
  }


  for(unsigned j=0;j<nmacrocell;j++){
    delete [] transRate[j];
    delete [] transThre[j];
  }
  delete [] transRate;
  delete [] transThre;

  for(unsigned i=0;i<nentries;i++){
    delete [] macroRate[i];
    delete [] macroThre[i];
  }
  delete [] macroRate;
  delete [] macroThre;
 
  return;
}
