#include <TString.h>
#include <TChain.h>
#include <TBranch.h>
#include <TCanvas.h>
#include <TFrame.h>
#include <TH1.h>
#include <TH1D.h>
#include <TBox.h>
#include <TMath.h>
#include <TStyle.h>
#include <TPaveText.h>
#include <TFile.h>

#include <iostream>
#include <cmath>
#include <vector>
#include <sstream>

#include <GCuts.h>

using namespace std;

double Crab(double x){
  return pow(x/300.,-2.31-0.26*log10(x/300.));
}


void ResBias(){
     TString Input("GA*_Q_*.root");
     TString stereoCon("MStereoParDisp");

     TFile * f = new TFile("Energy_ResBias.root","RECREATE");

     GCuts cuts("t75h90.cuts");

     cout << "## READING FILES... ";

     TChain *events = new TChain("Events");
     events->Add(Input.Data());

     Double_t hadronness;
     Float_t  theta2;
     Float_t  energy;
     Float_t  trueenergy;

     TBranch  *b_hadronness;
     TBranch  *b_theta2;
     TBranch  *b_energy;
     TBranch  *b_trueenergy;

     events->SetMakeClass(1);
     events->SetBranchAddress("MHadronness.fHadronness", &hadronness, &b_hadronness);
     events->SetBranchAddress("MEnergyEst.fEnergy", &energy, &b_energy);
     events->SetBranchAddress((stereoCon+".fTheta2").Data(), &theta2, &b_theta2);
     events->SetBranchAddress("MMcEvt_2.fEnergy", &trueenergy, &b_trueenergy);
     cout << "OK" << endl;

     const Int_t numEntries = events->GetEntries();
     cout << "## NumEntries: " << numEntries << endl;
     const Int_t onepercent = numEntries/100.;

     int nenbins = cuts.GetNBins();
     TH1D ** lc = new TH1D*[nenbins];
     for(int i=0;i<nenbins;i++){
       lc[i]  = new TH1D(Form("lc_%d",i),"",200,-2.,2.);
     }

     Int_t scartH = 0;
     Int_t scartT = 0;
     Int_t scartE = 0;

     double       Non  = 0;
     double       Noff = 0;

     double       NonP1 = 0;
     double       NonP2 = 0;

     for(Int_t i=0; i<numEntries; i++)
     {
         if(i%onepercent==0) cout<< "\r" << "Processing: " << ceil(i/(float)numEntries*100.) << "%" << flush;

         Long64_t localEntry = events->LoadTree(i);

         b_energy->GetEntry(localEntry);
         b_hadronness->GetEntry(localEntry);
         b_theta2->GetEntry(localEntry);
         b_trueenergy->GetEntry(localEntry);

         if((energy<cuts.GetMinEnergy() ) || ( energy>cuts.GetMaxEnergy())){
             scartE++;
             continue;
         }

         if( ( theta2>cuts.Theta2(energy) ) || ( theta2<0 ) ){
             scartT++;
             continue;
         }
         if((hadronness>cuts.Hadronness(energy))){
             scartH++;
             continue;
         }
 
         lc[cuts.WhichBin(trueenergy)]->Fill((energy-trueenergy)/trueenergy);//,Crab(energy)*pow(energy,1.6));//TMath::Power(trueenergy,(-5.3)+1.6));
     }
     cout << endl;
     cout << "## CUT EVENTS:" << endl;
     cout << "## (Energy)     (Theta2)     (Hadron.)     (Total cut)" << endl;
     cout << "## " << (Double_t)scartE/numEntries << "  +  "
          << (Double_t)scartT/numEntries << "  +  "
          << (Double_t)scartH/numEntries << "  =  "
          << (Double_t)scartH/numEntries+(Double_t)scartT/numEntries+(Double_t)scartE/numEntries
          << endl;
     cout << "## Surviving events: " <<   numEntries-scartE-scartT-scartH << " (";
     ios copia(NULL);
     copia.copyfmt(cout);
     cout << fixed << setprecision(2) << (1 - ((Double_t)scartH/numEntries+(Double_t)scartT/numEntries+(Double_t)scartE/numEntries))*100;
     cout.copyfmt(copia);
     cout << "%)" << endl;


     cout << endl;
     //gStyle->SetOptStat(0);
     gStyle->SetOptFit(111);
     TCanvas *can = new TCanvas("can","THisto Test");

     for(int i=0;i<nenbins;i++){

       //gPad->SetLogx(1);
       gPad->SetLogy(1);

       lc[i]->SetFillColor(kBlue-3);
       lc[i]->SetLineColor(kBlue-3);
       lc[i]->GetXaxis()->SetTitle("(E_{est}-E_{true})/E_{true}");
       lc[i]->SetTitle(Form("%f < E_{true} < %f",cuts.GetLowBinEdge(i),cuts.GetUpBinEdge(i)));
       lc[i]->Draw();

       lc[i]->Fit("gaus");
       //lc[i]->GetFunction("gaus")->SetNpx(1000);

       can->Modified();
       can->Update();
  
       can->SaveAs(Form("canvas_%d.pdf",i));
       can->SaveAs(Form("canvas_%d.C",i));

       can->Clear();
     }

     f->Write();
     //f->Close();

     return;
}
