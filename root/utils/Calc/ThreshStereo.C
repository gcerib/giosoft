#include <TString.h>
#include <TChain.h>
#include <TBranch.h>
#include <TCanvas.h>
#include <TFrame.h>
#include <TH1.h>
#include <TF1.h>
#include <TH1D.h>
#include <TBox.h>
#include <TMath.h>
#include <TStyle.h>
#include <TPaveText.h>
#include <TFile.h>

#include <iostream>
#include <cmath>
#include <sstream>

using namespace std;


//Configuration
#define THEDATA1 "GA_Q_LAPALMA/GA*.root"

//Setting this to 0 causes the macro to use
//the Log-parabola Nebula spectrum to weight
//the MCs.
#define SPECTRUM -1.6

//This was derived from 1GeV showers @100km.
#define P0_1ns 8.42292824700602749e-04

#define TTRG_SIGL 2.
#define TTRG_SIGH 20.
#define TTRG_NSBH 155.

//If not zero, cut above this energy.
#define CUTENERGY 0

///////////////
//Definitions//
///////////////


#define D0 8487.28460698709 //cm

//This time it is squared, we are cutting on both
//trigger times.
double P0 = P0_1ns*(TTRG_SIGH-TTRG_SIGL);

double Crab(double x){
  return pow(x/300.,-2.31-0.26*log10(x/300.));
}

double Weight(double energy, double index){
  double w;
  if(index==0){
    w = Crab(energy)*pow(energy,1.6);
  }
  else{
    if(index!=-1.6){
      w = pow(energy,index+1.6);
    }
    else{
      w = 1;
    }
  }
  //cerr << "#" << w << endl;
  return w;
}


void ThreshStereo(){
     TString Input1(THEDATA1);

     TFile * f = new TFile("Threshold.root","RECREATE");

     TChain *events1 = new TChain("Events");
     events1->Add(Input1.Data());

     Float_t   trueenergy1;
     UInt_t    stereo1;
     Float_t   trigger1[4];
     Float_t   zfirst1;
     UInt_t    phot1;
     Float_t   impact1;

     Float_t   trueenergy2;
     UInt_t    stereo2;
     Float_t   trigger2[4];
     Float_t   zfirst2;
     UInt_t    phot2;
     Float_t   impact2;

     Float_t   corex;
     Float_t   corey;

     TBranch  *b_trueenergy1;
     TBranch  *b_stereo1;
     TBranch  *b_trigger1;
     TBranch  *b_zfirst1;
     TBranch  *b_phot1;
     TBranch  *b_impact1;

     TBranch  *b_trueenergy2;
     TBranch  *b_stereo2;
     TBranch  *b_trigger2;
     TBranch  *b_zfirst2;
     TBranch  *b_phot2;
     TBranch  *b_impact2;

     TBranch  *b_corex;
     TBranch  *b_corey;

     events1->SetBranchStatus("*",0);

     events1->SetBranchStatus("MMcEvt_1.fEnergy", 1);
     events1->SetBranchStatus("MMcTrig_1.fTimeFirst[4]", 1);
     events1->SetBranchStatus("MRawEvtHeader_1.fStereoEvtNumber", 1);    
     events1->SetBranchStatus("MMcEvt_1.fZFirstInteraction", 1);
     events1->SetBranchStatus("MMcEvt_1.fPhotElfromShower", 1);   
     events1->SetBranchStatus("MMcEvt_1.fImpact", 1);

     events1->SetBranchStatus("MMcEvt_2.fEnergy", 1);
     events1->SetBranchStatus("MMcTrig_2.fTimeFirst[4]", 1);
     events1->SetBranchStatus("MRawEvtHeader_2.fStereoEvtNumber", 1);    
     events1->SetBranchStatus("MMcEvt_2.fZFirstInteraction", 1);
     events1->SetBranchStatus("MMcEvt_2.fPhotElfromShower", 1);   
     events1->SetBranchStatus("MMcEvt_2.fImpact", 1);

     events1->SetBranchStatus("MMcEvt_1.fCoreX", 1);
     events1->SetBranchStatus("MMcEvt_1.fCoreY", 1);

     events1->SetMakeClass(1);


     events1->SetBranchAddress("MMcEvt_1.fEnergy", &trueenergy1, &b_trueenergy1);
     //No & in here, it's an array!
     events1->SetBranchAddress("MMcTrig_1.fTimeFirst[4]", trigger1, &b_trigger1);
     events1->SetBranchAddress("MRawEvtHeader_1.fStereoEvtNumber", &stereo1, &b_stereo1);
     events1->SetBranchAddress("MMcEvt_1.fZFirstInteraction", &zfirst1, &b_zfirst1);
     events1->SetBranchAddress("MMcEvt_1.fPhotElfromShower", &phot1, &b_phot1);
     events1->SetBranchAddress("MMcEvt_1.fImpact", &impact1, &b_impact1);

     events1->SetBranchAddress("MMcEvt_2.fEnergy", &trueenergy2, &b_trueenergy2);
     //No & in here, it's an array!
     events1->SetBranchAddress("MMcTrig_2.fTimeFirst[4]", trigger2, &b_trigger2);
     events1->SetBranchAddress("MRawEvtHeader_2.fStereoEvtNumber", &stereo2, &b_stereo2);
     events1->SetBranchAddress("MMcEvt_2.fZFirstInteraction", &zfirst2, &b_zfirst2);
     events1->SetBranchAddress("MMcEvt_2.fPhotElfromShower", &phot2, &b_phot2);
     events1->SetBranchAddress("MMcEvt_2.fImpact", &impact2, &b_impact2);

     events1->SetBranchAddress("MMcEvt_1.fCoreX", &corex, &b_corex);
     events1->SetBranchAddress("MMcEvt_1.fCoreY", &corey, &b_corey);

     const Int_t numEntries = events1->GetEntries();
     cout << "## NumEntries: " << numEntries << endl;
     const Int_t onepercent = numEntries/100.;

     // thr = Threshold plot (normalized from trigger times)
     // nsb = NSB plot (from trigger times)
     // tgr = Trigger times (all)
     // all = All events
     // non = Events which did not trigger
     // zon = Height of the first interaction (ON trig times)
     // zof = Height of the last interaction (OFF trig times)
     // fon = N. of photoelectrons by the shower (ON trig times)
     // fof = N. of photoelectrons by the shower (OFF trig times)
     //       In this one you may get values not zero because
     //       phes from the simulated shower can arrive in the
     //       camera even if they do not trigger and the NSB
     //       triggers later on.
     // ion = Impact parameters (ON trig times)
     // iof = Impact parameters (OFF trig times)
     // jon = Impact parameter squared (ON trig times)
     // jof = Impact parameter squared (OFF trig times)
     // tmp,zmp,fmp,imp,jmp = temporary pointers.

     // We have three type of OFFs:
     //
     //    0. Pure OFF      (trigger1 > TTRIG_SIGH) && (trigger2 > TTRIG_SIGH):
     //       including only the NSB
     //    1. Cross OFF 1   (trigger1 < TTRIG_SIGH) && (trigger2 > TTRIG_SIGH):
     //       hybrid shower/NSB triggers, plus alpha * pure NSB ones. (M1)
     //    2. Cross OFF 2   (trigger1 > TTRIG_SIGH) && (trigger2 < TTRIG_SIGH):
     //       hybrid shower/NSB triggers, plus alpha * pure NSB ones. (M2)
     //
     // The resulting formula for the excess is:
     //
     //    EXC = ON - alpha*CROSS1 -alpha*CROSS2 + alpha*alpha*PURE
     //
     // The plus in the pure term is NOT a mistake! Do again the
     // calculation if you don't trust me, it's the square of a
     // bynomial (with non commutative algebra).


     TH1D *all  = new TH1D("all","All Events MONO",10000,0.,5);
     TH1D *non  = new TH1D("non","No Trigers MONO",10000,0.,5);

     TH1D *thr  = new TH1D("thr","Threshold",10000,0.,5);
     TH1D *nsb[3];
     nsb[0]  = new TH1D("nsb__0","NSB pure",10000,0.,5);
     nsb[1]  = new TH1D("nsb__1","NSB cross 1",10000,0.,5);
     nsb[2]  = new TH1D("nsb__2","NSB cross 2",10000,0.,5);

     TH1D *trg1  = new TH1D("trg1","First Trigger Time M1",342,-1.,170.);
     TH1D *trg2  = new TH1D("trg2","First Trigger Time M2",342,-1.,170.);

     //One bin per km
     TH1D *zon   = new TH1D("zon","Height ON",100,0,10000000);
     TH1D *zof[3];
     zof[0]   = new TH1D("zof__0","Height OFF pure",100,0,10000000);
     zof[1]   = new TH1D("zof__1","Height OFF cross 1",100,0,10000000);
     zof[2]   = new TH1D("zof__2","Height OFF cross 2",100,0,10000000);

     //One bin per phe
     TH1D *fon1  = new TH1D("fon1","PHE M1 ON",1000,0,1000);
     TH1D *fof1[3];
     fof1[0]  = new TH1D("fof1__0","PHE M1 OFF pure",1000,0,1000);
     fof1[1]  = new TH1D("fof1__1","PHE M1 OFF cross 1",1000,0,1000);
     fof1[2]  = new TH1D("fof1__2","PHE M1 OFF cross 2",1000,0,1000);

     TH1D *fon2  = new TH1D("fon2","PHE M2 ON",1000,0,1000);
     TH1D *fof2[3];
     fof2[0]  = new TH1D("fof2__0","PHE M2 OFF pure",1000,0,1000);
     fof2[1]  = new TH1D("fof2__1","PHE M2 OFF cross 1",1000,0,1000);
     fof2[2]  = new TH1D("fof2__2","PHE M2 OFF cross 2",1000,0,1000);

     //One bin per meter
     TH1D *ion1  = new TH1D("ion1","Impact M1 ON",1000,0,100000);
     TH1D *iof1[3];
     iof1[0]  = new TH1D("iof1__0","Impact M1 OFF pure",1000,0,100000);
     iof1[1]  = new TH1D("iof1__1","Impact M1 OFF cross 1",1000,0,100000);
     iof1[2]  = new TH1D("iof1__2","Impact M1 OFF cross 2",1000,0,100000);

     TH1D *ion2  = new TH1D("ion2","Impact M2 ON",1000,0,100000);
     TH1D *iof2[3];
     iof2[0]  = new TH1D("iof2__0","Impact M2 OFF pure",1000,0,100000);
     iof2[1]  = new TH1D("iof2__1","Impact M2 OFF cross 1",1000,0,100000);
     iof2[2]  = new TH1D("iof2__2","Impact M2 OFF cross 2",1000,0,100000);

     //Ten bins per hectar -> (lighpool ~ 1.44)
     TH1D *jon1  = new TH1D("jon1","Impact^{2} M1 ON" ,200,0,20);
     TH1D *jof1[3];
     jof1[0]  = new TH1D("jof1__0","Impact^{2} M1 OFF pure",200,0,20);
     jof1[1]  = new TH1D("jof1__1","Impact^{2} M1 OFF cross 1",200,0,20);
     jof1[2]  = new TH1D("jof1__2","Impact^{2} M1 OFF cross 2",200,0,20);

     TH1D *jon2  = new TH1D("jon2","Impact^{2} M2 ON" ,200,0,20);
     TH1D *jof2[3];
     jof2[0]  = new TH1D("jof2__0","Impact^{2} M2 OFF pure",200,0,20);
     jof2[1]  = new TH1D("jof2__1","Impact^{2} M2 OFF cross 1",200,0,20);
     jof2[2]  = new TH1D("jof2__2","Impact^{2} M2 OFF cross 2",200,0,20);


     //Temporary pointers
     TH1D *tmp;
     TH1D *zmp;

     TH1D *fmp1;
     TH1D *fmp2;
     TH1D *imp1;
     TH1D *imp2;
     TH1D *jmp1;
     TH1D *jmp2;


     //And the 2D plots.
     TH2D * trg_2d = new TH2D("trg_2d","First Trigger Time",342,-1.,170.,342,-1.,170.);


     //2D photoelectron count
     TH2D * fon_2d = new TH2D("fon_2d","PHE ON",1000,0,1000,1000,0,1000);
     TH2D * fof_2d[3];
     fof_2d[0] = new TH2D("fof_2d__0","PHE OFF pure",1000,0,1000,1000,0,1000);
     fof_2d[1] = new TH2D("fof_2d__1","PHE OFF cross 1",1000,0,1000,1000,0,1000);
     fof_2d[2] = new TH2D("fof_2d__2","PHE OFF cross 2",1000,0,1000,1000,0,1000);


     //2D impact plot
     TH2D * ion_2d = new TH2D("ion_2d","Impact ON",1000,0,100000,1000,0,100000);
     TH2D * iof_2d[3];
     iof_2d[0] = new TH2D("iof_2d__0","Impact OFF pure",1000,0,100000,1000,0,100000);
     iof_2d[1] = new TH2D("iof_2d__1","Impact OFF cross 1",1000,0,100000,1000,0,100000);
     iof_2d[2] = new TH2D("iof_2d__2","Impact OFF cross 2",1000,0,100000,1000,0,100000);

     //x -> u = impact1+impact2
     //y -> v = impact2-impact1
     //Units in telescope separations 8487.28460698709 cm
     TH2D * jon_2d = new TH2D("jon_2d","Impact density ON",1000,0,10,400,-2,2);
     TH2D * jof_2d[3];
     jof_2d[0] = new TH2D("jof_2d__0","Impact density OFF pure",1000,0,10,400,-2,2);
     jof_2d[1] = new TH2D("jof_2d__1","Impact density OFF cross 1",1000,0,10,400,-2,2);
     jof_2d[2] = new TH2D("jof_2d__2","Impact density OFF cross 2",1000,0,10,400,-2,2);

     TH2D * fmp_2d;
     TH2D * imp_2d;
     TH2D * jmp_2d;

     //Core position
     TH2D * xon_2d = new TH2D("xon_2d","Core density ON",1000,-50000,50000,1000,-50000,50000);
     TH2D * xof_2d[3];
     xof_2d[0] = new TH2D("xof_2d__0","Core density OFF pure",1000,-50000,50000,1000,-50000,50000);
     xof_2d[1] = new TH2D("xof_2d__1","Core density OFF cross 1",1000,-50000,50000,1000,-50000,50000);
     xof_2d[2] = new TH2D("xof_2d__2","Core density OFF cross 2",1000,-50000,50000,1000,-50000,50000);

     TH2D * xmp_2d;


     //I assume that the events are read in the same order
     //from the two files. If not it will be a complete
     //mess and you should better use MReadMultiTree.
     for(Int_t i=0; i<numEntries; i++)
     {

         if (i%onepercent==0){
           cout << "\r" << "Processing: " << ceil(i/(float)numEntries*100.) << "%" << flush;
         }

         Long64_t localEntry1 = events1->LoadTree(i);

         b_trueenergy1->GetEntry(localEntry1);
         b_stereo1->GetEntry(localEntry1);
         b_trigger1->GetEntry(localEntry1);
         b_zfirst1->GetEntry(localEntry1);
         b_phot1->GetEntry(localEntry1);
         b_impact1->GetEntry(localEntry1);

         b_trueenergy2->GetEntry(localEntry1);
         b_stereo2->GetEntry(localEntry1);
         b_trigger2->GetEntry(localEntry1);
         b_zfirst2->GetEntry(localEntry1);
         b_phot2->GetEntry(localEntry1);
         b_impact2->GetEntry(localEntry1);

         b_corex->GetEntry(localEntry1);
         b_corey->GetEntry(localEntry1);

         //Sanity check. All of these variables
         //should be equal. If not, something
         //strage is happening.
         //if (  (stereo1     !=     stereo2) ||
         //      (trueenergy1 != trueenergy2) ||
         //      (zfirst1     !=     zfirst2)  )
         //{
         //      cerr << "error on event " << i << ":" << endl;

         //      cerr << "  stereo " << stereo1     << " , " << stereo2     << endl
         //           << "  energy " << trueenergy1 << " , " << trueenergy2 << endl
         //           << "  zfrist " << zfirst1     << " , " << zfirst2     << endl;

         //      break;
         //}

         //All the events regardless of anything.
         all->Fill(trueenergy2/1000.,Weight(trueenergy2,SPECTRUM));

         //We fill the "non" distribution with events which did not trigger
         if((trigger1[0]==0) && (trigger2[0]==0))
           non->Fill(trueenergy2/1000.,Weight(trueenergy2,SPECTRUM));

         //We fill the trigger times.
         trg1->Fill(trigger1[0]);
         trg2->Fill(trigger2[0]);
         trg_2d->Fill(trigger1[0],trigger2[0]);

         //Skip non stereo events
         if(stereo1<1  &&  stereo2<1) continue;

         //Skip the first TTRG_SIGL ns and after TTRG_NSBH ns in the form.
         //as a result the total time window is (TTRG_NSBH-TTRG_SIGL) ns.
         if(trigger1[0]<TTRG_SIGL || trigger1[0]>=TTRG_NSBH) continue;
         if(trigger2[0]<TTRG_SIGL || trigger2[0]>=TTRG_NSBH) continue;


         //Special condition to see only the super lowest
         //energies.
         if(CUTENERGY>0)
           if(trueenergy2<1. || trueenergy2>CUTENERGY) continue;

         //Skipping very low light content events
         //which are just due to noise. In standard
         //MCs this is done by default at 10 phe.
         //if(phot1<10 || phot2<10) continue;

         //The first TTRIG_SIGH ns contain the signal (check the
         //Trigger times plot at the end!), the rest is
         //NSB induced triggers that we use to estimate
         //the distortion caused to the distribution.
         //So (TTRG_SIGH-TTRG_SIGL) ns for the signal and
         //(TTRG_NSBH-TTRG_SIGH) ns for the NSB. We use the
         //temporary pointers.
         if((trigger1[0]<=TTRG_SIGH) && (trigger2[0]<=TTRG_SIGH)){
           //ON
           tmp = thr;
           zmp = zon;

           fmp1 = fon1;
           fmp2 = fon2;
           imp1 = ion1;
           imp2 = ion2;
           jmp1 = jon1;
           jmp2 = jon2;

           fmp_2d = fon_2d;
           imp_2d = ion_2d;
           jmp_2d = jon_2d;

           xmp_2d = xon_2d;
         }
         else if((trigger1[0]>TTRG_SIGH) && (trigger2[0]>TTRG_SIGH)){
           //OFF pure
           tmp = nsb[0];
           zmp = zof[0];

           fmp1 = fof1[0];
           fmp2 = fof2[0];
           imp1 = iof1[0];
           imp2 = iof2[0];
           jmp1 = jof1[0];
           jmp2 = jof2[0];

           fmp_2d = fof_2d[0];
           imp_2d = iof_2d[0];
           jmp_2d = jof_2d[0];

           xmp_2d = xof_2d[0];
         }
         else if((trigger1[0]<TTRG_SIGH) && (trigger2[0]>TTRG_SIGH)){
           //OFF cross 1
           tmp = nsb[1];
           zmp = zof[1];

           fmp1 = fof1[1];
           fmp2 = fof2[1];
           imp1 = iof1[1];
           imp2 = iof2[1];
           jmp1 = jof1[1];
           jmp2 = jof2[1];

           fmp_2d = fof_2d[1];
           imp_2d = iof_2d[1];
           jmp_2d = jof_2d[1];

           xmp_2d = xof_2d[1];
         }
         else{
           //OFF cross 2
           tmp = nsb[2];
           zmp = zof[2];

           fmp1 = fof1[2];
           fmp2 = fof2[2];
           imp1 = iof1[2];
           imp2 = iof2[2];
           jmp1 = jof1[2];
           jmp2 = jof2[2];

           fmp_2d = fof_2d[2];
           imp_2d = iof_2d[2];
           jmp_2d = jof_2d[2];

           xmp_2d = xof_2d[2];
         }

         //I use TeV because otherwise a lognormal does not fit.
         tmp->Fill(trueenergy2/1000.,Weight(trueenergy2,SPECTRUM));

         zmp->Fill(zfirst1,Weight(trueenergy2,SPECTRUM));

         fmp1->Fill(phot1,Weight(trueenergy2,SPECTRUM));
         fmp2->Fill(phot2,Weight(trueenergy2,SPECTRUM));
         imp1->Fill(impact1,Weight(trueenergy2,SPECTRUM));
         imp2->Fill(impact2,Weight(trueenergy2,SPECTRUM));

         jmp1->Fill(impact1/100/100*impact1/100/100,Weight(trueenergy2,SPECTRUM));
         jmp2->Fill(impact2/100/100*impact2/100/100,Weight(trueenergy2,SPECTRUM));

         //And the 2d plots.
         fmp_2d->Fill(phot1,phot2,Weight(trueenergy2,SPECTRUM));
         imp_2d->Fill(impact1,impact2,Weight(trueenergy2,SPECTRUM));

         Double_t u = (impact1+impact2)/D0;
         Double_t v = (impact2-impact1)/D0;

         //This is the inverse of the area element on the u-v space
         // x = u*v/2 , y = 0.5*sqrt((u*u-1)*(1-v*v))
         // dx*dy = 0.25*(u*u-v*v)/sqrt((u*u-1)*(1-v*v)) * du*dv
         Double_t w = TMath::Sqrt((u*u-1)*(1-v*v))/(u*u-v*v);
         jmp_2d->Fill(u,v,Weight(trueenergy2,SPECTRUM)*w);

         //And the core positions
         xmp_2d->Fill(corex,corey,Weight(trueenergy2,SPECTRUM));

     }
     cout << endl;

     //Let's make a copy! This is
     //the subtraction Julian-style.
     //P0 comes from a previous run
     //on NSB-only events1. We can't
     //account for cross elements here
     //and take out only the pure ones
     TH1D * nov = new TH1D(*thr);
     nov->SetName("nov");
     nov->SetTitle("Threshold (Julian method)");
     nov->Add(all,-P0*P0);

     //We remove the NSB. This is
     //the subtraction Giovanni-style.
     Double_t alpha = (TTRG_SIGH-TTRG_SIGL)/(TTRG_NSBH-TTRG_SIGH);
     thr->Add(nsb[1],-alpha);
     thr->Add(nsb[2],-alpha);
     thr->Add(nsb[0],alpha*alpha);


     //And normalize to a suitable value
     //Double_t ip0 = thr->GetMaximum();
     //...or maybe not. ;-) This is
     //needed for the log-normal fits.
     //thr->Divide(uno,ip0);
     //nsb->Divide(uno,ip0);

     //Plottery
     thr->SetFillColor(kBlue-3);
     if(SPECTRUM!=0){
       thr->SetTitle(Form("Threshold for #Gamma=%0.2f",SPECTRUM));
     }
     else{
       thr->SetTitle("Threshold for the Crab Nebula");
     }

     thr->GetXaxis()->SetTitle("MC true energy [TeV]");

     zon->GetXaxis()->SetTitle("First interaction [cm]");

     fon1->GetXaxis()->SetTitle("Cherenkov PhE");
     ion1->GetXaxis()->SetTitle("Impact [cm]");
     jon1->GetXaxis()->SetTitle("Impact^{2} [ha]");

     fon2->GetXaxis()->SetTitle("Cherenkov PhE");
     ion2->GetXaxis()->SetTitle("Impact [cm]");
     jon2->GetXaxis()->SetTitle("Impact^{2} [ha]");

     for(i=0;i<3;i++){
       zof[i]->GetXaxis()->SetTitle("First interaction [cm]");

       fof1[i]->GetXaxis()->SetTitle("Cherenkov PhE");
       iof1[i]->GetXaxis()->SetTitle("Impact [cm]");
       jof1[i]->GetXaxis()->SetTitle("Impact^{2} [ha]");

       fof2[i]->GetXaxis()->SetTitle("Cherenkov PhE");
       iof2[i]->GetXaxis()->SetTitle("Impact [cm]");
       jof2[i]->GetXaxis()->SetTitle("Impact^{2} [ha]");
     }

     //Calculating the excesses. First we copy the ons.
     TH1D * zex = new TH1D(*zon);

     TH1D * fex1 = new TH1D(*fon1);
     TH1D * fex2 = new TH1D(*fon2);
     TH1D * iex1 = new TH1D(*ion1);
     TH1D * iex2 = new TH1D(*ion2);
     TH1D * jex1 = new TH1D(*jon1);
     TH1D * jex2 = new TH1D(*jon2);

     TH2D * fex_2d = new TH2D(*fon_2d);
     TH2D * iex_2d = new TH2D(*ion_2d);
     TH2D * jex_2d = new TH2D(*jon_2d);

     TH2D * xex_2d = new TH2D(*xon_2d);

     zex->SetName("zex");

     fex1->SetName("fex1");
     fex2->SetName("fex2");
     iex1->SetName("iex1");
     iex2->SetName("iex2");
     jex1->SetName("jex1");
     jex2->SetName("jex2");

     fex_2d->SetName("fex_2d");
     iex_2d->SetName("iex_2d");
     jex_2d->SetName("jex_2d");

     xex_2d->SetName("xex_2d");

     //Set some titles
     zex->SetTitle("Height EXC");

     fex1->SetTitle("PHE M1 EXC");
     fex2->SetTitle("PHE M2 EXC");
     iex1->SetTitle("Impact M1 EXC");
     iex2->SetTitle("Impact M2 EXC");
     jex1->SetTitle("Impact^{2} M1 EXC");
     jex2->SetTitle("Impact^{2} M2 EXC");

     fex_2d->SetTitle("PHE EXC");
     iex_2d->SetTitle("Impact EXC");
     jex_2d->SetTitle("Impact U-V EXC");
     jex_2d->GetXaxis()->SetTitle("U=Impact1+Impact2 [84.8m]");
     jex_2d->GetYaxis()->SetTitle("V=Impact2-Impact1 [84.8m]");


     xex_2d->GetXaxis()->SetTitle("CoreX [cm]");
     xex_2d->GetYaxis()->SetTitle("CoreY [cm]");


     //Now we subtract.

     zex->Add(zof[1],-alpha);
     zex->Add(zof[2],-alpha);
     zex->Add(zof[0],alpha*alpha);


     fex1->Add(fof1[1],-alpha);
     fex1->Add(fof1[2],-alpha);
     fex1->Add(fof1[0],alpha*alpha);
     fex2->Add(fof2[1],-alpha);
     fex2->Add(fof2[2],-alpha);
     fex2->Add(fof2[0],alpha*alpha);


     iex1->Add(iof1[1],-alpha);
     iex1->Add(iof1[2],-alpha);
     iex1->Add(iof1[0],alpha*alpha);
     iex2->Add(iof2[1],-alpha);
     iex2->Add(iof2[2],-alpha);
     iex2->Add(iof2[0],alpha*alpha);


     jex1->Add(jof1[1],-alpha);
     jex1->Add(jof1[2],-alpha);
     jex1->Add(jof1[0],alpha*alpha);
     jex2->Add(jof2[1],-alpha);
     jex2->Add(jof2[2],-alpha);
     jex2->Add(jof2[0],alpha*alpha);


     fex_2d->Add(fof_2d[1],-alpha);
     fex_2d->Add(fof_2d[2],-alpha);
     fex_2d->Add(fof_2d[0],alpha*alpha);


     iex_2d->Add(iof_2d[1],-alpha);
     iex_2d->Add(iof_2d[2],-alpha);
     iex_2d->Add(iof_2d[0],alpha*alpha);


     jex_2d->Add(jof_2d[1],-alpha);
     jex_2d->Add(jof_2d[2],-alpha);
     jex_2d->Add(jof_2d[0],alpha*alpha);

     xex_2d->Add(xof_2d[1],-alpha);
     xex_2d->Add(xof_2d[2],-alpha);
     xex_2d->Add(xof_2d[0],alpha*alpha);


     ////////////////////////////////
     //  The fit code is disabled. //
     ////////////////////////////////

     //TCanvas * can = new TCanvas("can","Threshold");
     //Int_t  binmax = thr->GetMaximumBin();
     //Double_t xmax = thr->GetXaxis()->GetBinCenter(binmax);
     ////Log-normal: exp(-log(x)*log(x)/2/s/s/m/m)/(x**(1.0/m))*sqrt(exp(-s*s))
     //This is a very long string split in two lines.
     //TF1 * logg = new TF1("logg",
     //"(TMath::Exp( -  ( TMath::Log(x) )*  ( TMath::Log(x) )/2.0   /[1]  /[0]  /[0]   )"
     //"/ ( TMath::Power(x,(1.0/[0]))  )*TMath::Sqrt (  TMath::Exp(-[1])  ))"
     //);

     //Double_t median = thr->GetMean()*3.0;
     //logg->SetParameter(0,median);
     //logg->SetParameter(1,-TMath::Log(xmax)/median);
     //logg->SetNpx(10000);

     //Int_t    bin1 = thr->FindFirstBinAbove(thr->GetMaximum()*0.6)-1;
     //Int_t    bin2 = thr->FindLastBinAbove (thr->GetMaximum()*0.6);
     //Double_t high = thr->GetBinCenter(bin2);
     //Double_t  low = thr->GetBinCenter(bin1);

     //thr->Fit("logg","","",low,high);

     //p1 is NOT the peak of the lognormal!
     //Double peak = TMath::Exp( - logg->GetParameter(1) * logg->GetParameter(0)  );

     //cerr << endl;
     //cerr << "##############################################################" << endl;
     //cerr <<  TMath::Exp( - logg->GetParameter(1) * logg->GetParameter(0)  )  << endl;
     //cerr << "##############################################################" << endl;
     //cerr << endl;


     //TText *t = new TText(peak+(0.2-peak)/2.5,0.9,Form("%0.1f GeV",peak*1000));
     //t->SetTextColor(kRed);
     //t->SetTextAlign(22);
     //t->SetTextFont(43);
     //t->SetTextSize(46);
     //t->Draw("same");

     //TLine * line = new TLine(peak,1.,peak,0);
     //line->SetLineColor(kRed);
     //line->SetLineStyle(9);
     //line->Draw();

     //can->Modified();
     //can->Update();
     //can->Write();

     f->Write();
     return;
}
