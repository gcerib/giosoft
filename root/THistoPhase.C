#include <TString.h>
#include <TChain.h>
#include <TBranch.h>
#include <TCanvas.h>
#include <TFrame.h>
#include <TH1.h>
#include <TH1D.h>
#include <TBox.h>
#include <TStyle.h>
#include <TPaveText.h>

#include <iostream>
#include <cmath>
#include <vector>
#include <sstream>

#include <GCuts.h>

using namespace std;


double CalcLima17(double Non, double Noff, double alfa);
bool   IsPhaseInRegion(double phase, vector<vector <double> > regions);
double CalcOnOffRatio(vector<vector <double> > on, vector<vector <double> > off);
void   GenerateRegions(vector <vector <double> > vecR, unsigned int kColor);



void THistoPhase() {
/*********************************************/
/*************** CONFIGURATION ***************/
/*********************************************/

     // INPUT FILES
     TString Input ("20170*_Q_*.root");
     TChain *events = new TChain("Events");
     events->Add(Input.Data());

     // INPUT CONTAINERS
     TString phaseStr("MPhase2_1.fPhase");
     TString stereoCon("MStereoParDisp");
     TString energyCon("MEnergyEstStereoRF");

     // ENERGY BINNING
     Double_t lowen  = 35;
     Double_t highen = 70;

     Int_t numlogbins = 1;

     // GRAPHICS
     bool legax=1;
     //bool plotsing=1;
     //if(numlogbins==1) plotsing = 0;

     // CUTS
     GCuts cuts("fluteRF.cuts");
     if(!cuts.GetInitState()) return;

     // PHASE BINS
     UInt_t nbins = 80;

     // REGIONS:

     // C++11 :

       // CRAB

          //vector <vector <double> > P1  { { 0.000, 0.026 },    //P1_2
          //                                { 0.983, 1.000 } };  //P1_1

          //vector <vector <double> > P2  { { 0.377, 0.422 } };  //P2
          
          
          //vector <vector <double> > ON  { { 0.000, 0.026 },    //P1_2
          //                                { 0.983, 1.000 },    //P1_1
          //                                { 0.377, 0.422 } };  //P2
          
          //vector <vector <double> > OFF { { 0.520, 0.870 } };
         
       // GEMINGA (OLD, J0633+1746_54683_57479_chol_2.par)

          //vector <vector <double> > P1  { { 0.072, 0.139 } };  //P1

          //vector <vector <double> > P2  { { 0.571, 0.625 } };  //P2
      

          //vector <vector <double> > ON  { { 0.072, 0.139 },    //P1
          //                                { 0.571, 0.625 } };  //P2

          //vector <vector <double> > P1  { { 0.200, 0.500 } };  //BRIDGE

          //vector <vector <double> > OFF { { 0.700, 0.950 } };


       // GEMINGA (NEW, J0633+1746_54683_58119_4DFG.par)

          vector <vector <double> > P1  { { 0.9844, 1.0000 },
                                          { 0.0000, 0.0514 } };  //P1

          vector <vector <double> > P2  { { 0.4830, 0.5370 } };  //P2
      

          vector <vector <double> > ON  { P1[0],    //P1_1
                                          P1[1],    //P1_2
                                          P2[0] };  //P2

          //vector <vector <double> > P1  { { 0.200, 0.500 } };  //BRIDGE

          vector <vector <double> > OFF { { 0.612, 0.862 } };


     // C++98 :

       // CRAB

           /* 
                vector <vector <double> >  ON;
                vector <vector <double> >  OFF;
           
                vector<double> P1_1;
                  P1_1.push_back(0.000);
                  P1_1.push_back(0.026);
                vector<double> P1_2;
                  P1_2.push_back(0.983);
                  P1_2.push_back(1.000);
                vector<double> P2;
                  P2.push_back(0.377);
                  P2.push_back(0.422);
           
                ON.push_back(P1_1);
                ON.push_back(P1_2);
                ON.push_back(P2);
           
           
                vector<double> O1;
                  O1.push_back(0.520);
                  O1.push_back(0.870);
           
                OFF.push_back(O1);
           */ 


/*********************************************/
/************ END OF CONFIGURATION ***********/
/*********************************************/


     Double_t * enbins = new Double_t[numlogbins+1];
     for(int k=0;k<numlogbins+1;k++){
        enbins[k] = lowen*pow(highen/lowen,(double)k/numlogbins);
     }

     vector<vector <double> >::iterator iter;
     cout << "## ON regions: ";
     for(iter = ON.begin(); iter != ON.end(); ++iter){
        cout << "(" << (*iter)[0] << ", " << (*iter)[1] << ") ";
     }
     cout << endl;
     cout << "## OFF regions: ";
     for(vector< vector <double> >::iterator iter = OFF.begin(); iter != OFF.end(); iter++){
        cout << "(" << (*iter)[0] << ", " << (*iter)[1] << ") ";
     }
     cout << endl;

     cout << "## READING FILES... ";
    

     Double_t phase;
     Double_t hadronness;
     Float_t  theta2;
     Float_t  energy;
     UShort_t trigpat;
     //Long64_t millisec;


     TBranch  *b_phase;
     TBranch  *b_hadronness;
     TBranch  *b_theta2;
     TBranch  *b_energy;
     TBranch  *b_trigpat;
     //TBranch  *b_millisec;

     events->SetMakeClass(1);
     events->SetBranchAddress(phaseStr.Data(), &phase, &b_phase);
     events->SetBranchAddress("MHadronness.fHadronness", &hadronness, &b_hadronness);
     events->SetBranchAddress((energyCon+".fEnergy").Data(), &energy, &b_energy);
     events->SetBranchAddress((stereoCon+".fTheta2").Data(), &theta2, &b_theta2);
     events->SetBranchAddress("MTriggerPattern_2.fPrescaled", &trigpat, &b_trigpat);
     //events->SetBranchAddress("MTime_1.fTime.fMilliSec", &millisec, &b_millisec);

     cout << "OK" << endl;

     const Int_t numEntries = events->GetEntries();
     cout << "## NumEntries: " << numEntries << endl;
     const Int_t onepercent = numEntries/100.;

     TH1D ** lc = new TH1D*[numlogbins];
     for(int k=0;k<numlogbins;k++){
       lc[k]  = new TH1D(Form("lc_%d",k),"",nbins,0.,2.);
       if(legax){
         lc[k]->GetXaxis()->SetTitle("Phase");
         lc[k]->GetYaxis()->SetTitle("Counts");
         lc[k]->GetYaxis()->SetTitleOffset(1.2);
       }
     }

     //for(int i=0;i<lc000_050->GetXaxis()->GetNbins()+2;i++){
     //  cout << i << "\t: " << lc000_050->GetXaxis()->GetBinLowEdge(i) << "  " << lc000_050->GetXaxis()->GetBinUpEdge(i) << endl;
     //}

     Int_t scartH = 0;
     Int_t scartT = 0;
     Int_t scartE = 0;
     Int_t scartTrig = 0;

     //Double_t minphase = 2.0;
     //Double_t maxphase = -2.0;

     double *Non   = new double[numlogbins]();
     double *Noff  = new double[numlogbins]();
     double *NonP1 = new double[numlogbins]();
     double *NonP2 = new double[numlogbins]();

     for(Int_t i=0; i<numEntries; i++)
     {
         if(i%onepercent==0) cout<< "\r" << "Processing: " << ceil(i/(float)numEntries*100.) << "%" << flush;

         Long64_t localEntry = events->LoadTree(i);

         b_energy->GetEntry(localEntry);
         b_hadronness->GetEntry(localEntry);
         b_theta2->GetEntry(localEntry);
         b_phase->GetEntry(localEntry);
         b_trigpat->GetEntry(localEntry);

         //b_millisec->GetEntry(localEntry);

         if((energy<cuts.GetMinEnergy() ) || ( energy>cuts.GetMaxEnergy())){
             scartE++;
             continue;
         }

         //maxphase=(phase>maxphase)?phase:maxphase;
         //minphase=(phase<minphase)?phase:minphase;

         //cerr << energy << " H:" << hadronness << " T2:" << theta2 << " cutH:" << CrabEDCJeza(energy) << " cutT2:" << CrabEDCJeza(energy,1) << endl;


         if( ( theta2>cuts.Theta2(energy) ) || ( theta2<0 ) ){
             scartT++;
             continue;
         }
         if((hadronness>cuts.Hadronness(energy))){
             scartH++;
             continue;
         }
 
         if(trigpat!=128){
	     scartTrig++;
             continue;
         }

         for(int k=0;k<numlogbins;k++){
            if(energy  > enbins[k] && energy <= enbins[k+1]){
              lc[k]->Fill(phase);
              lc[k]->Fill(phase+1);
              Non[k] += IsPhaseInRegion(phase,ON)  ?1:0;
              Noff[k] += IsPhaseInRegion(phase,OFF) ?1:0;
              NonP1[k] += IsPhaseInRegion(phase,P1)  ?1:0;
              NonP2[k] += IsPhaseInRegion(phase,P2)  ?1:0;
            }
         }
     }
     int numPrintEntries = numEntries-scartTrig;
     cout << endl;
     cout << "## CUT EVENTS:" << endl;
     cout << "## (Energy)     (Theta2)     (Hadron.)     (Total cut)" << endl;
     cout << "## " << (Double_t)scartE/numPrintEntries << "  +  "
          << (Double_t)scartT/numPrintEntries << "  +  "
          << (Double_t)scartH/numPrintEntries << "  =  "
          << (Double_t)scartH/numPrintEntries+(Double_t)scartT/numPrintEntries+(Double_t)scartE/numPrintEntries
          << endl;
     cout << "## Wrong trigger pattern: " << scartTrig << endl;
     cout << "## Surviving events: " <<   numPrintEntries-scartE-scartT-scartH << " (";

     ios copia(NULL);
     copia.copyfmt(cout);

     cout << fixed << setprecision(2) << (1 - ((Double_t)scartH/numPrintEntries+(Double_t)scartT/numPrintEntries+(Double_t)scartE/numPrintEntries))*100;

     cout.copyfmt(copia);
     cout << "%)" << endl;


     cout << endl;
     gStyle->SetOptStat(0);
     TCanvas *can = new TCanvas("can","THisto Test");

     int grafaxis = ceil(sqrt(numlogbins));
     can->Divide(grafaxis,grafaxis);

     double * sP1  = new double[numlogbins]();
     double * sP2  = new double[numlogbins]();
     double * sTOT = new double[numlogbins]();


     double alfa = CalcOnOffRatio(ON,OFF);
     double alfaP1 = CalcOnOffRatio(P1,OFF);
     double alfaP2 = CalcOnOffRatio(P2,OFF);

     stringstream flusso;
     flusso << fixed << setprecision(2);

     TPaveText ** testo1 = new TPaveText*[numlogbins];
     TPaveText ** testo2 = new TPaveText*[numlogbins];

     TCanvas * can2;
     //if(plotsing) can2 = new TCanvas("can2","can2");

     for(int k=0;k<numlogbins;k++){
         can->cd(k+1);
         lc[k]->Draw();
         can->Update();
         GenerateRegions(ON,kGreen);
         GenerateRegions(OFF,kGray);
         lc[k]->Draw("sameB");
         lc[k]->SetFillColor(kBlue-3);
         lc[k]->SetTitle(Form("%0.f - %0.f GeV",enbins[k],enbins[k+1]));
         lc[k]->SetMarkerStyle(1);
         lc[k]->SetLineColor(kGray);
         lc[k]->Draw("sameE");

         sP1[k]  = CalcLima17(NonP1[k],Noff[k],alfaP1);
         sP2[k]  = CalcLima17(NonP2[k],Noff[k],alfaP2);         // DA METTERE A POSTO!
         sTOT[k] = CalcLima17(Non[k],Noff[k],alfa);   //CalcHSig(lc000_050, ON, OFF);    


         double eradfoP1  = (NonP1[k]-alfaP1*Noff[k])/sqrt(Noff[k]*alfaP1);
         double eradfoP2  = (NonP2[k]-alfaP2*Noff[k])/sqrt(Noff[k]*alfaP2);
         double eradfoTOT = (NonP1[k]+NonP2[k]-alfa*Noff[k])/sqrt(Noff[k]*alfa);


         testo1[k] = new TPaveText(0.80,0.85,0.95,0.97,"NDC");
         testo2[k] = new TPaveText(0.80,0.71,0.95,0.83,"NDC");

         cout << Form("( %0.f < E < %0.f Gev ):",enbins[k],enbins[k+1]) << endl;


         //cerr << "      " << NonP2[k] << endl;
         //cerr << "      " << Noff[k] << endl;
         //cerr << "      " << alfaP2 << endl;
         //cerr << "      " << CalcLima17(NonP2[k],Noff[k],alfaP2) << endl;
 
         flusso << "  P1:  " << sP1[k];
         testo1[k]->AddText((flusso.str()+"#sigma").c_str());
         cout << (flusso.str()) << endl;
         flusso.str("");
         flusso << "  P2:  " << sP2[k];
         testo1[k]->AddText((flusso.str()+"#sigma").c_str());
         cout << (flusso.str()) << endl;
         flusso.str("");
         flusso << "  P1+P2: " << sTOT[k];
         testo1[k]->AddText((flusso.str()+"#sigma").c_str());
         cout << (flusso.str()) << endl;
         flusso.str("");
/*
         flusso << "P1: " << eradfoP1 << " ";
         testo2->AddText((flusso.str()+"S/#sqrt{B}").c_str());
         cout << (flusso.str()) << endl;
         flusso.str("");
         flusso << "P2: " << eradfoP2 << " ";
         testo2->AddText((flusso.str()+"S/#sqrt{B}").c_str());
         cout << (flusso.str()) << endl;
         flusso.str("");
         flusso << "P1+P2: " << eradfoTOT << " ";
         testo2->AddText((flusso.str()+"S/#sqrt{B}").c_str());
         cout << (flusso.str()) << endl;
         flusso.str("");
*/
         //testo2->SetLabel("P2 Excess");
         flusso << "  EXC: " << int(NonP2[k]-alfaP2*Noff[k]) << " ";
         testo2[k]->AddText((flusso.str()).c_str());
         cout << (flusso.str()) << endl;
         flusso.str("");
         flusso << "  BKG: " << int(Noff[k]*alfaP2) << " ";
         testo2[k]->AddText((flusso.str()).c_str());
         cout << (flusso.str()) << endl;
         flusso.str("");
         flusso << "  E/B%: " << (NonP2[k]-alfaP2*Noff[k])/(Noff[k]*alfaP2)*100 << " ";
         testo2[k]->AddText((flusso.str()).c_str());
         cout << (flusso.str()) << endl;
         flusso.str("");

         //cout << "Eccesso P2 = " << (NonP2-alfaP2*Noff) << ",  fondo = " << Noff*alfaP2 << ", rapporto% = " << (NonP2-alfaP2*Noff)/(Noff*alfaP2)*100 << endl;
          
 
         testo1[k]->SetTextAlign(12);
         testo2[k]->SetTextAlign(12);    

         testo1[k]->Draw();
         testo2[k]->Draw();

         //can->Update();
         //TText * t1 = new TText(0.5,0.5,"SALVE");
         //t1->SetNDC(1);
         //t1->Draw();
         can->Modified();
         can->Update();

         //if(plotsing){
         //TPad * pad = (TPad*)((can->GetPad(k))->Clone());
         //can2->cd();
         //pad->Draw();
         //can2->Update();
         //can2->SaveAs(Form("subcanvas_%d.pdf",k));
         //can2->Clear();
         //}

     }


     can->SaveAs("canvas.pdf");
     can->SaveAs("canvas.C");

     return;
}




double CalcLima17(double Non, double Noff, double alfa){
  double significance = sqrt(     2*Non* log( (1+alfa)/alfa * ( Non / (Non+Noff) ) )   + 
                                  2*Noff*log( (1+alfa)      * ( Noff/ (Non+Noff) ) )   );
  if(Non>alfa*Noff) return significance;
  else              return -significance;
}



/*
double CalcHSig(TH1D * isto, vector<vector <double> > on, vector<vector <double> > off){
  double Non =  0;
  double Noff = 0;
  double won = 0;
  double woff = 0;
  for(vector< vector <double> >::iterator iter = on.begin(); iter != on.end(); iter++){
    int lowbin = isto->GetXaxis()->FindBin( (*iter)[0] );
    int upbin  = isto->GetXaxis()->FindBin( (*iter)[1] );
    Non += isto->Integral(lowbin,upbin);
    Non -= isto->GetBinContent(lowbin)  *
           (((*iter)[0])-isto->GetXaxis()->GetBinLowEdge(lowbin)) /
           (isto->GetXaxis()->GetBinWidth(lowbin));
    Non -= isto->GetBinContent(upbin)   *
           (isto->GetXaxis()->GetBinUpEdge(upbin)-((*iter)[1]))  /
           (isto->GetXaxis()->GetBinWidth(upbin));
    won += ((*iter)[1] - (*iter)[0]);
  }
  for(vector< vector <double> >::iterator iter = off.begin(); iter != off.end(); iter++){
    int lowbin = isto->GetXaxis()->FindBin( (*iter)[0] );
    int upbin  = isto->GetXaxis()->FindBin( (*iter)[1] );
    Noff += isto->Integral(lowbin,upbin);
    Noff -= isto->GetBinContent(lowbin)  *
            (((*iter)[0])-isto->GetXaxis()->GetBinLowEdge(lowbin)) /
            (isto->GetXaxis()->GetBinWidth(lowbin));
    Noff -= isto->GetBinContent(upbin)   *
            (isto->GetXaxis()->GetBinUpEdge(upbin)-((*iter)[1]))  /
            (isto->GetXaxis()->GetBinWidth(upbin));
    woff += ((*iter)[1] - (*iter)[0]);
  }
  double alfa = won/woff;
  return CalcLima17(Non,Noff,alfa);
}
*/

bool IsPhaseInRegion(double phase, vector<vector <double> > regions){
  for(vector<vector <double> >::iterator iter = regions.begin(); iter != regions.end(); iter++){
     if( (phase >= (*iter)[0]) && (phase < (*iter)[1]) ){
       return 1;
     }
  }
  return 0;
}


double CalcOnOffRatio(vector<vector <double> > on, vector<vector <double> > off){
  double won = 0;
  double woff = 0;
  for(vector< vector <double> >::iterator iter = on.begin(); iter != on.end(); iter++){
    won += (*iter)[1] - (*iter)[0];
  }
  for(vector< vector <double> >::iterator iter = off.begin(); iter != off.end(); iter++){
    woff += (*iter)[1] - (*iter)[0];
  }

  return won/woff;
}

void GenerateRegions(vector <vector <double> > vecR, unsigned int kColor){
    TBox ** regions = new TBox*[2*vecR.size()];
    for(unsigned int i=0;i<vecR.size();i++){
      regions[i] = new TBox(vecR[i][0],gPad->GetUymin(),vecR[i][1],gPad->GetUymax());
      regions[i]->SetFillColor(kColor);
      regions[i]->SetFillStyle(3001);

      regions[vecR.size()+i] = new TBox(vecR[i][0]+1,gPad->GetUymin(),vecR[i][1]+1,gPad->GetUymax());
      regions[vecR.size()+i]->SetFillColor(kColor);
      regions[vecR.size()+i]->SetFillStyle(3001);

      regions[i]->Draw();
      regions[vecR.size()+i]->Draw();
    }
}

 





