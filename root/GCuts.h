#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

int nrighe(ifstream& file){
  int n_misure = 0;
  
  int capacita = 256;
  bool letto_bene;
  do{
    letto_bene = 1;
    char * linea = new char[capacita];
    for(int i=0;i<capacita;i++){
      linea[i] = 0;
    }
    while( file.getline(linea,capacita) ){
      if(linea[0] != 0){
          n_misure++;
      }
    }
    if(linea[capacita-2] != 0){
      capacita = 2*capacita;
      letto_bene = 0;
      n_misure = 0;
    }
    file.clear();
    file.seekg(0);
  }while(!letto_bene);

  return n_misure;
}

class GCuts{
  private:
    double * low_ebins;
    double * up_ebins;
    double * had_cuts;
    double * th2_cuts;
    double * dispdiff2_cuts;

    char cutsfilename[1024];
    char dispfilename[1024];

    int nbins;
    bool init=0;
    bool dispdiff2init=0;
  public:
    GCuts();
    GCuts(const char * nomefile);
    GCuts(const char * nomefile, const char * nomefile2);

    void SetCuts(const char * nomefile);
    void SetDispDiff2Cuts(const char * nomefile);
    void PrintCuts();
    void ClearCuts();
    void ClearDispDiff2Cuts();

    int  GetNBins();
    bool GetInitState();
    bool GetDispDiff2InitState();

    const char* GetCutsFileName();
    const char* GetDispFileName();

    double GetMinEnergy();
    double GetMaxEnergy();

    double GetLowBinEdge(int nbin);
    double GetUpBinEdge(int nbin);

    double Hadronness(double energy);
    double Theta2(double energy);
    double DispDiff2(double energy);

    double GetBinHadronness(int j);
    double GetBinTheta2(int j);
    double GetBinDispDiff2(int j);

    int WhichBin(double energy);
};

void GCuts::PrintCuts(){
  if(init==0){
    cout << "Cuts not initializated" << endl;
    return;
  }
  if(dispdiff2init==0){
    cout << "### Reading cuts from: " << cutsfilename << endl; 
    cout << "#        Energy bin         Theta2     Hadronness" << endl;
    ifstream copia;
    copia.copyfmt(cout);
    for(int i=0;i<nbins;i++){
      cout << "# ";
      cout << fixed << setprecision(2) << setw(8) << low_ebins[i] << "   "
           << fixed << setprecision(2) << setw(8) << up_ebins[i]  << "   "
           << fixed << setprecision(2) << setw(8) << th2_cuts[i]  << "   "
           << fixed << setprecision(2) << setw(8) << had_cuts[i]  << endl;
    }
    cout.copyfmt(copia);
    cout << endl;
  }
  else{
    cout << "### Reading cuts from: " << cutsfilename << ", and dispdiff2 from: " << dispfilename << endl;
    cout << "#        Energy bin         Theta2     Hadronness DispDiff2" << endl;
    ifstream copia;
    copia.copyfmt(cout);
    for(int i=0;i<nbins;i++){
      cout << "# ";
      cout << fixed << setprecision(2) << setw(8) << low_ebins[i] << "   "
           << fixed << setprecision(2) << setw(8) << up_ebins[i]  << "   "
           << fixed << setprecision(2) << setw(8) << th2_cuts[i]  << "   "
           << fixed << setprecision(2) << setw(8) << had_cuts[i]  << "   "
           << fixed << setprecision(2) << setw(8) << dispdiff2_cuts[i] << endl;
    }
    cout.copyfmt(copia);
    cout << endl;
  }
}

void GCuts::ClearCuts(){
  if(init==1){
    delete [] low_ebins;
    delete [] up_ebins;
    delete [] had_cuts;
    delete [] th2_cuts;
    strcpy(cutsfilename,"");
  }
  if(dispdiff2init==1){
    delete [] dispdiff2_cuts;
    strcpy(dispfilename,"");
  }
}

void GCuts::ClearDispDiff2Cuts(){
  if(dispdiff2init==1){
    delete [] dispdiff2_cuts;
    strcpy(dispfilename,"");
  }
}

const char* GCuts::GetCutsFileName(){
  if(init==0){
    cout << "Cuts not initializated" << endl;
    return "";
  }
  return cutsfilename;  
}

const char* GCuts::GetDispFileName(){
  if(dispdiff2init==0){
    cout << "Dispdiff2 cuts not initializated" << endl;
    return "";
  }
  return dispfilename;  
}

void GCuts::SetCuts(const char * nomefile){
  if(init==1){
    ClearCuts();
  }
  ifstream file;
  file.open(nomefile);
  if( !(file.good()) ){
    init = 0;
    cerr << nomefile << ": error" << endl;
    return;
  }

  nbins=nrighe(file);

  low_ebins = new double[nbins];
  up_ebins  = new double[nbins];
  had_cuts  = new double[nbins];
  th2_cuts  = new double[nbins];

  for(int i=0;i<nbins;i++){
     file >> low_ebins[i] >> up_ebins[i] >> th2_cuts[i] >> had_cuts[i];
  }
  file.close();

  strcpy(cutsfilename,nomefile);
  init=1;
  PrintCuts();
}

void GCuts::SetDispDiff2Cuts(const char * nomefile){
  if(init==0){
    cerr << "error: set theta2/hadronness cuts first" << endl;
    return;
  }
  if(dispdiff2init==1){
    ClearDispDiff2Cuts();
  }
  ifstream file2;
  file2.open(nomefile);
  if( !(file2.good()) ){
    dispdiff2init = 0;
    cerr << nomefile << ": error" << endl;
    return;
  }

  int nbins2=nrighe(file2);
  if(nbins2!=nbins){
    cerr << dispfilename << ": error, energy bins are different" << endl;
    dispdiff2init=0;
    return;
  }

  double * low_ebins2 = new double[nbins2];
  double * up_ebins2  = new double[nbins2];
  dispdiff2_cuts      = new double[nbins2];

  for(int i=0;i<nbins2;i++){
     file2 >> low_ebins2[i] >> up_ebins2[i] >> dispdiff2_cuts[i];
  }
  file2.close();

  bool flagbuono=1;
  for(int i=0;i<nbins;i++){
     if( (low_ebins[i]-low_ebins2[i]>1e-1) || (up_ebins[i]-up_ebins2[i]>1e-1) ){
       cerr << dispfilename << ": error, energy bins are different" << endl;
       flagbuono=0;
       break;
     }
  }

  delete [] low_ebins2;
  delete [] up_ebins2;

  if(flagbuono){
    strcpy(dispfilename,nomefile);
    dispdiff2init=1;
    PrintCuts();
  }
  else{
    ClearDispDiff2Cuts();
  }
}

double GCuts::Hadronness(double energy){
  if(init==0){
    cout << "Cuts not initializated" << endl;
    return 100;
  }
  int j=-1;
  for(int i=0;(i<nbins)&&(low_ebins[i]<energy);i++) j++;
  if(j==-1   || energy>up_ebins[nbins-1]) return -100;

  return had_cuts[j];
}     

double GCuts::Theta2(double energy){
  if(init==0){
    cout << "Cuts not initializated" << endl;
    return 100;
  }
  int j=-1;
  for(int i=0;(i<nbins)&&(low_ebins[i]<energy);i++) j++;
  if(j==-1   || energy>up_ebins[nbins-1]) return -100;

  return th2_cuts[j];
}

double GCuts::DispDiff2(double energy){
  if(init==0){
    cout << "Cuts not initializated" << endl;
    return 100;
  }
  if(dispdiff2init==0){
    cout << "DispDiff2 cuts not initializated" << endl;
    return 100;
  }
  int j=-1;
  for(int i=0;(i<nbins)&&(low_ebins[i]<energy);i++) j++;
  if(j==-1   || energy>up_ebins[nbins-1]) return -100;

  return dispdiff2_cuts[j];
}

double GCuts::GetBinHadronness(int j){
  if(init==0){
    cout << "Cuts not initializated" << endl;
    return 100;
  }
  if(j==-1   || (j > nbins-1)) return -100;

  return had_cuts[j];
}     

double GCuts::GetBinTheta2(int j){
  if(init==0){
    cout << "Cuts not initializated" << endl;
    return 100;
  }
  if(j==-1   || (j > nbins-1)) return -100;

  return th2_cuts[j];
}

double GCuts::GetBinDispDiff2(int j){
  if(init==0){
    cout << "Cuts not initializated" << endl;
    return 100;
  }
  if(dispdiff2init==0){
    cout << "DispDiff2 cuts not initializated" << endl;
    return 100;
  }
  if(j==-1   || (j > nbins-1)) return -100;

  return dispdiff2_cuts[j];
}



double GCuts::GetMinEnergy(){
  if(init==0){
    cerr << "Cuts not initializated" << endl;
    return -1;
  }

  return low_ebins[0];
}

double GCuts::GetMaxEnergy(){
  if(init==0){
    cerr << "Cuts not initializated" << endl;
    return -1;
  }

  return up_ebins[nbins-1];
}

double GCuts::GetUpBinEdge(int nbin){
  if(init==0){
    cerr << "Cuts not initializated" << endl;
    return -1;
  }
  return (nbin<nbins)?up_ebins[nbin]:-1;
}

double GCuts::GetLowBinEdge(int nbin){
  if(init==0){
    cerr << "Cuts not initializated" << endl;
    return -1;
  }
  return (nbin<nbins)?low_ebins[nbin]:-1;
}

int  GCuts::GetNBins(){
  if(init==0){
    cerr << "Cuts not initializated" << endl;
  }
  return nbins;
}

bool GCuts::GetInitState(){
  return init;
}

bool GCuts::GetDispDiff2InitState(){
  return dispdiff2init;
}

int  GCuts::WhichBin(double energy){
  for(int i=0;i<nbins;i++){
    if(low_ebins[i] <= energy && up_ebins[i] > energy) return i;
  }
  return -1;
}

GCuts::GCuts(){
  init = 0;
  nbins = 0;
}

GCuts::GCuts(const char * nomefile){
  init = 0;
  nbins = 0;
  SetCuts(nomefile);
}

GCuts::GCuts(const char * nomefile, const char * nomefile2){
  init = 0;
  nbins = 0;
  SetCuts(nomefile);
  SetDispDiff2Cuts(nomefile2);
}

