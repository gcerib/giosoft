#!/usr/bin/python2.7

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm

import copy
import glob
import uproot

from astropy.table import Table

import warnings
import Reader

LAMBDA = 28.75416666666667
DEC    = 17.77027777777778
#DEC    = 22 + 52.1758/3600

Input = ("/remote/sumtdata/Userspace/giovanni/10_GEMINGA/upper/melibea20180813/tempo2/20170123*.root",)
#Input = ("/remote/sumtdata/Userspace/giovanni/13_CRAB/THISTOS_pul/20*.root",)

ticks = [15,30,45,60,90]

nr =     180
ntheta = 360

PointingPos = False
Disp = True

PlotDistributions = False

##################################
DEC    = DEC   /180.*np.pi
LAMBDA = LAMBDA/180.*np.pi

#alfa is the hour-angle from culmination
#westwards.
def phi(alfa,DEC,LAMBDA):
  """Azimuth in rad"""
  return np.arctan2( np.cos(DEC)*np.sin(alfa),
           np.sin(LAMBDA)*np.cos(DEC)*np.cos(alfa)
           - np.cos(LAMBDA)*np.sin(DEC) ) + np.pi 

def theta(alfa,DEC,LAMBDA):
  """Zd in rad"""
  return np.arccos ( np.cos(LAMBDA)*np.cos(DEC)*np.cos(alfa)
                     + np.sin(LAMBDA)*np.sin(DEC) )

def altitude(alfa,DEC,LAMBDA):
  """Altitude in rad."""
  return np.pi/2.0-theta(alfa,DEC,LAMBDA)


if PlotDistributions:
  fig = plt.figure(figsize=(8.5,5))
  ax =  fig.add_axes([0.05,0.10,0.50,0.80], polar=True)
  ax1 = fig.add_axes([0.65,0.60,0.3,0.3])
  ax2 = fig.add_axes([0.65,0.15,0.3,0.3])
  
  ax1.set_xlabel("ZD [deg]")
  ax2.set_xlabel("AZ [deg]")
else:
  fig = plt.figure(figsize=(6,6))
  ax  = fig.add_subplot(111,projection='polar')

t = np.linspace(0,2*np.pi,1000)

PHI   = phi(t,DEC,LAMBDA)
THETA = theta(t,DEC,LAMBDA)
ALT   = altitude(t,DEC,LAMBDA)

sproxalt = np.rint(np.sign(ALT)-np.sign(np.roll(ALT,1))).astype(int)
argsorg  = np.argmax(sproxalt)


PHI = np.roll(PHI,-argsorg)
THETA = np.roll(THETA,-argsorg)

PHI1   = PHI[THETA<=np.pi/2]
THETA1 = THETA[THETA<=np.pi/2]

PHI0   = PHI[THETA>=np.pi/2]
THETA0 = THETA[THETA>=np.pi/2]

rticksdeg = np.array(ticks)
rticksortho = 90*np.sin((rticksdeg/180.*np.pi))

ax.set_yticks(rticksortho)
ax.set_yticklabels(rticksdeg)

ax.set_rlim(0,90)
ax.set_theta_zero_location("N")
ax.set_theta_direction(-1)

ax.plot(PHI1,90*np.sin(THETA1),'-',color='navy')
ax.plot(PHI0,90*np.sin(THETA0),':',color='navy',alpha=0.5)


print "Reading root files from: (",
filenames = []
for path in Input:
 print path,', ',
 filenames += glob.glob(path)
print ")"

if PointingPos:
  branches = (('ZD','MPointingPos_1.fZd'),('AZ','MPointingPos_1.fAz'))
else:
  if Disp:
    branches = (('ZD','MStereoParDisp.fDirectionZd'),('AZ','MStereoParDisp.fDirectionAz'))
  else:
    branches = (('ZD','MStereoPar.fDirectionZd'),('AZ','MStereoPar.fDirectionAz'))


r = Reader.Reader(Input,branches)

ZD = r['ZD']/180.*np.pi
AZ = r['AZ']/180.*np.pi

if not PointingPos:
  AZ = np.remainder(AZ,2*np.pi)

ORTHOR = 90*np.sin(ZD)
WEIGHTS = 2*np.pi*90*np.sqrt(1-(ORTHOR/90)**2)/(ORTHOR/90)

r_edges = np.linspace(0, 90, nr+1)
theta_edges = np.linspace(0, 2*np.pi, ntheta+1)

H, _, _ = np.histogram2d(ORTHOR, AZ, [r_edges, theta_edges], weights=WEIGHTS)
Theta, R = np.meshgrid(theta_edges, r_edges)

J = np.zeros( tuple(x+1 for x in H.shape) )

J[:-1,:-1] = H
J[:-1,-1] = H[:,0]
J[-1,:-1] = H[0,:]
J[-1,-1] = H[0,0]

Hm = np.ma.masked_where(np.abs(J) <= 0, J)

cmap = copy.copy(cm.get_cmap('plasma'))
ax.pcolormesh(Theta, R, Hm,cmap=cmap,shading='gouraud',zorder=10)
ax.grid()

if PlotDistributions:
  ax1.set_ylim((0,0.20))
  ax2.set_ylim((0,0.02))
  
  ax1.set_yticks([0,.05,.1,.15,.2])
  ax1.set_yticklabels(['0','5','10','15','20'])
  ax1.text(x = 0.0, y = 1.01, s = r"$\cdot10^{-2}$", transform=ax1.transAxes)
  
  ax2.set_yticks([0,.005,.010,.015,.020])
  ax2.set_yticklabels(['0','5','10','15','20'])
  ax2.text(x = 0.0, y = 1.01, s = r"$\cdot10^{-3}$", transform=ax2.transAxes)

  H_zd, bins_zd, patches_zd = ax1.hist(ZD/np.pi*180,120,range=(0,30),density=True,histtype='stepfilled',edgecolor='black', linewidth=0.7071,color='slateblue')
  H_az, bins_az, patches_az = ax2.hist(AZ/np.pi*180,120,range=(90,270),density=True,histtype='stepfilled',edgecolor='black', linewidth=0.7071,color='slateblue')

plt.show()
fig.savefig('orthonew.png', dpi=300, bbox_inches='tight')
