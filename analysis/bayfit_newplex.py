#!/usr/bin/python2.7

import sys

import matplotlib as mpl
mpl.use('Agg')

import numpy as np
import emcee
import corner

import scipy.optimize as opt

import matplotlib.pyplot as plt

from naima import read_run,save_run
from naima.utils import validate_data_table
import naima.plot as naiplt

from astropy.io import ascii
from astropy.table import vstack

import traceback
import copy

from multiprocessing import Pool
from multiprocessing import cpu_count
import os
os.environ["OMP_NUM_THREADS"] = "1"

### CONFIGURATION

UseCorrelation    = True
UseOnlyFermi      = False
EnableEnergyScale = True

#MCMC stuff
#nwalk = 1200
#nburn = 1200
#nstep = 36000+nburn
nwalk = 500
nburn = 500
nstep = 4000+nburn


#Pivot Energy (GeV)
E0 = 1

#Range for Fermi points
frange = slice(0,None)

# Set up the backend
#filename = "fits/output_PLEXscale.h5"

# Set up a suffix
suffix = "PLEX_gio"


#Load MAGIC points and errors
magicP2 = np.loadtxt("magic_gio.txt")

mP2e  = magicP2[:,2]
mP2f  = magicP2[:,4]
mP2fe = magicP2[:,5]
mP2ee = magicP2[:,1:4:2].T
mP2ee[0] = mP2e-mP2ee[0]
mP2ee[1] = mP2ee[1]-mP2e

#Load Fermi points and errors
fermiP2 = np.loadtxt("fermi_gio.txt")

fP2e  = fermiP2[frange,0]
fP2f  = fermiP2[frange,1]
fP2ee = fermiP2[frange,2:4].T
fP2fe = fermiP2[frange,4:6].T

#Joint MAGIC and Fermi points
P2e = np.concatenate((fP2e,mP2e))
P2f = np.concatenate((fP2f,mP2f))

WHOMAGIC = np.concatenate((np.zeros_like(fP2e),np.ones_like(mP2e)))
#print WHOMAGIC

if(UseOnlyFermi):
  P2e = fP2e
  P2f = fP2f
  WHOMAGIC = np.zeros_like(fP2e)

#Fermi points are assumed to be indipendent
#The [0] is there because the array contains
#upper and lower errors (but they are equal)
fCOV = np.diag(fP2fe[0])**2

#Loading MAGIC correlation matrix
mCOR = np.loadtxt("magic_gio.cor")

#Converting it into the covariance matrix
mSIG = np.diag(mP2fe)
mCOV = np.dot(mSIG.T,np.dot(mCOR,mSIG))

#No correlation between MAGIC and Fermi
r_fCOV = fCOV.shape[0]
r_mCOV = mCOV.shape[0]

cross_mf1 = np.zeros((r_fCOV,r_mCOV))
cross_mf2 = cross_mf1.T

#Building the joint matrix and its inverse
COV = np.block([ [fCOV, cross_mf1], [cross_mf2, mCOV] ])

if(UseOnlyFermi):
  COV = fCOV

INVCOV = np.linalg.inv(COV)

#print COV

def PL(x,logf0,gamma):
    return np.power(10.,logf0)*np.power(x/E0,gamma+2)

def expcut(x,pcutoff,beta):
    return np.exp( -np.power(x/np.abs(pcutoff),beta)  )

def PLEX(x,logf0,gamma,cutoff,beta):
  """Power-Law w. Exponential Cutoff function for the SED.
  """
  pcutoff = np.power(10,cutoff)
  plex = E0*E0/1e6*PL(x,logf0,gamma)*expcut(x,pcutoff,beta)
    
  return plex

def PLEX_logLike(theta, x, y, invcov, whomagic=None):
  """Log-Likelihood for the Power-Law on the SED,
     with inverse covariance matrix 'invcov'. See
     https://en.wikipedia.org/wiki/Maximum_likelihood_estimation#Non-independent_variables
  """
  global EnableEnergyScale
  if EnableEnergyScale:
    itheta = theta[:-1]
    scale  = theta[-1]
    logscale = np.power(10,np.float128(scale))
  else:
    itheta = theta

  gamma1 = theta[1]
  if gamma1 < -2 or gamma1>5:
    return -np.inf

  model = PLEX(x,*itheta)

  if EnableEnergyScale:
    nmagic = int(whomagic.sum())
    yscale  = y        * (whomagic*(logscale -1) + 1)
    sinvcov = copy.copy(invcov)
    sinvcov[-nmagic:,-nmagic:] = sinvcov[-nmagic:,-nmagic:]/logscale/logscale
  else:
    yscale  = y
    sinvcov = invcov

  v1 = (yscale-model)
  sigma2 = 1.0/np.diag(sinvcov)

  if UseCorrelation:
    return -0.5*np.dot(v1.T,np.dot(sinvcov,v1))
  else:
    return -0.5*np.sum((v1)**2/sigma2)

def PLEX_logPrior(theta):
  """Prior on the base flux and spectral index.
     for the Power-Law (on the SED).
  """
  global EnableEnergyScale
  if EnableEnergyScale:
    logf0, gamma, cutoff, beta, scale = theta
  else:
    logf0, gamma, cutoff, beta  = theta
    scale = 0
    
  if((-6 < logf0 < 0 ) and (-2 < gamma < 2)  and (-1 < cutoff < 2) and (0 < beta < 4) and (-2 < scale < 2)):
     return 0
  else:
     return -np.inf

def PLEX_logPost(theta,x,y,invcov,whomagic=None):
  """Posterior with the SED Power-Law.
  """
  lp = PLEX_logPrior(theta)
  if not np.isfinite(lp):
    return -np.inf, -np.inf
  if np.isnan(lp):
    return -np.inf, -np.inf
  ll = PLEX_logLike(theta,x,y,invcov,whomagic)
  return lp + ll, ll

#Using the MaxLikelihood method to find a first estimate
np.random.seed(27101991)
nll = lambda *args: -PLEX_logLike(*args)  #Negative logL
if EnableEnergyScale:
  initial = np.array([-3.25,-0.907,0.154,0.734,1.])
else:
  initial = np.array([-3.25,-0.907,0.154,0.734])

soln = opt.minimize(nll,initial,args=(P2e,P2f,INVCOV,WHOMAGIC))

#Maximum likelihood values.
if EnableEnergyScale:
  logf0_ml, gamma_ml, cutoff_ml, beta_ml, scale_ml = soln.x
else:
  logf0_ml, gamma_ml, cutoff_ml, beta_ml  = soln.x
  scale_ml = 1


print "Maximum likelihood estimates:"
print "  logf0     = %0.3g" % (logf0_ml)
print "  gamma     = %0.3g" % (gamma_ml)
print "  cutoff    = %0.3g" % (cutoff_ml)
print "  beta      = %0.3g" % (beta_ml)
if EnableEnergyScale:
  print "  scale     = %0.3g" % (scale_ml)
print
#We generate initial values for the walkers in a ball
#around the max log likelihood values.
ndim = len(soln.x)

#soln.x[-3]=-10
pos = soln.x + 1e-4*np.random.randn(nwalk,ndim)
#pos = initial + 1e-4*np.random.randn(nwalk,ndim)

#Let's run the thing.
print "Running the MCMC..."

#backend = emcee.backends.HDFBackend(filename)
#backend.reset(nwalk, ndim)

pool = Pool(cpu_count())
sampler = emcee.EnsembleSampler(nwalk, ndim, PLEX_logPost, args=(P2e, P2f, INVCOV, WHOMAGIC),pool=pool)#, backend=backend, threads=4)
sampler.run_mcmc(pos, nstep, progress=True)

print

#Adding labels to the sampler object for compatibility
#with naima plotting functions
sampler.labels = ["log10(f0)",r"$\Gamma$","log10(Ecut)",r"$\beta$"]
if EnableEnergyScale:
  sampler.labels += ["log10(scale)"]

index = np.unravel_index(np.argmax(sampler.lnprobability), sampler.lnprobability.shape)
MLp = sampler.chain[index]
ML = sampler.lnprobability[index]

print "Saving..."
np.savez_compressed("out_%s_chain.npz" %suffix, sampler.chain)
np.savetxt("out_%s_ml.txt" %suffix, MLp)
ofile = open("out_%s_ml.txt" %suffix,'a')
ofile.write('#ML = %0.6f' % ML)
ofile.close()

BIC = len(MLp) * np.log(len(P2e)) - 2 * ML

print "Check parameters:"
print "  LLmax = ", ML
print "  MLp   = ", MLp
print "  BIC   = ", BIC

#This is the autocorrelation time. nburn should be
#quite larger than this.
try:
  tau = sampler.get_autocorr_time(discard=nburn)
  print "  Tau   = ",tau.max()
  print "  accep = ",np.mean(sampler.acceptance_fraction)
  print
except:
  exc_info = sys.exc_info()
  print "Unexpected error:", sys.exc_info()[0]
  traceback.print_exception(*exc_info)
  pass
  

#We use naima to plot some walkers...
#The following code makes use of a slighly modified
#version of naima that allows to produce plots
#skipping the first nburn elements of the chain.
#To apply this modification, edit the file naima/plot.py
#of your naima installation and replace the definition of
#_plot_chain_func:
#   def _plot_chain_func(sampler, p, last_step=False):
#with:
#   def _plot_chain_func(sampler, p, last_step=False, skipburn=0):
#and few lines before replace:
#       traces = chain[:, :, p]
#with:
#       traces = chain[:, skipburn:, p]
#This is all just for plotting, the calculations
#are unaffected by it.
for i in xrange(ndim):
  fig = naiplt.plot_chain(sampler,i,skipburn=nburn)
  fig.savefig("out_%s_chain%02d.png" % (suffix,i),dpi=150,bbox_inches='tight')

#We get the samples.
flat_samples = sampler.get_chain(discard=nburn, flat=True)

#Useful options for the corner plot
corner_opts = {
    "quantiles": [0.16, 0.5, 0.84],
    "verbose": False,
    "show_titles": True,
    "labels": sampler.labels,
    "truths": MLp,
    "color" : '#111111',
    "bins"  : 200,
    #"title_fmt": '0.3g'
}

#Let's make the corner plot

oldlw = plt.rcParams["lines.linewidth"]
plt.rcParams["lines.linewidth"] = 0.7071
fig = corner.corner(flat_samples, **corner_opts)
fig.savefig("out_%s_corner.png" %suffix,dpi=150,bbox_inches='tight')
plt.rcParams["lines.linewidth"] = oldlw

MEDIANp = [ corner.quantile(flat_samples[:,i], [0.5])[0] for i in range(len(flat_samples[0])) ]
np.savetxt("out_%s_med.txt" %suffix, MEDIANp)

#We plot the spectrum and states of some walkers.
fig,ax = plt.subplots()
ax.set_yscale('log')
ax.set_xscale('log')


MYp = MLp  #MEDIANp

somex = np.logspace(np.log10(0.08),np.log10(100),1000)
inds = np.random.randint(len(flat_samples), size=400)
for ind in inds:
    sample = flat_samples[ind]
    if EnableEnergyScale:
      ax.plot(somex, PLEX(somex,*sample[:-1]), "C1", alpha=0.07071/2)
    else:
      ax.plot(somex, PLEX(somex,*sample), "C1", alpha=0.07071/2)

if EnableEnergyScale:
  ax.plot(somex,PLEX(somex,*MYp[:-1]), "r",linewidth=1,zorder=4)
else:
  ax.plot(somex,PLEX(somex,*MYp), "r",linewidth=1,zorder=4)

ax.errorbar(fP2e,fP2f,xerr=fP2ee,yerr=fP2fe,fmt="ko",mfc="#ffffff",label="P2 Fermi/LAT", zorder=5)
ax.errorbar(mP2e,mP2f,xerr=mP2ee,yerr=mP2fe,fmt="bo",label="P2 MAGIC",zorder=6)
if EnableEnergyScale:
  ax.errorbar(mP2e,mP2f*10**(MYp[-1]),xerr=mP2ee,yerr=mP2fe*10**(MYp[-1]),fmt="o",color="navy",label="P2 MAGIC - scaled",zorder=7,alpha=0.5)

ax.legend(fontsize=14)
ax.set_xlabel(r"Energy [GeV]")
ax.set_ylabel(r"$E^2$dN/dE [${\rm TeV}\,{\rm cm}^{-2}\,{\rm s^{-1}}$]");
ax.set_ylim(1e-14,1e-9)

fig.savefig("out_%s_spectrum.pdf" %suffix,bbox_inches='tight')

#plt.show()
