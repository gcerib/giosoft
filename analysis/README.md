# Joint Fermi/LAT & MAGIC fits

This is a sample python script to produce joint fits of MAGIC and Fermi/LAT data. Once the data is loaded, it uses the methods presented in Appendix B.
