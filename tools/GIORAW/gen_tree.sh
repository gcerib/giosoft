#!/bin/bash

if [ ! -d CALIBRATED ]; then
  mkdir CALIBRATED
fi
for NIGHT in $(echo RAW/20*_*_* | awk '{gsub("RAW/","");print}'); do
  if [[ ! -d CALIBRATED/$NIGHT ]]; then
    mkdir CALIBRATED/$NIGHT
  fi
done
