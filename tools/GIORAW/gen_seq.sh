#!/bin/bash

if [ -e REGISTER.list ]; then
  echo -n > REGISTER.list
fi

for NTEL in {1..2}; do
  TEL="M$NTEL"
  for DAY in $(ls -f RAW); do
    if [ -d CALIBRATED/$DAY ]; then

      EXT="0"
      ls -f RAW/$DAY/*$TEL*.raw    &> /dev/null
      if [[ $? -eq 0 ]]; then
        export EXT="raw"
        #echo $EXT
      fi
      ls -f RAW/$DAY/*$TEL*.raw.gz &> /dev/null
      if [[ $? -eq 0 ]]; then
        export EXT="raw.gz"
        #echo $EXT
      fi
      ls -f RAW/$DAY/*$TEL*.root   &> /dev/null
      if [[ $? -eq 0 ]]; then
        export EXT="root"
        #echo $EXT
      fi

      #echo $EXT
      if [ "$EXT" == "0" ]; then
        echo "Error in $DAY $TEL: no raw files were recognized.";
        continue
      fi

      DSUPERRUN=$(ls -f1 RAW/$DAY/*$TEL*_D_*.$EXT | awk '{gsub("/"," ");gsub("_"," ");gsub("\\."," ");print($7)}' | awk '!seen[$0]++')
      PEDRUNS=$(ls -f1 RAW/$DAY/*$TEL*_P_*.$EXT | awk '{gsub("/"," ");gsub("_"," ");print($7)}' | awk '!seen[$0]++')
      CALRUNS=$(ls -f1 RAW/$DAY/*$TEL*_C_*.$EXT | awk '{gsub("/"," ");gsub("_"," ");print($7)}' | awk '!seen[$0]++')
      PEDRUNSCOUNT=$(wc -w <<< "${PEDRUNS}")
      CALRUNSCOUNT=$(wc -w <<< "${CALRUNS}")
      ARRAYPED=0
      ARRAYCAL=0
      if [[ $PEDRUNSCOUNT -gt 1  || $CALRUNSCOUNT -gt 1 ]]; then
        echo "*****************************************"
        echo "It looks that they took the full pedestal"
        echo "and calibration run more than once during"
        echo "the $DAY night!  Check the runbook,"
        echo "try and understand why!!"
        echo "*****************************************"
        echo "  PEDRUNSCOUNT = $PEDRUNSCOUNT"
        echo "  CALRUNSCOUNT = $CALRUNSCOUNT"
        echo
        if [[ ! $PEDRUNSCOUNT -eq $CALRUNSCOUNT  ]]; then
          echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
          echo "!        FOR THE HEAVEN'S SAKE !         !"
          echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
          echo
          echo "I don't  know what happened here.  Are you"
          echo "sure that all the files have been rsynced?"
          echo "Otherwise it  means that nasty things went"
          echo "on during datataking. CHECK THE RUNBOOK!"
          echo "I will stop here immediately."
          break
        fi;
        echo "I will separate your data into $PEDRUNSCOUNT sets."
      fi;
      read -r -a ARRAYPED <<< $( echo "$PEDRUNS")
      read -r -a ARRAYCAL <<< $( echo "$CALRUNS")
      ##echo $DAY
      #echo "${ARRAYPED[0]},   ${ARRAYPED[1]}"
      #continue;
      for (( I=0; I<$PEDRUNSCOUNT; I++)); do
          PEDRUN=${ARRAYPED[$I]}
          PEDRUNNUM=$( echo ${PEDRUN%.*} | tail -c +2 )
          CALRUN=${ARRAYCAL[$I]}
          for RUN in $DSUPERRUN; do
            RUNNUM=$(echo -n $RUN | tail -c +2)

            #echo; echo; echo; echo;
            #echo "$RUN  $RUNNUM  $PEDRUN  $PEDRUNNUM"

            if [[ $RUNNUM -lt $PEDRUNNUM ]]; then
               #echo "$RUNNUM < $PEDRUNNUM: skipping";
               continue;
            fi;

            if [[ $I -lt $[$PEDRUNSCOUNT-1] ]]; then
              if [[ $RUNNUM -gt $( echo ${ARRAYPED[$[$I+1]]%.*} | tail -c +2 ) ]]; then
                 #echo "$RUNNUM > $( echo ${ARRAYPED[$[$I+1]]%.*} | tail -c +2 ): skipping";
                 continue;
              fi;
            fi;


            #echo "### $RUN ###"
            SUBRUNS=$(echo $(ls -f1 RAW/$DAY/*$TEL*$RUN*.$EXT | awk '{gsub("_"," ");print($5)}'))
            SEQNAME="${RUN}_${TEL}"
            NIGHT=$(echo $DAY | awk {'gsub("_","-");print substr($0,1,10)'})
            FILENAME="CALIBRATED/$DAY/${SEQNAME}.seq"
            echo "Sequence: $SEQNAME"  > $FILENAME
            echo "Night: $NIGHT"      >> $FILENAME
            echo "Telescope: $NTEL"   >> $FILENAME
            echo                      >> $FILENAME
            echo "PedRuns: $PEDRUN"  >> $FILENAME
            echo "CalRuns: $CALRUN"  >> $FILENAME
            echo "DatRuns: $SUBRUNS"  >> $FILENAME
    
            #echo " -- $FILENAME : "
            #echo "Sequence: $SEQNAME"
            #echo "Night: $NIGHT"     
            #echo "Telescope: $NTEL"  
            #echo                     
            #echo "PedRuns: $PEDRUN" 
            #echo "CalRuns: $CALRUN"
            #echo "DatRuns: $SUBRUNS" 
            #echo; echo;
    
            echo "$DAY $RUN $TEL $EXT" >> REGISTER.list
          done
      done
    fi
  done
done
