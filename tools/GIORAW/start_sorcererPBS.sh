#!/bin/bash -l

#PBS -N sorcerer
#PBS -j oe
#PBS -t 1-60%15
#PBS -m abe
#PBS -M ceribell@mpp.mpg.de

#PBS -q long
#PBS -l mem=4096

source ~/.bashrc
cd /nfs/magic-buffer03/ceribell/13_CRAB/sorcerer

INPUTCARD="mataju"

echo "###"
echo "###PBS_JOBNAME:      $PBS_JOBNAME"
echo "###PBS_JOBID:        $PBS_JOBID"
echo "###PBS_ARRAYID: $PBS_ARRAYID"
echo     "./run_yauto.sh $INPUTCARD $PBS_ARRAYID"
./run_yauto.sh $INPUTCARD $PBS_ARRAYID
echo '###'
