#!/bin/bash

NIGHT=$1
RUN=$2
TELESCOPE=$3
TYPE=$4
INPUTCARD=$5
CALIBRATION=$6


SEQUENCE="${RUN}_${TELESCOPE}"

printinfo() {
  echo 'run_single.sh NIGHT RUN TELESCOPE TYPE?[raw/raw.gz/root] INPUTCARD CALIBRATION?[C/Y]'
  exit
}

if [ ! $6 ] || [ ! $5 ] || [ ! $4 ] || [ ! $3 ] || [ ! $2 ] || [ ! $1 ]; then
  printinfo
fi

#echo $TYPE
if [ "$TYPE" == "root" ]; then
  export EXT=""
elif [ "$TYPE" == "raw" ]; then
  export EXT="-raw";
elif [ "$TYPE" == "raw.gz" ]; then
  export EXT="-rawzip";
else
  printinfo
fi;

echo
echo
echo -n '**********'
date
echo '**********'
echo

cd CALIBRATED/$NIGHT

MYPEDESTAL=$(grep PedRuns ${SEQUENCE}.seq | awk '{gsub("\\.","  ");print($2)}')

if [ "$CALIBRATION" == "C" ]; then
 echo "--------------------------------"
 echo "- RUNNING SORCERER IN \"C\" MODE -"
 echo "--------------------------------"
 echo "      sorcerer -c -b -q -f $EXT -ff"
 echo "               --ind=../../RAW/$NIGHT/"
 echo "               --config=../../inputcard/$INPUTCARD/sorcerer_${TELESCOPE}.rc"
 echo "               --log=logC_${SEQUENCE}_${INPUTCARD}.log"
 echo "                  $SEQUENCE.seq"
 sorcerer -c -b -q -f $EXT -ff                                            \
          --ind=../../RAW/$NIGHT/                                   \
          --config=../../inputcard/$INPUTCARD/sorcerer_${TELESCOPE}.rc \
          --log=log_C_${SEQUENCE}_${INPUTCARD}.log                     \
             $SEQUENCE.seq
ln -s scalib${SEQUENCE}.root scalib_${NIGHT}_${MYPEDESTAL}_${TELESCOPE}_${INPUTCARD}.root
elif [ "$CALIBRATION" == "Y" ]; then
 echo "--------------------------------"
 echo "- RUNNING SORCERER IN \"Y\" MODE -"
 echo "--------------------------------"
 echo "      sorcerer -y -b -q -f $EXT -ff"
 echo "               --ind=../../RAW/$NIGHT/"
 echo "               --config=../../inputcard/$INPUTCARD/sorcerer_${TELESCOPE}.rc"
 echo "               --calibname=scalib_${NIGHT}_${MYPEDESTAL}_${TELESCOPE}_${INPUTCARD}.root"
 echo "               --log=logY_${SEQUENCE}_${INPUTCARD}.log"
 echo "                  $SEQUENCE.seq"
 sorcerer -y -b -q -f $EXT -ff                                            \
          --ind=../../RAW/$NIGHT/                                      \
          --config=../../inputcard/$INPUTCARD/sorcerer_${TELESCOPE}.rc \
          --calibname=scalib_${NIGHT}_${MYPEDESTAL}_${TELESCOPE}_${INPUTCARD}.root   \
          --log=log_Y_${SEQUENCE}_${INPUTCARD}.log                     \
             $SEQUENCE.seq
else
 printinfo
fi

echo
echo -n '**********'
date
echo '**********'
echo
echo

cd ../..
