#!/bin/bash

mkdir -p RSYNC

if [ ! -e wait__RSYNC ]; then
  echo "Nothing in the queue."
  exit 1;
fi;

if [ -e RSYNC.lock ]; then
  echo -n "LOCKED: "
  cat RSYNC.lock
  exit 1;
fi;

touch RSYNC.lock
head -n 1 wait__RSYNC >> RSYNC.lock
OGGETTO=$(awk '{print($1)}' RSYNC.lock)
NOTTE=$(awk '{print($2)}' RSYNC.lock)
tail -n +2 wait__RSYNC > temp; mv temp wait__RSYNC

NOMEFILE="RSYNC_${OGGETTO}__${NOTTE}.list"

cd RSYNC
rsync -PLauhve ssh --include-from=../$NOMEFILE username@mic.magic.pic.es:/ .
if [[ ! $? -eq 0 ]];
  then
    echo "WARNING: RSYNC had some problems! There may be missing data."
    echo "Inspect the log and, if needed, start the process again."
    cd ..
    cat RSYNC.lock >> problems__RSYNC
else
    cd ..
    cat RSYNC.lock >> download__RSYNC
fi

for i in $(find . -type d);
  do
    chmod 760 $i
done
chmod 760 .
rm RSYNC.lock
