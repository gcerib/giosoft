#!/bin/bash

if [ ! $1 ]; then
  echo "No object has been specified."
  echo "  ./down_rsync.sh OBJECT NIGHT [RUNS]"
  exit 1;
fi

if [ ! $2 ]; then
  echo "No night has been specified."
  echo "  ./down_rsync.sh OBJECT NIGHT [RUNS]"
  exit 1;
fi

mkdir -p RSYNC

OGGETTO=$1
NOTTE=$2

NOMEFILE="RSYNC_${OGGETTO}__${NOTTE}.list"

 +() { echo "+ $1" >> $NOMEFILE; }
++() {
  CARTELLE=$(echo $1 | awk '{gsub("/"," ");print}')
  PREFISSO="/"
  for i in $CARTELLE;
   do
    PREFISSO="${PREFISSO}${i}/"
    + $PREFISSO
   done;
}

if [ -e $NOMEFILE ];
  then
    echo "File $NOMEFILE already exists!"
    echo "Probably this one is already in the waiting queue:"
    ls wait__RSYNC
    if [[ ! $? -eq 0 ]];
      then
       echo "Damn! probably the process was interrupted. Resume it manually."
    fi
    exit 1;
fi
    
++ /nfs/magic/DataCenter/System/CCdata
 + /nfs/magic/DataCenter/System/CCdata/M1
 + /nfs/magic/DataCenter/System/CCdata/M2

++ /pnfs/pic.es/data/magic/Data/RAW
 + /pnfs/pic.es/data/magic/Data/RAW/M1
 + /pnfs/pic.es/data/magic/Data/RAW/M2
 + /pnfs/pic.es/data/magic/Data/RAW/M1/$OGGETTO
 + /pnfs/pic.es/data/magic/Data/RAW/M2/$OGGETTO

MESE=$(echo $NOTTE | cut -c1-7)
+ /nfs/magic/DataCenter/System/CCdata/M1/$MESE
+ /nfs/magic/DataCenter/System/CCdata/M2/$MESE
+ /nfs/magic/DataCenter/System/CCdata/M1/$MESE/$NOTTE
+ /nfs/magic/DataCenter/System/CCdata/M2/$MESE/$NOTTE
if [ "$#" -eq "2" ]
then
+ /nfs/magic/DataCenter/System/CCdata/M1/$MESE/$NOTTE/**$OGGETTO**
+ /nfs/magic/DataCenter/System/CCdata/M2/$MESE/$NOTTE/**$OGGETTO**
fi
for (( i=3; i<=$#; i++ ))
do
+ /nfs/magic/DataCenter/System/CCdata/M1/$MESE/$NOTTE/**${!i}**$OGGETTO**
+ /nfs/magic/DataCenter/System/CCdata/M2/$MESE/$NOTTE/**${!i}**$OGGETTO**
done

+ /nfs/magic/DataCenter/System/CCdata/M1/$MESE/$NOTTE/**.rbk

+ /pnfs/pic.es/data/magic/Data/RAW/M1/$OGGETTO/$NOTTE
+ /pnfs/pic.es/data/magic/Data/RAW/M2/$OGGETTO/$NOTTE
if [ "$#" -eq "2" ]
then
+ /pnfs/pic.es/data/magic/Data/RAW/M1/$OGGETTO/$NOTTE/**$OGGETTO**
+ /pnfs/pic.es/data/magic/Data/RAW/M2/$OGGETTO/$NOTTE/**$OGGETTO**
fi
for (( i=3; i<=$#; i++ ))
do
+ /pnfs/pic.es/data/magic/Data/RAW/M1/$OGGETTO/$NOTTE/**${!i}**$OGGETTO**
+ /pnfs/pic.es/data/magic/Data/RAW/M2/$OGGETTO/$NOTTE/**${!i}**$OGGETTO**
done

echo '- *' >> $NOMEFILE

#######################################################################

if [ -e RSYNC.lock ];
   then
     echo "Something is already being downloaded here!"
     echo "Content of RSYNC.lock: "
     cat RSYNC.lock
     echo "I add it to the queue for later download."
     echo "$OGGETTO   $NOTTE" >> wait__RSYNC
     echo "Current queue: "
     cat wait__RSYNC
     echo
     exit 2;
fi;


touch RSYNC.lock;
echo "$OGGETTO   $NOTTE" >> RSYNC.lock
cd RSYNC
rsync -PLauhve ssh --include-from=../$NOMEFILE username@mic.magic.pic.es:/ .
if [[ ! $? -eq 0 ]];
  then
    echo "WARNING: RSYNC had some problems! There may be missing data."
    echo "Inspect the log and, if needed, start the process again."
    cd ..
    cat RSYNC.lock >> problems__RSYNC
else
    cd ..
    cat RSYNC.lock >> download__RSYNC
fi

for i in $(find . -type d);
  do
    chmod 760 $i
done
chmod 760 .
rm RSYNC.lock
