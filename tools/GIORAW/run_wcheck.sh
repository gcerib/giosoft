#!/bin/bash

NIGHT=$1
RUN=$2
TELESCOPE=$3
TYPE=$4
INPUTCARD=$5

if [ ! $5 ] || [ ! $4 ] || [ ! $3 ] || [ ! $2 ] || [ ! $1 ]; then
  echo "ERROR: insufficient parameters."
  echo " run_wcheck.sh NIGHT RUN TELESCOPE TYPE INPUTCARD"
  exit 1
fi

if [[ ! -d CALIBRATED/$NIGHT ]]; then
  echo "ERROR: $NIGHT does not exist in CALIBRATED."
  echo "Did you run gen_tree.sh?"
  exit 1
fi

if [[ ! -e CALIBRATED/$NIGHT/${RUN}_${TELESCOPE}.seq ]]; then
  echo "ERROR: no ${RUN}_${TELESCOPE}.seq in $NIGHT."
  echo "Did you run gen_seq.sh?"
  exit 1
fi

if [[ ! -d inputcard/$INPUTCARD ]]; then
  echo "ERROR: inputcard group $INPUTCARD does not exist."
  exit 1
fi

if [[ ! -e inputcard/$INPUTCARD/sorcerer_${TELESCOPE}.rc ]]; then
  echo "ERROR: no sorcerer_${TELESCOPE}.rc in $INPUTCARD."
  exit 1
fi

ls RAW/$NIGHT/20*.$TYPE   &>  /dev/null
if [ "$?" != "0" ]; then
  echo "ERROR: there are no $TYPE files in /RAW/$NIGHT."
  exit 1
fi

############## ENVIRONMENT SEEMS SANE, PROCEEDING WITH SORCERER

SEQUENCE="${RUN}_${TELESCOPE}"
MYPEDESTAL=$(grep PedRuns CALIBRATED/$NIGHT/${SEQUENCE}.seq | awk '{gsub("\\.","  ");print($2)}')

if [[ ! -e CALIBRATED/$NIGHT/scalib_${NIGHT}_${MYPEDESTAL}_${TELESCOPE}_${INPUTCARD}.root ]]; then
  echo "$NIGHT has no scalib file yet for $TELESCOPE with $INPUTCARD and pedestal ${MYPEDESTAL}."
  LOCKFILE="lock__${NIGHT}_${MYPEDESTAL}_${TELESCOPE}_${INPUTCARD}"
  #sleep .$[ $RANDOM*1000/32767 ]
  SLEEP1=$(( ( $RANDOM % 10 ) +1 ))
  echo -n "SLEEP: $SLEEP1, "
  sleep $SLEEP1
  SLEEP2=$[ $RANDOM*1000/32767 ]
  echo "SLEEP: $SLEEP2"
  sleep .$SLEEP2
  if [[ ! -e $LOCKFILE ]]; then
    touch $LOCKFILE
    echo "$NIGHT $RUN $TELESCOPE $TYPE $INPUTCARD C" >> $LOCKFILE
    echo "I (this instance of the script) will run sorcerer -c for this night."
    sleep 10
    ./run_single.sh $NIGHT $RUN $TELESCOPE $TYPE $INPUTCARD C
    mv $LOCKFILE un$LOCKFILE
  else
    echo "Another instance of the script is running sorcerer -c for this night."
    echo "I will kindly wait for the other one to finish before continuing."
    while [[ -e $LOCKFILE ]]; do
      sleep 1
    done
  fi
fi

./run_single.sh $NIGHT $RUN $TELESCOPE $TYPE $INPUTCARD Y
