#!/bin/bash

RED='\033[0;31m'
BLU='\033[0;34m'
GIA='\033[0;33m'
NC='\033[0m'

for NOTTE in $(ls -d1 20??_??_??);
 do
  MESE=$(echo $NOTTE | cut -c 1-7)
  cd $NOTTE;
    echo "${NOTTE}...   "
    if [ -f merpp2.com ];
     then
      echo -e "   ${BLU}merpp2.com${NC}      exists. Reports may have already been merged!"
      rm merpp2.com
    fi
    if [ -f start_merpp2.sh ];
     then
      echo -e "   ${BLU}start_merpp2.sh${NC} exists. Reports may have already been merged!"
      rm start_merpp2.sh
    fi
    ls MERGLOG* &>/dev/null
    if [ "$?" -eq "0" ]
     then
      echo -e "   ${GIA}MERGLOG*       ${NC} exists. It really looks that reports have been merged! ${RED}Be VERY careful!${NC}"
    fi
    mkdir -p REPS
    cd REPS
      #echo "Linking $NOTTE M1 reports..."
      ln -s /nfs/magic/DataCenter/System/CCdata/M1/$MESE/$NOTTE/*.rep . 2> /dev/null
      #echo "Linking $NOTTE M2 reports..."
      ln -s /nfs/magic/DataCenter/System/CCdata/M2/$MESE/$NOTTE/*.rep . 2> /dev/null
    cd ..
    ERR=0
    if [ -f missing_reports.txt ];
     then
      rm missing_reports.txt
    fi
    for i in $(ls 20*.root);
     do
      REPFILE=$(echo $i | awk '{gsub("_Y_","_D_");gsub(".root",".rep");print}');
      if [ ! -f REPS/$REPFILE ];
       then
         echo "   Report file REPS/$REPFILE does not exist!! Skipping."
         echo "   File $i will not have reports!"
         echo "$i" >> missing_reports.txt
         ERR=1
         continue
      fi
      echo "merpp -u REPS/$REPFILE $i --log=MERGLOG_${i}.log" >> merpp2.com;
      #echo -n "$(ls REPS/$REPFILE)"; echo   "    $(ls $i)"; done 
     done
    if [ "$ERR" -eq "1" ];
     then
      echo -e "$RED"
      echo "   --------------------------------------------"
      echo "   - WARNING: there were errors in $NOTTE -"
      echo "   --------------------------------------------"
      echo -e "$NC"
    fi

    cat <<EOF > start_merpp2.sh 
#!/bin/bash -l

#PBS -N merpp
#PBS -j oe
#PBS -t 1-$(wc -l merpp2.com | awk '{print($1)}')%20
#PBS -m abe
#PBS -M ceribell@mpp.mpg.de

#PBS -q short
#PBS -l mem=4096

source ~/.bashrc
cd $(pwd)

echo "###"
echo "###PBS_JOBNAME:      \$PBS_JOBNAME"
echo "###PBS_JOBID:        \$PBS_JOBID"
echo "###PBS_ARRAYID: \$PBS_ARRAYID"
echo     "\$(head -n \$PBS_ARRAYID merpp2.com | tail -n 1)"
echo      \$(head -n \$PBS_ARRAYID merpp2.com | tail -n 1)  > tmp_\${PBS_JOBNAME}.sh
bash tmp_\${PBS_JOBNAME}.sh
echo '###'
EOF
    chmod u+x start_merpp2.sh
  echo
  cd ..;
 done
