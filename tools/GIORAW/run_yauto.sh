#!/bin/bash

INPUTCARD=$1
REGISTER_LINE_NUMBER=$2

if [ ! $1 ] || [ ! $2 ]; then
  echo "Wrong invocation."
  echo " run_yauto.sh INPUTCARD REGISTER_LINE_NUMBER"
  exit 1
fi

MAXLINE=$(wc -l REGISTER.list | awk '{print($1)}')

if [ "$REGISTER_LINE_NUMBER" -gt "$MAXLINE" ]; then
  echo "ERROR: there are only $MAXLINE in REGISTER.list."
  echo "You requested line number $REGISTER_LINE_NUMBER."
  exit 1
fi

REGISTERLINE=$(head -n $REGISTER_LINE_NUMBER REGISTER.list | tail -n 1)
./run_wcheck.sh $REGISTERLINE $INPUTCARD
