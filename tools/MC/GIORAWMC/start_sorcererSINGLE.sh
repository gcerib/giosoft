#!/bin/bash -l

#SBATCH -J sorcerer
#SBATCH -o sorcerer_%A_%a.out
#SBATCH --array=1-2
#SBATCH -D ./
#SBATCH --mail-user ceribell@mpp.mpg.de
#SBATCH --mail-type=ALL
#SBATCH --export=ALL

####SBATCH -t 4:00:0

#SBATCH --partition=standard
#SBATCH --mem=8192
#SBATCH -A mpp

source ~/.bashrc

#INPUTCARD="mataju"

echo "###"
echo "###SLURM_JOB_NAME:      $SLURM_JOB_NAME"
echo "###SLURM_JOB_ID:        $SLURM_JOB_ID"
echo "###SLURM_ARRAY_JOB_ID:  $SLURM_ARRAY_JOB_ID"
echo "###SLURM_ARRAY_TASK_ID: $SLURM_ARRAY_TASK_ID"
#echo     "./run_yauto.sh $INPUTCARD $SLURM_ARRAY_TASK_ID"
#./run_yauto.sh $INPUTCARD $SLURM_ARRAY_TASK_ID
./run_singleMC.sh 2000_01_09  9000001 M${SLURM_ARRAY_TASK_ID} mc matajuMC3 Y
echo '###'
