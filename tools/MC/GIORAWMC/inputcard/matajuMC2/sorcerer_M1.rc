# This is an input card for calibrating M1 MCs with sorcerer
# it is appropriate for the DRS4 data after the change of sampling speed to 1.64GHz
# The assumed size of the readout window is 50 capacitors and SPE distribution with F2=1.232
# as all MC input cards it has many extra features disabled  (all interleaved updates, 
# time pedestal correction, DRS arrival time calibration)
# zero suppression is disabled

#####################################
# Calibration (-c mode)             #
#####################################

MJSorcererCalib.Geometry: MGeomCamMagicTwo

# bad pixels (excluded from the analysis)
MJSorcererCalib.BadPixelsFile: mjobs/badpixels_M2_20101001.rc
#MJSorcererCalib.BadPixelsFile: only_inner.rc
#MJSorcererCalib.BadPixelsFile: only_outer.rc

MJSorcererCalib.MappingFile: pixel_mapping_M1_DRS4.txt
# fundamental pedestal calculated from _P_ file (if not, beginning of the window is used
MJSorcererCalib.ExternalBaseline: yes
MJSorcererCalib.MSorcererPedFundCalc.Histogram_Pedestal_Nbins: 500
MJSorcererCalib.MSorcererPedFundCalc.Histogram_Pedestal_Min:  9500
MJSorcererCalib.MSorcererPedFundCalc.Histogram_Pedestal_Max: 10500
MJSorcererCalib.MSorcererPedFundCalc.First: 0
MJSorcererCalib.MSorcererPedFundCalc.Last: 49

# pseudo-pulses which appear in DRS4
#MJSorcererCalib.FindPseudoPulses: yes
#MJSorcererCalib.MSorcererLocateSpikes.Threshold: 100

#MJSorcererCalib.DRS4SpikePosJS: yes
#MJSorcererCalib.MSorcererDRS4SpikePosJS.SpikePosPlus: 75
#MJSorcererCalib.MSorcererDRS4SpikePosJS.SpikePosMinus: 949

# corrects the baseline depending on the time to previous capacitor reading 
MJSorcererCalib.MSorcererBaselineCalc.DoTimeCorr: no
MJSorcererCalib.MSorcererBaselineCalc.RemoveSpikes: yes
MJSorcererCalib.MSorcererBaselineCalc.SpikenessCut: 0.32

# extractor parameters
#MJSorcererCalib.MSorcererExtractorSW.PedestalSize: 16
MJSorcererCalib.MSorcererExtractorSW.PedestalSize: 0
MJSorcererCalib.MSorcererExtractorSW.PedestalOff: 0
MJSorcererCalib.MSorcererExtractorSW.WindowSize: 5
MJSorcererCalib.MSorcererExtractorSW.First: 0
#MJSorcererCalib.MSorcererExtractorSW.Last: 1000
MJSorcererCalib.MSorcererExtractorSW.Last: 49

# F2Factor
MJSorcererCalib.MSorcererFillFFactor.F2Factor: 1.2
# Should we use Nphe averaged over all pixels (if no: NPhe calculated for each pixel from F-Factor method)
MJSorcererCalib.MSorcererFillFFactor.UseMeanNphe: yes

# ranges of histograms for individual pixels
MJSorcererCalib.MSorcererFillFFactor.Histogram_Signal_Nbins: 200
MJSorcererCalib.MSorcererFillFFactor.Histogram_Signal_Min: 1000
MJSorcererCalib.MSorcererFillFFactor.Histogram_Signal_Max: 40000

MJSorcererCalib.MSorcererFillPedestalRnd.Histogram_Pedestal_Nbins: 90
MJSorcererCalib.MSorcererFillPedestalRnd.Histogram_Pedestal_Min: -900
MJSorcererCalib.MSorcererFillPedestalRnd.Histogram_Pedestal_Max: 900

MJSorcererCalib.MSorcererFillPedestalExtr.Histogram_Pedestal_Nbins: 90
MJSorcererCalib.MSorcererFillPedestalExtr.Histogram_Pedestal_Min: -900
MJSorcererCalib.MSorcererFillPedestalExtr.Histogram_Pedestal_Max: 900

# required probability for fits of signal histograms
# if fit gives worse probability, the value of Mean and RMS is taken directly from the histogram
MJSorcererCalib.RequiredPedestalFitProbability: 1.e-5
MJSorcererCalib.RequiredSignalFitProbability:   1.e-4

# domino time calibration
# NumCapacitors is the total number of capacitors for each channel,
# NumCombine tells what is the width of the bin for the fits 
# (it is best if NumCapacitors / NumCombine is integer number)
# NumHarmonics tells how many harmonic components are computed for the Fourier series 
# if you set NumHarmonics to 0 instead of the Fourier series simple pol7 is fitted
#MJSorcererCalib.FirstCapTimeCalibration: yes
#MJSorcererCalib.MSorcererCalcTimeCalibration.NumCapacitors: 1024
#MJSorcererCalib.MSorcererCalcTimeCalibration.NumCombine: 8
#MJSorcererCalib.MSorcererCalcTimeCalibration.NumHarmonics: 6
##MJSorcererCalib.MSorcererCalcTimeCalibration.NumHarmonics: 0
#MJSorcererCalib.MSorcererCalcTimeCalibration.NumBins: 320
#MJSorcererCalib.MSorcererCalcTimeCalibration.MinTimeHist: 0
#MJSorcererCalib.MSorcererCalcTimeCalibration.MaxTimeHist: 49

MJSorcererCalib.FirstCapTimeCalibration: no
MJSorcererCalib.MSorcererCalcMeanTime.NumBins: 320
MJSorcererCalib.MSorcererCalcMeanTime.MinTimeHist: 0
MJSorcererCalib.MSorcererCalcMeanTime.MaxTimeHist: 49

# computing the pulse shape
MJSorcererCalib.CalcPulseShape: yes
MJSorcererCalib.MSorcererCalcPulseShape.OverSample: 3
MJSorcererCalib.MSorcererCalcPulseShape.ToRight: 15

# final histograms of all pixels (just for drawing)
#'FundPed' tab
MJSorcererCalib.HistFundPedMin:     9975
MJSorcererCalib.HistFundPedMax:    10025
MJSorcererCalib.HistFundPedRMSMax:    45
#'FitCharge' tab
MJSorcererCalib.HistMeanChargeMin:  1000
MJSorcererCalib.HistMeanChargeMax: 12000
MJSorcererCalib.HistRMSChargeMin:    500
MJSorcererCalib.HistRMSChargeMax:   2500
#'PedRun' tab
MJSorcererCalib.HistMeanPedestalMin: -90
MJSorcererCalib.HistMeanPedestalMax:  90
MJSorcererCalib.HistRMSPedestalMin:    0
MJSorcererCalib.HistRMSPedestalMax:  150
#'Nphe' tab
MJSorcererCalib.HistNpheMin:           0
MJSorcererCalib.HistNpheMax:         150
#'Conv' tab
MJSorcererCalib.HistConvMin:           0
MJSorcererCalib.HistConvMax:           0.04
#'PedBiased' and 'PedUnbiased' tabs
MJSorcererCalib.HistMeanPedPheMin:    -1.
MJSorcererCalib.HistMeanPedPheMax:     5.
MJSorcererCalib.HistRMSPedPheMin:      0.
MJSorcererCalib.HistRMSPedPheMax:      1.5
#'TimeCalib' tab
MJSorcererCalib.HistMeanTimeMin:       0
MJSorcererCalib.HistMeanTimeMax:      49
MJSorcererCalib.HistRMSTimeMin:        0
MJSorcererCalib.HistRMSTimeMax:       20

### Limits for bad pixels
# signal histograms
MJSorcererCalib.MSorcererFillFFactor.MaxAllowedUnderOverFlow: 0.2
# F-Factor method
MJSorcererCalib.MSorcererFillFFactor.MinAllowedNphe:     15.
MJSorcererCalib.MSorcererFillFFactor.MaxAllowedNphe:    600.
MJSorcererCalib.MSorcererFillFFactor.MinAllowedConv:      0.003
MJSorcererCalib.MSorcererFillFFactor.MaxAllowedConv:      0.026
MJSorcererCalib.MSorcererFillFFactor.MinAllowedSNratio:   5.
MJSorcererCalib.MSorcererFillFFactor.MaxFlatFieldDeviation: 0.6

# arrival time (DRS time calibration)
MJSorcererCalib.MSorcererCalcTimeCalibration.MinAllowedArrTime:    6.
MJSorcererCalib.MSorcererCalcTimeCalibration.MaxAllowedArrTime:   40.
MJSorcererCalib.MSorcererCalcTimeCalibration.MaxAllowedChi2:      10.
MJSorcererCalib.MSorcererCalcTimeCalibration.MaxAllowedSpread:     9.

# arrival time (mean time calculation)
#MJSorcererCalib.MSorcererCalcMeanTime.MinAllowedArrTime:   25.
#MJSorcererCalib.MSorcererCalcMeanTime.MaxAllowedArrTime:   65.
#MJSorcererCalib.MSorcererCalcMeanTime.MaxAllowedRMS:      6.

MJSorcererCalib.MSorcererCalcMeanTime.MinAllowedArrTime:   0.
MJSorcererCalib.MSorcererCalcMeanTime.MaxAllowedArrTime:  49.
MJSorcererCalib.MSorcererCalcMeanTime.MaxAllowedRMS:      20.

### security limits  
# maximum percentage of newly found bad pixels (without those in BadPixels file !)
MJSorcererCalib.MaxPercBadPixels:     5.
MJSorcererCalib.MaxPercUnrelPixels:  15.

#####################################
# Signal extraction (-y mode)       #
#####################################
# maximum number of events to process (uncomment for fast tests)
#MJSorcererSignal.MaxEvents:     3333

MJSorcererSignal.Geometry: MGeomCamMagicTwo

# Mataju cleaning
MJSorcererSignal.UseMaximClean: yes
MJSorcererSignal.MSorcererTriggerTimes.IsStereoSumT: no #If SumT data
MJSorcererSignal.MSorcererTriggerTimes.Threshold: 20
MJSorcererSignal.MSearchNN.CleanMethod: Absolute

MJSorcererSignal.MSearchNN.2NNThreshold: 7.3
MJSorcererSignal.MSearchNN.3NNThreshold: 4.2
MJSorcererSignal.MSearchNN.4NNThreshold: 3.3
MJSorcererSignal.MSearchNN.Window2NN: 2.31
MJSorcererSignal.MSearchNN.Window3NN: 2.81
MJSorcererSignal.MSearchNN.Window4NN: 3.14

MJSorcererSignal.MMaximCleanCalc.MaxTimeDiff: 3.5
MJSorcererSignal.MMaximCleanCalc.NumRings: 3
MJSorcererSignal.MMaximCleanCalc.ThrBound: 2.3
MJSorcererSignal.MMaximCleanCalc.First: 0
MJSorcererSignal.MMaximCleanCalc.Last: 49
MJSorcererSignal.MaxTimeDiffCore: 12
# enable this to store also calibration events in _Y_ files
#MJSorcererSignal.SaveAllEvents: yes

MJSorcererSignal.MaxPercBadPixels:     10.

# corrects the baseline depending on the time to previous capacitor reading 
MJSorcererSignal.MSorcererBaselineCalc.DoTimeCorr: no
MJSorcererSignal.MSorcererBaselineCalc.RemoveSpikes: yes
MJSorcererSignal.MSorcererBaselineCalc.SpikenessCut: 0.32

# removing pseudopulses from the data
#MJSorcererSignal.DRS4SpikePosJS: yes
#MJSorcererSignal.MSorcererDRS4SpikePosJS.SpikePosPlus: 75
#MJSorcererSignal.MSorcererDRS4SpikePosJS.SpikePosMinus: 949

# update of the baseline from the interleaved pedestal events 
MJSorcererSignal.BaselineUpdate: no
MJSorcererSignal.MSorcererPedFundCalc.NumEventsUpdate: 100

### interleaved pedestals and calibration events 
MJSorcererSignal.InterlacedPed: no
MJSorcererSignal.InterlacedCal: no
# update of the pedestal RMS from the interleaved pedestals
MJSorcererSignal.MSorcererFillPedestal.NumEventsUpdate: 500
MJSorcererSignal.MSorcererFillPedestal.Histogram_Pedestal_Nbins: 90
MJSorcererSignal.MSorcererFillPedestal.Histogram_Pedestal_Min: -900
MJSorcererSignal.MSorcererFillPedestal.Histogram_Pedestal_Max: 900
# update of the conversion factors from interleaved calibration events
MJSorcererSignal.MSorcererFillFFactor.NumEventsUpdate: 1000

# Should we use Nphe averaged over all inner pixels (if no: NPhe calculated for each pixel from F-Factor method)
MJSorcererSignal.MSorcererFillFFactor.UseMeanNphe: yes
MJSorcererSignal.MSorcererFillFFactor.PixPin1: 1040
MJSorcererSignal.MSorcererFillFFactor.PixPin2: 1041

# limits for interleaved calibration
MJSorcererSignal.MSorcererFillFFactor.MinAllowedNphe:     15.
MJSorcererSignal.MSorcererFillFFactor.MaxAllowedNphe:    600.
MJSorcererSignal.MSorcererFillFFactor.MinAllowedConv:      0.003
MJSorcererSignal.MSorcererFillFFactor.MaxAllowedConv:      0.026
MJSorcererSignal.MSorcererFillFFactor.MinAllowedSNratio:   5.
MJSorcererSignal.MSorcererFillFFactor.MaxFlatFieldDeviation: 0.6

# required probability for fits of signal histograms
# if fit gives worse probability, the value of Mean and RMS is taken directly from the histogram
MJSorcererSignal.RequiredPedestalFitProbability: 1.e-5
MJSorcererSignal.RequiredSignalFitProbability:   1.e-4

# ranges of histograms for individual pixels (calibration events)
MJSorcererSignal.Fill_Info_Calib.Histogram_Signal_Nbins: 250
MJSorcererSignal.Fill_Info_Calib.Histogram_Signal_Min:    1.5 
MJSorcererSignal.Fill_Info_Calib.Histogram_Signal_Max:   300
MJSorcererSignal.Fill_Info_Calib.Histogram_Time_Nbins:   320
MJSorcererSignal.Fill_Info_Calib.Histogram_Time_Min:       0
MJSorcererSignal.Fill_Info_Calib.Histogram_Time_Max:      50

# ranges of histograms for individual pixels (showers)
MJSorcererSignal.Fill_Info_Shower.Histogram_Signal_Nbins: 260
MJSorcererSignal.Fill_Info_Shower.Histogram_Signal_Min:    -5
MJSorcererSignal.Fill_Info_Shower.Histogram_Signal_Max:   125
MJSorcererSignal.Fill_Info_Shower.Histogram_Time_Nbins:   320
MJSorcererSignal.Fill_Info_Shower.Histogram_Time_Min:       0
MJSorcererSignal.Fill_Info_Shower.Histogram_Time_Max:      50
MJSorcererSignal.Fill_Info_Shower.MinNPhe:                 15

# domino time correction (if set to 'no' only relative time difference are corrected)
MJSorcererSignal.FirstCapTimeCorr: no
#MJSorcererSignal.MSorcererCalibrateTimes.UseLUT: yes

# final histograms of all pixels (just for drawing)
MJSorcererSignal.HistMeanCalChargeMin:  0
MJSorcererSignal.HistMeanCalChargeMax:  20

MJSorcererSignal.HistRMSCalTimeMin:  0.
MJSorcererSignal.HistRMSCalTimeMax:  1.5

MJSorcererSignal.HistMeanChargeMin:  50
MJSorcererSignal.HistMeanChargeMax: 170
MJSorcererSignal.HistRmsChargeMin:    0
MJSorcererSignal.HistRmsChargeMax:   25
MJSorcererSignal.HistMeanTimeMin:     0
MJSorcererSignal.HistMeanTimeMax:    50
MJSorcererSignal.HistRMSTimeMin:      0
MJSorcererSignal.HistRMSTimeMax:      5 

# data reduction: signals below this threshold will not be stored
#MJSorcererSignal.MZeroSupression.ThresholdInner: 2.3

# Adding Crosstalk (only for MonteCarlos)
#MJSorcererSignal.ApplyCrosstalk: yes
# Removing Crosstalk (only for the data)
#MJSorcererSignal.RemoveCrosstalk: yes
# pixel mapping
#MJSorcererSignal.PixelMappingFile: pixel_full_07032010.txt
# cross-talk one channel to another matrixes 
#MJSorcererSignal.CrosstalkCoefficientsFile: xtalk_DRS_allchannels_07032010.txt
