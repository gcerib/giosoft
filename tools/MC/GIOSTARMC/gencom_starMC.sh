#!/bin/bash

mkdir -p com
cd inputcard;
PARSETS=$(echo */);
cd ..;
for parset in $PARSETS; do
  if [[ ${parset:0:1} == "_" ]] ; then echo "Skipping $parset"; continue; fi
  wobble="0"
  ISWOBBLE=$(echo $parset | awk '$1~/_w/ {print 1}')
  if [[ "$ISWOBBLE" == "1" ]]; then
     wobble=$(echo $parset | awk '{gsub("_w"," ");gsub("/"," ");print($2)}')
     echo "$parset abitilato solo per il wobble $wobble"
  fi
  cd CALIBRATED;
  DATADIRS=$(echo */*/);
  echo $DATADIRS
  cd ..;
  echo "cattura wobble $wobble:"
  for datadir in $DATADIRS; do
     mkdir -p star/$parset/$datadir;
     objname=$(echo $datadir | awk '{gsub("/"," ");print($2)}')
     wobname=$(echo $datadir | awk '{gsub("/"," ");print($3)}')
     parname=$(echo $parset  | awk '{gsub("/"," ");print($1)}')

     if [[ ! "$wobble"  == "0" ]]; then
       if [[ ! "$wobname" == "$wobble" ]]; then
         echo "   $datadir non contiene $wobble"
         continue
       fi
       echo "   $datadir contiene $wobble"
     fi
     #RUNS=$(ls -1 CALIBRATED/$datadir/*_M1_*_Y_*.root | awk '{gsub("_"," ");gsub("\\."," ");print($5)}' | awk '!NF || !seen[$0]++')
     #for run in $RUNS; do
     echo star -b -f -q -mc  --config="inputcard/$parset/star_M1__MC.rc" --ind="CALIBRATED/$datadir/GA*_M1_*.root" --out="star/$parset/$datadir" --log="star/$parset/$datadir/star_M1.log" --outname="star_M1" >> com/${parname}.${objname}.com;
     echo star -b -f -q -mc  --config="inputcard/$parset/star_M2__MC.rc" --ind="CALIBRATED/$datadir/GA*_M2_*.root" --out="star/$parset/$datadir" --log="star/$parset/$datadir/star_M2.log" --outname="star_M2" >> com/${parname}.${objname}.com;
  done;
done;
