# Input card to be used by OSA for current M1 data (after change of sampling to 1.64GHz)
# the cleaning is 6-3.5 SUM cleaning with time constraints relaxed by Julian in March 2013
# there is no dynamic cleaning 
# coma correction is applied 
# there is no cross-talk correction
# signals are clipped at pixel size 750 phe (for saturation)
# software trigger thresholds are computed
# the card is also valid for processing MCs

# ==========================================================================
#############################################################################
# ==========================================================================
#                              General
# ==========================================================================
#############################################################################
# ==========================================================================

# -------------------------------------------------------------------------
# Use this if you want to setup the logging stream for the jobs
# (overwrites command line options)
# -------------------------------------------------------------------------
#MLog.VerbosityLevel: 2
#MLog.DebugLevel:     1
#MLog.NoColors:       yes

# ==========================================================================
#############################################################################
# ==========================================================================
#                                   Star
# ==========================================================================
#############################################################################
# ==========================================================================

MJStar.CheckImageCleaning: no

#MJStar.MPointingPosInterpolate.CheckSourceRADEC: no
# -------------------------------------------------------------------------
# Use this if you want to modify the exclusion of pixels at this stage 
# -------------------------------------------------------------------------
#MJStar.BadPixelsUpdate: no
#MJStar.BadPixelsFile:   mjobs/badpixels_0_559.rc

# excluding pixels with high pedestal RMS
# this excludes pixels wich are affected by stars and thoose who are screwed up anyway
# the cut value (4.5) is a general value and should work always
MJStar.FindHotPixels: yes
MJStar.MBadPixelsCalc.PedestalLevel:         400.
MJStar.MBadPixelsCalc.PedestalLevelVariance: 4.5

# excluding of a star, now not needed, because pedestal sampling in callisto was improved, and
# stars can be excluded using PedestalLevelVariance
#MJStar.ExcludeStar: yes
# tau zeta star
#MJStar.MStarPosCalc.SourceRaDec: 5.627416 21.1425
# radius of the exclusion around the star position (in mm) 
# this is just a rought value !
#MJStar.MStarPixels.StarRadius: 100
#MJStar.MStarPixels.AberrationCorrection: 1.0713 

MJStar.DisplayPrescFactors: no

MJStar.UseDynamic: no
#MJStar.MImgCleanDynamic.ScaleDynamic: 0.002

# -------------------------------------------------------------------------
# -------------------------------------------------------------------------
# The flag "UseTime" enables the use of time constrains in the cleaning: 
# MaxTimeOff is the constrain (in ns) imposed between the mean core pixels
# arrival time and the single core pixel arrival time
# MaxTimeDiff is the constrain (in ns) imposed between a boundary pixel arrival 
# time and its core pixel neigbor arrival time
#
#MJStar.MImgCleanStd.UseTime:    yes
#MJStar.MImgCleanStd.MaxTimeOff:  4.5
#MJStar.MImgCleanStd.MaxTimeDiff: 1.5
#MJStar.MImgCleanStd.UseSum: yes
#MJStar.MImgCleanStd.CleanLevel1: 4.5
#MJStar.MImgCleanStd.CleanLevel2: 3.0
#MJStar.MImgCleanStd.CleanMethod: Absolute

# mataju cleaning
MJStar.UseMaxim: yes
MJStar.MImgCleanAdv.CleanRings: 2

#for the dynamical image cleaning

#MJStar.MImgCleanDynamic.UseTime:    yes
#MJStar.MImgCleanDynamic.MaxTimeOff:  4.5
#MJStar.MImgCleanDynamic.MaxTimeDiff: 1.5
#MJStar.MImgCleanDynamic.UseSum:     yes
#MJStar.MImgCleanDynamic.CleanLevel1: 6.
#MJStar.MImgCleanDynamic.CleanLevel2: 3.5
###MJStar.MImgCleanDynamic.CleanMethod: Absolute


# -------------------------------------------------------------------------
# Expert initialization of the sum cleaning algorithm:
MJStar.MImgCleanStd.SumThresh2NNPerPixel: 1.8
MJStar.MImgCleanStd.SumThresh3NNPerPixel: 1.3
MJStar.MImgCleanStd.SumThresh4NNPerPixel: 1.0
# these time thresholds are given in the units of time slices !
MJStar.MImgCleanStd.Window2NN:  0.82
MJStar.MImgCleanStd.Window3NN:  1.15
MJStar.MImgCleanStd.Window4NN:  1.80

#for the dynamical image cleaning
#MJStar.MImgCleanDynamic.SumThresh2NNPerPixel: 1.8
#MJStar.MImgCleanDynamic.SumThresh3NNPerPixel: 1.3
#MJStar.MImgCleanDynamic.SumThresh4NNPerPixel: 1.0
#MJStar.MImgCleanDynamic.Window2NN:  1.0
#MJStar.MImgCleanDynamic.Window3NN:  1.4
#MJStar.MImgCleanDynamic.Window4NN:  2.2

# -------------------------------------------------------------------------
# Enable/disable the calculation of time-related image parameters:
#
# MHillasTime    -> Mean arrival time, time RMS, time RMS weighted by pixels charge, etc.
# MHillasTimeFit -> Time RMS respect linear and polinomial fit along major axis, etc.
#
MJStar.MHillasCalc.EnableTask_MHillasTime: yes
MJStar.MHillasCalc.EnableTask_MHillasTimeFit: yes
# The following task is for test purposes
MJStar.MHillasCalc.EnableTask_MHillasTime3D: no

# Correction of the camera geometry to account for aberration. Default is 1 (no correction)
# The pixel coordinates and linear sizes will be divided by the factor below. For the MAGIC
# parabolic dish the factor is 1.0713
MJStar.MHillasCalc.AberrationCorrection: 1.0713 


#for the dynamical image cleaning
#MJStar.MHillasCalcDynamic.EnableTask_MHillasTime: yes
#MJStar.MHillasCalcDynamic.EnableTask_MHillasTimeFit: yes
#MJStar.MHillasCalcDynamic.EnableTask_MHillasTime3D: no
#MJStar.MHillasCalcDynamic.AberrationCorrection: 1.0713 

# -------------------------------------------------------------------------
# Example on how to set a source position (in RA Dec J2000) for doing the
# analysis with respect to it, in the case it is not in the center (for
# instance in wobble mode). RA is in hours, Dec in degree
# -------------------------------------------------------------------------
# Crab:
# MJStar.MSrcPosCalc.SourceRaDec: 5.5755555555 22.014444444
# Anti-CrabW1:
# MJStar.MSrcPosCalc.SourceRaDec: 5.612222 22.592222
#

# -------------------------------------------------------------------------
# If you want to set the source position to the default ones that can be 
# found in MRawRunHeader::fSourceRA and MRawRunHeader::fSourceDEC
# NOTE: in data taken before April 2006 this information was not available!
#
# MJStar.MSrcPosCalc.DefaultSourceRaDec: no
MJStar.MSrcPosCalc.DefaultSourceRaDec: yes

# -------------------------------------------------------------------------
# (fixed) source position for the case of (for instance) MC wobble. 
# Units mm. The position w+ (except for zbin = 0, in which the shift 
# is along the y axis; see TDAS 01-05):
#
# MJStar.MSrcPosCalc.FixedSourceX: 120.
# MJStar.MSrcPosCalc.FixedSourceY: 0.
#
# The position w-
# MJStar.MSrcPosCalc.FixedSourceX: -120.
# MJStar.MSrcPosCalc.FixedSourceY:  0.
#
# NOTE: the different options above to define the source position on the camera
# are obviously mutually excluding. If more than one is active, check MSrcPosCalc::ReadEnv
# to see the precedence of these commands.
#

# -------------------------------------------------------------------------
# -------------------------------------------------------------------------
#MJStar.MHillasCalc.IdxIsland: 0

# -------------------------------------------------------------------------
#
# Steering of the Starguider treatment
# To switch on the starguider correction, 
# uncomment the two following lines and set the flag to "yes"
#
MJStar.CalibStarguider: yes
MJStar.UseStarguider: yes
#
# Further Starguider treatment
#
#MJStar.MPointingPosInterpolate.IgnoreStatusRepErrors: yes
###############################################################
#
# OLD METHOD ONLY CULMINATION PROBLEM CORRECTION
#
#MJStar.MCalibrateStarguider.Method: Culmination
#MJStar.MPointingPosInterpolate.CorrectZenith: yes
#MJStar.MPointingPosInterpolate.CorrectAzimuth: no
#
# NEW METHOD FULL ABSOLUTE STARGUIDER CALIBRATION
# ModelType can be 'Wuerzburg' or 'Ifae' (default)
# M2 models only exist in Ifae upto now!!
# 
MJStar.MCalibrateStarguider.Method: Wuerzburg
MJStar.MCalibrateStarguider.ModelType: Ifae
MJStar.MPointingPosInterpolate.CorrectZenith: yes
MJStar.MPointingPosInterpolate.CorrectAzimuth: yes

######################################################################

MJStar.UsePyrometer: yes

# -------------------------------------------------------------------------
#
# Filter cuts: 
#
# Add a possible filter for the trigger pattern
#	
#MJStar.TrigPatFilter.Require: SUMT 
#MJStar.TrigPatFilter.Deny:    LT1
#
# Time difference filter: Filters events with time difference greater than 
#                         UpperLimit
#MJStar.SkipDeltaT: no
#MJStar.MFDeltaT.UpperLimit: 0.5
#
# DC calibration:         Applies DC calibration constants from the file:
#
MJStar.UseDCCalibration: no
# MJStar.CalibDCsFile: mjobs/calibDCs_2008.rc
# (obtained from runs 1003671-1003677)
#
MJStar.BrokenDTsFile:
#
# DCs to be remove from software trigger:
#
#MJStar.MBadPixelsSet.ExclusionDC: relative
#MJStar.MBadPixelsSet.MaxDCRequired: 5
#
# Software Trigger:       Applies a simulated NN-coincidence trigger 
#
MJStar.CalcSWTriggerThreshold:  yes
MJStar.MappingFile: pixel_mapping_M2_DRS4.txt
#
#MJStar.SWTrigger:  yes
#MJStar.MFSoftwareTriggerLvl1.Threshold:    5.5
#MJStar.MFSoftwareTriggerLvl1.NumNeighbors: 4
#MJStar.MFSoftwareTriggerLvl1.TimeWindow:   6
#MJStar.MFSoftwareTriggerLvl1.TriggerType:  SinglePixel
#MJStar.MFSoftwareTriggerSum.Threshold:    3
#MJStar.MFSoftwareTriggerSum.NumNeighbors: 4
#MJStar.MFSoftwareTriggerSum.TimeWindow:   4.5
#MJStar.MFSoftwareTriggerSum.TriggerType:  SinglePixel
#
# Size Cut:               Applies a Size cut
#
MJStar.SizeCut: 0
#
# Further Cuts:           Like in osteria, any type of cuts can be applied here:
# 
#MJStar.FilterCuts.Continue0.Condition:  1.5-4.*log10(MNewImagePar.fConc)<log10(MHillas.fSize)
#MJStar.FilterCuts.Continue1.Condition:  log10(MHillas.fWidth*MHillas.fLength/MHillasSrc.fDist/297)>(-0.3)
#MJStar.FilterCuts.Continue2.Condition:  MHillas.fWidth<0
#MJStar.FilterCuts.Continue3.Condition:  MHillas.fLength<0
#MJStar.FilterCuts.Continue4.Condition:  MHillasSrc.fDist<0
#MJStar.FilterCuts.Continue5.Condition:  MHillas.fSize<80
#MJStar.FilterCuts.Continue6.Condition:  MHillas.fSize>1000000
#MJStar.FilterCuts.Continue7.Condition:  MNewImagePar.fLeakage1>0.2
MJStar.FilterCuts.Continue0.Condition:  MNewImagePar.fNumCorePixels<0
#MJStar.FilterCuts.Continue9.Condition:  MImagePar.fNumIslands>3
#MJStar.FilterCuts.Continue10.Condition: MHillasSrc.fDist<40
#MJStar.FilterCuts.Continue11.Condition: MHillasSrc.fDist>340

###############################################################
#
# Crosstalk simulation/removal. ONLY ACTIVATE THIS OPTIONS FOR M2 with DRS2 !!!!
#
# Use the following option to add Crosstalk to MonteCarlo
#MJStar.ApplyCrosstalk: yes
# Use the following option to remove Crosstalk from the data
MJStar.RemoveCrosstalk: no
# You need to uncomment the following 2 lines in order to add or remove 
# crosstalk. Make sure that both files exist in your $MARSSYS dir
# those are the default files for DRS2 data !
#MJStar.PixelMappingFile: pixel_full_07032010.txt
#MJStar.CrosstalkCoefficientsFile: xtalk_DRS_allchannels_07032010.txt
#

# Experimental non-linearity correction. It requires a file with 
# TGraphs describing non-linearity of each pixel.
# check also mtemp/mhardware/Nanalysis_fast.cpp
#MJStar.CorrectNonLinearity: yes
#MJStar.MLinearityCorr.LinearityFile: /nfs/magic-ifae01/jsitarek/M1_DRS/upgrade2/Crab/an4/star_M1/linearity_20130203_M1_05022985_40phe.root
#MJStar.MLinearityCorr.MinNphe: 40.

# If the data were taken with different sampling than written in 
# MRawRunHeader.fSamplingFrequency, specify the number here
#MJStar.ForceSamplingMHz: -1

MJStar.UseClipping: yes
MJStar.MSignalClip.ClipLevel: 750
#MJStar.MSignalClipDynamic.ClipLevel: 750


#############
# Some containers not used in the standard analysis chain are not writen in the data.  
# The following option will force to write all of them.
#
# MJStar.KeepAllContainers: yes
