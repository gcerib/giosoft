#!/bin/bash

mkdir -p comsuper
cd inputcard;
PARSETS=$(echo */);
cd ..;
cd CALIBRATED;
SETS=$(echo */);
cd ..;
for parset in $PARSETS; do
  if [[ ${parset:0:1} == "_" ]] ; then echo "Skipping $parset"; continue; fi
  for insieme in $SETS; do
    #cd star/$parset/$insieme;
    #OBJDIRS=$(echo */);
    #cd ../../../;
    #for objdir in $OBJDIRS; do
       mkdir -p super/$parset/$insieme/  #$objdir;
       parname=$(echo $parset   | awk '{gsub("/"," ");print($1)}')
       insname=$(echo $insieme  | awk '{gsub("/"," ");print($1)}')
       #objname=$(echo $objdir   | awk '{gsub("/"," ");print($1)}')
       objdir=""
       objname=""
       NIGHTS=$(cat star/$parset/$insieme/$objdir/NIGHTS.txt)
       echo $parset $insname $objdir $parname $objname
       for night in $NIGHTS; do
          STARDIR="star/$parname/$insname/$objname/$night"
          SUPERDIR="super/$parname/$insname/$objname/$night"
          echo "      $STARDIR -> $SUPERDIR"
          mkdir -p $SUPERDIR;
          #RUNS=$(ls -1 $STARDIR/*_M1_*_I_*.root | awk '{gsub("_"," ");gsub("\\."," ");print($5)}' | awk '!NF || !seen[$0]++')
          #for run in $RUNS; do
          echo "superstar -b -f -q -mc --config=inputcard/superstar.rc --ind1=$STARDIR/\"GA*M1*.root\" --ind2=$STARDIR/\"GA*M2*.root\" --out=$SUPERDIR --log=$SUPERDIR/superstar.log" >> comsuper/superstar.com
          #done;
       done;
    done;
  #done;
done;
