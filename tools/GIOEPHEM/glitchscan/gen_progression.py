#!/usr/bin/env python

import sys

freq  = 29.639378221506401
fdot  = -3.68701E-10
tzero = 57981

for j in range(19):
    i = j-1
    freq_i = freq+i*fdot*864000
    t_i = tzero+i*10
    fout = open("crab_%d.conf" % t_i, "w+")
    fout.write("crab_TOA2018.txt\n")
    fout.write("%0.15f\n" % freq_i)
    fout.write("%0.15f\n" % fdot)
    fout.write("%d\n" % t_i)
    fout.write("100\n")
    fout.write("%d\n" % (t_i-5))
    fout.write("%d\n" % (t_i+5))
    fout.write("output.txt\n")
    fout.write("1e-6\n")
    fout.write("25\n")
    fout.write("1.5e-12\n")
    fout.write("25\n")
    fout.close()
