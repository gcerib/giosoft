#!/usr/bin/env python

import sys

import math
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import scipy.optimize as opt

# Do you want me to plot the fit contours?
# It can get difficult to see the maximum
# with it on.
sure = False


def forceAspect(ax,aspect=1):
    im = ax.get_images()
    extent =  im[0].get_extent()
    ax.set_aspect(abs((extent[1]-extent[0])/(extent[3]-extent[2]))/aspect)

def gaus2d((x, y), amplitude, xo, yo, sigma_x, sigma_y, theta, offset):
    xo = float(xo)
    yo = float(yo)
    a = (math.cos(theta)**2)/(sigma_x**2) + (math.sin(theta)**2)/(sigma_y**2)
    b = 2*math.sin(theta)*math.cos(theta)*(1.0/(sigma_y**2) - 1.0/(sigma_x**2))
    c = (math.sin(theta)**2)/(sigma_x**2) + (math.cos(theta)**2)/(sigma_y**2)
    g = offset+(amplitude**2)*np.exp( - 0.5*(a*((x-xo)**2) + b*(x-xo)*(y-yo) + c*((y-yo)**2)))
    return g.ravel()

a  =  np.loadtxt(sys.argv[1])

readnm = open(sys.argv[1],'r')
firstline = readnm.readline().split()
readnm.close()

nbins_freq = int(firstline[1])
nbins_fdot = int(firstline[2])

x  = (a[:,0].reshape((nbins_freq,nbins_fdot)).T-a[:,0].mean())*1e6
y  = (a[:,1].reshape((nbins_freq,nbins_fdot)).T-a[:,1].mean())*1e12
z  = a[:,2].reshape((nbins_freq,nbins_fdot)).T


ims=plt.imshow(z, extent=(x.min(), x.max(), y.min(), y.max()),
                   interpolation='bicubic', origin='lower')

init = (30,0,0,0.1,0.5,0.1,10)
plotcont=1
try:
  popt,pcov = opt.curve_fit(gaus2d, (x, y), z.ravel(), p0=init)
except RuntimeError:
  print "error"
  plotcont=0
print "---------------------------------------------------------------"
print "Average freq.    [Hz]:     \t%.12f" % a[:,0].mean()
print "Average freq.dot [Hz**2]:  \t%.12e" % a[:,1].mean()
print "-----------------"

if(plotcont):
  g=gaus2d((x,y),*popt).reshape((nbins_fdot,nbins_freq))
  if(sure):
    cont=plt.contour(x,y,g, extent=(x.min(), x.max(), y.min(), y.max()))

  perr = np.sqrt(np.diag(pcov))

  print "Amplitude          \t%.12f   \t%.12f" % (popt[0],perr[0])
  print "deltaF    [uHz]    \t%.12f   \t%.12f" % (popt[1],perr[1])
  print "deltaFdot [uHz**2] \t%.12f   \t%.12f" % (popt[2],perr[2])
  print "sigma_dF           \t%.12f   \t%.12f" % (popt[3],perr[3])
  print "sigma_d2F          \t%.12f   \t%.12f" % (popt[4],perr[4])
  print "theta              \t%.12f   \t%.12f" % (popt[5]/np.pi*180,perr[5]/np.pi*180)
  print "offset             \t%.12f   \t%.12f" % (popt[6],perr[6])
  print "---------------------------------------------------------------"

plt.colorbar(ims)
plt.title("$Z_{10}^2$ scan")
plt.xlabel("$\Delta f$ [$\mu$Hz]")
plt.ylabel("$\Delta \dot{f}$ [$\mu$Hz$^{2}$]")
forceAspect(plt.gca())
plt.show()
