#include <iostream>
#include <fstream>
#include <iomanip>
#include <cmath>

using namespace std;

bool test_integrer_string(char * str);
bool test_decimal_string(char * str);
double calculate_harmonic_sum(unsigned long nevents, const double *phases, unsigned harmonic);

//Syntax: ./rodola configuration.conf
int main(int argc, char ** argv){

  if(argc != 2){ cerr << "wrong syntax." << endl; return 1; }

  ifstream conffile;
  conffile.open(argv[1]);
  if(!conffile.good()){ cerr << "input error: " << argv[1] << endl; conffile.close(); return 1; }

  char * str_infilename  = new char[256];
  char * str_freq        = new char[256];
  char * str_freqdot     = new char[256];
  char * str_origin      = new char[256];
  char * str_nbins       = new char[256];
  char * str_timemin     = new char[256];
  char * str_timemax     = new char[256];
  char * str_outfilename = new char[256];
  char * str_wscanfreq   = new char[256];
  char * str_nscanfreq   = new char[256];
  char * str_wscanfdot   = new char[256];
  char * str_nscanfdot   = new char[256];

  conffile >> str_infilename;
  conffile >> str_freq;
  conffile >> str_freqdot;
  conffile >> str_origin;
  conffile >> str_nbins;
  conffile >> str_timemin;
  conffile >> str_timemax;
  conffile >> str_outfilename;
  conffile >> str_wscanfreq;
  conffile >> str_nscanfreq;
  conffile >> str_wscanfdot;
  conffile >> str_nscanfdot;

  conffile.close();

  //Sanity checks

  if(!test_decimal_string(str_freq)){ cerr << "error: \"" << str_freq << "\" is not a float." << endl; return 1; }
  double freq = strtold(str_freq,NULL);

  if(!test_decimal_string(str_freqdot)){ cerr << "error: \"" << str_freqdot << "\" is not a float." << endl; return 1; }
  double freqdot = strtold(str_freqdot,NULL);

  if(!test_decimal_string(str_origin)){ cerr << "error: \"" << str_origin << "\" is not a float." << endl; return 1; }
  double origin = strtold(str_origin,NULL);

  if(!test_integrer_string(str_nbins)){ cerr << "error: \"" << str_nbins << "\" is not an int." << endl; return 1; }
  long long nbins = atoll(str_nbins);

  if(!test_decimal_string(str_timemin)){ cerr << "error: \"" << str_timemin << "\" is not a float." << endl; return 1; }
  double timemin = strtold(str_timemin,NULL);

  if(!test_decimal_string(str_timemax)){ cerr << "error: \"" << str_timemax << "\" is not a float." << endl; return 1; }
  double timemax = strtold(str_timemax,NULL);

  if(!test_decimal_string(str_wscanfreq)){ cerr << "error: \"" << str_wscanfreq << "\" is not a float." << endl; return 1; }
  double wscanfreq = strtold(str_wscanfreq,NULL);

  if(!test_decimal_string(str_wscanfdot)){ cerr << "error: \"" << str_wscanfdot << "\" is not a float." << endl; return 1; }
  double wscanfdot = strtold(str_wscanfdot,NULL);

  if(!test_integrer_string(str_nscanfreq)){ cerr << "error: \"" << str_nscanfreq << "\" is not an int." << endl; return 1; }
  long long nscanfreq = atoll(str_nscanfreq);

  if(!test_integrer_string(str_nscanfdot)){ cerr << "error: \"" << str_nscanfdot << "\" is not an int." << endl; return 1; }
  double nscanfdot = atoll(str_nscanfdot);

  if(timemax < timemin){ cerr << "time-range error: " << timemax << " < " << timemin << endl; conffile.close(); return 1;}

  //IO checks

  ifstream infile;
  infile.open(str_infilename);
  if(!infile.good()){ cerr << "input error: " << str_infilename << endl; infile.close(); return 1; }


  ofstream ofile;
  ofile.open(str_outfilename);
  if(!ofile.good()){ cerr << "output error: " << str_outfilename << endl; ofile.close(); return 1; }


  const unsigned long NBINSFREQ = 2*nscanfreq+1;
  const unsigned long NBINSFDOT = 2*nscanfdot+1;

  //Numbers of frequency and fdot bins
  ofile << "## " << NBINSFREQ << " " << NBINSFDOT << endl;

  //// Automatic method to read the number of
  //   lines in the file.
  unsigned long nlines = 0;

  int capacity = 256;
  bool wellreaden;
  do{
    wellreaden = 1;
    char * line = new char[capacity]();
    while( infile.getline(line,capacity) ){
      nlines++;
    }
    if(line[capacity-2] != 0){
          capacity = 2*capacity;
          wellreaden = 0;
          nlines = 0;
    }
    infile.clear();
    infile.seekg(0);
    delete [] line;
  }while(!wellreaden);
  //// End of automatic method.

  //Determine time interval
  double tmp,junk;
  unsigned long nvalid=0;
  for(unsigned long i=0;i<nlines;++i){
    infile >> junk >> junk >> tmp >> junk >> junk;
    if((tmp >= timemin) && (tmp <= timemax)) nvalid++;
  }
  infile.seekg(0);

  //Fill times within the interval
  double * times  = new double[nvalid];
  unsigned long j = 0;
  for(unsigned long i=0; i<nlines; ++i){
    infile >> junk >> junk >> tmp >> junk >> junk;
    if((tmp >= timemin) && (tmp <= timemax)){
      times[j]=tmp;
      j++;
    }
  }
  infile.close();

  double * zeta10 = new double[NBINSFREQ*NBINSFDOT]();
  double * freqs  = new double[NBINSFREQ*NBINSFDOT]();
  double * fdots  = new double[NBINSFREQ*NBINSFDOT]();

  const double freq_orig = freq;
  const double fdot_orig = freqdot;

  unsigned long ifreq,ifdot,i;
  
  //Calculation
  for(ifreq=0; ifreq<NBINSFREQ; ++ifreq){
        long IFREQ = ifreq-nscanfreq;
        double deltaFREQ = IFREQ*wscanfreq/nscanfreq;
        for(ifdot=0;ifdot<NBINSFDOT;++ifdot){
        	long IFDOT = ifdot-nscanfdot;
        	double deltaFDOT = IFDOT*wscanfdot/nscanfdot;
  
        	freq=freq_orig+deltaFREQ;
        	freqdot=fdot_orig+deltaFDOT;
  
  		double * phases = new double[nvalid]();
        	for(i=0; i<nvalid; ++i){
        		double delta = (times[i] - origin)*86400.0;
        		double phase = freq*delta + 0.5*freqdot*delta*delta;
        		double fracphase = fmod(phase,1);
        		phases[i]=fracphase;
        	}
  
        	freqs[ifreq*NBINSFDOT+ifdot]  = freq;
        	fdots[ifreq*NBINSFDOT+ifdot]  = freqdot;

        	//zeta[0] is equal to 0 and stays so.
        	double zeta[10+1] = {0};
        	for(int zi=1;zi<=10;++zi){
        		zeta[zi]  = calculate_harmonic_sum(nvalid,phases,zi);
        		zeta[zi] += zeta[zi-1];
        	}
                delete [] phases;

        	zeta10[ifreq*NBINSFDOT+ifdot] = zeta[10];

  	}
  }

  //Output
  for(long ifreq=0;ifreq<NBINSFREQ;++ifreq){
      for(long ifdot=0;ifdot<NBINSFDOT;++ifdot){
              ofile << scientific << setprecision(15)
                    << freqs[ifreq*NBINSFDOT+ifdot]    << " "
                    << fdots[ifreq*NBINSFDOT+ifdot]    << " "
                    << zeta10[ifreq*NBINSFDOT+ifdot]   << endl;
      }
  }      
  ofile.close();

  delete [] str_infilename ; 
  delete [] str_freq       ; 
  delete [] str_freqdot    ; 
  delete [] str_origin     ; 
  delete [] str_nbins      ; 
  delete [] str_timemin    ; 
  delete [] str_timemax    ; 
  delete [] str_outfilename; 
  delete [] str_wscanfreq  ; 
  delete [] str_nscanfreq  ; 
  delete [] str_wscanfdot  ;
  delete [] str_nscanfdot  ;

  delete [] times;
  delete [] freqs;
  delete [] fdots;
  delete [] zeta10;

  return 0;
}


//Tests if a string is a valid integer
bool test_integrer_string(char * str){
  int nchararg = 0;
  do{
    char c = str[nchararg];
    switch(c){
      case '0':
      case '1':
      case '2':
      case '3':
      case '4':
      case '5':
      case '6':
      case '7':
      case '8':
      case '9':
        nchararg++;
        break;
      default:
        return 0;
    }
  }while(str[nchararg]!='\0');
  
  return 1;
}

//Tests if a string is a valid float (approximately)
bool test_decimal_string(char * str){
  int nchararg = 0;
  bool dot = 0;
  bool is_e=0;
  bool is_sign=0;
  unsigned short n_minus=0;
  do{
    char c = str[nchararg];
    switch(c){
      case '0':
      case '1':
      case '2':
      case '3':
      case '4':
      case '5':
      case '6':
      case '7':
      case '8':
      case '9':
        nchararg++;
        break;
      case '.':
        if(!dot){
          nchararg++;
          dot = 1;
          break;
        }
	return 0;
      case 'e':
      case 'E':
        if(!is_e){
          nchararg++;
          is_e = 1;
          break;
        }
	return 0;
      case '+':
        if(!is_sign){
          nchararg++;
          is_sign = 1;
          break;
        }
	return 0;
      case '-':
        if(n_minus<2){
          nchararg++;
          n_minus++;
          break;
        }
	return 0;
      default:
        return 0;
    }
  }while(str[nchararg]!='\0');
  
  return 1;
}


//Harmonic sum of n-th harmonic
double calculate_harmonic_sum(unsigned long nevents, const double *phases, unsigned harmonic){
  double a=0,b=0;
  for(unsigned long i=0;i<nevents;++i){
    a += cos(harmonic*phases[i]*2*M_PI);
    b += sin(harmonic*phases[i]*2*M_PI);
  }
  a /= nevents;
  b /= nevents;
  a = a*a;
  b = b*b;
  return 2*nevents*(a+b);
}
