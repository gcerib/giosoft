#include <iostream>
#include <fstream>
#include <cstdlib>
#include <iomanip>
#include <cmath>
#include <ctime>

#include "boost/math/distributions/chi_squared.hpp"

using namespace std;
using boost::math::chi_squared;
using boost::math::cdf;


long long istobin(double x, double xmin, double xmax, unsigned long long nbins);
bool test_stringa_interi(char * str);
bool test_stringa_decimale(char * str);
double calculate_harmonic_sum(unsigned long nevents, const double *phases, unsigned harmonic, unsigned long & nused);


int main(int argc, char ** argv){
  char * str_nomefile;
  char * str_freq;
  char * str_freqdot;
  char * str_origin;
  char * str_nbins;
  char * str_outfilename;
  char * str_wscanfreq;
  char * str_nscanfreq;
  char * str_wscanfdot;
  char * str_nscanfdot;

  double tempomin, tempomax;
  tempomin = 0.0;
  tempomax = 0.0;

  if(argc != 2){ cerr << "wrong syntax." << endl; return 1; }

  ifstream conffile;
  conffile.open(argv[1]);
  if(!conffile.good()){ cerr << "input error: " << argv[1] << endl; conffile.close(); return 1; }
  char * t_nomefile = new char[256];
  char * t_freq = new char[256];
  char * t_freqdot = new char[256];
  char * t_origin = new char[256];
  char * t_nbins = new char[256];
  char * t_outfilename = new char[256];
  char * t_wscanfreq = new char[256];
  char * t_nscanfreq = new char[256];
  char * t_wscanfdot = new char[256];
  char * t_nscanfdot = new char[256];
  conffile >> t_nomefile;
  conffile >> t_freq;
  conffile >> t_freqdot;
  conffile >> t_origin;
  conffile >> t_nbins;
  conffile >> tempomin;
  conffile >> tempomax;
  if(tempomax < tempomin){ cerr << "time-range error: " << tempomax << " < " << tempomin << endl; conffile.close(); return 1;}
  conffile >> t_outfilename;
  conffile >> t_wscanfreq;
  conffile >> t_nscanfreq;
  conffile >> t_wscanfdot;
  conffile >> t_nscanfdot;
  str_nomefile = t_nomefile;
  str_freq = t_freq;
  str_freqdot = t_freqdot;
  str_origin = t_origin;
  str_nbins = t_nbins;
  str_outfilename = t_outfilename;
  str_wscanfreq = t_wscanfreq;
  str_nscanfreq = t_nscanfreq;
  str_wscanfdot = t_wscanfdot;
  str_nscanfdot = t_nscanfdot;
  conffile.close();

  ifstream file;
  file.open(str_nomefile);
  if(!file.good()){ cerr << "input error: " << str_nomefile << endl; file.close(); return 1; }

  //// Automatic method to read the number of
  //   lines in the file.
  unsigned long nrighe = 0;

  int capacita = 32;
  bool letto_bene;
  do{
    letto_bene = 1;
    char * linea = new char[capacita];
    for(int i=0;i<capacita;i++){
      linea[i] = 0;
    }
    while( file.getline(linea,capacita) ){
      nrighe++;
    }
    if(linea[capacita-2] != 0){
          capacita = 2*capacita;
          letto_bene = 0;
          nrighe = 0;
    }
    file.clear();
    file.seekg(0);
  }while(!letto_bene);
  //// End of automatic method.

  cerr << nrighe << endl;

  if(!test_stringa_decimale(str_freq)){ cerr << "error: \"" << str_freq << "\" is not a float." << endl; file.close(); return 1; }
  double freq = strtold(str_freq,NULL);

  if(!test_stringa_decimale(str_freqdot)){ cerr << "error: \"" << str_freqdot << "\" is not a float." << endl; file.close(); return 1; }
  double freqdot = strtold(str_freqdot,NULL);

  if(!test_stringa_decimale(str_origin)){ cerr << "error: \"" << str_origin << "\" is not a float." << endl; file.close(); return 1; }
  double origin = strtold(str_origin,NULL);

  if(!test_stringa_interi(str_nbins)){ cerr << "error: \"" << str_nbins << "\" is not an int." << endl; file.close(); return 1; }
  long long nbins = atoll(str_nbins);

  if(!test_stringa_decimale(str_wscanfreq)){ cerr << "error: \"" << str_wscanfreq << "\" is not a float." << endl; file.close(); return 1; }
  double wscanfreq = strtold(str_wscanfreq,NULL);

  if(!test_stringa_decimale(str_wscanfdot)){ cerr << "error: \"" << str_wscanfdot << "\" is not a float." << endl; file.close(); return 1; }
  double wscanfdot = strtold(str_wscanfdot,NULL);

  if(!test_stringa_interi(str_nscanfreq)){ cerr << "error: \"" << str_nscanfreq << "\" is not an int." << endl; file.close(); return 1; }
  long long nscanfreq = atoll(str_nscanfreq);

  if(!test_stringa_interi(str_nscanfdot)){ cerr << "error: \"" << str_nscanfdot << "\" is not an int." << endl; file.close(); return 1; }
  double nscanfdot = atoll(str_nscanfdot);


  ofstream ris;
  ris.open(str_outfilename);
  if(!ris.good()){ cerr << "output error: " << str_outfilename << endl; ris.close(); return 1; }

  //unsigned long long * isto = new unsigned long long[nbins+2]();  //Initialized with 0.
  double * times = new double[nrighe];
  double * phases = new double[nrighe];

  clock_t start;
  start = clock();

  //if(tempomax > tempomin){
  for(unsigned long i=0;i<nrighe;++i){
    //if(i%1000 == 0){cout << '\r' << str_nomefile << ": " << floor(((double)i)/nrighe*100) << "%"; cout.flush();}
    file >> times[i];
    //cout << times[i] << " ";
  }

  double freq_orig = freq;
  double fdot_orig = freqdot;


  for(long ifreq=0;ifreq<2*nscanfreq+1;++ifreq){
    long IFREQ = ifreq-nscanfreq;
    double deltaFREQ = IFREQ*wscanfreq/nscanfreq;
    for(long ifdot=0;ifdot<2*nscanfdot+1;++ifdot){
      long IFDOT = ifdot-nscanfdot;
      double deltaFDOT = IFDOT*wscanfdot/nscanfdot;

      freq=freq_orig+deltaFREQ;
      freqdot=fdot_orig+deltaFDOT;

      //cerr << freq << "  " << freqdot << endl;

      for(unsigned long i=0;i<nrighe;++i){
        if((times[i] >= tempomin) && (times[i] <= tempomax)){
          double delta = (times[i] - origin)*86400.0;
          double phase = freq*delta + 0.5*freqdot*delta*delta;
          double fracphase = fmod(phase,1);
          //if(fracphase<0) fracphase +=1;
          phases[i]=fracphase;
          //cerr << delta << "  " << phase << "  " << fracphase << endl;
          //long long binindex = istobin(fracphase,0,1,nbins);
          //isto[binindex]++;
        }
        else{
          phases[i]=-10;
        }
      }
      //else{
      //   for(int i=0;i<nrighe;i++){
      //    //if(i%1000 == 0){cout << '\r' << str_nomefile << ": " << floor(((double)i)/nrighe*100) << "%"; cout.flush();}
      //    double temp;
      //    file >> temp;
      //      double delta = (temp - origin)*86400.0;
      //      double phase = freq*delta + 0.5*freqdot*delta*delta;
      //      double fracphase = fmod(phase,1);
      //      phases[i]=fracphase;
      //      //////long long binindex = istobin(fracphase,0,1,nbins);
      //      //////isto[binindex]++;
      //  }
      //}
      //cout << '\r' << str_nomefile << ": 100%" << endl;
      file.close();
      //cout << "Time: " << (clock() - start) / (double)(CLOCKS_PER_SEC / 1000) << " ms" << endl; 
    
      //for(int i=1;i<2*nbins+3;i++){
      //  if(i==(nbins+1) || i==(nbins+2)) continue;
      //  ris << ((double)i+0.5)/nbins << "   " << isto[i%(nbins+2)] << endl;
      //}
      //ris.close();
    
      start = clock();
      double zeta[20];
      double hvalue=-1;
      unsigned optimum_harmonics = 1;
      unsigned long nused = 0;
      for(int i=1;i<=20;++i){
        //cerr << "# Cycle1: " << i << endl;
        zeta[i-1]=calculate_harmonic_sum(nrighe,phases,i,nused);
        if(i>1) zeta[i-1] += zeta[i-2];
        //cerr << "# zeta["<< i-1 << "] = " << zeta[i-1] << endl; 
    
        //for(int j=1;j<i;j++){
        //  //cerr << "## Cycle2 " << j << endl;
        //  //cerr << "## zeta[" << i-1 << "] + zeta[" << j-1 << "]  = " << zeta[i-1] << " + " << zeta[j-1] << "  = " << zeta[i-1] + zeta[j-1] << endl;
        //  zeta[i-1] += zeta[j-1];
        //}
        ////cerr << "## End of Cycle 2" << endl;
      }
      //cout << "Time: " << (clock() - start) / (double)(CLOCKS_PER_SEC / 1000) << " ms" << endl; 
      //cerr << "# End of Cycle 1" << endl;
    
      for(int i=0;i<20;++i){
        //cerr << "# Cycle 3: " << i << endl;
        zeta[i] *= 2*nused;
        double hi = zeta[i]-4*i;
        if(hi>hvalue){
          hvalue = hi;
          optimum_harmonics = i+1;
        }
      }
   
      //cerr << optimum_harmonics << endl;

      double pvalue = exp(-0.4*hvalue);
      chi_squared chisq_dist_opt(2*optimum_harmonics);
      chi_squared chisq_dist_10(20);
      double pvalue_zopt = 1-cdf(chisq_dist_opt,zeta[optimum_harmonics-1]);
      double pvalue_10   = 1-cdf(chisq_dist_10,zeta[9]);
  
      if((ifreq==0) && (ifdot==0)){
        ris << "## " << 2*nscanfreq+1 << " " << 2*nscanfdot+1 << endl;
        ris << "## FREQ"
            << "   FDOT"
            << "   OPT_HARM"
            << "   H"
            << "   ZETA_OPT^2"
            << "   ZETA_10^2"
            << "   PVALUE_H"
            << "   PVALUE_ZOPT"
            << "    PVALUE_Z10" << endl;
      }

      ris << scientific << setprecision(15) << freq                      << "   "
                                            << freqdot                   << "     "
                                            << optimum_harmonics         << "   "
                                            << hvalue                    << "   "
                                            << zeta[optimum_harmonics-1] << "   "
                                            << zeta[9]                   << "   "
                                            << pvalue                    << "   "
                                            << pvalue_zopt               << "   "
                                            << pvalue_10                 << endl;
    
  }
  }

  ris.close();

/*
  double media = 0.0;
  double chiquadro = 0.0;

  for(int i=1;i<nbins+1;i++){
    media += isto[i];
  }
  media /= nbins;
  for(int i=1;i<nbins+1;i++){
    chiquadro += (isto[i]-media)*(isto[i]-media)/media;
  }
  chiquadro /= nbins-2;
  
  cout << fixed << setprecision(14) << freq << "   " << media << "   " << chiquadro << endl;
*/
 
  return 0;
}



long long istobin(double x, double xmin, double xmax, unsigned long long nbins){
  if( x < xmin ){ return 0; }
  if( x >= xmax){ return nbins+1; }

  return round(     (     ( x - xmin )/( xmax - xmin )*nbins    )*1e6    )   /  1000000  + 1;

}


bool test_stringa_interi(char * str){
  int ncararg = 0;
  do{
    char c = str[ncararg];
    switch(c){
      case '0':
      case '1':
      case '2':
      case '3':
      case '4':
      case '5':
      case '6':
      case '7':
      case '8':
      case '9':
        ncararg++;
        break;
      default:
        return 0;
    }
  }while(str[ncararg]!='\0');
  
  return 1;
}

bool test_stringa_decimale(char * str){
  int ncararg = 0;
  bool punto = 0;
  bool is_e=0;
  bool is_sign=0;
  unsigned short n_minus=0;
  do{
    char c = str[ncararg];
    switch(c){
      case '0':
      case '1':
      case '2':
      case '3':
      case '4':
      case '5':
      case '6':
      case '7':
      case '8':
      case '9':
        ncararg++;
        break;
      case '.':
        if(!punto){
          ncararg++;
          punto = 1;
          break;
        }
	return 0;
      case 'e':
      case 'E':
        if(!is_e){
          ncararg++;
          is_e = 1;
          break;
        }
	return 0;
      case '+':
        if(!is_sign){
          ncararg++;
          is_sign = 1;
          break;
        }
	return 0;
      case '-':
        if(n_minus<2){
          ncararg++;
          n_minus++;
          break;
        }
	return 0;
      default:
        return 0;
    }
  }while(str[ncararg]!='\0');
  
  return 1;
}


double calculate_harmonic_sum(unsigned long nevents, const double *phases, unsigned harmonic, unsigned long & nused){
  double a=0,b=0;
  nused=0;
  for(unsigned long i=0;i<nevents;++i){
    if(phases[i]<-5) continue;
    a += cos(harmonic*phases[i]*2*M_PI);
    b += sin(harmonic*phases[i]*2*M_PI);
    nused += 1;
  }
  a /= nused;
  b /= nused;
  a = a*a;
  b = b*b;
  return (a+b);
}
