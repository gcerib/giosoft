#!/usr/bin/python2.7

import numpy as np
import re

import os

#General name of the ephemerides:
ephname = 'boomerang'

#The width of the segmented ephemerides
segment = 20.0

#Loads the table with the piece-wise ephemerides and their extremes
table = np.loadtxt("ephtable.txt",dtype=[('start',np.float128),('stop',np.float128),('ver',object)])

if(table.shape == ()):
 table = np.array([table])

#print table.shape
#print table

def main():
  listofsegments = []
  for i,eph in enumerate(table):
    local_pepoch = eph['start']+segment/2.0
    j = 0
    while(local_pepoch<eph['stop']):
      transportEPH(ephname+"_"+str(eph['ver'])+".par",local_pepoch,("temp_%01d-%02d" % (i,j))+".par")
      local_pepoch=local_pepoch+segment
      j=j+1
    for k in range(j):
      stripDerivatives(("temp_%01d-%02d" % (i,k))+".par","temptemp.par")
      os.system("mv temptemp.par "+("temp_%01d-%02d" % (i,k))+".par")
      listofsegments.append(("temp_%01d-%02d" % (i,k))+".par")
  
  #initial = listofsegments.pop(0)
  initial = listofsegments[0]
  os.system("cp "+initial+" chimera.par")
  for i,seg in enumerate(listofsegments):
    if(i==0):
      continue
    delt,n = compareEPH(listofsegments[i-1],seg)
    addGlitch("chimera.par",delt,i,"temp4.par")
    os.system("mv temp4.par chimera.par")
  

#########################################################################################################

def transportEPH(ephem,newpepoch,outname):
   """transportEPH( (str)ephem, (float)newpepoch, (str)outname):
      reads an ephemeris from "ephem", shifts it to newpepoch and
      writes the result in "outname".
   """

   A = np.genfromtxt(ephem,dtype=object,missing_values="nan",filling_values="nan",loose=True)
   
   key = A[:,0].astype(str)
   val = A[:,1].astype(object)
   
   oldpepoch = 0
   f = []
   rest = []
   
   #Parsing the file, reading frequencies into "f",
   #the pepoch into "oldpepoch" and all the rest into
   #the "rest" array.
   for k,v in zip(key,val):
     if(re.match("F\d+",k)):
       f.append((np.float128(k[1:]),np.float128(v)))
     elif(re.match("PEPOCH",k)):
       oldpepoch = np.float128(v)
     else:
       rest.append((k,v))
   
   #Sorting the frequency and its derivatives by their oder
   f = np.sort(np.asarray(f,dtype=[('order',np.float128),('value',np.float128)]),order='order')['value']
   
   #print "## OLD PEPOCH:",oldpepoch
   #print "## GRADE:",len(f)
   #print "## FREQS:",
   #for fn in f:
   # print '%0.5e' % fn,' ',
   #print
   
   delta = np.float128((newpepoch - oldpepoch)*86400.)
   
   newf = np.zeros_like(f)

   #Taylor expansion of the frequencies to compute the new ones
   for i,newfn in enumerate(newf):
     #print i
     for j,oldfn in enumerate(f[i:]):
      #print '    ',j,oldfn
      #print '       ? ',newfn,'  +  ',oldfn,' * ',delta,'**(',j,')/fact(',j,')'
      newfn = newfn + oldfn*delta**(j)/np.math.factorial(j)
      #print '       =>',newfn
     newf[i]=newfn
   #print "## NEW PEPOCH:",newpepoch
   #print "## FREQS:",

   #Appending them to the rest of the ephemeris:
   for i,fn in enumerate(newf):
    #print '%0.5e' % fn,' ',
    rest.append(('F%d'%i,repr(fn)))
   rest.append(('PEPOCH',repr(newpepoch)))
   #print
   
   ofile = open(outname,"w")

   for tupla in rest:
     ofile.write(tupla[0])
     ofile.write('   ')
     ofile.write(tupla[1])
     ofile.write('\n')
   ofile.close()
   #Bye.

def stripDerivatives(infile,outfile):
  """stripDerivatives( (str)infile, (str)outfile ):
     reads a par file from "infile", removes all derivatives
     higher than the second one and writes it in "outfile".
  """

  ifile = open(infile,"r")
  ofile = open(outfile,"w")
  lines = ifile.readlines()
  for line in lines:
    if(( re.match("F\d+",line[:2]) ) and ( int(line[1:2]) > 2 ) ):
      continue
    for elem in line.split():
      ofile.write(elem)
      ofile.write("      ")
    ofile.write('\n')
  ofile.close()

def compareEPH(infile1,infile2):
  """compareEPH( (str)infile1, (str)infile2 ):
     Computes the difference between the frequency, its first
     and second derivaties of the ephemeris in "infile1" and
     the ones in "infile2" at the epoch of "infile2".
     Returns a tuple with the deltas and the number of glitches
     in "infile1".
  """

  #Reads the ephemerides
  eph1 = np.genfromtxt(infile1,dtype=[('key',object),('val',object)])
  eph2 = np.genfromtxt(infile2,dtype=[('key',object),('val',object)])

  print "Comparing "+infile1+" and "+infile2+":"

  pepoch1 = np.float128(eph1[eph1['key']=='PEPOCH']['val'][0])
  pepoch2 = np.float128(eph2[eph2['key']=='PEPOCH']['val'][0])

  #Shifts the first one to the pepoch of the second one.
  transportEPH(infile1,pepoch2,"temp3.par")

  eph1 = np.genfromtxt("temp3.par",dtype=[('key',object),('val',object)])

  f0_1 = np.float128(eph1[eph1['key']=='F0']['val'][0])
  f1_1 = np.float128(eph1[eph1['key']=='F1']['val'][0])
  f2_1 = np.float128(eph1[eph1['key']=='F2']['val'][0])

  print "   file1:",pepoch1,f0_1, f1_1, f2_1

  f0_2 = np.float128(eph2[eph2['key']=='F0']['val'][0])
  f1_2 = np.float128(eph2[eph2['key']=='F1']['val'][0])
  f2_2 = np.float128(eph2[eph2['key']=='F2']['val'][0])

  print "   file2:",pepoch2,f0_2, f1_2, f2_2

  deltaF0 = f0_2 - f0_1
  deltaF1 = f1_2 - f1_1
  deltaF2 = f2_2 - f2_1

  print "   df2-1:",deltaF0,deltaF1,deltaF2

  ngli = 0

  for k in eph1['key']:
    if(re.match("GLEP_\d+",k)):
      ngli = ngli +1

  print "   number of glitches before: ",ngli

  for b in range(ngli):
    n = b+1
    glf0_n = np.float128(eph1[eph1['key']==("GLF0_%d" % n)]['val'][0])
    glf1_n = np.float128(eph1[eph1['key']==("GLF1_%d" % n)]['val'][0])
    glf2_n = np.float128(eph1[eph1['key']==("GLF2_%d" % n)]['val'][0])
    deltaF0 = deltaF0 - glf0_n
    deltaF1 = deltaF1 - glf1_n
    deltaF2 = deltaF2 - glf2_n
    print "      read glitch n."+str(n)+":"
    print "         glitch"+str(glf0_n)+" "+str(glf1_n)+" "+str(glf2_n)
    print "         newdel",deltaF0,deltaF1,deltaF2

  print "   final deltas: ",(pepoch2,deltaF0,deltaF1,deltaF2),ngli

  return (pepoch2,deltaF0,deltaF1,deltaF2),ngli

def addGlitch(infile,deltas,ngli,outfile):
  """addGlitch( (str)infile, (float,float,float,float)deltas, (int)ngli ):
     adds a glitch to the ephemeris in "infile" by using the
     epoch,df0,df1,df2 values in the truple "deltas" and the index ngli.
  """
  os.system("cp "+infile+" "+outfile)
  ofile2 = open(outfile,"a")
  ofile2.write("GLEP_%d    %0.15e\n" %(ngli,deltas[0])) #Epoch
  ofile2.write("GLF0_%d    %0.15e\n" %(ngli,deltas[1])) #dF0
  ofile2.write("GLF1_%d    %0.15e\n" %(ngli,deltas[2])) #dF1
  ofile2.write("GLF2_%d    %0.15e\n" %(ngli,deltas[3])) #dF2
  ofile2.close()

main()

