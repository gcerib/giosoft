#!/usr/bin/python2.7

import numpy as np
import os

#arctable = np.loadtxt("arctable.txt",dtype=[("name",object),('start',np.float128),('end',np.float128)])
arctable = np.loadtxt("ephtable.txt",dtype=[('start',np.float128),('end',np.float128),("name",object)])
toas     = np.loadtxt("output.txt",dtype=[('n',int),('mjd',np.float128),('bary',np.float128),('phase',np.float128),('intphase',int)])

for i,arc in enumerate(arctable):
  mask = ((toas['bary']>=arc['start'])*(toas['bary']<arc['end']))
  A = toas[mask]
  np.savetxt(str(arc['name'])+'.toa',A)

  os.system("root -b -q istofasi.C\\(\\\""+str(arc['name'])+'.toa'+"\\\"\\)")
  if(i==0):
    os.system("mv PH000.phg saveme.phg")
  else:
    os.system("mv PH000.phg PH%03d.phg" % i)

os.system("mv saveme.phg PH000.phg")
