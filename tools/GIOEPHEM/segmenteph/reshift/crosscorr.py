#!/usr/bin/python2.7

import sys
from scipy import signal
import numpy as np

from numpy.fft import fft, ifft

i=int(sys.argv[1])

ref = np.loadtxt("REF.phg")
ph0 = np.loadtxt("PH"+("%03d" % i)+".phg")

nbins=ref.shape[0]
A = np.copy(ref[:,1])
B = np.copy(ph0[:,1])

sA = np.copy(ref[:,2])
sB = np.copy(ph0[:,2])

def periodic_corr(x, y):
  return ifft(fft(x) * fft(y).conj()).real

def errcorr(x, y, sx, sy):
  n=len(x)
  errcorr=np.zeros((n,1))
  for m in range(n):
    for r in range(n):
      errcorr[m] = errcorr[m]  + ( x[(r+m)%n]*x[(r+m)%n]*sy[r]*sy[r] + y[(r-m)%n]*y[(r-m)%n]*sx[r]*sx[r] )
  errcorr = np.sqrt(errcorr)
  return np.array(errcorr)

def getcovariance(m,n):
  cov = 0
  for r in range(nbins):
    cov  = cov + sA[r]*sA[r]*B[(r-m)%nbins]*B[(r-n)%nbins] + sB[r]*sB[r]*A[(r+m)%nbins]*A[(r+n)%nbins]
  if(m==n):
    for r in range(nbins):
     cov = cov + sA[r]*sA[r]*sB[(r-m)%nbins]*sB[(r-m)%nbins]
  return cov

print "Computing cross-correlation...",
cor = periodic_corr(ref[:,1],ph0[:,1])
#err = errcorr(ref[:,1],ph0[:,1],ref[:,2],ph0[:,2])
ph0[:,1] = cor
ph0[:,0] = ph0[:,0] - 1 - 1.0/(len(ph0[:,0]))
#ph0 = np.append(ph0,err,axis=1)
np.savetxt("COR"+("%03d" % i)+".xcr",ph0)
print "done!"

#print err[3]

#print "Computing reference auto-correlation...",
#auto = periodic_corr(ref[:,1],ref[:,1])
#auterr = errcorr(ref[:,1],ref[:,1],ref[:,2],ref[:,2])
#ref[:,1] = auto
#ref[:,0] = ref[:,0] - 1 - 1.0/(len(ph0[:,0]))
#ref = np.append(ref,auterr,axis=1)
#np.savetxt("AUTO.xcr",ref)
#print "done!"



COVARIANCE=np.zeros((nbins,nbins))

it = np.nditer(COVARIANCE, flags=['multi_index'], op_flags=['readwrite'])
while not it.finished:
  it[0]=getcovariance(it.multi_index[0],it.multi_index[1])
  #print "(",it.multi_index[0],",",it.multi_index[1],")  =  ",it[0],"  ",it.iterindex
  if((it.iterindex%1000)==0):
    sys.stderr.write("                                     \r")
    sys.stderr.write("Computing covariance matrix... %0.2f %%" % (float(it.iterindex)/nbins/nbins*100))
    sys.stderr.flush()
  it.iternext()
sys.stderr.write('\n')

np.savetxt("COR"+("%03d" % i)+".cov",COVARIANCE)

print len(ph0[:,0])
