#!/usr/bin/python2.7

import matplotlib.pyplot as plt
import scipy.optimize as opt
from   scipy.stats import chi2
import numpy as np

import sys
import os

def parabola(x,a,b,c):
    return b*(x-a)**2 + c

def iperbole(x,a,b,c):
    #print x,a,b,c
    return -np.sqrt(np.abs(b)*(x-a)**2+1)+c

def gaussiana(x,a,b,c):
    return c*np.exp(-0.5*((x-a)/b)**2)

def run(i):
  nome = "COR%03d.xcr" % i
  a = np.loadtxt(nome)

  nomecov = "COR%03d.cov" % i
  COV = np.loadtxt(nomecov)
  #print COV.shape
  
  PROFONDITA = 0.6

  nbins=len(a)
  unbins=nbins/2
  semi=unbins/2
  
  picco=a[(a[semi:,1]).argmax()+semi]
  valle=a[(a[semi:,1]).argmin()+semi]
  argpicco=(a[semi:,1]).argmax()+semi
  
  delta = picco[1]-valle[1]
  
  a[:,1]=(a[:,1]-valle[1])/delta
  COV = COV/delta/delta
  ERR  = np.sqrt(np.diag(COV))
 
  b = a.copy()
  (b[:,1])[b[:,1]>PROFONDITA]=0
  (b[:,1])[argpicco:nbins-1]=0
  (b[:,1])[0:argpicco-int(0.2*unbins)]=0
  
  c = a.copy()
  (c[:,1])[c[:,1]>PROFONDITA]=0
  (c[:,1])[argpicco+int(0.2*unbins):nbins-1]=0
  (c[:,1])[0:argpicco]=0
  
  #plt.plot(b[:,0],b[:,1])
  #plt.show()
  
  maxs = [np.argmax(b[:,1]),np.argmax(c[:,1])+1]
  
  #print ((a[:,0])[maxs[0]],(a[:,0])[maxs[1]])
  fitrange=a[maxs[0]:maxs[1],0]
  fitvalue=a[maxs[0]:maxs[1],1]
  #print maxs
  #print fitrange
  print fitvalue

  COV1 = COV[maxs[0]:maxs[1],maxs[0]:maxs[1]]
  ERR1 = ERR[maxs[0]:maxs[1]]

  vetl =  np.array([fitvalue])
  vetc =  np.array([fitvalue]).T
  exey =  np.dot(vetc,vetl)

  #for j in range(33):
  # for k in range(33):
  #  print exey[j][k]

  #print COV1
  #print
  #print exey
  #print
  #print

  COVLOG1 = np.log(1.0 + COV1/exey)
  ERRLOG1 = np.sqrt(np.diag(COVLOG1))

  #COVLOG1  = np.ones_like(COV1)

  """
  plt.subplot(121)
  plt.plot(fitrange,np.log(fitvalue))
  
  
  plt.subplot(122)
  plt.plot(a[:,0],a[:,1])
  plt.show()
  """

  #fitvalue[fitvalue.argmin()] = 1e-5

  try:  
    popt,pcov = opt.curve_fit(parabola,fitrange,np.log(fitvalue),(0.01,20,0),sigma=COVLOG1,absolute_sigma=True)
    popt2,pcov2 = opt.curve_fit(iperbole,fitrange,np.log(fitvalue),(0.01,20,1),sigma=COVLOG1,absolute_sigma=True)
  except:
    print "ERROR"
    return
  #popt3,pcov3 = opt.curve_fit(gaussiana,fitrange,np.log(fitvalue),(0.01,0.03,1),sigma=COVLOG1,absolute_sigma=True)
  perr = np.sqrt(np.diag(pcov))
  perr2 = np.sqrt(np.diag(pcov2))
  #perr3 = np.sqrt(np.diag(pcov3))

  delta1  = np.array([(np.log(fitvalue) - parabola(fitrange, *popt))])
  chirid1 = (np.dot(delta1,np.dot(np.linalg.inv(COVLOG1),delta1.T))/(len(fitrange)-3))[0][0]
  delta2  = np.array([(np.log(fitvalue) - iperbole(fitrange, *popt2))])
  chirid2 = (np.dot(delta2,np.dot(np.linalg.inv(COVLOG1),delta2.T))/(len(fitrange)-3))[0][0]
 
  #print 
  #print "Parabola:"
  #print "  par  = ", popt
  #print "  err  = ", perr
  #print " chiqr = ", chirid1
  #print
  #print "Iperbole:"
  #print "  par  = ", popt2
  #print "  err  = ", perr2
  #print " chiqr = ", chirid2
  #print
  #print "Gaussiana:"
  #print "  par = ", popt3
  #print "  err = ", perr3

  os.system("echo -n \"$(head -n 1 PH%03d.phg | awk '{gsub(\"MJD=\",\"\");print($2)}')    \" >> DRIFT.txt" % i)
  ofile=open("DRIFT.txt","a")

  ofile.write("%.15f   %.15f   %.15f   %.15f   %.15f   %.15f\n" % (float(popt[0]),float(perr[0]),float(chirid1),float(popt2[0]),float(perr2[0]),float(chirid2)))
  ofile.close()

  ngdl = len(fitrange)-3
  chiq = chi2(ngdl)
  showplot=1
  if(showplot):
    fig, axes = plt.subplots(2, figsize=(14, 7))
    finerange = np.linspace(fitrange[0],fitrange[-1],200)
    plt.tight_layout()
    plt.subplot(122)
    plt.tight_layout()
    plt.errorbar(fitrange,np.log(fitvalue),yerr=ERRLOG1,fmt='o')
    plt.plot(finerange,(parabola(finerange, *popt)), 'r', label='fit: a=%5.3f, b=%5.3f, c=%5.3f' % tuple(popt))
    plt.plot(finerange,(iperbole(finerange, *popt2)), 'g', label='fit: a=%5.3f, b=%5.3f, c=%5.3f' % tuple(popt2))
    plt.figtext(0.85, 0.12, r'$\chi^2_r =$ %0.2f, P=%0.2f'%(chirid1,chiq.sf(chirid1*ngdl)), fontsize=16, color='r')
    plt.figtext(0.85, 0.09, r'$\chi^2_r =$ %0.2f, P=%0.2f'%(chirid2,chiq.sf(chirid2*ngdl)), fontsize=16, color='g')
    plt.title(r'Fits of $\log{\mathcal{X}_{%d}}$ with a parabola and a hyperbole (taking correlation or data errors into account)'%i)
    #plt.plot(fitrange,np.exp(gaussiana(fitrange, *popt3)), 'c', label='fit: a=%5.3f, b=%5.3f, c=%5.3f' % tuple(popt3))
    
    
    plt.subplot(121)
    plt.tight_layout()
    plt.errorbar(a[:,0],a[:,1],yerr=ERR,fmt='o')
    plt.plot(finerange,np.exp(parabola(finerange, *popt)), 'r', label='fit: a=%5.3f, b=%5.3f, c=%5.3f' % tuple(popt))
    plt.plot(finerange,np.exp(iperbole(finerange, *popt2)), 'g', label='fit: a=%5.3f, b=%5.3f, c=%5.3f' % tuple(popt2))
    plt.title(r'Cross-correlation $\mathcal{X}_{%d}$ function with (correlated) errors'%i)
    #plt.plot(fitrange,(gaussiana(fitrange, *popt3)), 'g', label='fit: a=%5.3f, b=%5.3f, c=%5.3f' % tuple(popt3))
  
    plt.show()
  else:
    print chirid1,"    ",chiq.sf(chirid1*ngdl),"          ",chirid2,"    ",chiq.sf(chirid2*ngdl)

#for i in np.arange(0,9):
#  run(i)
run(7)
run(8)
