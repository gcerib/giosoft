void plotshifts(){

  TCanvas * c1 = new TCanvas("c1","c1",1200,500);

  //TH2D * isto = new TH2D("isto","Phase Stack",571,54500,58550,60,0,2);
  ifstream file;
  file.open("DRIFT.txt");
  int ndati = 84;
  double *mjd = new double[ndati];
  double *phase = new double[ndati];
  double *pherr = new double[ndati];
  for(int i=0;i<ndati;i++){
    string oblio;
    file >> mjd[i] >> phase[i] >> pherr[i] >> oblio >> oblio >> oblio >> oblio;
    //isto->Fill(mjd,phase);
    //isto->Fill(mjd,phase+1);
  }
  file.close();
  //isto->SetMaximum(500);
  //isto->GetXaxis()->SetTitle("Time [MJD]");
  //isto->GetYaxis()->SetTitle("Phase");
  //isto->Draw("CONT4");
  //isto->Draw();

  TGraphErrors * grafo = new TGraphErrors(ndati,mjd,phase,0,pherr);
  grafo->Draw();

  c1->Update();

  ifstream ephtable;
  ephtable.open("ephtable.txt");
  int neph = 4;
  double * jump = new double[neph];
  double * jumpi = new double[neph];
  for(int i=0;i<neph;i++){
     string oblio;
     ephtable >> jumpi[i] >> jump[i] >> oblio;
     //cerr << jump[i];
  }
  ephtable.close();

/*
  ifstream arctable;
  arctable.open("arctable.txt");
  int narc = 86;
  double * arcs = new double[narc];
  for(int i=0;i<narc;i++){
     string oblio;
     arctable >> oblio >> oblio >> arcs[i];
     //cerr << jump[i];
  }
  arctable.close();
*/

  double * phasejump = new double[neph];
  double * phasejumpe = new double[neph];

  //double glitch[6] = {53064,54110,54781.54,55134,55601,56366.59};
  TLine **line = new TLine*[neph];
  for(int j=0;j<neph;j++){
     if(jump[j]<c1->GetUxmin()) continue;
     line[j] = new TLine(jump[j],c1->GetUymin(),jump[j],c1->GetUymax());
     line[j]->SetLineColor(kRed);
     line[j]->SetLineWidth(1);
     line[j]->Draw();

     cerr << jumpi[j] << "   " << jump[j] << endl;

     TFitResultPtr ris = grafo->Fit("pol0","S","",jumpi[j],jump[j]);
     phasejump[j] = ris->Parameter(0); 
     phasejumpe[j] = ris->ParError(0); 

  }

  double * phasejump0 = new double[neph];

  for(int j=1;j<neph;j++){

     phasejump0[j] = -(phasejump[j]-phasejump[j-1]);
     cerr << setprecision(9) << phasejump0[j] << endl; //"    " << phasejumpe[j] << endl;
  }
  
}
