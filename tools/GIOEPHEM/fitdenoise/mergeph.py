#!/usr/bin/python

import numpy as np

a=np.loadtxt("EPH0.txt")
b=np.loadtxt("EPH_DELTA.txt")

c=a+b
d=np.sqrt(a*a+b*b)

for i in range(len(a)):
  print "F%d\t\t%.15e\t\t0\t\t%.15e" % (i,c[i][0],d[i][1])
