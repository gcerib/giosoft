#!/usr/bin/python2.7

import numpy as np
import sys

f = open("DRIFT.txt","r")

lines = f.readlines()

add = 0
goodlines = []

for i,e in enumerate(lines):
  if(e[0]=='#' and e[1]=='@'):
    add = int(e[2:])
    continue
  if(e[0]=='#'):
    continue
  nums = np.asarray(e.split(),dtype=np.float128)
  nums[1] = nums[1] + add
  nums[4] = nums[4] + add
  goodlines.append(nums)

np.savetxt("NEWDRIFT.txt",np.asarray(goodlines,dtype=np.float128))
