#!/usr/bin/python2.7

import numpy as np
import sys

A = np.loadtxt(sys.argv[1])

massimo = (A[:,2]).max()
minimo  = (A[:,2]).min()

delta = massimo-minimo


A[:,3] = A[:,3]/delta
A[:,2] = (A[:,2]-minimo)/delta

#valore2 = A[-1][2]
#A[:,3] = A[:,3]/valore2
#A[:,2] = A[:,2]/valore2

np.savetxt('s'+sys.argv[1],A)
