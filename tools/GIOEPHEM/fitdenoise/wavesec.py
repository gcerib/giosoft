#!/usr/bin/env python2.7

import numpy as np

F0 = np.float128('4.217543367886925e+00')

A = np.loadtxt("WAVES.txt",dtype=object)

((A[1:])[:,1]) = ((((A[1:])[:,1]).astype(np.float128))/F0).astype(str)
((A[1:])[:,2]) = ((((A[1:])[:,2]).astype(np.float128))/F0).astype(str)

A = A.astype(str)
np.savetxt("WAVESEC.txt",A,fmt="%s")
