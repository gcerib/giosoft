#!/usr/bin/python2.7

import numpy as np
import sys

import re

ephem = sys.argv[1]
newpepoch = np.float128(sys.argv[2])

A = np.genfromtxt(ephem,dtype=object,missing_values="nan",filling_values="nan",loose=True)

key = A[:,0].astype(str)
val = A[:,1].astype(object)

D = dict(zip(key,val))

oldpepoch = 0
f = []
rest = []

for k,v in zip(key,val):
  if(re.match("F\d+",k)):
    f.append((np.float128(k[1:]),np.float128(v)))
  elif(re.match("PEPOCH",k)):
    oldpepoch = np.float128(v)
  else:
    rest.append((k,v))

f = np.sort(np.asarray(f,dtype=[('order',np.float128),('value',np.float128)]),order='order')['value']

print "## OLD PEPOCH:",oldpepoch
print "## GRADE:",len(f)
print "## FREQS:",
for fn in f:
 print '%0.5e' % fn,' ',
print

delta = (newpepoch - oldpepoch)*86400.

newf = np.zeros_like(f)
for i,newfn in enumerate(newf):
  #print i
  for j,oldfn in enumerate(f[i:]):
   #print '    ',j,oldfn
   #print '       ? ',newfn,'  +  ',oldfn,' * ',delta,'**(',j,')/fact(',j,')'
   newfn = newfn + oldfn*delta**(j)/np.math.factorial(j)
   #print '       =>',newfn
  newf[i]=newfn
print "## NEW PEPOCH:",newpepoch
print "## FREQS:",
for i,fn in enumerate(newf):
 print '%0.5e' % fn,' ',
 rest.append(('F%d'%i,repr(fn)))
rest.append(('PEPOCH',repr(newpepoch)))
print

for tupla in rest:
 print tupla[0],'   ',tupla[1]
