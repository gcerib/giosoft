#include <string>
#include <strstream>

using namespace std;
int gettimebin(double mjd, const double* mjdbins, int ntimebins);

void istofasi(){
  
  int nbin = 512;
  int ntimebins = 360;
  
  
  ifstream fileref;
  fileref.open("00.txt");
  
  unsigned long nrighe_ref = 0;
  string line;
  while (getline(fileref, line)) nrighe_ref++;
  
  cout << nrighe_ref << endl;
  
  fileref.seekg(0);
  fileref.close();
  fileref.open("00.txt");
  
  
  ifstream file;
  file.open("01.txt");
  
  string iline,uline;
  getline(file,iline);
  
  unsigned long nrighe = 0;
  do{
    nrighe++;
    uline=line;
  } while (getline(file, line));

  cout << nrighe << endl;

  file.seekg(0);
  file.close();
  file.open("01.txt");
  
  
  TH1D * istoref = new TH1D("istoref","istoref",nbin,0,2);
  for(unsigned long i=0;i<nrighe_ref;i++){
   double oblio;
   double fase;
   fileref >> oblio >> oblio >> oblio >> fase >> oblio;
   istoref->Fill(fase);
   istoref->Fill(fase+1);
  }
  fileref.close();
  istoref->Draw();
  
  ofstream ofileref;
  ofileref.open("REF.phg");
  
  for(int j=1;j<nbin+1;j++){
    ofileref << istoref->GetBinCenter(j) << "      " << istoref->GetBinContent(j)  -  2*(double)nrighe_ref/nbin << "      " << TMath::Sqrt(istoref->GetBinContent(j)) << endl;
  }
  
  ofileref.close();
  
  
  double oblio,mjdstart,mjdstop;
  stringstream primariga;
  primariga << iline;
  primariga >> oblio >> oblio >> mjdstart;
  
  stringstream ultimariga;
  ultimariga << uline;
  ultimariga >> oblio >> oblio >> mjdstop;
 
  mjdstart -= 1.0/86400.0;
  mjdstop  += 1.0/86400.0; 
  cout << fixed << setprecision(15) << mjdstart << "      " << mjdstop << endl;
  
  double intervallo = mjdstop-mjdstart;
  double passo = intervallo/ntimebins;

  cout << "[ ";
  double *mjdbins = new double[ntimebins+1];
  for(int j=0;j<ntimebins+1;j++){
    mjdbins[j] = mjdstart+(j*passo);
    cout << mjdbins[j];
    if(j!=ntimebins) cout << ", ";
  }
  double *mjdbincen = new double[ntimebins];
  for(int j=0;j<ntimebins;j++){
    mjdbincen[j] = (mjdbins[j]+mjdbins[j+1])/2.0;
  }
  cout << " ]" << endl;
  double *mjdmedio = new double[ntimebins]();
  double *nphot    = new double[ntimebins]();

  for(int ii=0;ii<ntimebins;ii++){
     mjdmedio[ii]=0;
     nphot[ii]=0;
  }
  
  TH1D ** isto = new TH1D*[ntimebins];
  for(int j=0;j<ntimebins;j++){
    isto[j] = new TH1D(Form("isto_%d",j),Form("isto_%d",j),nbin,0,2);
  }
  
  ofstream ofile;
  
  for(unsigned long i=0;i<nrighe;i++){
    double mjdbar = 0;
    double oblio;
    double mjdbar;
    double fase;
    file >> oblio >> oblio >> mjdbar >> fase >> oblio;
 
    int j = gettimebin(mjdbar,mjdbins,ntimebins);
    //if((i%1000)==0){
    //  cerr << mjdbar << "   " << j << endl;
    //}
    if(j<0){
      cerr << "Underflow: " << mjdbar << endl;
      continue;
    }
    if(j>=ntimebins){
      cerr << "Overflow:" << mjdbar << endl;
      continue;
    }
    isto[j]->Fill(fase);
    isto[j]->Fill(fase+1);
    mjdmedio[j] += mjdbar;
    if(mjdmedio[j] != mjdmedio[j]){
      cerr << "It became nan!" << endl;
    }
    nphot[j]++;
  }
  
  for(int j=0;j<ntimebins;j++){
    mjdmedio[j] /= nphot[j];
    ofile.open(Form("PH%03d.phg",j));
    ofile << fixed << setprecision(15) << "### MJD=" << mjdbincen[j] << " , MJDW=" << mjdmedio[j] << " , PHOTONS=" << nphot[j] << endl;
    for(int k=1;k<nbin+1;k++){
      ofile << isto[j]->GetBinCenter(k) << "     " << isto[j]->GetBinContent(k)-2*nphot[j]/nbin <<  "      " << TMath::Sqrt(isto[j]->GetBinContent(k))  << endl;
    }
    ofile << flush;
    ofile.close();
  }
  
  /*
  for(int j=0;j<ntimebins;j++){
    double mjdbar = 0;
    bool stop = 0;
    long nphot = 0;
    while(!stop){
      double oblio;
      double mjdbarpart;
      double fase;
      file >> oblio >> oblio >> mjdbarpart >> fase >> oblio;
      int k = j;
  
      if(mjdbarpart  >=  mjdstart+(j+1)*passo){
        k = (j<ntimebins-1)?j+1:j;
        stop=1;
        cerr << "(" << j << ")     salto!" << endl;
      }
  
      isto[k]->Fill(fase);
      isto[k]->Fill(fase+1);
      mjdbar += mjdbarpart;
      nphot++;
    }
  
    mjdbar /= nphot;
    ofile.open(Form("PH%02d.phg",j));
    ofile << "### " << mjdbar << endl;
    for(int k=0;k<nbin+1;k++){
      ofile << isto[j]->GetBinCenter(k) << "     " << isto[j]->GetBinContent(k) << endl;
    }
    ofile << flush;
    ofile.close();
  }
  */
  
  //isto->Draw();
  return;
}


int gettimebin(double mjd, const double* mjdbins, int ntimebins){
  int indice=-1;
  bool trovato;
  for(int i=-1;i<ntimebins;i++){
    if(mjd<mjdbins[i+1]){
      trovato=1;
      indice=i;
      break;
    }
  }
  if(indice==-1){
    cerr << fixed << setprecision(15) << "      " << mjd << "   " << mjdbins[0] << endl;
  }
  return (trovato==1)?indice:ntimebins;
}
