#!/usr/bin/python2.7

from scipy import signal
import numpy as np

from numpy.fft import fft, ifft, rfft

from math import pi

fasi = np.loadtxt("GLOBAL.phg")[:256]
fourier = rfft(fasi[:,1])

#for i in range(len(fourier)):
#  print fourier[i]

F0=4.217543367886925e+00
TZRMJD=55641.655276521851192


print "TZRMJD       = %0.15f" % TZRMJD
print
print "F1           = ", fourier[1]
print "|F1|         = ", np.abs(fourier[1])
print "F1/|F1|      = ", fourier[1]/np.abs(fourier[1])
print "PHI          = ", np.angle(fourier[1]/np.abs(fourier[1]))
print "D_PHI        = ", (-pi -np.angle(fourier[1]/np.abs(fourier[1])))
print "D_FASE       = ", (-pi -np.angle(fourier[1]/np.abs(fourier[1])))/(2*pi)
print "D_TEMPO[sec] = ", (-pi -np.angle(fourier[1]/np.abs(fourier[1])))/(2*pi)/F0
print "D_TEMPO[mjd] = ", (-pi -np.angle(fourier[1]/np.abs(fourier[1])))/(2*pi)/F0/86400.0
print
print "TZRMJD       = %0.15f" % (   TZRMJD   -  ((-pi -np.angle(fourier[1]/np.abs(fourier[1])))/(2*pi)/F0)/86400.0  )
