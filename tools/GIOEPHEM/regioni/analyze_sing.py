#!/usr/bin/python2.7

import matplotlib.pyplot as plt
import matplotlib.patches as patches
import scipy.optimize as opt
import numpy as np

import sys

def parabola(x,a,b,c):
    return b*(x-a)**2 + c

def iperbole(x,a,b,c):
    return -np.sqrt(b*(x-a)**2+1)+c

a = np.loadtxt(sys.argv[1])

#a[:,0] = a[:,0]

nbins=len(a)
unbins=nbins/2
semi=unbins/2

picco=a[(a[:,1]).argmax()]
valle=a[(a[:,1]).argmin()]
argpicco=(a[:,1]).argmax()

delta = picco[1]-valle[1]

a[:,1]=(a[:,1]-valle[1])/delta

### CALCOLO FWHM DI P2
b = a.copy()
(b[:,1])[b[:,1]>0.5]=0
(b[:,1])[argpicco:nbins+1]=0
(b[:,1])[0:argpicco-int(0.2*unbins)]=0

c = a.copy()
(c[:,1])[c[:,1]>0.5]=0
(c[:,1])[argpicco+int(0.2*unbins):nbins+1]=0
(c[:,1])[0:argpicco]=0

maxs = [np.argmax(b[:,1]),np.argmax(c[:,1])]

### CALCOLO FWHM DI P1
d = a.copy()
d[maxs[0]:maxs[1],1]=0
d[(maxs[0]+unbins):(maxs[1]+unbins),1]=0
dmax = np.amax(d[semi:nbins,1])
dargpicco = np.argmax(d[semi:nbins,1])+semi

e = d.copy()
(e[:,1])[e[:,1]>dmax/2.0]=0
(e[:,1])[dargpicco:nbins+1]=0
(e[:,1])[0:dargpicco-int(0.2*unbins)]=0

f = d.copy()
(f[:,1])[f[:,1]>dmax/2.0]=0
(f[:,1])[dargpicco+int(0.2*unbins):nbins+1]=0
(f[:,1])[0:dargpicco]=0

dmaxs = [np.argmax(e[:,1]),np.argmax(f[:,1])]


### CALCOLO DELL'OFF
NSIGME = 4
fwhm_p2 = a[maxs[1]][0]-a[maxs[0]][0]
fwhm_p1 = a[dmaxs[1]][0]-a[dmaxs[0]][0]

sigma_p2= (fwhm_p2/(2*np.sqrt(2*np.log(2))))
sigma_p1= (fwhm_p1/(2*np.sqrt(2*np.log(2))))

fig, ax = plt.subplots()

ax.add_patch( patches.Rectangle( (a[argpicco][0]+NSIGME*sigma_p1, 0), a[dargpicco][0]-NSIGME*sigma_p2-a[argpicco][0]-NSIGME*sigma_p1, 1, hatch='\\',fill=False) )


### PLOTTERIA
plt.tight_layout()
#plt.subplot(221)
plt.plot(a[:,0],a[:,1])
#plt.subplot(222)
plt.plot(b[:,0],b[:,1],'r')
#plt.subplot(223)
plt.plot(c[:,0],c[:,1],'r')

plt.plot(e[:,0],e[:,1],'g')
plt.plot(f[:,0],f[:,1],'g')



print "P2:"
#print maxs
print "%0.9f      %0.9f" % (a[maxs[0]][0],a[maxs[1]][0])
print
print "P1:"
#print dmaxs
print "%0.9f      %0.9f" % (a[dmaxs[0]][0],a[dmaxs[1]][0])
print
print "OFF:"
print "%0.9f      %0.9f" % (a[argpicco][0]+NSIGME*sigma_p1, a[dargpicco][0]-NSIGME*sigma_p2)



plt.show()
"""
#print ((a[:,0])[maxs[0]],(a[:,0])[maxs[1]])
fitrange=a[maxs[0]:maxs[1],0]
fitvalue=a[maxs[0]:maxs[1],1]
#print fitrange
#print fitvalue


plt.subplot(121)
plt.plot(fitrange,np.log(fitvalue))


plt.subplot(122)
plt.plot(a[:,0],a[:,1])
plt.show()


popt,pcov = opt.curve_fit(parabola,fitrange,np.log(fitvalue),(0,20,0),check_finite=False)
popt2,pcov2 = opt.curve_fit(iperbole,fitrange,np.log(fitvalue),(0,20,1),check_finite=False)
perr = np.sqrt(np.diag(pcov))
perr2 = np.sqrt(np.diag(pcov2))

print "Parabola (gaussiana):"
print "  par = ", popt
print "  err = ", perr
print
print "Iperbole:"
print "  par = ", popt2
print "  err = ", perr2

plt.subplot(121)
plt.plot(fitrange,np.log(fitvalue),'c')
plt.plot(fitrange,parabola(fitrange, *popt), 'r', label='fit: a=%5.3f, b=%5.3f, c=%5.3f' % tuple(popt))
plt.plot(fitrange,iperbole(fitrange, *popt2), 'g', label='fit: a=%5.3f, b=%5.3f, c=%5.3f' % tuple(popt2))


plt.subplot(122)
plt.plot(a[:,0],a[:,1])
plt.plot(fitrange,np.exp(parabola(fitrange, *popt)), 'r', label='fit: a=%5.3f, b=%5.3f, c=%5.3f' % tuple(popt))
plt.plot(fitrange,np.exp(iperbole(fitrange, *popt2)), 'g', label='fit: a=%5.3f, b=%5.3f, c=%5.3f' % tuple(popt2))
plt.show()
"""

