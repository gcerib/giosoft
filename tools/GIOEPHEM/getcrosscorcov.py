#!/usr/bin/python2.7

import sys
from scipy import signal
import numpy as np

from numpy.fft import fft, ifft

i=int(sys.argv[1])
x=int(sys.argv[2])
y=int(sys.argv[3])

ref = np.loadtxt("REF.phg")
ph0 = np.loadtxt("PH"+("%03d" % i)+".phg")

nbins=ref.shape[0]
A  = np.copy(ref[:,1])
B  = np.copy(ph0[:,1])
sA = np.copy(ref[:,2])
sB = np.copy(ph0[:,2])

def getcovariance(m,n):
  cov = np.zeros(1,dtype=np.uint64)
  for r in range(nbins):
    cov  = cov + sA[r]*sA[r]*B[(r-m)%nbins]*B[(r-n)%nbins] + sB[r]*sB[r]*A[(r+m)%nbins]*A[(r+n)%nbins]
  if(m==n):
    for r in range(nbins):
     cov = cov + sA[r]*sA[r]*sB[(r-m)%nbins]*sB[(r-m)%nbins]
  return cov

print getcovariance(x,y)
np.savetxt("COR"+("%03d" % i)+"_"+("%d" % x)+"_"+("%d" % y)+".cov",getcovariance(x,y))
