#!/bin/bash -l

#SBATCH -J star
#SBATCH -o star_%A_%a.out
#SBATCH --array=1-26
#SBATCH -D ./
#SBATCH --mail-user ceribell@mpp.mpg.de
#SBATCH --mail-type=ALL
#SBATCH --export=ALL

####SBATCH -t 4:00:0

#SBATCH --partition=standard
#SBATCH --mem=2048
#SBATCH -A mpp

source ~/.bashrc

echo "###"
echo "###SLURM_JOB_NAME:      $SLURM_JOB_NAME"
echo "###SLURM_JOB_ID:        $SLURM_JOB_ID"
echo "###SLURM_ARRAY_JOB_ID:  $SLURM_ARRAY_JOB_ID"
echo "###SLURM_ARRAY_TASK_ID: $SLURM_ARRAY_TASK_ID"
echo     "$(head -n $SLURM_ARRAY_TASK_ID com/zetatauri.CrabNebula.com | tail -n 1)"
echo      $(head -n $SLURM_ARRAY_TASK_ID com/zetatauri.CrabNebula.com | tail -n 1)  > tmp_${SLURM_JOB_NAME}_${SLURM_JOB_ID}_${SLURM_ARRAY_TASK_ID}.sh
bash tmp_${SLURM_JOB_NAME}_${SLURM_JOB_ID}_${SLURM_ARRAY_TASK_ID}.sh
echo '###'
