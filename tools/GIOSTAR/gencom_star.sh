#!/bin/bash

mkdir -p com
cd inputcard;
PARSETS=$(echo */);
cd ..;
for parset in $PARSETS; do
  if [[ ${parset:0:1} == "_" ]];
    then
     echo "$parset is masked (begins with "_"). I will skip it."
     continue
  fi;
  cd CALIBRATED;
  DATADIRS=$(echo */*/);
  cd ..;
  for datadir in $DATADIRS; do
     mkdir -p star/$parset/$datadir;
     objname=$(echo $datadir | awk '{gsub("/"," ");print($1)}')
     parname=$(echo $parset  | awk '{gsub("/"," ");print($1)}')
     RUNS=$(ls -1 CALIBRATED/$datadir/*_M1_*_Y_*.root | awk '{gsub("_"," ");gsub("\\."," ");print($5)}' | awk '!NF || !seen[$0]++')
     for run in $RUNS; do
        echo star -b -f -q --config="inputcard/$parset/star_M1.rc" --ind="CALIBRATED/$datadir/*_M1_*${run}*_Y_*.root" --out="star/$parset/$datadir" --log="star/$parset/$datadir/star_${run}_M1.log" --outname="star_${run}_M1" >> com/${parname}.${objname}.com;
        echo star -b -f -q --config="inputcard/$parset/star_M2.rc" --ind="CALIBRATED/$datadir/*_M2_*${run}*_Y_*.root" --out="star/$parset/$datadir" --log="star/$parset/$datadir/star_${run}_M2.log" --outname="star_${run}_M2" >> com/${parname}.${objname}.com;
     done;
  done;
done;
