#!/bin/bash

mkdir -p comsuper
cd inputcard;
PARSETS=$(echo */);
cd ..;
for parset in $PARSETS; do
  if [[ ${parset:0:1} == "_" ]];
    then
     echo "$parset is masked (begins with "_"). I will skip it."
     continue
  fi;
  if [[ ! -d  star/$parset ]]; then
     echo "star/$parset non esiste"
     continue;
  fi;
  cd star/$parset;
  OBJDIRS=$(echo */);
  cd ../../;
  for objdir in $OBJDIRS; do
     mkdir -p super/$parset/$objdir;
     parname=$(echo $parset  | awk '{gsub("/"," ");print($1)}')
     objname=$(echo $objdir  | awk '{gsub("/"," ");print($1)}')
     if [[ ! -e star/$parset/$objdir/NIGHTS.txt ]]; then
        echo "star/$parset/$objdir/NIGHTS.txt non esiste"
        rmdir super/$parset/$objdir;
        continue;
     fi;
     NIGHTS=$(cat star/$parset/$objdir/NIGHTS.txt)
     #echo $parset $objdir $parname $objname
     for night in $NIGHTS; do
        STARDIR="star/$parname/$objname/$night"
        SUPERDIR="super/$parname/$objname/$night"
        #echo "      $STARDIR -> $SUPERDIR"
        mkdir -p $SUPERDIR;
        RUNS=$(ls -1 $STARDIR/*_M1_*_I_*.root | awk '{gsub("_"," ");gsub("\\."," ");print($5)}' | awk '!NF || !seen[$0]++')
        #echo $RUNS
        for run in $RUNS; do
           echo "superstar -b -f -q --config=inputcard/superstar.rc --ind1=$STARDIR/\"20*M1*${run}*.root\" --ind2=$STARDIR/\"20*M2*${run}*.root\" --out=$SUPERDIR --log=$SUPERDIR/superstar_${run}.log" >> comsuper/superstar.com
        done;
     done;
  done;
done;
