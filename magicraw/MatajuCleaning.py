#!/usr/bin/python2.7

import numpy as np
import warnings
warnings.filterwarnings("ignore")

import Heptindex
import MagicRaw as mr

import matplotlib
import matplotlib.pyplot as plt
from   matplotlib.patches import RegularPolygon as Poly

import xml.etree.ElementTree as ET

#from matplotlib.font_manager import fontManager as fm

SMALL_SIZE = 18
MEDIUM_SIZE = 24
BIGGER_SIZE = 28

plt.rc('font',**{'family':'serif','serif':['Akkadian']})
#plt.rc('font', size=SMALL_SIZE)          # controls default text sizes
#plt.rc('font', weight='bold')  # fontsize of the figure title
#plt.rc('axes', titlesize=SMALL_SIZE)     # fontsize of the axes title
#plt.rc('axes', labelsize=MEDIUM_SIZE)    # fontsize of the x and y labels
#plt.rc('xtick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
#plt.rc('ytick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
#plt.rc('legend', fontsize=SMALL_SIZE)    # legend fontsize
#plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title

plt.rcParams['axes.titlepad'] = 16

tree  = ET.parse('SMILE_52434.xml')
xml   = tree.getroot()
try:
  event = list(xml.iter('event'))[0]
except IndexError:
  raise IOError("There are no events!")

event = mr.Event(event,extract=False)

##Draw the charge and mask pixles
#not in the core.
fig,axs = plt.subplots(2,2,figsize=(12,9))

#Re-extraction around the trigger time
event.ReExtract(29.8,window_size=8,maxdelta=12)

#Four panels: the (re-extracted) event,
#             the core pixels,
#             two rings (the third is empty)
#             the cleaned event.
event.DrawCharge(axs[0,0],lw=0.5)
event.DrawCharge(axs[0,1],lw=0.5)
event.DrawCharge(axs[1,0],lw=0.5)
event.DrawCharge(axs[1,1],lw=0.5)

axs[0,0].set_title("1. Raw Event"     , fontweight='bold', color='#505050', size=18)
axs[0,1].set_title("2. Core Pixels"   , fontweight='bold', color='#505050', size=18)
axs[1,0].set_title("3. Boundary Rings", fontweight='bold', color='#505050', size=18)
axs[1,1].set_title("4. Clean event"   , fontweight='bold', color='#505050', size=18)

event.SearchCore()
event.SearchBoundary()


black = lambda x: '#000000'

noclean = [pix for pix in event['id'] if pix not in event.clean]
nocore  = [pix for pix in event['id'] if pix not in event.core ]
norings = [pix for pix in event['id'] if pix not in event.rings[1] and pix not in event.rings[2]]


#Masking pixels
mr.DrawMagicCamera(axs[0,1],nocore        ,0,black,alpha=0.6 ,autoscale=False,lw=0.5)
mr.DrawMagicCamera(axs[1,0],norings       ,0,black,alpha=0.6 ,autoscale=False,lw=0.5)
mr.DrawMagicCamera(axs[1,0],event.rings[2],0,black,alpha=0.3 ,autoscale=False,lw=0.5)
mr.DrawMagicCamera(axs[1,1],noclean       ,0,black,alpha=0.6 ,autoscale=False,lw=0.5)

#Drawing the NN groups
r = 2
wax = axs[0,0].get_xlim()[1]

hexpos2 = np.array(Heptindex.Dec2Vec([0,1]    ,b1=Heptindex.__magicbase__))*r   + np.array([1.38, 0.2])*wax
hexpos3 = np.array(Heptindex.Dec2Vec([0,1,2]  ,b1=Heptindex.__magicbase__))*r   + np.array([1.48, 0.4])*wax
hexpos4 = np.array(Heptindex.Dec2Vec([0,1,6,5],b1=Heptindex.__magicbase__))*r   + np.array([1.68, 0.3])*wax

for pos in hexpos2:
  hexi = Poly(pos,6,r,facecolor='#1c5ca7',alpha=0.9,edgecolor='k',orientation=Heptindex.__magicalpha__,lw=0.7071,clip_on=False)
  axs[0,0].add_patch(hexi) 
for pos in hexpos3:
  hexi = Poly(pos,6,r,facecolor='#fdc900',alpha=0.9,edgecolor='k',orientation=Heptindex.__magicalpha__,lw=0.7071,clip_on=False)
  axs[0,0].add_patch(hexi) 
for pos in hexpos4:
  hexi = Poly(pos,6,r,facecolor='#f22a2a',alpha=0.9,edgecolor='k',orientation=Heptindex.__magicalpha__,lw=0.7071,clip_on=False)
  axs[0,0].add_patch(hexi) 

axs[0,0].text(1.65*wax,0.6*wax,'kNN'      ,clip_on=False,family='serif',size='13',color="k")
axs[0,0].text(1.75*wax,0.5*wax,'Q   W'    ,clip_on=False,family='serif',size='10.5',color="#505050")
axs[0,0].text(1.84*wax,0.48*wax,'k      k',clip_on=False,family='serif',size='8.7',color="#505050")

#Arrows
axs[0,0].arrow(0.445,0.735,0.11,0,width      =  0.001,
                          head_width  =4*0.003,
                          head_length =4*0.003,
                          length_includes_head=True,
                          transform=fig.transFigure,
                          clip_on=False,
                          facecolor='#505050',
                          edgecolor='#505050',
                          lw=0.7071)

axs[1,0].arrow(0.445,0.245,0.11,0,width      =  0.001,
                          head_width  =4*0.003,
                          head_length =4*0.003,
                          length_includes_head=True,
                          transform=fig.transFigure,
                          clip_on=False,
                          facecolor='#505050',
                          edgecolor='#505050',
                          lw=0.7071)

#Queste sono in coordinate dei dati perche' farle
#nell'altro modo era impossibile.
a0,a1 = (-24.2652,-18.9559)
b0,b1 = ( 25.7743, 20.7008)

axs[0,1].arrow(a0,a1,-6,a1/a0*(-6),width    =  0.22,
                          head_width  =10*0.22,
                          head_length =10*0.22,
                          length_includes_head=True,
                          transform=axs[0,1].transData,
                          clip_on=False,
                          facecolor='#505050',
                          edgecolor='#505050',
                          lw=0.7071,
                          zorder=10)

axs[1,0].arrow(b0+6,b1+6./b0*b1,-6,b1/b0*(-6),width    =  0.22,
                          head_width  =10*0.22,
                          head_length =10*0.22,
                          length_includes_head=True,
                          transform=axs[1,0].transData,
                          clip_on=False,
                          facecolor='#505050',
                          edgecolor='#505050',
                          lw=0.7071,
                          zorder=10)

#Qualche hack sui font di matplotlib
matplotlib.rcParams['mathtext.fontset'] = 'custom'
matplotlib.rcParams['mathtext.rm'] = 'MathJax_Math'
matplotlib.rcParams['mathtext.it'] = 'MathJax_Math'
matplotlib.rcParams['mathtext.bf'] = 'MathJax_Math'
matplotlib.rcParams['mathtext.cal'] = 'MathJax_Math'

#Testo somma anelli
axs[1,1].text(-1.78*wax,-0.2*wax,r'$\Sigma$',clip_on=False,size='24',color="#505050")
axs[1,1].text(-1.63*wax,-0.22*wax,'i=0',clip_on=False,size='12',color="#505050")
axs[1,1].text(-1.62*wax,-0.12*wax,'N',clip_on=False,size='12',color="#505050")
axs[1,1].text(-1.54*wax,-0.13*wax,'rings',clip_on=False,size='10',color="#505050")

#I miniassi
miniax1 = fig.add_axes((0.435,0.5025,0.14,0.08))
event[650].Draw(miniax1,auto=False,
                   timeband={'color':'#fdc900','lw':3,'zorder':-1},
                   facecolor='#1c5ca7',edgecolor='navy',linewidth=0.5)
miniax1.set_ylim(event[650].waveform.min()-50.,event[650].waveform.max()+50.)
miniax1.set_xlim(event[650].calslice.min()-0.5,event[650].calslice.max()-0.5)

miniax1.text(18,event[650].waveform.max()-30,'Core',size=12,color="#505050")

miniax1.set_xticks([])
miniax1.set_xticklabels([])
miniax1.set_yticks([])
miniax1.set_yticklabels([])


miniax2 = fig.add_axes((0.435,0.4175,0.14,0.08))
event[649].Draw(miniax2,auto=False,
                   timeband='',
                   facecolor='#1c5ca7',edgecolor='navy',linewidth=0.5)
miniax2.plot([event[650].arrival]*2,miniax2.get_ylim(),'-',color='#f22a2a',lw=40,alpha=0.15)
miniax2.plot([event[650].arrival]*2,miniax2.get_ylim(),'-',color='#fdc900',lw=3,alpha=0.7)
miniax2.set_ylim(event[649].waveform.min()-50.,event[649].waveform.max()+50.)
miniax2.set_xlim(event[650].calslice.min()-0.5,event[650].calslice.max()-0.5)

miniax2.text(8.8,event[649].waveform.max()-10,'Boundary',size=12,color="#505050")

miniax2.set_xticks([])
miniax2.set_xticklabels([])
miniax2.set_yticks([])
miniax2.set_yticklabels([])

miniax2.text(14,event[649].waveform.max()-76,'Q   W'    ,size=10.5,color="#505050")
miniax2.text(18,event[649].waveform.max()-76,'b       b',size=8.7 ,color="#505050")

miniax2.set_title('Re-Extraction',pad=-70,size=13)


for ax in [miniax1, miniax2]:
    plt.setp(ax.spines.values(), color='#505050')


plt.tight_layout()
plt.show()
