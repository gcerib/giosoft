#!/usr/bin/python2.7
"""
   Heptindex.py
   Un modulo per indicizzare reticoli esagonali con un indice ricorsivo in
   base sette. Analogo al ternario bilanciato generalizzato:
      L. Middleton, J. Sivaswamy "Hexagonal Image Processing - A Practical
      Approach", Springer Verlag London 2005, 978-1-85233-914-2
      https://www.springer.com/gp/book/9781852339142
"""

import copy
import numpy as np

#Il vettore b0 e' il vettore nullo
__b0__ = np.array( [0,0] , dtype=float)

#Il vettore b1 e' la base
#del reticolo fondamentale.
#Definisce la sua scala e
#rotazione. Questo valore
#va bene per celle con
#distanza dei vertici dal
#circocentro pari a 1 e 
#la prima cella vicina a
#quella nulla e' a destra
#sull'asse delle ascisse.
__b1__ = np.array( [np.sqrt(3),0] )


#La rotazione antioraria di
#sessanta gradi fondamentale
__R1__ = np.array( [   [ np.cos(np.pi/3),-np.sin(np.pi/3)]  ,
                   [ np.sin(np.pi/3), np.cos(np.pi/3)]  ] )  

#e la sua inversa...
__R1i__ = np.array( [   [ np.cos(np.pi/3), np.sin(np.pi/3)]  ,
                   [-np.sin(np.pi/3), np.cos(np.pi/3)]  ] )  



def R(v,n=1):
  """Ruota il vettore n volte con la
     matrice di rotazione R1, ossia lo
     ruota n volte di 60 gradi in
     direzione antioraria.
       n=0 non lo ruota affatto
       6=0
  """
  if n==0:
    return v
  else: 
    if n>0:
      return R(np.dot(__R1__,v.T),n-1)
    if n<0:
      return R(np.dot(__R1i__,v.T),n+1)

def Base(order,b1=__b1__):
  """Fornisce la base dei reticoli
     di ordine successivo, che sono
     quelli formati dagli esagononi
     fatti con sette esagoni di
     ordine precedente.

     La base di ordine 1 e' quella del
     reticolo fondamentale, ossia b1.

     La base del reticolo di ordine 2
     e' quella del reticolo formato dai
     gruppi di 7 esagoni fondamentali.
     Se ne puo' scegliere uno qualsiasi
     dei 6 presenti. Io ho preso quello
     vicino a b1 e un po' piu' sotto.
     Per arrivarci si ruota b1 5 volte
     e ci si aggiunge due volte b1.
     Oppure lo si ruota -1 volta e si
     aggiungono 2 b1.

     Se si chiede la base di ordine 0
     la funzione restituisce b0 = (0,0)

     Il parametro b1 permette di
     cambiare vettore di base.
  """
  if order==0:
    return __b0__
  if order==1:
    return b1
  else:
    pBase = Base(order-1,b1=b1)
    return R(pBase,-1)+2*pBase

def Hep2Vec(heptindex,b1=__b1__):
  """Converte un numero in base
     7 in un vettore del reticolo
  """

  #Se l'argomento e' iterabile,
  #ci iteriamo sopra.
  if hasattr(heptindex,'__iter__'):
    heptindex = np.array(heptindex,dtype=int).astype(str)
    return [Hep2Vec(h,b1=b1) for h in heptindex]

  if isinstance(heptindex,Heptindex):
    heptindex = heptindex.hep

  #Ci assicuriamo che l'argomento
  #sia una stringa di cifre da 0 a 9
  heptindex = str(int(heptindex))

  #La invertiamo. Partiremo sommando
  #la cifra piu' piccola (le unita')
  #per poi sommare le settine, le
  #quarantanovine,...
  revindex = heptindex[::-1]

  #Python e' cattivo. Mezz'ora di debug.
  vec = copy.copy(__b0__)

  for i,r in enumerate(revindex):
    #o e' l'ordine della cifra,
    #r e' la cifra. Qui r=0 non
    #vuol dire "non ruotare", ma
    #"non muoverti", ossia non
    #aggiungere nemmeno una base
    #di questo ordine e infatti
    #se r=0 saltiamo un ciclo.
    o = i+1
    r = int(r)
    if r > 6:
      raise ValueError(str(r)+' non e\' in base 7')
    if r == 0:
      continue
    #... invece per la funzione
    #R, zero significa "non
    #ruotato". Pertanto se la
    #cifra e' 1 (base del
    #reticolo dell'ordine a cui
    #si trova) la rotazione deve
    #essere 0. Ossia si ruota di
    #r-1.
    vec += R(Base(o,b1=b1),r-1)
  return vec

def Dec2Hep(n):
  """Funzione per convertire
     un numero nella sua stringa
     in base sette.
  """
  if hasattr(n,'__iter__'):
    n = np.array(n,dtype=int)
    return [Dec2Hep(i) for i in n]
  n = int(n)
  if n < 0:
    raise ValueError('Non so fare le divisioni.')
  cifre = []
  if n == 0:
      cifre.append('0')
  while n:
      cifre.append(str(int(n % 7)))
      n /= 7
  return ''.join(cifre[::-1])
  
def Hep2Dec(s):
  if hasattr(s,'__iter__'):
    return [Hep2Dec(i) for i in s]
  dec=0
  for i,c in enumerate(s[::-1]):
    dec += int(c)*7**i
  return dec


def Dec2Vec(n,b1=__b1__):
  """Composizione Hep2Vec(Dec2Hep(n)),
     restituisce il vettore dato il
     suo indice in base 10.
  """
  return Hep2Vec(Dec2Hep(n),b1) 


###############################################################
# Quanto segue serve solo per fare l'algebra degli indici, che
# a sua volta serve se si vogliono avere i kNN.

#La negazione di un indice
__inversetable__ = np.array(['0','4','5','6','1','2','3'])

#La tabellina di addizione: a+b = __additiontable__[a,b]
__additiontable__ = np.array(
   [ [0, 1, 2, 3, 4, 5, 6],
     [1,13,25, 2, 0, 6,14],
     [2,25,24,36, 3, 0, 1],
     [3, 2,36,35,41, 4, 0],
     [4, 0, 3,41,46,52, 5],
     [5, 6, 0, 4,52,51,63],
     [6,14, 1, 0, 5,63,62] ],
   dtype=str)


#I gruppi NN di zero
__zero2NN__ = [ (0,1), (0,2), (0,3),
                (0,4), (0,5), (0,6) ]
__zero3NN__ = [ (0,1,2), (0,2,3), (0,3,4),
                (0,4,5), (0,5,6), (0,6,1) ]
__zero4NN__ = [ (0,1,2,19), (0,2,3,27), (0,3,4,29), 
                (0,4,5,37), (0,5,6,45), (0,6,1,11),
                (0,1,2,3) , (0,2,3,4) , (0,3,4,5) ,
                (0,4,5,6) , (0,5,6,1) , (0,6,1,2) ]

class Heptindex():
  """Classe per gli eptaindici. E' utile per farne  l'algebra,
     cosi' si possono sommare gli indici con + e sottrarli col
     meno. L'eptaindice e' salvato in .hep come stringa e in
     .dec come intero in base dieci.
  """
  def __init__(self,n=0):
    """Accetta una stringa come eptaindice, oppure un intero
       che e' l'eptaindice in base dieci, oppure un altro
       eptaindice. Se gli si da' una lista, restituisce una
       lista con gli oggetti sommati.
    """
    if hasattr(n,'__iter__'):
      #Ricorsione
      return [Heptindex(i) for i in n]
    if type(n) is int or isinstance(n,np.integer):
      self.dec    = n
      self.hep    = Dec2Hep(n)
    elif type(n) is str or type(n) is np.string_:
      tmp = str(int(n))
      if any(z in tmp for z in '789'):
        errstr = '%s is not a valid heptal number.' % tmp
        raise ValueError(errstr)
      else:
        self.hep = tmp
        self.dec = 0
      for i,c in enumerate(self.hep[::-1]):
        self.dec += int(c)*7**i
    elif isinstance(n,Heptindex):
      self.dec = copy.copy(n.dec)
      self.hep = copy.copy(n.hep)
    else:
      errstr = 'Not a valid initializer: '+repr(n)+' , '+str(type(n))
      raise ValueError(errstr)

  def __repr__(self):
     return 'Heptindex: (hep %s, dec %d)' % (self.hep,self.dec)

  def __str__(self):
     return self.hep

  def __neg__(self):
     """La negazione, ossia -Heptindex(). Usa la tabella
        __inversetable__
     """
     return Heptindex(''.join(__inversetable__[int(h)] for h in self.hep))

  def __add__(self,other):
     """Operatore di addizione che viene interpretato quando si
        chiama il '+'. Accetta un intero (che sara' trattato come
        eptaindice in base dieci), una stringa come eptaindice o
        un altro eptaindice, oppure una lista di qualunque di questi
        tipi. Usa la tabellina delle addizioni __additiontable__
     """

     #print self,other
     #print type(self),type(other)

     if hasattr(other,'__iter__'):
       return [self.__add__(x) for x in other]
     if type(other) is str or type(other) is int or type(other) is np.string_ or type(other) is np.integer:
       other = Heptindex(other)

     #print self,other
     #print type(self),type(other)
    
     #print self.hep, other.hep
     #print type(self.hep), type(other.hep)
 
     #Qualsiasi altra cosa
     if not isinstance(other,Heptindex):
       errstr = 'The value %other is not an int, a str or a Heptindex.' %other
       raise ValueError(errstr)

     #Le conformiamo per l'addizione in colonna.
     add1 =  self.hep
     add2 = other.hep
     if len(add1) < len(add2):
       (add1,add2) = (add2,add1)
     order = len(add1)
     add2 = '0'*(order-len(add2)) + add2

     #print add1,add2
     #print type(add1),type(add2)
     
     #La somma in colonna. L'algebra dei riporti e'
     #piu' complicata del solito perche' il riporto
     #nella somma puo' essere diverso da 1! Puo'
     #essere qualsiasi numero da 0 a 6, ma in ogni
     #caso e' sempre di una sola cifra, il che
     #equivale a dire che la somma di tre indici
     #semplici qualunque e' comunque rappresentabile
     #come un indice con due cifre. Meno male senno'
     #avremmo dovuto fare un array dei riporti!
     somma = ''
     riporto = '0'
     for c1,c2 in zip(add1[::-1],add2[::-1]):
       #print "c1,c2 =",c1,c2
                                                      #   c1|+
                                                      #   c2|=
       p  = __additiontable__[int(c1),int(c2)]        # r1p1|

       #print "p =",p

       p1 = p[-1] #Unita'  (parziali)

       #print "p1=",p1

       #r1 e' il riporto della somma, ma potrebbe
       #esserci anche un riporto dalla colonna
       #precedente, quindi non abbiamo ancora
       #finito.
       if(len(p)>1):
         r1 = p[0]  #Settine
       else:
         r1 = '0'

       #print "r1=",r1

       #Il riporto della colonna precedente.
       if riporto is not '0':
         #print "  riporto_precedente=",riporto
                                                        #    r|+
         #Aggiungo il riporto di prima a p1             #   p1|=
         q  = __additiontable__[int(riporto),int(p1)]   # r2s1|
  
         #print "  q=",q

         #s1 e' il risultato di questa colonna
         s1 = q[-1] #Unita' della somma
         if(len(q)>1):
           r2 = q[0]  #Settine parte seconda
         else:
           r2 = '0'

         #print "  s1=",s1
         #print "  r2=",r2

         #Se mi era restato un riporto da prima, lo
         #aggiungo a questo. La struttura degli
         #eptaindici assicura che il risultato avra'
         #una sola cifra.
         if r1 is not '0': 
                                                          #   r1|+
           #Aggiungo i resti (1 sola cifra)               #   r2|=
           riporto = __additiontable__[int(r1),int(r2)]   #    r|
         else:
           riporto = r2
         #print "  riporto_nuovo=",riporto
  
         somma = s1 + somma
       else:
         somma = p1 + somma
         riporto = r1
       #print "somma =",somma
       #print "riporto =",riporto

     #L'ultimo riporto va aggiunto
     if riporto is not '0':
       somma = riporto+somma

     #se le ultime due cifre sommano a zero lo tolgo
     if somma[0]=='0':
       somma = somma[1:]
     return somma


  def __sub__(self,other):
    """La sottrazione nega l'operando e somma"""
    if hasattr(other,'__neg__'):
      return self.__add__(-other)
    else:
      return (self.__neg__()+other).__neg__()

  def __radd__(self,other):
    """Addizione a destra. L'algebra degli eptaindici
       e' commutativa, per fortuna.
    """
    return self.__add__(other)

  def __rsub__(self,other):
    """Sottrazione a destra. Uguale alla sottrazione
       ma neghiamo il risultato per ovvie ragioni.
    """
    return self.__sub__(other).__neg__()

def GetkNN(indice,k):
  """Ritorna una lista ordinata di tuple
     che rappresentano i kNN dell'indice
     dato.
  """
  if hasattr(indice,'__iter__'):
    return [GetkNN(i,k) for i in indice]
  if k==0:
    return []
  if k==1:
    return [(indice,)]
  if k==2:
    return Heptindex(indice)+__zero2NN__
  if k==3:
    return Heptindex(indice)+__zero3NN__
  if k==4:
    return Heptindex(indice)+__zero4NN__
  errstr = 'kNN are supported up to 4.'
  raise ValueError(errstr)

###############################################################################
##Seguono elementi specifici di MAGIC. La seguente tabella, fatta pazientemente
##prima a mano e poi a semimano, associa a ogni indice di MAGIC un eptaindice
##in formato decimale. Modificarla equivale a cambiare la geometria della
##camera.

__magictable__ = { 0:0, 1:1, 2:2, 3:3, 4:4, 5:5, 6:6, 7:10, 
   8:  19,    9:  18,     10:27, 11:26, 12:29, 13:34, 14:37, 15:36, 
  16:  45,   17:  44,     18:11, 19:9, 20:20, 21:14, 22:17, 23:22, 
  24:  21,   25:  25,     26:30, 27:28, 28:33, 29:38, 30:35, 31:41, 
  32:  46,   33:  42,     34:43, 35:12, 36:7, 37:138, 38:137, 39:15, 
  40:  16,   41: 195,    42:194, 43:23, 44:24, 45:204, 46:209, 47:31, 
  48:  32,   49: 261,    50:260, 51:39, 52:40, 53:318, 54:317, 55:47, 
  56:  48,   57:  81,     58:80, 59:13, 60:8, 61:139, 62:133, 63:136, 
  64: 131,   65: 130,    66:190, 67:189, 68:193, 69:188, 70:187, 71:205, 
  72: 203,   73: 208,    74:239, 75:244, 76:262, 77:259, 78:265, 79:254, 
  80: 253,   81: 319,    82:315, 83:316, 84:311, 85:310, 86:82, 87:77, 
  88:  79,   89:  74,     90:73, 91:144, 92:134, 93:135, 94:132, 95:126, 
  96: 129,   97: 159,    98:191, 99:192, 100:183, 101:182, 102:186, 103:216, 
 104: 206,  105: 207,   106:240, 107:238, 108:243, 109:267, 110:263, 111:264, 
 112: 255,  113: 252,   114:258, 115:324, 116:320, 117:321, 118:312, 119:308, 
 120: 309,  121:  87,    122:83, 123:78, 124:75, 125:70, 126:72, 127:140, 
 128: 143,  129: 103,   130:102, 131:127, 132:128, 133:160, 134:154, 135:158, 
 136: 153,  137: 152,   138:184, 139:185, 140:211, 141:210, 142:215, 143:197, 
 144: 202,  145: 241,   146:242, 147:268, 148:266, 149:272, 150:247, 151:246, 
 152: 256,  153: 257,   154:325, 155:322, 156:323, 157:297, 158:296, 159:313, 
 160: 314,  161:  88,    162:84, 163:86, 164:53, 165:52, 166:76, 167:71, 
 168: 145,  169: 141,   170:142, 171:104, 172:98, 173:101, 174:124, 175:123, 
 176: 155,  177: 156,   178:157, 179:148, 180:147, 181:151, 182:181, 183:180, 
 184: 212,  185: 213,   186:214, 187:198, 188:196, 189:201, 190:232, 191:237, 
 192: 269,  193: 270,   194:271, 195:248, 196:245, 197:251, 198:289, 199:288, 
 200: 326,  201: 327,   202:328, 203:298, 204:294, 205:295, 206:304, 207:303, 
 208:  89,  209:  90,    210:85, 211:54, 212:49, 213:51, 214:67, 215:66, 
 216: 146,  217: 962,   218:110, 219:109, 220:99, 221:100, 222:125, 223:119, 
 224: 122,  225:1363,  226:1362, 227:167, 228:166, 229:149, 230:150, 231:176, 
 232: 175,  233: 179,  234:1469, 235:1468, 236:218, 237:223, 238:199, 239:200, 
 240: 233,  241: 231,   242:236, 243:1821, 244:1826, 245:275, 246:274, 247:249, 
 248: 250,  249: 290,   250:287, 251:293, 252:2221, 253:2220, 254:332, 255:331, 
 256: 299,  257: 300,   258:305, 259:301, 260:302, 261:563, 262:562, 263:95, 
 264:  94,  265:  55,    266:50, 267:68, 268:63, 269:65, 270:963, 271:961, 
 272: 111,  273: 105,   274:108, 275:117, 276:116, 277:120, 278:121, 279:1364, 
 280:1358,  281:1361,   282:162, 283:161, 284:165, 285:174, 286:173, 287:177, 
 288: 178,  289:1464,  290:1463, 291:1467, 292:219, 293:217, 294:222, 295:225, 
 296: 230,  297: 234,   298:235, 299:1822, 300:1820, 301:1825, 302:276, 303:273, 
 304: 279,  305: 282,   306:281, 307:291, 308:292, 309:2222, 310:2219, 311:2225, 
 312: 333,  313: 329,   314:330, 315:339, 316:338, 317:306, 318:307, 319:564, 
 320: 560,  321: 561,    322:96, 323:91, 324:93, 325:60, 326:59, 327:69, 
 328:  64,  329: 964,   330:959, 331:957, 332:956, 333:106, 334:107, 335:118, 
 336: 112,  337: 115,  338:1370, 339:1369, 340:1359, 341:1360, 342:1357, 343:1356, 
 344: 163,  345: 164,   346:169, 347:168, 348:172, 349:1434, 350:1433, 351:1465, 
 352:1466,  353:1457,  354:1462, 355:220, 356:221, 357:226, 358:224, 359:229, 
 360:1828,  361:1833,  362:1823, 363:1824, 364:1857, 365:1856, 366:277, 367:278, 
 368: 283,  369: 280,   370:286, 371:2228, 372:2227, 373:2223, 374:2224, 375:2215, 
 376:2214,  377: 334,   378:335, 379:340, 380:336, 381:337, 382:570, 383:569, 
 384: 565,  385: 566,   386:557, 387:556, 388:97, 389:92, 390:61, 391:56, 
 392:  58,  393: 970,   394:969, 395:965, 396:960, 397:958, 398:952, 399:955, 
 400: 922,  401: 921,   402:113, 403:114, 404:1371, 405:1365, 406:1368, 407:1328, 
 408:1327,  409:1352,  410:1351, 411:1355, 412:1322, 413:1321, 414:170, 415:171, 
 416:1429,  417:1428,  418:1432, 419:1427, 420:1426, 421:1458, 422:1456, 423:1461, 
 424:1674,  425:1679,   426:227, 427:228, 428:1829, 429:1827, 430:1832, 431:1814, 
 432:1819,  433:1858,  434:1855, 435:1861, 436:1780, 437:1779, 438:284, 439:285, 
 440:2229,  441:2226,  442:2232, 443:2207, 444:2206, 445:2216, 446:2212, 447:2213, 
 448:2180,  449:2179,   450:341, 451:342, 452:571, 453:567, 454:568, 455:542, 
 456: 541,  457: 558,   458:553, 459:555, 460:522, 461:521, 462:62, 463:57, 
 464: 971,  465: 966,   466:968, 467:935, 468:934, 469:949, 470:953, 471:954, 
 472: 923,  473: 917,   474:920, 475:915, 476:914, 477:1366, 478:1367, 479:1329, 
 480:1323,  481:1326,  482:1349, 483:1353, 484:1354, 485:1317, 486:1316, 487:1320, 
 488:1315,  489:1314,  490:1430, 491:1431, 492:1422, 493:1421, 494:1425, 495:1455, 
 496:1459,  497:1460,  498:1675, 499:1673, 500:1678, 501:1709, 502:1714, 503:1830, 
 504:1831,  505:1815,  506:1813, 507:1818, 508:1849, 509:1859, 510:1860, 511:1781, 
 512:1778,  513:1784,  514:1773, 515:1772, 516:2230, 517:2231, 518:2208, 519:2205, 
 520:2211,  521:2249,  522:2217, 523:2218, 524:2181, 525:2177, 526:2178, 527:2173, 
 528:2172,  529: 572,   530:573, 531:543, 532:539, 533:540, 534:549, 535:559, 
 536: 554,  537: 523,   538:518, 539:520, 540:515, 541:514, 542:972, 543:967, 
 544: 936,  545: 931,   546:933, 547:945, 548:948, 549:929, 550:928, 551:918, 
 552: 919,  553: 916,   554:910, 555:913, 556:1335, 557:1334, 558:1324, 559:1325, 
 560:1350,  561:1344,  562:1348, 563:1287, 564:1286, 565:1318, 566:1319, 567:1310, 
 568:1309,  569:1313,  570:1441, 571:1440, 572:1423, 573:1424, 574:1450, 575:1449, 
 576:1454,  577:1681,  578:1686, 579:1676, 580:1677, 581:1710, 582:1708, 583:1713, 
 584:1835,  585:1840,  586:1816, 587:1817, 588:1850, 589:1848, 590:1854, 591:1787, 
 592:1786,  593:1782,  594:1783, 595:1774, 596:1771, 597:1777, 598:2235, 599:2234, 
 600:2209,  601:2210,  602:2250, 603:2247, 604:2248, 605:2187, 606:2186, 607:2182, 
 608:2183,  609:2174,  610:2170, 611:2171, 612:577, 613:576, 614:544, 615:545, 
 616: 550,  617: 546,   618:548, 619:529, 620:528, 621:524, 622:519, 623:516, 
 624: 511,  625: 513,   626:977, 627:976, 628:937, 629:932, 630:950, 631:946, 
 632: 947,  633: 930,   634:924, 635:927, 636:887, 637:886, 638:911, 639:912, 
 640:1336,  641:1330,  642:1333, 643:1342, 644:1341, 645:1345, 646:1346, 647:1347, 
 648:1282,  649:1281,  650:1285, 651:1280, 652:1279, 653:1311, 654:1312, 655:1436, 
 656:1435,  657:1439,  658:1448, 659:1447, 660:1451, 661:1452, 662:1453, 663:1682, 
 664:1680,  665:1685,  666:1667, 667:1672, 668:1711, 669:1712, 670:1836, 671:1834, 
 672:1839,  673:1842,  674:1847, 675:1851, 676:1852, 677:1853, 678:1788, 679:1785, 
 680:1791,  681:1766,  682:1765, 683:1775, 684:1776, 685:2236, 686:2233, 687:2239, 
 688:2242,  689:2241,  690:2251, 691:2252, 692:2253, 693:2188, 694:2184, 695:2185, 
 696:2159,  697:2158,  698:2175, 699:2176, 700:578, 701:574, 702:575, 703:584, 
 704: 583,  705: 551,   706:552, 707:547, 708:530, 709:525, 710:527, 711:494, 
 712: 493,  713: 517,   714:512, 715:978, 716:973, 717:975, 718:942, 719:941, 
 720: 951,  721: 724,   722:719, 723:718, 724:925, 725:926, 726:888, 727:882, 
 728: 885,  729: 908,   730:907, 731:1331, 732:1332, 733:1343, 734:1337, 735:1340, 
 736:1076,  737:1075,  738:1070, 739:1069, 740:1283, 741:1284, 742:1275, 743:1274, 
 744:1278,  745:1308,  746:1307, 747:1437, 748:1438, 749:1443, 750:1442, 751:1446, 
 752:1385,  753:1384,  754:1415, 755:1420, 756:1683, 757:1684, 758:1668, 759:1666, 
 760:1671,  761:1702,  762:1707, 763:1837, 764:1838, 765:1843, 766:1841, 767:1846, 
 768:1730,  769:1735,  770:1724, 771:1723, 772:1789, 773:1790, 774:1767, 775:1764, 
 776:1770,  777:1808,  778:1807, 779:2237, 780:2238, 781:2243, 782:2240, 783:2246, 
 784:2081,  785:2080,  786:2075, 787:2074, 788:2189, 789:2190, 790:2160, 791:2156, 
 792:2157,  793:2166,  794:2165, 795:579, 796:580, 797:585, 798:581, 799:582, 
 800: 374,  801: 373,   802:368, 803:367, 804:531, 805:526, 806:495, 807:490, 
 808: 492,  809: 508,   810:507, 811:979, 812:974, 813:943, 814:938, 815:940, 
 816: 725,  817: 720,   818:714, 819:717, 820:894, 821:893, 822:883, 823:884, 
 824: 909,  825: 903,   826:906, 827:1118, 828:1117, 829:1338, 830:1339, 831:1077, 
 832:1071,  833:1065,  834:1064, 835:1068, 836:1294, 837:1293, 838:1276, 839:1277, 
 840:1303,  841:1302,  842:1306, 843:1518, 844:1517, 845:1444, 846:1445, 847:1380, 
 848:1379,  849:1416,  850:1414, 851:1419, 852:1688, 853:1693, 854:1669, 855:1670, 
 856:1703,  857:1701,  858:1706, 859:1870, 860:1875, 861:1844, 862:1845, 863:1731, 
 864:1729,  865:1725,  866:1722, 867:1728, 868:1794, 869:1793, 870:1768, 871:1769, 
 872:1809,  873:1806,  874:1812, 875:2270, 876:2269, 877:2244, 878:2245, 879:2082, 
 880:2079,  881:2076,  882:2072, 883:2073, 884:2194, 885:2193, 886:2161, 887:2162, 
 888:2167,  889:2163,  890:2164, 891:612, 892:611, 893:586, 894:587, 895:375, 
 896: 371,  897: 369,   898:364, 899:366, 900:536, 901:535, 902:496, 903:491, 
 904: 509,  905: 504,   906:506, 907:1012, 908:1011, 909:944, 910:939, 911:726, 
 912: 721,  913: 716,   914:895, 915:889, 916:892, 917:901, 918:900, 919:904, 
 920: 905,  921:1119,  922:1113, 923:1116, 924:1111, 925:1110, 926:1067, 927:1289, 
 928:1288,  929:1292,  930:1301, 931:1300, 932:1304, 933:1305, 934:1513, 935:1512, 
 936:1516,  937:1511,  938:1510, 939:1418, 940:1689, 941:1687, 942:1692, 943:1695, 
 944:1700,  945:1704,  946:1705, 947:1871, 948:1869, 949:1874, 950:1905, 951:1910, 
 952:1727,  953:1795,  954:1792, 955:1798, 956:1801, 957:1800, 958:1810, 959:1811, 
 960:2271,  961:2268,  962:2274, 963:2263, 964:2262, 965:2078, 966:2195, 967:2191, 
 968:2192,  969:2201,  970:2200, 971:2168, 972:2169, 973:613, 974:609, 975:610, 
 976: 605,  977: 604,   978:365, 979:537, 980:532, 981:534, 982:501, 983:500, 
 984: 510,  985: 505,  986:1013, 987:1008, 988:1010, 989:1005, 990:1004, 991:891, 
 992: 902,  993: 896,   994:899, 995:1125, 996:1124, 997:1114, 998:1115, 999:1291, 
1000:1296, 1001:1295, 1002:1299, 1003:1483, 1004:1482, 1005:1514, 1006:1515, 1007:1691, 
1008:1696, 1009:1694, 1010:1699, 1011:1877, 1012:1882, 1013:1872, 1014:1873, 1015:1797, 
1016:1802, 1017:1799, 1018:1805, 1019:2277, 1020:2276, 1021:2272, 1022:2273, 1023:2197, 
1024:2202, 1025:2198, 1026:2199, 1027:619, 1028:618, 1029:614, 1030:615, 1031:533, 
1032: 502, 1033: 497,  1034:499, 1035:1019, 1036:1018, 1037:1014, 1038:1009, }

#L'angolo di rotazione usato per i grafici di MAGIC: 19 gradi.
__magicalpha__ = 19.11460397105*np.pi/180

#Rotazione associata
__Rmagic__     = np.array( [   [np.cos(19*np.pi/180),-np.sin(19*np.pi/180)]  ,
                               [np.sin(19*np.pi/180), np.cos(19*np.pi/180)]  ] )  

#E base fondamentale del reticolo ruotato. Per ottenere il vettore
#dati un'indice software midx di Magic fare qualcosa come
#
#  Dec2Vec(__magictable__[midx],b1=__magicbase__)

__magicbase__  = np.dot(__Rmagic__,__b1__.T).T

def GetMagicNN(k):
  decindices = __magictable__.values()
  hepindices = Dec2Hep(__magictable__.values())
  #No! Non provare a capirla. Sappi solo che l'hai costruita tu e
  #che ci hai messo un'ora. La parte per rimuovere le permutazioni
  #viene da un sito.
  return set(tuple(sorted(g)) for pixnns in GetkNN(decindices,k) for g in pixnns if all(i in hepindices for i in g))

def Magic2Vec(mi):
  if hasattr(mi,'__iter__'):
    return np.array([Magic2Vec(mmi) for mmi in mi])
  try:
    deci = __magictable__[mi]
  except IndexError:
    errstr = ("Index %s is not a MAGIC index." % str(mi))
    raise(errstr)
  return Dec2Vec(deci,b1=__magicbase__)

##############################################################
#Comportamento di default. Fa vedere un reticolo di ordine 3.
if __name__ == '__main__':
  import matplotlib.pyplot as plt
  from matplotlib.patches import RegularPolygon as Poly

  #Cambiarlo a piacere ma sopra 4 diventa enorme.
  #Scala come 7 elevato alla ordine.
  ordine = 3

  #L'angolo di rotazione dell'
  #intero spazio.
  alfa = 27


  #Rotazione per l'angolo fondamentale
  Rmatrix = np.array( [   [np.cos(alfa*np.pi/180),-np.sin(alfa*np.pi/180)]  ,
                          [np.sin(alfa*np.pi/180), np.cos(alfa*np.pi/180)]  ] )  
  
  #Una base per il reticolo.
  my_b1 = np.dot(Rmatrix,__b1__.T).T

  #Creo la figura
  fig,ax = plt.subplots(figsize=(9,9))
  xy = []

  indici = xrange(7**ordine)

  for i in indici:
    v = Dec2Vec(i,my_b1)
    hexi = Poly(v,6,1,facecolor='w',edgecolor='k',orientation=alfa*np.pi/180)
    ax.add_patch(hexi)
    xy.append(v)

  lato = np.sqrt(np.array(xy)[:,0]**2 + np.array(xy)[:,1]**2).max()+1

  ax.set_xlim(-lato,lato)
  ax.set_ylim(-lato,lato)
  ax.set_aspect('equal')

  lbase = ((ax.transData.transform((1,0)))[0] - ax.transData.transform((0,0))[0])
 
  #Aggiustamenti grafici empirici
  emp1 = 20
  emp2 = 0
  if ordine == 4:
    emp1 = 25
  if ordine == 3:
    emp2 = 0.05
  if ordine == 2:
    emp2 = 0.01

  #Mostra i numeri di cella
  scala = lbase/emp1
  if ordine < 5:
    for i in range(7**ordine):
      t = ax.text(xy[i][0]-emp2,xy[i][1]-emp2,Dec2Hep(i),fontsize=10*scala,ha='center',va='center')


  #fig.savefig("esagoni.png",dpi=600)
  plt.show()
