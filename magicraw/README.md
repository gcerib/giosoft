# MagicRaw

Set of tools to visualize various MAGIC events in the camera and process them. This is intended for graphical display and was used to produce several plots in the thesis, but the results of the various procedures (MaTaJu cleaning, Hillas parametrization) were cross-checked with the standard MAGIC analysis software and found to agree. 

The graphical part and the kNN cleanings depend on the Heptindex.py class. This defines an indexing method on hexagonal lattices with a generalized balanced ternary numbering system in base 7. It will be extended in the future to include a fast haxagonal fourier transform algorithm.

A sample event is provided, and can be used to produce the MaTaJu cleaning scheme of the thesis with the MatajuCleaning.py script. The patch subfolder contains patches that can be applied to the "sorcerer" MAGIC software to dump raw waveforms as XML files that are readable with almost everything.
