#!/usr/bin/python2.7
"""
Waveforms

This module  contains routines to  read raw events
from an  xml file created by  a modified sorcerer and
plot them. A simpler sliding window extraction method
has been implemented to be  able to see the images of
the events. However, there is no F-factor calibration
of the PHE because the information about that has not
being yet implemented in the xml file.
"""

import numpy as np

import xml.etree.ElementTree as ET

import matplotlib.pyplot  as plt
from   matplotlib.patches import RegularPolygon as Poly
from   matplotlib.collections import PatchCollection
from   matplotlib.patches import Ellipse,Wedge
from   matplotlib import colors
import matplotlib.patheffects as patheff
from   matplotlib import cm
import matplotlib as mpl

import matplotlib.animation as animation

import warnings
import copy
import time
import sys


#Heptindex contains the machinery to handle hexagonal
#lattices. Only the highest  level function Dec2Vec()
#is  used here, which  gives the  vector of a hexagon
#given its heptal (base-7) index expressed in decimal
#base. It contains also some very useful stuff like a
#table relating the MAGIC software index to the index
#in the recursive heptal space.  
import Heptindex

#This is a fixed value, equal to kTimeCalibrationZero
__TimeCalibrationZero__ = 30.

__invtable__ = {v: k for k, v in Heptindex.__magictable__.items()}

__Magic2NNs__ = [[__invtable__[k] for k in g] for g in Heptindex.Hep2Dec(Heptindex.GetMagicNN(2))]
__Magic3NNs__ = [[__invtable__[k] for k in g] for g in Heptindex.Hep2Dec(Heptindex.GetMagicNN(3))]
__Magic4NNs__ = [[__invtable__[k] for k in g] for g in Heptindex.Hep2Dec(Heptindex.GetMagicNN(4))]

def DrawMagicCamera(ax,indices,values,qcolor,click=False,autoscale=True,fullout=False,**kwargs):
    """A function to draw  an hexagonal lattice image.
       It needs an  axes onto  which to draw, indices
       on the MAGIC Camera (software ones), which are
       automatically  converted  into  heptindices to
       draw the camera, values specifying what should
       be drawn and a  colormap qcolor. Setting click
       to True allows a signal to  be produced by the
       hexagons when they are clicked.
    """
    try:
      _ = float(values)
      values = np.zeros(len(indices))
    except:
      pass

    xy = []
    hexagons = []
    for index,value in zip(indices,values):
       try:
         decidx = Heptindex.__magictable__[index]
       except:
         continue
        
       pixvec = Heptindex.Dec2Vec(decidx,Heptindex.__magicbase__)

       hexagon = Poly(pixvec,6,1,facecolor=qcolor(value),edgecolor='k',orientation=Heptindex.__magicalpha__,picker=click,**kwargs)
       #ax.add_patch(hexagon)
       xy.append(pixvec)
       hexagons.append(hexagon)

    pc = PatchCollection(hexagons, match_original=True)
    ax.add_collection(pc)
    lato = np.sqrt(np.array(xy)[:,0]**2 + np.array(xy)[:,1]**2).max()+1

    if autoscale:
      ax.set_xlim(-lato,lato)
      ax.set_ylim(-lato,lato)
    ax.set_aspect('equal')
    ax.set_axis_off()

    if fullout:
      return ax,hexagons
    else:
      return ax

def DrawMagicIndices(ax,indices,mode='software',scale=1,fullout=False):
    """A function to draw  the magic pixel numbers on
       top of the grid. Modes include 'software' that
       shows the MAGIC indices, 'hardware', which is
       just software+1, 'heptdec' which shows the
       decimal representation of the heptindices and
       'heptal', showing the heptindices themselves.
    """
    texts = []
    for index in indices:
       try:
         decidx = Heptindex.__magictable__[index]
       except:
         continue
        
       pixvec = Heptindex.Dec2Vec(decidx,Heptindex.__magicbase__)
       label = ''
       if mode == 'software':
         label = str(index)
       elif mode == 'hardware':
         label = str(index+1)
       elif mode == 'heptdec':
         label = str(Heptindex.__magictable__[index])
       elif mode == 'heptal':
         label = str(Heptindex.Dec2Hep(Heptindex.__magictable__[index]))
       
       scala = ((ax.transData.transform((1,0)))[0] - ax.transData.transform((0,0))[0])/19.*scale

       if label:
         txt = ax.text(pixvec[0],pixvec[1],label,fontsize=10*scala,ha='center',va='center')
         texts.append(txt)

    if fullout:
      return ax,texts
    else:
      return ax

class Pixel():
  """The Pixel class, containing information for a single pixel.
     This one has an id (magic software index) a valid flag, a
     pedestal value, a first capacitor, a time calibration (if
     it was present in sorcerer, the ones without it either are
     silicon PMs or don't have anything and are discarded from
     most of the functions), the original *CALIBRATED* and
     possibly SPIKE-REMOVED waveform (as opposite to the one in
     MRawEvtData, which is raw), the calibrated slices (just the
     array [0,...,50] moved back by self.timecal. If exctraction
     can be performed, it contains also the extracted charge and
     the calibrated time of arrival.
  """
  
  def __init__(self,pix=None,extract=True):
      """Constructor. If called without anything it just creates a
         void pixel. If called on a "pixel" xml instance, instead,
         it reads, validates and calculates the data.
      """
      self.id       = -1
      self.valid    = False
      self.suitable = False
      self.pedestal = -1
      self.ffcalib  = 0
      self.timecal  = __TimeCalibrationZero__
      self.nsamples = 0

      self.waveform = np.array([])
      self.calslice = np.array([])
      self.charge   = 0
      self.arrival  = -1

      if pix is not None:
        self.id       = int(pix.attrib['id'])
        self.pedestal = float(pix.attrib['pedestal'])

        #The attribute 'valid' in the XML is actually
        #'suitable'. Unsuitable pixels can be drawn,
        #whereas the one missing timecal are the
        #ones for which no calibration was possible,
        #for instance SiPM and empty pixels. These
        #are 'invalid' because they are not in the
        #geometry of the camera and they can not
        #be drawn.
        #CAREFUL! The __getitem__ method of the Event
        #class returns only valid pixels, including
        #the unsuitable ones.
        if(pix.attrib['valid']=='1'):
          self.ffcalib  = float(pix.attrib['ffcalib'])
          self.suitable = True
        else:
          self.suitable = False

        if 'timecal' in pix.attrib:
          self.timecal  = float(pix.attrib['timecal'])
          self.valid = True
        else:
          self.valid = False

        self.waveform = np.array(pix.text.split(','),dtype='float')
        self.nsamples = len(self.waveform)

        self.calslice = np.arange(self.nsamples)-self.timecal
        if extract:
          self.ReExtract()


  def __getitem__(self,i):
      """Behavior of []: gives the corresponding waveform sample.
      """
      return self.waveform[i]

  def ReExtract(self,time=__TimeCalibrationZero__,window_size=5,maxdelta=100,first=None,last=None):
      """The sliding window extraction routine, a copy of the one
         in MSorcererExtractorSW.cc. This one accepts a timezero
         which is by default the calibration zero time (but it may
         be replaced with the trigger time, for instance), a size
         of the window and the maximum time difference, unless
         first and last are specified explicitly.
      """
      if(first is None) and (last is None):
        expcaltime = time - __TimeCalibrationZero__ + self.timecal
        first      = int(expcaltime - window_size*0.5 - maxdelta + 0.5)
        last       = int(expcaltime + window_size*0.5 + maxdelta + 0.5)

        if first<0:
          first=0
        if last>self.nsamples-1:
          lasr =self.nsamples-1

      trange     = slice(first,last)
      swave      = self.waveform[trange]-self.pedestal
      stime      = self.calslice[trange]
      slide      = swave[0:-4]+swave[1:-3]+swave[2:-2]+swave[3:-1]+swave[4:]
      maxwin     = slice(slide.argmax(),slide.argmax()+5)
      self.charge  = slide.max()*self.ffcalib
      try:
        self.arrival = np.average(stime[maxwin],weights=swave[maxwin])     
      except ZeroDivisionError:
        self.valid = False
        self.arrival = -1
      
  def Draw(self,ax=None,auto=True,timeband={'color':'gold','lw':3,'alpha':0.5},**kwargs):
     """This draws the waveform for the pixel. It accepts an axes
        and if none is given it creates a new one. This axis is
        decorated with a few stuff that can be reproduced also if
        the axes is passed externally, by setting auto to True.
        Besides this, any other keyword is passe to the plt.bar 
        function. The axes is returned.
     """
     if (ax is None) or (auto):
       if ax is None:
         fig,ax = plt.subplots()
       ax.set_ylim(self.waveform.min()-50 ,self.waveform.max()+50 )
       ax.set_xlim(self.calslice.min()-0.5,self.calslice.max()-0.5)
       ax.set_title("Pixel "+str(self.id))
       if self.timecal != 0:
         ax.set_xlabel("Calibrated DRS4 Slice")
       else:
         ax.set_xlabel("DRS4 Slice")
     #Drawing also the pedestal and the calibrated arrival time.
     ax.fill_between([self.calslice[0]-0.5,self.calslice[-1]+0.5],self.pedestal,ax.get_ylim()[0],color='lightgray')
     ax.bar(self.calslice,self.waveform,width=1,**kwargs)
     if timeband:
       ax.plot((self.arrival,self.arrival),ax.get_ylim(),'-',**timeband)
     return ax


class Event():
  """The Event class has as core an array of pixels, but with some
     more information. The DAQ number, trigger type (should be 0 =
     shower), stereo number, and the time values mjd, milli, nanosec
     are also stored. A planned class "Run" or "Subrun" could contain
     more Events and also Pedestals and Calibration events, but this
     is not yet implemented.
  """
  def __init__(self,evt=None,extract=True,ismc=False):
      """The Constructor reads an "Event" XML object, similarly to what
         Pixel() does.
      """
      self.daqnum    =-1
      self.trigtype  =-1
      self.stereonum =-1
      self.mjd       =-1
      self.milli     =-1
      self.nano      =-1
      self.pixels    = []

      self.core      = []
      self.rings     = []
      self.clean     = []

      self.energy    = 0

      if evt is not None:
        #Only in data
        try:
          self.daqnum     = int(evt.attrib['daqnum'])
        except KeyError:
          pass
        #Maybe in data
        try:
          self.trigtype   = int(evt.attrib['trigtype'])
        except KeyError:
          pass
        #Always
        self.stereonum    = int(evt.attrib['stereonum'])
        #Only in data
        try:
          self.mjd        = int(evt.attrib['mjd'])
          self.milli      = int(evt.attrib['milli'])
          self.nano       = int(evt.attrib['nano'])
        except KeyError:
          pass
        #Only in MC
        try:
          self.energy     = float(evt.attrib['energy'])
        except:
          pass

        for pix in evt:
           pypix = Pixel(pix,extract=extract)
           self.pixels.append([pypix.id,pypix])
        #Creates the pixels array and stores them in ascending order
        #according to the MAGIC Software Index.
        self.pixels = [x for _, x in sorted(self.pixels, key=lambda pair: pair[0])]

      if ismc:
        for i in xrange(1039):
          if self[i].suitable:
            self[i].valid = True
        self[0].valid = True
        self[0].suitable = True
        self[0].ffactor = 1.0/63.

  def __getitem__(self,i):
      """The behavior of [] on an event is peculiar. If called with an
         integer Event()[N] it gives back the Nth pixel. If called with a
         string it searches for it among the Pixel() attributes. If found
         it produces and returns a numpy array with all the values on that
         attribute for all the pixels. The array has dimension (Npix * k).
         As an example Event()['waveform'] gives back a 2d array with rows
         corresponding to pixels and columns corresponding to samples!
      """                                                        #  Note this!! #
                                                                 #     ||       #
      if type(i) is int or isinstance(i,np.integer):             #   \ || /     #
        return self.pixels[i]                                    #    \||/      #
      if i in Pixel().__dict__.keys():                           #     \/       # 
        return np.array([ pix.__dict__[i] for pix in self.pixels if pix.valid])
      if hasattr(i,'__iter__'):
        return [self[j] for j in i]


  def ReExtract(self,time=__TimeCalibrationZero__,window_size=5,maxdelta=100,first=None,last=None):
      """Simply calls ReExtract on each of the pixels.
      """
      for pix in self.pixels:
        pix.ReExtract(time=time,window_size=window_size,maxdelta=maxdelta,first=first,last=last)


  def SearchCore(self,q2=8.4,q3=4.6,q4=3.6,w2=2.31,w3=2.81,w4=3.14):
      """This function identifies the core of the event using the
         standard procedure. The values of qk and wk defaults to
         the mataju ones for M2.
      """
     
      ids    = self['id']
      #If a pixel is unsuitable we skip all its groups.
      good   = self['suitable']
      photo  = self['charge']
      times  = self['arrival'] 


      #2NN
      for g in __Magic2NNs__:
        if not all(good[g]):
          continue
        if all(photo[g] > q2):
          if all(np.abs(times[g] - np.average(times[g],weights=photo[g])) < w2):
            for pix in g:
              if pix not in self.core:
                self.core.append(pix)

      #3NN
      for g in __Magic3NNs__:
        if not all(good[g]):
          continue
        if all(photo[g] > q3):
          if all(np.abs(times[g] - np.average(times[g],weights=photo[g])) < w3):
            for pix in g:
              if pix not in self.core:
                self.core.append(pix)

      #4NN
      for g in __Magic4NNs__:
        if not all(good[g]):
          continue
        if all(photo[g] > q4):
          if all(np.abs(times[g] - np.average(times[g],weights=photo[g])) < w4):
            for pix in g:
              if pix not in self.core:
                self.core.append(pix)

      self.core   = np.sort(self.core)
      self.clean += self.core.tolist()

  def SearchBoundary(self,qb=2.5,wb=3.5,window_size=5,maxdelta=3.5,rings=3):
      """Search the boundary of the event with the Mataju
         procedure. The values are the default ones for M2.
      """
      if self.core == []:
        errstr = 'Can not search boundaries: there are no core pixels.'
        raise IndexError(errstr)
      self.rings = [self.core]
      for _ in xrange(rings):
        newring = []
        for pixi in self.rings[-1]:
           #print pixi,'->',
           #I broke this on two lines because otherwise it was unreadable
           border = [Heptindex.Hep2Dec(g[1]) for g in Heptindex.GetkNN(Heptindex.__magictable__[pixi],2)]
           border = [__invtable__[g] for g in border if g in __invtable__]
           #border = [self.pixels[b] for b in border if b not in self.clean]
           #print border
           #for bpix in border:
           for bpix in (self.pixels[b] for b in border if b not in self.clean and b in self['id']):
              #print bpix
              #print self[pixi].arrival
              bpix.ReExtract(time=self[pixi].arrival+__TimeCalibrationZero__,window_size=window_size,maxdelta=maxdelta)
              if bpix.charge > qb:
                if np.abs(bpix.arrival-self[pixi].arrival) < wb:
                  newring.append(bpix.id)
                  self.clean.append(bpix.id)
        self.rings.append(copy.copy(newring))

  def SearchCoreStd(self,core_thr=6,**kwargs):
      """Performs the standard core extraction, which is the same as the
         mataju one but for an additional threshold (6 phe) on core
         pixels. Run this before the re-extraction if you want to do
         the standard cleaning. Passes kwargs to SeachCore for the NN
         thresholds and windows.
      """
      self.SearchCore(**kwargs)
      corethr = [pixi for pixi in self.core if self[pixi].charge >= core_thr]
      newcore = []
      for pixi in corethr:
        border = [Heptindex.Hep2Dec(g[1]) for g in Heptindex.GetkNN(Heptindex.__magictable__[pixi],2)]
        border = [__invtable__[g] for g in border if g in __invtable__]
        single = True
        for bpix in border:
          if bpix in self.core:
            single = False
            break
        if not single:
          newcore.append(pixi)
      
      self.core  = np.sort(newcore)
      self.clean = self.core.tolist()

  def SearchBoundaryStd(self,bound_thr=3.5):
      """Performs the standard boundary extraction. Searches NNs of
         the core and adds the ones above threshold.
      """
      self.rings = [self.core.tolist()]
      bound = []
      for pixi in self.core:
        border = [Heptindex.Hep2Dec(g[1]) for g in Heptindex.GetkNN(Heptindex.__magictable__[pixi],2)]
        border = [__invtable__[g] for g in border if g in __invtable__]
        for bpix in border:
          if self[bpix].charge > bound_thr and bpix not in self.core:
            self.clean.append(bpix)
            bound.append(bpix)
      self.rings.append(bound)
         

  def CalcHillas(self):
      """Calculate the simplest Hillas parameters
         for the event: Size, Length, Width, Delta.
      """
      charges = self['charge'][self.clean]
      self.Size = charges.sum()
    
      vectors = Heptindex.Magic2Vec(self.clean)
    
      xs = vectors[:,0]
      ys = vectors[:,1]
    
      self.Xc = np.average(xs,weights=charges)
      self.Yc = np.average(ys,weights=charges)
    
      corrxx = (charges*(xs-self.Xc)**2).sum()
      corryy = (charges*(ys-self.Yc)**2).sum()
      corrxy = (charges*(xs-self.Xc)*(ys+self.Yc)).sum()
    
      d0 = corryy - corrxx
      d1 = corrxy*2
      d2 = d0 + np.sqrt(d0*d0 + d1*d1)
      tand  = d2 / d1
      tand2 = tand*tand
      s2 = tand2+1
      axis1 = (tand2*corryy + d2 + corrxx)/s2/self.Size
      axis2 = (tand2*corrxx - d2 + corryy)/s2/self.Size
    
      self.Length = np.sqrt(axis1)
      self.Width  = np.sqrt(axis2)
    
      self.Delta = np.arctan(tand)

      #Eigenvectors of the charge distribution
      self.v0   = self.Length/2*np.array((np.cos(self.Delta),np.sin(self.Delta)))
      self.v1   = self.Width/2*np.array((-np.sin(self.Delta),np.cos(self.Delta)))

  def DrawCharge(self,ax=None,click=False,fullout=False,**kwargs):
      """Draws the *extracted* charge on the camera. The usual plot that
         one sees also in SuperArehucas. It behaves similarly to
         Pixel().Draw() with respect to the axes object, but it returns
         also a list of patches containing the hexagons (in magic indexing)
      """
       
      if ax is None:
         fig,ax = plt.subplots()

      qmax = self['charge'].max()
      qmin = self['charge'].min()
      
      norm = colors.Normalize(vmin=qmin, vmax=qmax)
      cmap = cm.get_cmap('rainbow')
      #qcolor is a normalized colormap, i.e. it expects to get values in the
      #qmin - qmax range.
      qcolor = lambda x: cmap(norm(x))
      
      #Look at how handy the getitem method is!
      if fullout:
        return DrawMagicCamera(ax,self['id'],self['charge'],qcolor,click=click,fullout=fullout,**kwargs)+(cmap,norm)
      else:
        return DrawMagicCamera(ax,self['id'],self['charge'],qcolor,click=click,**kwargs)

  def DrawArrival(self,ax=None,tmin=None,tmax=None,nanosec=False,click=False,fullout=False,**kwargs):
      """Draws the *calibrated* arrival times, taking into account the DRS4
         corrections. It is very similar to the previous one but allows the
         user to set also a range to the time colorscale.
      """
      if ax is None:
         fig,ax = plt.subplots()
      
      if tmin is None:
        tmin = self['arrival'].min()
      if tmax is None:
        tmax = self['arrival'].max()

      #if nanosec:
      #  tmin /= 1.64
      #  tmax /= 1.64

      norm = colors.Normalize(vmin=tmin, vmax=tmax)
      cmap = cm.get_cmap('Spectral')
      tcolor = lambda x: cmap(norm(x))

      times = self['arrival']
      if nanosec:
        times /= 1.64
      
      if fullout:
        return DrawMagicCamera(ax,self['id'],times,tcolor,click=click,fullout=fullout,**kwargs)+(cmap,norm)
      else:
        return DrawMagicCamera(ax,self['id'],times,tcolor,click=click,**kwargs)


  def DrawAt(self,ax=None,T=None,click=False,fullout=False,**kwargs):
      """Here thigs get deep. This function draws the *ISTANTANEOUS*
         charge present on all pixels at a certain time T. It is the
         complementary dimension to Pixel().Draw(): whereas that one
         picks a pixel and draws all times (the waveform), this one
         picks a time and draws all pixels. The value at a time T is
         chosen to be the one of the closest sample in the pixel's
         waveform to the time T. That is, there is a "nearest"
         approximation level. Calibrating such charges in PHE may be
         very difficult.
      """
      if T  is None:
        warnings.warn('No time [is slices] given.')
        return
      if ax is None:
        fig,ax = plt.subplots()
      
      ids       = self['id']
      
      #The getitem method filters automatically on pixels marked as
      #bad. See the big arrow in the code before.
      allwaveforms = self['waveform']
      allpedestals = self['pedestal']
      allcalslice  = self['calslice']
      
      #We clip parts of the waveforms which are clearly bad (this is
      #not yet pedestal subtracted!)
      allwaveforms = np.clip(allwaveforms,0,None)
      
      #Pedestal subtraction. Each waveform has its own.
      allwaveforms = np.array([ iwaveform-ipedestal for iwaveform,ipedestal in zip(allwaveforms,allpedestals)])
      
      #We clip extremal values before computing the max and min. This
      #is to avoid problems with some pixels.
      absqmax    = allwaveforms[:,4:-4].max()
      absqmin    = allwaveforms[:,4:-4].min()
      
      norm = colors.Normalize(vmin=absqmin, vmax=absqmax)
      cmap = cm.get_cmap('rainbow')
      qcolor = lambda x: cmap(norm(x))
      
      #We compute the instantaneous charge in one go. We calculate the
      #closest sample to the time T with abs().argmin() and use it on
      #that pixel's waveform to get the charge. Very cool!
      instantQ = np.array([iwaveform[np.abs(icalslice-T).argmin()] for icalslice,iwaveform in zip(allcalslice,allwaveforms)])
      
      #And drawing.
      if fullout:
        return DrawMagicCamera(ax,ids,instantQ,qcolor,click=click,fullout=fullout,**kwargs)+(cmap,norm)
      else:
        return DrawMagicCamera(ax,ids,instantQ,qcolor,click=click,**kwargs)

  def DrawChargeAndArrival(self,tmin=None,tmax=None,nanosec=False,scale=1,labsize=15,click=False,fullout=False,**kwargs):
      """Draws a standard canvas with the charge, the arrival times
         and two nice colorbars. scale can be used to scale the
         size of the colorbar ticks, labsize to scale its title.
      """
      fig,axs = plt.subplots(1,2,figsize=(16*scale,8*scale))
      ax1,hex1,qcmap,qnorm = self.DrawCharge(axs[0],click=click,fullout=True,**kwargs)
      ax2,hex2,tcmap,tnorm = self.DrawArrival(axs[1],tmin=tmin,tmax=tmax,nanosec=nanosec,click=click,fullout=True,**kwargs)

      cb1 = fig.add_axes((0.05,0.16,0.025,0.65))
      cb2 = fig.add_axes((0.95,0.16,0.025,0.65))
      b1  = mpl.colorbar.ColorbarBase(cb1,cmap=qcmap,norm=qnorm,orientation='vertical')
      b2  = mpl.colorbar.ColorbarBase(cb2,cmap=tcmap,norm=tnorm,orientation='vertical')

      for tick1 in cb1.yaxis.get_major_ticks():
        tick1.label.set_fontsize(12)
      for tick2 in cb2.yaxis.get_major_ticks():
        tick2.label.set_fontsize(12)

      cb1.text(0.05,1.01,'PhE',transform=cb1.transAxes,clip_on=False,size=labsize)
      if nanosec:
        cb2.text(0.19,1.01,'ns',transform=cb2.transAxes,clip_on=False,size=labsize)
      else:
        cb2.text(0.05,1.01,'sl.',transform=cb2.transAxes,clip_on=False,size=labsize)

      cb2.yaxis.set_ticks_position('left')
      cb1.yaxis.set_ticks_position('left')

      if fullout:
        return (((ax1,hex1)+(qcmap,qnorm)),((ax2,hex2)+(tcmap,tnorm)))
      else:
        return (ax1,ax2)

  def DrawHillas(self,ax=None,drawcam=True,maskout=True,maskalpha=0.42,labels=True,elonly=False,elcolor='w',dircolor='w',click=False,fullout=False,**kwargs):
      """Draw the Hillas parameters on top of a camera image, or
         on top of some axis specified by the user. If ax is None
         it creates it. If drawcam is false a new camera is not
         drawn (maybe you want to plot it on top of the times?).
         The argument labels activates labels for Width, Length
         and Delta. Other arguments are passed to the DrawCamera
         function.
      """
      if ax is None:
         fig,ax = plt.subplots()
      if drawcam:
         out = self.DrawCharge(ax,click=click,fullout=fullout,**kwargs)


      #Border effects to make the objects pop on the camera.
      effK = [patheff.Stroke(linewidth=3, foreground='k'), patheff.Normal()]
      effW = [patheff.Stroke(linewidth=4.142, foreground='w'), patheff.Normal()]

      #3.41 sigma of the 2d gaussian is like 3 sigma
      #for a 1d gaussian (=99.7% contaiment radius).
      #End point for the length and width lines.
      EndL = np.array((self.Xc,self.Yc))+(3.41)*self.v0
      EndW = np.array((self.Xc,self.Yc))+(3.41)*self.v1
   
      #Intercept with the x axis 
      lambd = self.Yc/self.v0[1]
      Xinter = self.Xc-lambd*self.v0[0]
   
      #Masking the outer pixels.
      outer_mask = []
      for pixid in self['id']:
        vec = Heptindex.Magic2Vec(pixid)
        Rpix = vec - (self.Xc,self.Yc)
        Lpix = (Rpix[0]*self.v0[0]+Rpix[1]*self.v0[1])/np.sqrt(self.v0[0]*self.v0[0]+self.v0[1]*self.v0[1])
        Wpix = (Rpix[0]*self.v1[0]+Rpix[1]*self.v1[1])/np.sqrt(self.v1[0]*self.v1[0]+self.v1[1]*self.v1[1])
        R    = np.sqrt((Lpix/self.Length*2)**2 + (Wpix/self.Width*2)**2)
        if R >= 3.41:
          outer_mask.append(pixid)
      if maskout:
        black = lambda x: '#000000' 
        DrawMagicCamera(ax,outer_mask,0,black,click=click,autoscale=False,lw=0.5,alpha=maskalpha)
     
      
      #Two ellipses: semi-trasparent white background
      #and bold white line with a black border. 
      #el1 = Ellipse((self.Xc,self.Yc),3.41*self.Length,3.41*self.Width,self.Delta/np.pi*180,alpha=0.1,facecolor='w')
      el2 = Ellipse((self.Xc,self.Yc),3.41*self.Length,3.41*self.Width,self.Delta/np.pi*180,facecolor="None",edgecolor=elcolor,lw=1.4142,path_effects=effK)
      #ax.add_patch(el1)
      ax.add_patch(el2)
   
      if not elonly: 
        #Draw the delta angle.
        if self.Delta>=0:
          ang = Wedge((Xinter,0),2.5,0,self.Delta/np.pi*180,facecolor='plum',edgecolor='k',lw=1.4142,clip_on=False)
        else:
          ang = Wedge((Xinter,0),2.5,self.Delta/np.pi*180,0,facecolor='plum',edgecolor='k',lw=1.4142,clip_on=False)
        ax.add_patch(ang)
    
        #Draw the line to the x-axis.
        ax.plot([self.Xc,Xinter],[self.Yc,0],'--',dashes=(1, 1),color='k',lw=2.1415)#,alpha=0.7071)
        #The x-axis
        ax.plot([Xinter,Xinter+4.3],[0,0],'-',color='k',lw=2.4142,clip_on=False)

    
        #Length and Width, and the center.
        ax.plot([self.Xc,EndL[0]],[self.Yc,EndL[1]],'r-',lw=1.4142,solid_capstyle='butt',path_effects=effW)
        ax.plot([self.Xc,EndW[0]],[self.Yc,EndW[1]],'b-',lw=1.4142,solid_capstyle='butt',path_effects=effW)
        ax.plot(self.Xc,self.Yc,'ko',ms='8',alpha=0.7)
    
        if labels :
          ax.text(EndL[0]+self.v0[0]/self.Length*2*2  ,EndL[1]+self.v0[1]/self.Length*2*2,'Length',color='r',path_effects=effW,weight='bold',size='large')
          ax.text(EndW[0]+self.v1[0]/self.Width*2*1.41,EndW[1]+self.v1[1]/self.Width*2*1.41,'Width',color='b',path_effects=effW,weight='bold',size='large')
          ax.text(Xinter+1.4142,1.4142,'Delta',color='plum',path_effects=effK,weight='bold',size='large')

      if elonly:
        EndL = np.array((self.Xc,self.Yc))+(30)*self.v0
        BegL = np.array((self.Xc,self.Yc))-(30)*self.v0
        ax.plot([BegL[0],EndL[0]],[BegL[1],EndL[1]],'--',color=dircolor,lw=2.,solid_capstyle='butt')

      if drawcam:
        return out,outer_mask
      else:
        return outer_mask

  def save_animation(self,filename='movie.mp4',func=None,mintime=None,maxtime=None,nint=625,framerate=25):
      """Let's go cinema! This one animates the event using DrawAt() and
         saves an mp4 video to send to your friends on Instagram! (or
         whatever people use). It takes the output filename, a function
         to "evolve" the plot (it can be redefined by the user), the
         minimum and maximum time to play, the total number of frames and
         the framerate. As drawing the 1039 hexagons is quite tought,
         matplotlib doesn't actually handle this as a true animation.
      """
  
      fig,ax = plt.subplots(figsize=(7.2,7.2),dpi=150)
      
      if func    is None:
        func = self.evolve

      if mintime is None:
        mintime = self['calslice'].min()
      
      if maxtime is None:
        maxtime = self['calslice'].max()
    
      mintime = float(mintime)
      maxtime = float(maxtime) 
      delta = maxtime - mintime
 
      _, hexagons,_,_ = self.DrawAt(ax,mintime,fullout=True)
      plt.rcParams['animation.frame_format'] = 'png'
      
      mywriter = animation.FFMpegWriter(fps=25,metadata={'title':str(self.stereonum), 'artist': 'MAGIC Telescopes, G. Ceribella'})
      anim = animation.FuncAnimation(fig,func,frames=nint,interval=1,blit=False,fargs=(fig,ax,nint,mintime,delta))
      
      anim.save(filename,writer=mywriter)
      
      return
   
  def evolve(self,i,fig,ax,nint,mintime,delta):
      """Function to evolve the plot by one step. It is called
         automatically by save_animation(). And accepts similar
         parameters.
      """
      timenow = mintime + delta/(nint-1)*i
      ax.clear()
      
      print "\rAnim: i=%3d nint=%d (%2d%%), time=%+0.2f, final=%0.2f, step=%0.3f=%0.3f ns" % \
                    (i,    nint, int(100*float(i)/nint),  timenow,  mintime+delta,  delta/(nint-1),  delta/(nint-1)/1.64),
      sys.stdout.flush()
      
      _,hexagons,_,_ = self.DrawAt(ax,timenow,fullout=True)
      
      ax.text(0.02, 0.98, '%+0.2f ns' % (timenow/1.64), transform=ax.transAxes, fontsize=16, fontweight='bold', va='top')
      
      fig.canvas.draw()
      
      return hexagons

class Rawfile():
  """A class to store multiple events. This is the equivalent
     of the <file> tag in the XML.
  """
  def __init__(self,raw=None):
      self.run    = 0
      self.subrun = 0
      self.events = []
      if raw is not None:
        if 'run' in raw.attrib:
          self.events = list(raw.iter('event'))
          self.run    = raw.run
          self.subrun = raw.subrun
        if 'run' in raw[0].attrib:
          filelist = raw.iter('file')
          self.run = []
          self.subrun = []
          for fi in filelist:
            if 'run' in fi.attrib:
              self.events.append(list(fi.iter('event')))
              self.run.append(fi.run)
              self.subrun.append(fi.subrun)
            else:
              raise ValueError('Can\'t walk this XML. No \'<run>\' found.')
            

if __name__ == '__main__':
  #Read the XML file and generates the
  #event object from the first event
  #that is found.
  tree  = ET.parse('SMILE_52434.xml')
  #tree  = ET.parse('Crab252535.xml')
  xml   = tree.getroot()
  try:
    event = list(xml.iter('event'))[0]
  except IndexError:
    raise IOError("There are no events!")
  
  #If you are reading MCs, specify
  #also ismc=True
  event = Event(event)
  
  ################################################################################
  # Examples:
  
  
  ## (1) Save an animation of the event
  #event.save_animation('movie.mp4',mintime=-10.25,maxtime=10.25)
  
  ## (2) Show the integrated charge and arrival time
  ##fig,axs = plt.subplots(1,2,figsize=(16,8))
  ##This can be used to disable a pixel
  ##event[519].valid = False
  ##Draw the charge
  ##event.DrawCharge(axs[0])
  ##To set a time range:
  ##event.DrawArrival(axs[1],tmin=-12,tmax=12)
  ##or simply use:
  #event.DrawChargeAndArrival(tmin=-12,tmax=12,nanosec=True)
  ##Drawing indices
  ##DrawMagicIndices(axs[1],event['id'])
  #plt.show()
  
  ## (3) Draw just the charge
  fig,ax = plt.subplots(figsize=(8,8))
  event.DrawCharge(ax)
  plt.show()
 
  ## (4) Re-extract the event, apply the MaTaJu cleaning,
  ##     calculate the Hillas parameters  and draw them
  ##     on top of the charge distribution.
  #fig,ax = plt.subplots(figsize=(7.2,7.2))
  #event.ReExtract(24.644,window_size=5,maxdelta=12)
  #event.SearchCore()
  #event.SearchBoundary()
  #event.CalcHillas()
  #event.DrawHillas(ax)
  #plt.show()
 
  ## (5) Show one waveform 
  #fig,ax = plt.subplots()
  #opt = { 'color':'royalblue', 'edgecolor':'k', 'alpha':1}
  #event[137].Draw(ax,auto=True,**opt)
  #plt.show()
  
  ## (6) Getting information
  #print event[137].charge
  #print event[137].arrival
  
  ## (7) Overlay some calibrated waveforms on top of each other
  #opt = {'color':'royalblue', 'edgecolor':None, 'alpha':0.1}
  #fig,ax = plt.subplots()
  #for pixel in event[:10]:
  #  ax = pixel.Draw(ax, **opt) 
  #ax.set_ylim(auto=True)
  #plt.show()
