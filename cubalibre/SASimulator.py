#!/usr/bin/python3

"""

                           `.:/+oshdddhyso++/:.
                       -+sdmmNNNNNNNNNNNNNNNNNmho/-`
                    -odmNNNNNNNNNmdmNNNNNNNNNNNNNNmmdhso/-`
                  -ymNNNNNNNNNNNds.+sdNNNNNNNNNNNNNNNNNNNmmy+.
                /hmNNNNNNNNNNNNNdo--hNNNNNNNNNNNNNNNNNNNNNNNNmh/
               +mNNNNNNNNNNNNNNNNmmmmNNNNNNNNNNNNNNNNNNNNNNNNNNmy`
             `smNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNmy`
             :mNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNmd/`
             :mNNNNNNNNNNmdyo+/:-......---://+syhmmNNNNNNNNNNNNNNNNmd:
            `sNNNNNNNmds:`                       `.:+ydmNNNNNNNNNNNNNm-
            /NNNNNNNm/`                                -hNNNNNNNNNNNNNy`
            .dNNNNNh.                              `    .dNNNNNNNNNNNNm+
             :mNNNmh+                    `.-//++sshddo:` -mNNNNNNNNNNNNs
              +NNNNNs .sddyyyyyyyy+`    .dNNNNNNNNNNNNNd+-hNNNNNNNNNNNNs
             -dNNNNm+.dmsohNNNNNNNm/    oNNNmdmNNmmNNNNNNNNNNNNNNNNNNNms/-.`
            -dNNNNNm. `:sdmmNm+:+s:`    oNy--`/so:hmNNNNNNNNNNNNNNNNNdo/
         `:ymNNNNNNh` omdh/.:.          +d+    ``` `:/+syymNNNNNNNNNNm::-
     `-ohmNNNNNNNNNo   `                -.`              :mNNNNNNNNNNNmy+/
`.://+sdNNNNNNNNNNN+                     `o/            .dNNNNNNNNNNNNNNNmdy/.
 `    `-hNNNNNNNNNNs                    `..-`         `/dNNNNNNNNNNNNNNNNNmo+/`
    `-+ydNNNNNNNNNNm.            /yhyshysyhmm+       :mNNNNNNNNNNNNNNNNNNNs  `-`
      `/mNNNNNNNNNNNy`           -:   ``./.::`    :mh.mNNNNNNNNNNNNNNNNNNNh    `
       .mNNNNNNNNNNNm+             /o   :mdo-`    /d/ :mNNNNNNNNNNNNNNNNNNm.
       .dNNNNNNNNNNNNd`         .:-ho   .oydmmdyo:`-+::dNNNNNNNNNNNNNNNNNNm-
   :.  `dNNNNNNNNNNNNmo      :ymm+`:sdmdmmmdmNNNNmdmNNNNNNNNNNNNNNNNNNNNNNm-
   yo-:ymmmNNNNNNNNNNNmy-   omm/:+o//:::-..:///:hNNNNNNNNNNNNNNNNNNNNNNNNNh`
   `:oss+.`+dNNNNNNNNNNNmy: sNs   -+//++oshhmdsohNNNNNNNNNNNNNNNNNNNNNNNNd.
      `h-   +mNNNNNNNNNNNNs .s-    -osssssshddhydNNNNNNNNNNNNNNNNNNNNNmmd-
      `dh+:omNNNNNNNNNNNNNd` `                 `hNNNNNNNNNNNNNNNNNNNNmy-y`
      .-sdmNNNNNNNNNNNNNNNd./m:              .+hNNNNNNNNNNNNNNNNNNNNNNNhh.
      .yyhdNNNNNNNNNNNNNNNNmmNms-`       .:+smNNNNNNNNNNNNNNNNNNNNNNNNNms`
       .ydddmmNNNNNNNNNNNNNNNNNNmmddhhhhdmNNNNNNNNNNNNNNNNNNNNNmyosdmmNs
           `smNddNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNmh``.hh.
           :dy+.:mNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNmh/ `:-
           `    `hNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNo
                 +mNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNh`
                  -smNNNNNNNNmmmmmmmmNNNmmmNNNNNNNNNNNNNNNNNNNNm:
                    `:ohmmNNNy-..```-+hmd.omNNNNNNNNNNNNNNNNNNNNm+.
                        `.:+sys.       -:` omNNNNNNNNNNNNNNNNNm/:::.`
                                            :hmNNNNNNNNNNNNNNNd`
                                             `:sdmmNNNNNNNNNmy-
                                                 `-/oshhhyo:`
       ________  ______  ___    ___ __            
      / ____/ / / / __ )/   |  / (_) /_  ________ 
     / /   / / / / __  / /| | / / / __ \/ ___/ _ \
    / /___/ /_/ / /_/ / ___ |/ / / /_/ / /  /  __/
    \____/\____/_____/_/  |_/_/_/_.___/_/   \___/ 
                                              
                         _______     _____            __     __          
                        / __/ _ |   / __(_)_ _  __ __/ /__ _/ /____  ____
                       _\ \/ __ |  _\ \/ /  ' \/ // / / _ `/ __/ _ \/ __/
                      /___/_/ |_| /___/_/_/_/_/\_,_/_/\_,_/\__/\___/_/   
                         ___           ________  _________________   __  
                        / _/__  ____  / ___/ _ \/  _/ __/_  __/ _ | / /  
                       / _/ _ \/ __/ / /__/ , _// /_\ \  / / / __ |/ /__ 
                      /_/ \___/_/    \___/_/|_/___/___/ /_/ /_/ |_/____/ 

--------------------------------------------------------------------------------

This program uses the Qt5 toolkit to give a graphical interface to let CRISTAL
believe that it is speaking with SuperArehucas. Periodical read/write operations
are managed by PyQt5 timers, so if you want to port the program to a different
toolkit or if you want to drop the GUI you will have to reimplement them.

--------------------------------------------------------------------------------

"""


import sys
import socket
import time

#import fcntl, os
#import select



#CRISTAL WRITES TO THIS PORT
#SA LISTENS THIS PORT
#(SERVER)
PORT_LISTEN = 7313


#CRISTAL READS THIS PORT
#SA WRITES TO THIS PORT
#(CLIENT)
PORT_WRITES = 7413


#COMMON BUFFER SIZE FOR
#BOTH STREAMS
BUFFER_SIZE = 8192


################################################################################
################################################################################
################################################################################
################################################################################
#########         __    _      __                      
#########        / /   (_)____/ /____  ____  ___  _____
#########       / /   / / ___/ __/ _ \/ __ \/ _ \/ ___/
#########      / /___/ (__  ) /_/  __/ / / /  __/ /    
#########     /_____/_/____/\__/\___/_/ /_/\___/_/     
                                         


socket_listen = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
socket_listen.bind(('',PORT_LISTEN))
socket_listen.listen(2)

socket_listen.setblocking(0)


##### Function reading the socket. Don't touch it, it's the
##### result of hours of trial and error.
__DEBUG__ = 1
connected = False
conntimer = 0
def ReadReport():
    """
    We start with some global variables.
    The flag connected is initially false
    and is switched once a connection
    trial has success. In such a case, the
    connection and address object are also
    kept global.
    """
    global connected
    global conntimer
    global conn_listen
    global addr_listen

    if __DEBUG__:
        print('(LISTEN) connected = ',connected)

    """
    If we are not yet connected, let's
    try and connect now.
    """
    if( not connected ):

        try:
            if __DEBUG__:
                print('(LISTEN) Trying to connect...')

            """
            While the client doesn't ask the server
            (meaning this program) for a connection
            this will fail and the exeption code below
            will be executed instead.
            """
            conn_listen, addr_listen = socket_listen.accept()
            """
            If instead it succeed, then let's immediately
            set the stream to be unblocking, otherwise
            the recv() calls will wait for input blocking
            the rest of the program. Since the connection
            is established, from next time this whole block
            if( not connected ) will be skipped.
            """
            conn_listen.setblocking(0)
            connected = True
            conntimer = time.perf_counter()

            if __DEBUG__:
              print('(LISTEN) ...got connected')
        except:
            """
            The client didn't show up, we'll try again
            at the next cycle. In the meanwhile, we return
            the empty string, so that the buffer doesn't
            get filled up with junk.
            """
            if __DEBUG__:
                print('(LISTEN) ...did not get connected')
            return ''

    """
    This gets executed only once we are connected.
    """
    string = ''
    try:
        if __DEBUG__:
            print('(LISTEN) conn_listen = ',repr(conn_listen))
        """
        We try and get something from the socket. This
        will very likely fail because nothing is being
        written at the moment (errno 11, resource
        temporary unavailable). In such a case, the
        exception will be cought below. If the client
        disconnects no error is thrown, so that we
        can't know if we should try and reconnect.
        """
        string = conn_listen.recv(BUFFER_SIZE).decode("ascii")
    except socket.error as error:
        if __DEBUG__:
            print('(LISTEN) Error:', error.errno, end='')
            if error.errno == 11:
                print(' (there is nothing to read right now)')
            else:
                print('')
        pass

    if __DEBUG__:
        print('(LISTEN) Received: {{{%s}}}' %string)

    """
    Here we check when we received the last communication
    from the client. If nothing has been received in the
    last 20 seconds we consider the connection dead and
    close it.
    """
    if not string:
        now = time.perf_counter()
        if __DEBUG__:
            print('(LISTEN) Last receive %0.3f seconds ago.' %(now-conntimer))
        if now-conntimer > 20:
            print ('(LISTEN) WARNING: Nothing received in the last 20 seconds. Disconnect')
            connected = False
            conntimer = 0
    else:
        conntimer = time.perf_counter()

    """
    This will contain most certainly just nothing,
    but if the recv call was successful it will contain
    the readen stuff instead.
    """
    return string


#### Timers are set up at the end of the program
################################################################################









################################################################################
################################################################################
################################################################################
################################################################################
#########             _       __     _ __           
#########            | |     / /____(_) /____  _____
#########            | | /| / / ___/ / __/ _ \/ ___/
#########            | |/ |/ / /  / / /_/  __/ /    
#########            |__/|__/_/  /_/\__/\___/_/      
     
     

#The writing socket
socket_writes = socket.socket(socket.AF_INET, socket.SOCK_STREAM)



#This function keeps trying to connect to a server, to let the 
#following function be able to send commands. Is is included in
#a loop later in the program.

writeconnected = False
def TryAndConnect():
    """
    Again we start with a global variable
    that keeps track of the connection state.
    """
    global writeconnected
    global socket_writes

    if __DEBUG__:
        print('(WRITE) writeconnected = ',writeconnected)
        print('(WRITE) socket_writes  = ',repr(socket_writes))

    if(not writeconnected):
        """
        If we are not connected, let's try to connect
        """
        try:
            if __DEBUG__:
                print('(WRITE) Trying to connect...')
            socket_writes.connect(('',PORT_WRITES))
            writeconnected = True
            """
            We got connected, let's change the status
            and return a successful code. This code
            is used to enable the send buttons.
            """
            if __DEBIG__:
                print('(WRITE) ...got connected.')
            return 0
        except socket.error as error:
            """
            If we failed, let's report it.
            """
            if __DEBUG__:
                print('(WRITE) ...did not get connected.')
                print('(WRITE) Error:', error)
            writeconnected = False
            """
            If the connection is prevented because a
            previous socket is still open We close it
            and open a new one.
            """
            if error.errno == 106:
                print('(WRITE) Hanging connection. Starting a new socket.')
                socket_writes.close()
                socket_writes = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            
            return error.errno
    else:
        """
        If we are already connected check that it's still alive
        and give a positive outcome.
        """
        try:
          remotetuple = socket_writes.getpeername()
          if __DEBUG__:
              print('(WRITE) Connected to %s on port %d' %remotetuple)
        except OSError:
          if __DEBUG__:
              print('(WRITE) The socket disconnected.')
              writeconnected = False
          return -1
        return 0

#This function sends data on the socket, provided that
#the connection is established.
def WriteCommand(string):
    global writeconnected
    if(writeconnected):
        if __DEBUG__:
            print('(WRITE) Send: {{{%s}}}' %string)
        """
        We try and send. If for some reason we fail,
        we will reset the connection status variable,
        so that if it is possible we will reconnect.
        """
        try:
            socket_writes.sendall(string.encode('ascii'))
        except socket.error as err:
            if __DEBUG__:
                print('(WRITE)',err)
            writeconnected = False


#This alternative version open and closes the socket every time.
#I don't think that CRISTAL likes this. But it may be useful for
#debugging. If you want to use it disable the timer that uses
#TryAndConnect(), at the end.
#def WriteCommand(string):
#    socket_writes = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
#    try:
#        socket_writes.connect(('127.0.0.1',PORT_WRITES))
#    except socket.error as err:
#        print err
#    try:
#        socket_writes.sendall(string)
#    except socket.error as err:
#        print err
#    socket_writes.close()

#### Timers are set up at the end of the program
################################################################################










####################################################
########### GRAPHICAL USER INTERFACE ###############
####################################################

from PyQt5.QtWidgets import *
from PyQt5.QtCore import QTimer,Qt


app = QApplication([])
window = QWidget()
window.setGeometry(0,0,800,600)
window.setWindowTitle('CubaLibre')

command_label = QLabel('Send command to CRISTAL:')
command_field = QPlainTextEdit()
send_command  = QPushButton('Send')
empty         = QLabel('')
repeat_label  = QLabel('Keep sending command:')
repeat_field  = QPlainTextEdit()
label_milli   = QLabel('Interval [ms]:')
repeat_milli  = QLineEdit()
start_repeat  = QPushButton('Start')
stop_repeat   = QPushButton('Stop')

command_field.setPlainText('CC-REPORT M0 00 2020 02 02 15 37 22 180 4 9 9 9 9 9 9 9 9 9 9 3 3 3 3 1 1 9 4 00.00 00.00 00.00 00.00 11.95 787.70 25.99 26.31 00.00 00.00 SCHEDULE Park 99 9 9 9 9 9 9 9 9 9 9 3 3 3 3 1 3 93.63 00.00 Day GRB 0 GPSERROR 0 CT_ACTIVE 1 1 OVER\n')

repeat_field.setPlaceholderText('Remember to add a newline at the end.')

display_label = QLabel('Reports from CRISTAL:')
display_box   = QPlainTextEdit()
display_box.setDisabled(True)
display_box.setPlainText('waiting');

clisten_label = QLabel('Listen port: %d' %PORT_LISTEN)
clisten_label.setAlignment(Qt.AlignRight)
cwrites_label = QLabel('Write port: %d' %PORT_WRITES)
#cwrites_label.setAlignment(Qt.AlignRight)

layout = QGridLayout()

layout.addWidget(command_label,0,0,1,1)
layout.addWidget(command_field,1,0,1,1)
layout.addWidget(send_command ,2,0,1,1)
layout.addWidget(empty        ,3,0,1,1)
layout.addWidget(repeat_label ,4,0,1,1)
layout.addWidget(repeat_field ,5,0,1,1)
layout.addWidget(label_milli  ,6,0,1,1)
layout.addWidget(repeat_milli ,7,0,1,1)
layout.addWidget(start_repeat ,8,0,1,1)
layout.addWidget(stop_repeat  ,9,0,1,1)

layout.addWidget(display_label,0,1,1,1)
layout.addWidget(display_box  ,1,1,9,1)

layout.addWidget(cwrites_label,10,0,1,1)
layout.addWidget(clisten_label,10,1,1,1)

window.setLayout(layout)

####################################
# Defining signals and connections #
####################################

########################
# For the SINGLE COMMAND
def CLICKsend_command():
    WriteCommand(command_field.toPlainText())

send_command.clicked.connect(CLICKsend_command)




#######################
# For the REPEAT COMMAD

#Repeater function called by the timer
def RepeatCommand():
    repeat_string = repeat_field.toPlainText()
    WriteCommand(repeat_string)


#The timer
repeat_timer = QTimer()
repeat_timer.timeout.connect(RepeatCommand)


#Function called when the button is pushed,
#starts the timer and disables the editing
def CLICKstart_repeat():
    repeat_field.setDisabled(True)
    repeat_milli.setDisabled(True)
    milli  = repeat_milli.text()
    try:
        milli = int(milli)
    except:
        print('(GUI) Error: ',repr(milli),' is not a number.')
        repeat_field.setEnabled(True)
        repeat_milli.setEnabled(True)
        return
    repeat_timer.start(milli)
    return

start_repeat.clicked.connect(CLICKstart_repeat)

#Function called when the stop button is
#pushed: stops the timer and re-enables
#editing.
def CLICKstop_repeat():
    repeat_timer.stop()
    repeat_field.setEnabled(True)
    repeat_milli.setEnabled(True)

stop_repeat.clicked.connect(CLICKstop_repeat)





################################################################################
################################################################################
################################################################################
################################################################################
##########      ________      __          __   __  _                         
##########     / ____/ /___  / /_  ____ _/ /  / /_(_)___ ___  ___  __________
##########    / / __/ / __ \/ __ \/ __ `/ /  / __/ / __ `__ \/ _ \/ ___/ ___/
##########   / /_/ / / /_/ / /_/ / /_/ / /  / /_/ / / / / / /  __/ /  (__  ) 
##########   \____/_/\____/_.___/\__,_/_/   \__/_/_/ /_/ /_/\___/_/  /____/  
                                                                



################################
## Listening/Connecting       ##
################################

#This timer opens to incoming
#connections and if so fluxes
#the text in the GUI box.
def FetchReport():
    global connected
    if __DEBUG__:
        print('(LISTEN) ReadReport()')
    sys.stdout.flush()
    string = ReadReport()
    string = (string + display_box.toPlainText())
    string = '\n'.join(string.split('\n',1)[:1])
    display_box.setPlainText(string)
    if connected:
        clisten_label.setStyleSheet('color: green')
    else:
        clisten_label.setStyleSheet('color: red')

display_timer = QTimer()
display_timer.timeout.connect(FetchReport)
display_timer.start(100)





###################################
## Connecting client-side (write) #
###################################

#This timer tries to open a client
#connection. If it is OK, it enables
#the send buttons, otherwise it
#disables them.
def RepeatingConnectTrials():
    errcode = TryAndConnect()
    if(errcode):
        send_command.setDisabled(True)
        start_repeat.setDisabled(True)
        stop_repeat.setDisabled(True)
        cwrites_label.setStyleSheet('color: red')
        return
    else:
        send_command.setEnabled(True)
        start_repeat.setEnabled(True)
        stop_repeat.setEnabled(True)
        cwrites_label.setStyleSheet('color: green')

writeconnecting_timer = QTimer()
writeconnecting_timer.timeout.connect(RepeatingConnectTrials)
writeconnecting_timer.start(1000)




### This starts the application
window.show()
app.exec_()

### When it finished, let's clean up.
socket_writes.close()
socket_listen.close()
